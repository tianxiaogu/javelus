

public class MethodSignatureChange {
	
	public static void main(String[] a){
		if(a.length!=1){
			  System.out.println("the first argument should be the location of dsu specification file");
			  System.exit(0);
		  }
		  String dsuPath=a[0];
		  MethodSignatureChange s = new MethodSignatureChange();
		  Changed c = createChanged();
		  s.loop(c, dsuPath);
	}
	
	private static Changed createChanged() {
	    return new Changed();
	  }

	private void loop(Changed c, String dsuPath) {
		int j=10;
		for(int i=0; i<10; i++){
			wrapper(c, i, j);
			if(i==5)
				org.javelus.invoker.DSUInvoker.invokeDSU(dsuPath);
		  }
	  }

	  /*
	   * Wrapper is a separate function, so that loop() does not have to be
	   * recompiled.
	   */
	  private void wrapper(Changed c, int i, int j) {
	    c.add(i, j, j);
	  }
}
