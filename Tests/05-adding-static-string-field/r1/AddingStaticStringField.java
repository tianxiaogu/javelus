

public class AddingStaticStringField {
	public static void main(String a[]) {
		  if(a.length!=1){
			  System.out.println("the first argument should be the location of dsu specification file");
			  System.exit(0);
		  }
		  String dsuPath=a[0];
		  for (int i = 0; i < 10; i++) {
			  wrapper();
			  if (i == 5) {
				  org.javelus.invoker.DSUInvoker.invokeDSU(dsuPath);
	      }
	    }
	  }

	  /*
	   * Wrapper is a separate function, so that main() does not have to be
	   * recompiled.
	   */
	  private static void wrapper() {
	    Changed.increment();
	    System.out.format("version 2: s: %s\n ", Changed.s);
	    System.out.format("version 2: t: %s\n ", Changed.t);
	  }
}
