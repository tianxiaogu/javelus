import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


public class Manager {
	public static int max=100;
	public int count;
	public List<Employee> employees;
	public Manager(){
		count=0;
		employees=new LinkedList<Employee>();
	}
	
	public boolean addEmployee(Employee em){
		if(count<max &&employees.add(em)){
			count++;
			return true;
		}
		return false;
	}

	public boolean delEmployee(String ID){
		if(count>0){
			Iterator<Employee> it=employees.iterator();
			while(it.hasNext()){
				Employee em=it.next();
				if(em.ID.equals(ID) && employees.remove(em)){
					count--;
					return true;
				}
					
			}
		}
		return false;
	}
	
	public void display(String dsuPath){
		System.out.println("version2: display:");
		Iterator<Employee> it=employees.iterator();
		while(it.hasNext()){
			Employee em=it.next();
			System.out.println(em.toString(dsuPath));
		}
		System.out.println("version2: there are "+count+" employees");
	}
	
}
