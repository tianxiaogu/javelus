
public class Employee {
	public String ID;
	public String name;
	public int age;
	public float salary;
	public String depart;

	public Employee(String ID){
		this.ID=ID;
		name="";
		age=25;
		salary=3000;
		depart="Computer";
	}
	
	public Employee(String ID,String name, int age, float s, String d){
		this.ID=ID;
		this.name=name;
		this.age=age;
		this.salary=s;
		this.depart=d;
	}

	
	public String toString(String dsuPath){
		return "version2:"+ID+" "+name+" "+age+" "+salary+" "+depart;
	}
}
