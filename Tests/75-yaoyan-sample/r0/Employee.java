
public class Employee {
	public String ID;
	public String name;
	public int age;
	public float salary;
	
	public Employee(String ID){
		this.ID=ID;
		name="name"+ID;
		age=25;
		salary=3000;
	}
	
	public Employee(String ID,String name, int age, float s){
		this.ID=ID;
		this.name=name;
		this.age=age;
		this.salary=s;
	}
	
	public String toString(String dsuPath){
		if(ID.endsWith("2")){
			org.javelus.invoker.DSUInvoker.invokeDSU(dsuPath);
			System.out.println("Replace Employee.class and Manager.class");
		}
		return "version1:"+ID+" "+name+" "+age+" "+salary;
	}
}
 