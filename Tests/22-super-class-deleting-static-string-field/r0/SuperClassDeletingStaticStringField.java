

public class SuperClassDeletingStaticStringField {
  public static void main(String a[]) {
	  if(a.length!=1){
		  System.out.println("the first argument should be the location of dsu specification file");
		  System.exit(0);
	  }
	  String dsuPath=a[0];
	  SuperClassDeletingStaticStringField s = new SuperClassDeletingStaticStringField();
	 
	  s.loop( dsuPath);
  }

  private void loop( String dsuPath) {
    for (int local_i = 0; local_i < 10; local_i++) {
      wrapper();
      if (local_i == 5) {
        org.javelus.invoker.DSUInvoker.invokeDSU(dsuPath);
      }
    }
  }

  /*
   * Wrapper is a separate function, so that loop() does not have to be
   * recompiled.
   */
  private void wrapper() {
    ChangedSub.increment();
  }
}
