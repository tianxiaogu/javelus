
public class ChangedOuter {

	public int i=1;
	public int j;
	public int  test(){
		
		class ChangedInner {
			public int k = 10;
			
			public void increment(){
				i++;
				j++;
				k+=10;
				System.out.println("version 2: i = "+ i);
				System.out.println("version 2: j = "+ j);
				System.out.println("version 2: k = "+ k );
			}
		}
		
		ChangedInner inner=new ChangedInner();
		inner.increment();
		return i+j+inner.k;
	}
	
	
}
