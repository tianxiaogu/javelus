
public class ChangedOuter {

	public int i;
	
	public class ChangedInner{
		public int k;
		public int m;
		
		public void increment(){
			i++;
			k++;
			System.out.println("version 2: i = "+ i);
			System.out.println("version 2: k = "+ k );
			System.out.println("version 2: m = "+ m);
		}
	}
}
