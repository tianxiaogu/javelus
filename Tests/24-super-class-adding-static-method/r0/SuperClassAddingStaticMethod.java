

public class SuperClassAddingStaticMethod {
	
	public static void main(String[] a){
		if(a.length!=1){
			  System.out.println("the first argument should be the location of dsu specification file");
			  System.exit(0);
		  }
		  String dsuPath=a[0];
		  SuperClassAddingStaticMethod s = new SuperClassAddingStaticMethod();
		  s.loop(dsuPath);
	}

	private void loop( String dsuPath) {
		for(int i=5; i<15; i++){
			wrapper(i);
			if(i==9){
				org.javelus.invoker.DSUInvoker.invokeDSU(dsuPath);
			}
		  }
	  }

	  /*
	   * Wrapper is a separate function, so that loop() does not have to be
	   * recompiled.
	   */
	  private void wrapper(int i) {
	    ChangedSub.add(i);
	  }
}
