

public class AddingInstanceIntegerField {
  public static void main(String a[]) {
	  if(a.length!=2){
		  System.out.println("the first argument should be the location of dsu specification file");
		  System.exit(0);
	  }
	  String dsuPath1=a[0];
	  String dsuPath2=a[1];
	  AddingInstanceIntegerField s = new AddingInstanceIntegerField();
	  Changed c = createChanged();
	  s.loop(c, dsuPath1,dsuPath2);
  }

  /*
   * This is a separate function so that main does not have to recompiled.
   * createChanged() has to be recompiled not because of the new, but
   * because it called Changed.<init>() after the new.
   */
  private static Changed createChanged() {
    return new Changed();
  }

  private void loop(Changed c, String dsuPath1, String dsuPath2) {
    for (int local_i = 0; local_i < 10; local_i++) {
      wrapper(c);
      if (local_i == 5) {
        org.javelus.invoker.DSUInvoker.invokeDSU(dsuPath1);
      }else if(local_i == 7){
        org.javelus.invoker.DSUInvoker.invokeDSU(dsuPath2);
      }
    }
  }

  /*
   * Wrapper is a separate function, so that loop() does not have to be
   * recompiled.
   */
  private void wrapper(Changed c) {
    c.increment();
  }
}
