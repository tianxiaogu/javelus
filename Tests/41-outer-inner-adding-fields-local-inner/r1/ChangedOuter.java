
public class ChangedOuter {

	public int i=1;
	public int j;
	public void test(){
		
		class ChangedInner {
			public int k = 10;
			public int m=0;
			
			public void increment(){
				i++;
				j=i;
				k+=10;
				m=k;
				System.out.println("version 2: i = "+ i);
				System.out.println("version 2: j = "+ j);
				System.out.println("version 2: k = "+ k);
				System.out.println("version 2: m = "+ m );
			}
		}
		
		ChangedInner inner=new ChangedInner();
		inner.increment();
	}
	
	
}
