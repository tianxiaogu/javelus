



public class InterfaceAddingInstanceMethod {
	public static void testEmployee(String dsuPath){
		EmployeeInf[] employees=new EmployeeInf[10010];
		for(int i=1;i<10010;i++){
			String name="Name"+i;
			employees[i-1]=new Employee(name, 20+i, 10*i);
		}	
		for(int i=1;i<10010;i++){
			if( i %5000 ==0){
			System.out.println(employees[i-1].getName11());
			System.out.println(employees[i-1].toString());
			}
			
			if(i==6000)
				org.javelus.invoker.DSUInvoker.invokeDSU(dsuPath);
		}
	}
	
	public static void main(String[] args){
		if(args.length!=1){
			System.out.println("the first argument should be the location of dsu specification file");
			System.exit(0);
		}
		String dsuPath=args[0];	
		testEmployee(dsuPath);	
	}
}