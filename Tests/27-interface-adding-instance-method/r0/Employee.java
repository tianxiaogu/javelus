

public class Employee implements EmployeeInf {
	String name;
	int age;
	int salary;
	
	public Employee(String name, int age, int salary){
		this.name=name;
		this.age=age;
		this.salary=salary;
	}
	
	@Override
	public int getSalary() {
		return salary;
	}

	@Override
	public int getAge() {
		return age;
	}

	@Override
	public String getName() {
		return name;
	}

	public String getName1() {
		return name;
	}
	
	public String getName2() {
		return name;
	}

	public String getName3() {
		return name;
	}	
	
	public String getName4() {
		return name;
	}

	public String getName5() {
		return name;
	}
	
	public String getName6() {
		return name;
	}

	public String getName7() {
		return name;
	}	

	public String getName8() {
		return name;
	}

	public String getName9() {
		return name;
	}
	
	public String getName10() {
		return name;
	}

	public String getName11() {
		return name;
	}	
	
	@Override
	public void setAge(int age) {
		this.age=age;	}

	@Override
	public void setName(String name) {
		this.name=name;
	}

	@Override
	public void setSalary(int salary) {
		this.salary=salary;
	}

	public String toString(){
		String s=new String("version1: name:"+name+", age:"+age+", salary:"+salary);
		return s;
	}
}
