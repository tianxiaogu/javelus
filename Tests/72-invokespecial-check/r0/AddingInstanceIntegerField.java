

public class AddingInstanceIntegerField {
  public static class Changed {
  private int i;

  private void increment() {
    i++;
    
      System.out.format("version1: i: %d\n", i);
   }
  }


  public static void main(String a[]) {
	  if(a.length!=1){
		  System.out.println("the first argument should be the location of dsu specification file");
		  System.exit(0);
	  }
	  String dsuPath=a[0];
	  AddingInstanceIntegerField s = new AddingInstanceIntegerField();
	  Changed c = createChanged();
	  Changed c2 = createChanged();
	  s.loop(c, dsuPath);
	  s.wrapper(c2);
  }

  /*
   * This is a separate function so that main does not have to recompiled.
   * createChanged() has to be recompiled not because of the new, but
   * because it called Changed.<init>() after the new.
   */
  private static Changed createChanged() {
    return new Changed();
  }

  private void loop(Changed c, String dsuPath) {
    for (int local_i = 0; local_i < 10; local_i++) {
      wrapper(c);
      if (local_i == 5) {
        org.javelus.invoker.DSUInvoker.invokeDSU(dsuPath);
      }
    }
  }

  

  /*
   * Wrapper is a separate function, so that loop() does not have to be
   * recompiled.
   */
  private void wrapper(Changed c) {
    c.increment();
  }
}
