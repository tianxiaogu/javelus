

public interface EmployeeInf {

	public String getName();
	public void setName(String name);
	public int getAge();
	public void setAge(int age);
	public int getSalary();
	public void setSalary(int salary);
	public boolean getSex();
	public void setSex(boolean sex);
}
