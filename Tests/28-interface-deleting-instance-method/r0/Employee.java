

public class Employee implements EmployeeInf {
	String name;
	int age;
	int salary;
	boolean sex;
	
	public Employee(String name, int age, int salary){
		this.name=name;
		this.age=age;
		this.salary=salary;
		this.sex=true;
	}
	
	public Employee(String name, int age, int salary, boolean sex){
		this.name=name;
		this.age=age;
		this.salary=salary;
		this.sex=sex;
	}
	
	@Override
	public int getSalary() {
		return salary;
	}

	@Override
	public int getAge() {
		return age;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setAge(int age) {
		this.age=age;	}

	@Override
	public void setName(String name) {
		this.name=name;
	}

	@Override
	public void setSalary(int salary) {
		this.salary=salary;
	}

	public boolean getSex(){
		return sex;
	}
	
	public void setSex(boolean sex){
		this.sex=sex;
	}
	
	public String toString(){
		String sexString= sex?"male":"female";
		String s=new String("version 1: name:"+name+", age:"+age+", sex:"+sexString+", salary:"+salary);
		return s;
	}
}
