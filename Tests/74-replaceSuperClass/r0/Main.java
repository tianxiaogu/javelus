public class Main {

	public static void main(String[] a){
		if(a.length!=1){
			  System.out.println("the first argument should be the location of dsu specification file");
			  System.exit(0);
		  }
		  String dsuPath=a[0];
		ChangeSub c = new ChangeSub();
		int i=0;
		while(i<5){
			c.increment();
			System.out.println(c.toString());
			i++;
			if(i==2){
				org.javelus.invoker.DSUInvoker.invokeDSU(dsuPath);
			}
		}
	}
}
