
public class ChangedOuter {

	public static int i;
	public static int j;
	
	public static class ChangedInner{
		public int k;
		
		public void increment(){
			i++;
			k++;
			System.out.println("version 2: i = "+ i);
			System.out.println("version 2: j = "+ j);
			System.out.println("version 2: k = "+ k );
		}
	}
}
