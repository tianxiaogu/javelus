

public class AddingStaticMethod {
	
	public static void main(String[] a){
		if(a.length!=1){
			  System.out.println("the first argument should be the location of dsu specification file");
			  System.exit(0);
		  }
		  String dsuPath=a[0];
		  AddingStaticMethod s = new AddingStaticMethod();
		  s.loop(dsuPath);
	}

	private void loop( String dsuPath) {
		int j=10;
		for(int i=0; i<10; i++){
			wrapper(i, j);
			if(i==5){
				org.javelus.invoker.DSUInvoker.invokeDSU(dsuPath);
				j=20;
			}
		  }
	  }

	  /*
	   * Wrapper is a separate function, so that loop() does not have to be
	   * recompiled.
	   */
	  private void wrapper(int i, int j) {
	    Changed.add(i, j);
	  }
}
