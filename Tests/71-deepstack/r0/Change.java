public class Change{
	boolean flag=false;
	public int i=0;
	
	public void increment(String dsuPath){
		System.out.println("version1: increment begin");
		i++;
		if(i==2 && !flag){
			org.javelus.invoker.DSUInvoker.invokeDSU(dsuPath);
			flag=true;
		}
		System.out.println("version1: increment end");
	}
	
	public void run(String dsuPath){
		System.out.println("version1: run begin");
		for(int i=0;i<3;i++){
			increment(dsuPath);
			System.out.println("i is "+this.i);
		}
		System.out.println("version1: run end");
	}
	
	public String getName(){
		return "Change";
	}
	
}
