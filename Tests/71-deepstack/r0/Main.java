public class Main {

	public static void main(String[] a){
		if(a.length!=1){
			  System.out.println("the first argument should be the location of dsu specification file");
			  System.exit(0);
		  }
		  String dsuPath=a[0];
		Change c = new Change();
		int i=0;
		while(i<2){
			c.run(dsuPath);
			i++;
		}
	}
}
