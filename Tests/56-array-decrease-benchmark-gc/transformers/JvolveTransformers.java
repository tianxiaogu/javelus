public abstract class JvolveTransformers {
  public static void jvolveObject(ChangedMixedStruct6 to, r0_ChangedMixedStruct6 from) {
    to.i0 = from.i0;
    to.o1 = from.o1;
    to.i2 = from.i2;
    to.o3 = from.o3;
  }
  public static void jvolveClass(ChangedMixedStruct6 unused) {
  }
}
