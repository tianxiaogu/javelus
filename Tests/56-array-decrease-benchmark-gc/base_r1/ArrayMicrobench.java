
import java.util.Random;

public class ArrayMicrobench {

  public static void main(String args[]) {
    long seed = 0L;
    int arraySize = 0;
    int changePercent = 0;
    try {
      seed = Long.parseLong(args[0]);
      arraySize = Integer.parseInt(args[1]);
      changePercent = Integer.parseInt(args[2]);
    } catch (ArrayIndexOutOfBoundsException e) {
      System.out.println(e);
      System.exit(1);
    }
    if ((changePercent < 0) || (changePercent > 100)) {
      System.out.println("Percentage of changed objects should be integer between 0..100");
      System.exit(1);
    }
    Struct[] array = initialize(seed, arraySize, changePercent);
    //org.javelus.invoker.DSUInvoker.invokeDSU(null);
	// Invoke update first
    touch(array);
	// Measure MixObject Model performance
	touch(array);
    System.gc();
	touch(array);
	touch(array);
  }

  private static void touch(Struct [] array){
	long start=System.currentTimeMillis();
	for(int i=0;i<array.length;i++){
		array[i].touch();
	}
	long end=System.currentTimeMillis();
	System.out.println(end-start);
  }
  
  private static Struct[] initialize(long seed, int arraySize, int changePercent) {
    Random generator = new Random(seed);
    Struct[] array = new Struct[arraySize];
    for (int i = 0; i < arraySize; i++) {
      Struct struct = null;
      int r = generator.nextInt(100);
      if (r < changePercent) {
        struct = new ChangedMixedStruct(i);
      } else {
        struct = new UnChangedMixedStruct(i);
      }
      array[i] = struct;
    }
    return array;
  }
}
