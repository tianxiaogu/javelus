
public class ChangedOuter {

	public static int i;
	public static int j;
	
	public static class ChangedInner{
		public int k;
		public int m;
		
		public void increment(){
			i++;
			j++;
			k++;
			m++;
			System.out.println("version 2: i = "+ i);
			System.out.println("version 2: j = "+ j);
			System.out.println("version 2: k = "+ k);
			System.out.println("version 2: m = "+ m);
		}
	}
}
