

public class SuperClassAddingInstanceMethod {
	
	public static void main(String[] a){
		if(a.length!=1){
			  System.out.println("the first argument should be the location of dsu specification file");
			  System.exit(0);
		  }
		  String dsuPath=a[0];
		  SuperClassAddingInstanceMethod s = new SuperClassAddingInstanceMethod();
		  ChangedSub c = createChanged();
		  s.loop(c, dsuPath);
	}
	
	private static ChangedSub createChanged() {
	    return new ChangedSub();
	  }

	private void loop(ChangedSub c, String dsuPath) {
		for(int i=5; i<15; i++){
			wrapper(c, i);
			if(i==9){
				org.javelus.invoker.DSUInvoker.invokeDSU(dsuPath);
			}
		  }
	  }

	  /*
	   * Wrapper is a separate function, so that loop() does not have to be
	   * recompiled.
	   */
	  private void wrapper(ChangedSub c, int i) {
	    c.add(i);
	  }
}
