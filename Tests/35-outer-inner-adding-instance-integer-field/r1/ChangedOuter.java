
public class ChangedOuter {

	public int i;
	public int j;
	
	public class ChangedInner{
		public int k;
		public int m;
		
		public void increment(){
			i++;
			j++;
			k++;
			m++;
			System.out.println("version 2: i = "+ i);
			System.out.println("version 2: j = "+ j);
			System.out.println("version 2: k = "+ k);
			System.out.println("version 2: m = "+ m);
		}
	}
}
