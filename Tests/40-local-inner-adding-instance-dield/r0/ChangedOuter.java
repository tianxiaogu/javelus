
public class ChangedOuter {

	public int i=1;
	
	public void  test(){
		
		class ChangedInner {
			public int k = 10;
			
			public void increment(){
				i++;
				k+=10;
				System.out.println("version 1: i = "+ i);
				System.out.println("version 1: k = "+ k );
			}
		}
		
		ChangedInner inner=new ChangedInner();
		inner.increment();
	}
	
	
}
