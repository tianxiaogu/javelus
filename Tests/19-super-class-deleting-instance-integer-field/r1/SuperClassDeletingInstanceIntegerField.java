

public class SuperClassDeletingInstanceIntegerField {
  public static void main(String a[]) {
	  if(a.length!=1){
		  System.out.println("the first argument should be the location of dsu specification file");
		  System.exit(0);
	  }
	  String dsuPath=a[0];
	  SuperClassDeletingInstanceIntegerField s = new SuperClassDeletingInstanceIntegerField();
	  ChangedSub c = createChanged();
	  s.loop(c, dsuPath);
  }

  /*
   * This is a separate function so that main does not have to recompiled.
   * createChanged() has to be recompiled not because of the new, but
   * because it called Changed.<init>() after the new.
   */
  private static ChangedSub createChanged() {
    return new ChangedSub();
  }

  private void loop(ChangedSub c, String dsuPath) {
    for (int local_i = 0; local_i < 10005; local_i++) {
      wrapper(c);
      if (local_i == 5002) {
        org.javelus.invoker.DSUInvoker.invokeDSU(dsuPath);
      }
    }
  }

  /*
   * Wrapper is a separate function, so that loop() does not have to be
   * recompiled.
   */
  private void wrapper(ChangedSub c) {
    c.increment();
  }
}
