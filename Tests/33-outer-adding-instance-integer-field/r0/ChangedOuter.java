
public class ChangedOuter {

	public int i;
	
	public class ChangedInner{
		public int k;
		
		public void increment(){
			i++;
			k++;
			System.out.println("version 1: i = "+ i);
			System.out.println("version 1: k = "+ k );
		}
	}
}
