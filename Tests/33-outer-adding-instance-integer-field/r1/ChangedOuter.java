
public class ChangedOuter {

	public int i;
	public int j;
	
	public class ChangedInner{
		public int k;
		
		public void increment(){
			i++;
			k++;
			System.out.println("version 2: i = "+ i);
			System.out.println("version 2: j = "+ j);
			System.out.println("version 2: k = "+ k );
		}
	}
}
