
public class Changed {
	
	public static int add(int i, int j){
		if(i>10 || j>10){
			long t=(long)i;
			long s=(long)j;
			long m=add(t, s);
			return (int)m;
		}else{
			int m=i+j;
			System.out.println("version 1: add(int, int): "+i+" + "+j+" = "+ m);
			return m;
		}
	}
	
	public static long add(long i, long j){
		long m=i+j;
		System.out.println("version 1: add(long, long): "+i+" + "+j+" = "+ m);
		return m;
	}
}
