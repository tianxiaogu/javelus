



public class InterfaceChanged {
	public static void testEmployee(String dsuPath){
		Object[] employees=new Object[5];
		for(int i=1;i<6;i++){
			String name="Name"+i;
			employees[i-1]=new Employee(name, 20+i, 1000*i);
			System.out.println(((Employee)employees[i-1]).toString());
			if(i%3==0)
				org.javelus.invoker.DSUInvoker.invokeDSU(dsuPath);
		}	
	}
	
	public static void main(String[] args){
		if(args.length!=1){
			System.out.println("the first argument should be the location of dsu specification file");
			System.exit(0);
		}
		String dsuPath=args[0];	
		testEmployee(dsuPath);	
	}
}
