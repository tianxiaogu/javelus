public class ChangedMixedStruct6 extends Struct {
  
  int i4;
  Object o5;
  
  int i0;
  Object o1;
  int i2;
  Object o3;
  

  public ChangedMixedStruct6(int i) {
    i0 = i;
    o1 = null;
    i2 = i + 2;
    o3 = null;
    i4 = i + 4;
    o5 = null;
  }
  
  public void touch(){
	i0++;
	i2++;
	i4++;
  }
}
