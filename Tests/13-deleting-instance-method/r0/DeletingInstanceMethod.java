

public class DeletingInstanceMethod {
	
	public static void main(String[] a){
		if(a.length!=1){
			  System.out.println("the first argument should be the location of dsu specification file");
			  System.exit(0);
		  }
		  String dsuPath=a[0];
		  DeletingInstanceMethod s = new DeletingInstanceMethod();
		  Changed c = createChanged();
		  s.loop(c, dsuPath);
	}
	
	private static Changed createChanged() {
	    return new Changed();
	  }

	private void loop(Changed c, String dsuPath) {
		int j=10;
		for(int i=5; i<15; i++){
			wrapper(c, i, j);
			if(i==11){
				org.javelus.invoker.DSUInvoker.invokeDSU(dsuPath);
				j=20;
			}
		  }
	  }

	  /*
	   * Wrapper is a separate function, so that loop() does not have to be
	   * recompiled.
	   */
	  private void wrapper(Changed c, int i, int j) {
	    c.add(i, j);
	  }
}
