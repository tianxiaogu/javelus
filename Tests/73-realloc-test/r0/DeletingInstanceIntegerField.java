

public class DeletingInstanceIntegerField {
  public static void main(String a[]) {
	  if(a.length!=1){
		  System.out.println("the first argument should be the location of dsu specification file");
		  System.exit(0);
	  }
	  String dsuPath=a[0];
    DeletingInstanceIntegerField s = new DeletingInstanceIntegerField();
    Changed c = createChanged();
    s.loop(c, dsuPath);
  }

  /*
   * This is a separate function so that main does not have to recompiled.
   * createChanged() has to be recompiled not because of the new, but
   * because it called Changed.<init>() after the new.
   */
  private static Changed createChanged() {
    return new Changed();
  }

  private void loop(Changed c, String dsuPath) {
    for (int local_i = 0; local_i < 10005; local_i++) {
      wrapper(c);
      if (local_i == 5002) {
        org.javelus.invoker.DSUInvoker.invokeDSU(dsuPath);
      }
    }
  }

  /*
   * Wrapper is a separate function, so that loop() does not have to be
   * recompiled.
   */
  private void wrapper(Changed c) {
    c.increment();
  }
}
