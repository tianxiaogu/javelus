

public class AddingInstanceIntegerField {
  public static void main(String a[]) {
	  if(a.length!=1){
		  System.out.println("the first argument should be the location of dsu specification file");
		  System.exit(0);
	  }
	  String dsuPath=a[0];
	  AddingInstanceIntegerField s = new AddingInstanceIntegerField();
	  Changed c = createChanged();
	  s.loop(c, dsuPath);
  }

  /*
   * This is a separate function so that main does not have to recompiled.
   * createChanged() has to be recompiled not because of the new, but
   * because it called Changed.<init>() after the new.
   */
  private static Changed createChanged() {
    Changed c = new Changed();
    c.setNext(new Changed());
    return c;
  }

  private void loop(Changed c, String dsuPath) {
    for (int local_i = 0; local_i < 10; local_i++) {
      wrapper(c);
      wrapper(c.next());
      if (local_i == 5) {
        org.javelus.invoker.DSUInvoker.invokeDSU(dsuPath);
      }else if(local_i == 9){
        System.gc();
      }
    }
  }

  /*
   * Wrapper is a separate function, so that loop() does not have to be
   * recompiled.
   */
  private void wrapper(Changed c) {
    c.increment();
  }
}
