
public class Changed {
  
  Changed next;
  
  public int i;
  
  public void setNext(Changed next){
    this.next = next;
  }
  
  public Changed next(){
    return next;
  }
  
  public void increment() {
    i++;    
    System.out.format("version1: i: %d\n", i);
  }
}
