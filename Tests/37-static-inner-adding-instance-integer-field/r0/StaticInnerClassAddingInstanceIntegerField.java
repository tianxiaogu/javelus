

public class StaticInnerClassAddingInstanceIntegerField {
		
	public static void main(String[] a){
		 if(a.length!=1){
			  System.out.println("the first argument should be the location of dsu specification file");
			  System.exit(0);
		  }
		  String dsuPath=a[0];
		  ChangedOuter.ChangedInner changedInner=createChangedInner();
		  StaticInnerClassAddingInstanceIntegerField s=new StaticInnerClassAddingInstanceIntegerField();
		  s.loop(changedInner, dsuPath);
	}
	
	public static ChangedOuter.ChangedInner createChangedInner(){
		ChangedOuter.ChangedInner changedInner=new ChangedOuter.ChangedInner();
		return changedInner;
	}
	
	public void loop(ChangedOuter.ChangedInner inner, String dsuPath){
		for(int i=0;i<10;i++){
			inner.increment();
			if(i==5)
				org.javelus.invoker.DSUInvoker.invokeDSU(dsuPath);
		}
	}
}
