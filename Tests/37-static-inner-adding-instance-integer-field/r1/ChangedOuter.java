
public class ChangedOuter {

	public static int i;
	
	public static class ChangedInner{
		public int k;
		public int m;
		
		public void increment(){
			i+=10;
			k+=10;
			m+=10;
			System.out.println("version 2: i = "+ i);
			System.out.println("version 2: k = "+ k);
			System.out.println("version 2: m = "+ m);
		}
	}
}
