
/*
 * All that is changed is a string.
 */

public class MethodBodyChangeWithWrapper {
  public int i;
  public static void main(String a[]) {
	  if(a.length!=1){
		  System.out.println("the first argument should be the location of dsu specification file");
		  System.exit(0);
	  }
	  String dsuPath=a[0];
	  MethodBodyChangeWithWrapper m = new MethodBodyChangeWithWrapper();
	  m.i = 0;
	  m.loop(dsuPath);
  }

  private void loop(String dsuPath) {
    while (true) {
      wrapper(dsuPath);
    }
  }

  private void wrapper(String dsuPath) {
    increment();
    if (i == 1250) {
      org.javelus.invoker.DSUInvoker.invokeDSU(dsuPath);
    }
    if (i == 2550) {
      System.out.println("Goodbye...");
      System.exit(0);
    }
  }

  /*
   * This is the function that is modified in the new version.
   */
  private void increment() {
    i++;
    if (i % 500 == 0) {
      System.out.format("Version 2 i: %d\n", i);
    }
  }
}
