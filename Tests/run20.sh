for i in `seq 1 20` 
do 
#    ant batch-run-array-bench -Dtag=56-array-decrease-benchmark-gc > with-gc-56-${i}.txt
    ant batch-run-base-bench -Dtag=56-array-decrease-benchmark-gc > base-with-gc-56-${i}.txt
done

for i in `seq 1 20` 
do 
#    ant batch-run-array-bench -Dtag=54-array-reorder-benchmark-gc > with-gc-54-${i}.txt
    ant batch-run-base-bench -Dtag=54-array-reorder-benchmark-gc > base-with-gc-54-${i}.txt
done

for i in `seq 1 20` 
do 
#    ant batch-run-array-bench -Dtag=55-array-increase-benchmark-gc > with-gc-55-${i}.txt
    ant batch-run-base-bench -Dtag=55-array-increase-benchmark-gc > base-with-gc-55-${i}.txt
done

