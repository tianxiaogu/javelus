

public class Employee {
	String name;
	int age;
	int salary;
	boolean sex;
	
	public Employee(String name, int age, int salary){
		this.name=name;
		this.age=age;
		this.salary=salary;
		this.sex=true;
	}
	
	public Employee(String name, int age, int salary, boolean sex){
		this.name=name;
		this.age=age;
		this.salary=salary;
		this.sex=sex;
	}
	
	public int getSalary() {
		return salary;
	}

	public int getAge() {
		return age;
	}
	
	public String getName() {
		return name;
	}

	public void setAge(int age) {
		this.age=age;	}

	public void setName(String name) {
		this.name=name;
	}

	public void setSalary(int salary) {
		this.salary=salary;
	}

	public boolean getSex(){
		return sex;
	}
	
	public void setSex(boolean sex){
		this.sex=sex;
	}
	
	public String toString(){
		String sexString= sex?"male":"female";
		String s=new String("version2: name:"+name+", age:"+age+", sex:"+sexString+", salary:"+salary+", company: "+Company.getName());
		return s;
	}
}
