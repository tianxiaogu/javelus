public class ChangedMixedStruct extends Struct {
  int i0;
  Object o1;
  int i2;
  Object o3;


  public ChangedMixedStruct(int i) {
    i0 = i;
    o1 = null;
    i2 = i + 2;
    o3 = null;

  }
  
  public void touch(){
	i0++;
	i2++;
  }
}
