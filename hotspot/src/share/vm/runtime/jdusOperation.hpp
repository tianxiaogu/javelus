/*
 * Copyright (C) 2012  Tianxiao Gu. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Please contact Instituite of Computer Software, Nanjing University, 
 * 163 Xianlin Avenue, Nanjing, Jiangsu Provience, 210046, China,
 * or visit moon.nju.edu.cn if you need additional information or have any
 * questions.
*/

#ifndef SHARE_VM_RUNTIME_JDUSOPERATION_HPP
#define SHARE_VM_RUNTIME_JDUSOPERATION_HPP

#include "runtime/vm_operations.hpp"
#include "runtime/thread.hpp"


class VM_JDUSOperation;

class JDUSTask : public CHeapObj {

private:
  VM_JDUSOperation * _op;
  JDUSTask* _next;
public:
  JDUSTask(VM_JDUSOperation * op);

  JDUSTask* next() const                      { return _next; }
  void         set_next(JDUSTask* next)       { _next = next; }
  VM_JDUSOperation * operation() {return _op;}
};




class VM_JDUSOperation : public VM_Operation {
private:
  static objArrayOop     _old_methods;
  static objArrayOop     _new_methods;
  static methodOop*      _matching_old_methods;
  static methodOop*      _matching_new_methods;
  static methodOop*      _deleted_methods;
  static methodOop*      _added_methods;
  static int             _matching_methods_length;
  static int             _deleted_methods_length;
  static int             _added_methods_length;
  static klassOop        _the_class_oop;
  static klassOop        _scratch_class_oop;
  static klassOop        _mix_class_oop;


	static GrowableArray<DSUClass>*      _classes_to_relink;

  DSU * _dsu;
//  const DSUClass  *_class_defs;  // ptr to _class_count defs
//  jint                        _class_count;
//  instanceKlassHandle *       _scratch_classes;
//  instanceKlassHandle *       _mix_old_classes;

  jdusError                   _res;






  static bool do_redefine_scratch_klass(instanceKlassHandle the_class,
      instanceKlassHandle scratch_class,jdusClassUpdatingType change_type);
  static bool do_create_mix_version(instanceKlassHandle the_class,
      instanceKlassHandle scratch_class,jdusClassUpdatingType change_type);
  static bool do_create_temp_version(instanceKlassHandle the_class,
      instanceKlassHandle scratch_class,jdusClassUpdatingType change_type);
  static bool do_compute_match_fields(instanceKlassHandle the_class,
      instanceKlassHandle scratch_class,jdusClassUpdatingType change_type);
  static bool do_run_class_transformer(instanceKlassHandle the_class,
      instanceKlassHandle scratch_class,jdusClassUpdatingType change_type);
public:
  VM_JDUSOperation(DSU * dsu);
  virtual ~VM_JDUSOperation();

  VMOp_Type type() const {return VMOp_JDUS;};

  virtual void doit();
  virtual bool doit_prologue();
  virtual void doit_epilogue();

	DSU* dsu() const {return _dsu;}

	void free_memory();
	
  bool is_finished()    const {return _dsu->is_finished();}
  bool is_discarded()   const {return _dsu->is_discarded();}
  bool is_interrupted() const {return _dsu->is_interrupted();}
  bool is_init()        const {return _dsu->is_init();}
  void set_request_state(jdusRequestState state){
    _dsu->set_request_state(state);
  }

  bool policy_system_safe_point() const{
    return _dsu->policy() == JDUS_SYSTEM_SAFE_POINT;
  }

  bool policy_thread_safe_point() const{
    return _dsu->policy() == JDUS_THREAD_SAFE_POINT;
  }

  bool policy_passive_system_safe_point() const{
    return _dsu->policy() == JDUS_PASSIVE_SYSTEM_SAFE_POINT;
  }

  jdusPolicy policy() const {
    return _dsu->policy();
  }

	///////////////////////////////////////////////////////////////
	// static help functions
	//////////////////////////////////////////////////////////////


	// **********************************************************
	// function used before VM safe point
	// **********************************************************

	// create a mix version if we should assign a valid instanceKlass 
	// for MixOldObject after update
  static void create_mix_version(instanceKlassHandle the_class,
		instanceKlassHandle scratch_class, DSUClass *dsu_class,TRAPS);
	// create a temp version if we should assign a valid instanceKlass for 
	// MixOldObject during invoking transformer before update is finished.
	static void create_temp_version(instanceKlassHandle the_class,
		instanceKlassHandle scratch_class, DSUClass *dsu_class,TRAPS);
  
  static void compute_and_set_fields(instanceKlassHandle the_class,
		instanceKlassHandle scratch_class, DSUClass *dsu_class, TRAPS);

  static void compute_and_cache_common_info(instanceKlassHandle the_class, 
		instanceKlassHandle scratch_class, 
		DSUClass *dsu_class, TRAPS);

  static void make_invalid_class(instanceKlassHandle klass, 
		int min_object_size_in_bytes, 
		int min_vtable_size, TRAPS);

  static void set_invalid_methods(instanceKlassHandle the_class,
		instanceKlassHandle scratch_class, DSUClass *dsu_class, TRAPS);

  static void set_transformer_method(instanceKlassHandle the_class,
		instanceKlassHandle scratch_class, DSUClass *dsu_class, TRAPS);
  
  

  static void compute_ycsc(instanceKlassHandle the_class, 
		instanceKlassHandle scratch_class,
		instanceKlassHandle &ycsc_old,
		instanceKlassHandle &ycsc_new, TRAPS);
  
	// ****************************************************
	// helper functions used to load new version
	// ****************************************************
  static jdusError prepare_dsu(DSU* dsu,TRAPS);
  static jdusError load_new_class_versions(DSU* dsu,TRAPS);
	// just read load class to create instance klass
  // load and set the scratch klass
	// load new class
  static jdusError load_new_class(DSUClass *dsu_class,instanceKlassHandle &scratch_class, symbolHandle the_class_sym, Handle the_class_loader, Handle protection_domain, TRAPS);
  static jdusError load_new_stub_class(DSUClass *dsu_class, TRAPS);
  // extra preparation work for swapping
  static jdusError load_new_class_for_swapping(DSUClass *dsu_class, TRAPS);
  static jdusError load_new_class_for_redefining(DSUClass *dsu_class, TRAPS);



  static void flush_dependent_code(TRAPS);
	static void update_jmethod_ids();
  static void compute_added_deleted_matching_methods();
  

  static void run_class_transformer(DSUClass * dsu_class, instanceKlassHandle the_class,
      instanceKlassHandle scratch_class, TRAPS);

	// seems deprecated, will be removed in next version
  static void invalid_old_instance_class(instanceKlassHandle the_class,
      instanceKlassHandle scratch_class,
      instanceKlassHandle mix_version,
      jdusClassUpdatingType change_type,
      TRAPS);

  static void update_subklass_vtable_and_itable(instanceKlassHandle subklass,TRAPS);


	// Help function used to check DSU safe point
  static void set_restricted_methods(DSUClass *dsu_class);



	static void update_single_class(DSUClass *dsu_class,TRAPS);
  
  //For method body changed class
  static void swap_single_class(DSUClass *dsu_class,TRAPS);
  //For deleted old classes
  static void remove_single_class(DSUClass *dsu_class,TRAPS);
	//For indirect update class
	static void relink_single_class(DSUClass *dsu_class, TRAPS);
	
	
	static void check_and_append_relink_class(klassOop k_oop,
		oop initiating_loader, TRAPS);

	static void collect_classes_to_relink(TRAPS);
	static void relink_collected_classes(TRAPS);

	static void append_classes_to_relink(instanceKlassHandle ik);
	static void free_classes_to_relink();

	static void fix_new_constantPoolCache(instanceKlassHandle the_klass,
      instanceKlassHandle scratch_class,
      TRAPS);

	//FIELD,METHOD,BOTH
  static void redefine_single_class(DSUClass *dsu_class,TRAPS);
  static void install_scratch_class(instanceKlassHandle the_class,
      instanceKlassHandle scratch_class, jdusClassUpdatingType change_type,TRAPS);

	



  static void rename_and_define_old_class(instanceKlassHandle the_class,
      instanceKlassHandle scratch_class,
      DSUClass *dsu_class,
      jdusClassUpdatingType change_type,
      TRAPS);

  static void post_fix_the_class(instanceKlassHandle the_class,
      instanceKlassHandle scratch_class,
      DSUClass *dsu_class,
      jdusClassUpdatingType change_type,
      TRAPS);

  static void post_fix_scratch_class(instanceKlassHandle the_class,
      instanceKlassHandle scratch_class,
      DSUClass *dsu_class,
      jdusClassUpdatingType change_type,
      TRAPS);

  static void post_fix_ycsc(instanceKlassHandle the_class,
      instanceKlassHandle scratch_class,
      DSUClass *dsu_class,
      jdusClassUpdatingType change_type,
      TRAPS);

  static void post_fix_mix_version(instanceKlassHandle the_class,
      instanceKlassHandle scratch_class,
      DSUClass *dsu_class,
      jdusClassUpdatingType change_type,
      TRAPS);

  static void post_fix_temp_version(instanceKlassHandle the_class,
      instanceKlassHandle scratch_class,
      DSUClass *dsu_class,
      jdusClassUpdatingType change_type,
      TRAPS);

	// helper function answers whether a class can be swapped or redefined.
	static bool is_modifiable_class(oop klass_mirror);

	// seems deprecated, will be removed in the next version
  static objArrayHandle clone_methods(instanceKlassHandle scratch_class, TRAPS);
  static void mark_old_and_obsolete_methods(DSUClass *dsu_class, TRAPS);
  
  
  //void clear_restricted_methods(int method_count, jdusMethod * methods);
};

class VM_RelinkMixObjects : public VM_Operation {
protected:
  Handle* _obj;
  Handle* _new_mix_new_obj;

public:
  VM_RelinkMixObjects(Handle * obj, Handle * new_mix_new_obj);
  virtual VMOp_Type type() const { return VMOp_RelinkMixObjects; }
  virtual bool doit_prologue();
  virtual void doit();
};

class VM_UnlinkMixObjects : public VM_Operation {
protected:
  Handle* _obj;


public:
  VM_UnlinkMixObjects(Handle * obj);

  virtual VMOp_Type type() const { return VMOp_UnlinkMixObjects; }
  virtual bool doit_prologue();
  virtual void doit();
};
#endif
