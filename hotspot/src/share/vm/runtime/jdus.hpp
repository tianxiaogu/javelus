/*
 * Copyright (C) 2012  Tianxiao Gu. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Please contact Instituite of Computer Software, Nanjing University, 
 * 163 Xianlin Avenue, Nanjing, Jiangsu Provience, 210046, China,
 * or visit moon.nju.edu.cn if you need additional information or have any
 * questions.
*/

#ifndef SHARE_VM_RUNTIME_JDUS_HPP
#define SHARE_VM_RUNTIME_JDUS_HPP

#include "utilities/accessFlags.hpp"
#include "memory/iterator.hpp"
#include "runtime/thread.hpp"
//#include "classfile/classLoader.hpp"

class Dictionary;
class DSUMethod;
class DSUClass;
class DSUClassLoader;
class DSU;
class DSUStreamProvider;
class DSUDynamicPatchBuilder;
class ClassPathEntry;

class DoNothinCodeBlobClosure : public CodeBlobClosure {
public:
  // Called for each code blob.
  virtual void do_code_blob(CodeBlob* cb) {}
};

class StackRepairClosure: public OopClosure {
  Thread * _thread;
public:
  void set_thread(Thread *thread){
    _thread = thread;
  }
  virtual void do_oop(oop* p);
  virtual void do_oop(narrowOop* p) {
    //  ShouldNotReachHere();
  }
};

typedef enum {
  JDUS_ERROR_NONE = 0,
  JDUS_ERROR_INVALID_THREAD = 10,
  JDUS_ERROR_INVALID_THREAD_GROUP = 11,
  JDUS_ERROR_INVALID_PRIORITY = 12,
  JDUS_ERROR_THREAD_NOT_SUSPENDED = 13,
  JDUS_ERROR_THREAD_SUSPENDED = 14,
  JDUS_ERROR_THREAD_NOT_ALIVE = 15,
  JDUS_ERROR_INVALID_OBJECT = 20,
  JDUS_ERROR_INVALID_CLASS = 21,
  JDUS_ERROR_CLASS_NOT_PREPARED = 22,
  JDUS_ERROR_INVALID_METHODID = 23,
  JDUS_ERROR_INVALID_LOCATION = 24,
  JDUS_ERROR_INVALID_FIELDID = 25,
  JDUS_ERROR_NO_MORE_FRAMES = 31,
  JDUS_ERROR_OPAQUE_FRAME = 32,
  JDUS_ERROR_TYPE_MISMATCH = 34,
  JDUS_ERROR_INVALID_SLOT = 35,
  JDUS_ERROR_DUPLICATE = 40,
  JDUS_ERROR_NOT_FOUND = 41,
  JDUS_ERROR_INVALID_MONITOR = 50,
  JDUS_ERROR_NOT_MONITOR_OWNER = 51,
  JDUS_ERROR_INTERRUPT = 52,
  JDUS_ERROR_INVALID_CLASS_FORMAT = 60,
  JDUS_ERROR_CIRCULAR_CLASS_DEFINITION = 61,
  JDUS_ERROR_FAILS_VERIFICATION = 62,
  JDUS_ERROR_UNSUPPORTED_REDEFINITION_METHOD_ADDED = 63,
  JDUS_ERROR_UNSUPPORTED_REDEFINITION_SCHEMA_CHANGED = 64,
  JDUS_ERROR_INVALID_TYPESTATE = 65,
  JDUS_ERROR_UNSUPPORTED_REDEFINITION_HIERARCHY_CHANGED = 66,
  JDUS_ERROR_UNSUPPORTED_REDEFINITION_METHOD_DELETED = 67,
  JDUS_ERROR_UNSUPPORTED_VERSION = 68,
  JDUS_ERROR_NAMES_DONT_MATCH = 69,
  JDUS_ERROR_UNSUPPORTED_REDEFINITION_CLASS_MODIFIERS_CHANGED = 70,
  JDUS_ERROR_UNSUPPORTED_REDEFINITION_METHOD_MODIFIERS_CHANGED = 71,
  JDUS_ERROR_UNMODIFIABLE_CLASS = 79,
  JDUS_ERROR_NOT_AVAILABLE = 98,
  JDUS_ERROR_MUST_POSSESS_CAPABILITY = 99,
  JDUS_ERROR_NULL_POINTER = 100,
  JDUS_ERROR_ABSENT_INFORMATION = 101,
  JDUS_ERROR_INVALID_EVENT_TYPE = 102,
  JDUS_ERROR_ILLEGAL_ARGUMENT = 103,
  JDUS_ERROR_NATIVE_METHOD = 104,
  JDUS_ERROR_CLASS_LOADER_UNSUPPORTED = 106,
  JDUS_ERROR_OUT_OF_MEMORY = 110,
  JDUS_ERROR_ACCESS_DENIED = 111,
  JDUS_ERROR_WRONG_PHASE = 112,
  JDUS_ERROR_INTERNAL = 113,
  JDUS_ERROR_UNATTACHED_THREAD = 115,
  JDUS_ERROR_INVALID_ENVIRONMENT = 116,
  JDUS_ERROR_BAD_REQUEST_TYPE = 120,
  JDUS_ERROR_INVALID_MEMBER  = 121,
  JDUS_ERROR_COMPUTE_SET_FIELDS  = 122,
  JDUS_ERROR_SET_TRANSFORMER  = 123,
  JDUS_ERROR_REWRITER  = 124,
  JDUS_ERROR_LINK_OLD_CLASS  = 125,
  JDUS_ERROR_LINK_NEW_CLASS  = 126,
  JDUS_ERROR_TEMP_VERSION  = 127,
  JDUS_ERROR_MIX_VERSION  = 128,
  JDUS_ERROR_GET_UPDATING_TYPE  = 130,
  JDUS_ERROR_PREPARE_SUPER_FAIL = 131,
  JDUS_ERROR_PREPARE_INTERFACES_FAIL = 132,
  JDUS_ERROR_RESOLVE_CLASS_LOADER_FAIL = 133,
  JDUS_ERROR_TO_BE_ADDED = 134,
  JDUS_ERROR_MAX = 135
}jdusError;

typedef enum{
  /*The default policy, see Jvolve*/
  JDUS_SYSTEM_SAFE_POINT = 0,
  /*Passive System Safepoint, which suspend threads which have reached safepoint*/
  JDUS_PASSIVE_SYSTEM_SAFE_POINT = 1,
  /*Concurrent Version for different threads*/
  JDUS_THREAD_SAFE_POINT = 2,
}jdusPolicy;

typedef enum {
  JDUS_REQUEST_INIT = 0,
  JDUS_REQUEST_DISCARDED = 1,
  JDUS_REQUEST_INTERRUPTED = 2,
  JDUS_REQUEST_FINISHED = 3
}jdusRequestState;

typedef enum {
  JDUS_CLASS_UNKNOWN = -1,  //
  JDUS_CLASS_UNKNOWN_INDEX = 0,

  JDUS_CLASS_NONE = 0,  // 0, no changes between the old and new version...
  JDUS_CLASS_NONE_INDEX = JDUS_CLASS_UNKNOWN_INDEX + 1, 

  JDUS_CLASS_MC  = 1,   // 1, relink new classes
  JDUS_CLASS_MC_INDEX = JDUS_CLASS_NONE_INDEX + 1,

  JDUS_CLASS_BC  = JDUS_CLASS_MC << 1,   // swap method body
  JDUS_CLASS_BC_INDEX = JDUS_CLASS_MC_INDEX + 1,

  JDUS_CLASS_INDIRECT_BC = JDUS_CLASS_BC << 1,   // no changes, but has to update vtable due to an swapped super class.
  JDUS_CLASS_INDIRECT_BC_INDEX = JDUS_CLASS_BC_INDEX + 1,

  JDUS_CLASS_SMETHOD = JDUS_CLASS_INDIRECT_BC << 1,   // only add or del static methods
  JDUS_CLASS_SMETHOD_INDEX = JDUS_CLASS_INDIRECT_BC_INDEX + 1,

  JDUS_CLASS_SFIELD = JDUS_CLASS_SMETHOD << 1,   // only add or del static fields
  JDUS_CLASS_SFIELD_INDEX = JDUS_CLASS_SMETHOD_INDEX + 1,

  JDUS_CLASS_SBOTH = JDUS_CLASS_SMETHOD + JDUS_CLASS_SFIELD, // add or del both static methods and fields
  JDUS_CLASS_SBOTH_INDEX = JDUS_CLASS_SFIELD_INDEX + 1,

  JDUS_CLASS_METHOD = JDUS_CLASS_SFIELD << 1,   // add 
  JDUS_CLASS_METHOD_INDEX = JDUS_CLASS_SBOTH_INDEX + 1,

  JDUS_CLASS_FIELD = JDUS_CLASS_METHOD << 1,    //
  JDUS_CLASS_FIELD_INDEX = JDUS_CLASS_METHOD_INDEX + 1,

  JDUS_CLASS_BOTH = JDUS_CLASS_FIELD + JDUS_CLASS_METHOD,   //
  JDUS_CLASS_BOTH_INDEX = JDUS_CLASS_FIELD_INDEX + 1,

  JDUS_CLASS_DEL = JDUS_CLASS_FIELD << 1, //
  JDUS_CLASS_DEL_INDEX = JDUS_CLASS_BOTH_INDEX + 1,

  JDUS_CLASS_ADD = JDUS_CLASS_DEL << 1,   //
  JDUS_CLASS_ADD_INDEX = JDUS_CLASS_DEL_INDEX + 1,

  JDUS_CLASS_STUB = JDUS_CLASS_ADD << 1,   //
  JDUS_CLASS_STUB_INDEX = JDUS_CLASS_ADD_INDEX + 1,

  JDUS_CLASS_COUNT = JDUS_CLASS_STUB_INDEX + 1,
}jdusClassUpdatingType;

typedef enum {
  JDUS_METHOD_NONE = 0,
  JDUS_METHOD_MC = 1,
  JDUS_METHOD_BC = 2,
  JDUS_METHOD_DEL = 3,
  JDUS_METHOD_ADD = 4,
}jdusMethodUpdatingType;


// A DSUObject is used for following updating.
// Each DSUObject is parsed from the dynamic patch 
// or just parsed by comparing jvmtiClassDefinitions
class DSUObject : public CHeapObj{
  friend class JDUS;
public:
  DSUObject();
  ~DSUObject();
  
protected:
  bool _validated;
  void set_validated(bool v) { _validated = v;}
public: 
  bool validated() const { return _validated;}
  virtual bool validate(TRAPS) = 0;
  virtual void print();
};


// A DSUMethod contains updating information about a method.
// The method must be declared in the old program.
class DSUMethod : public DSUObject{
private:
  jdusMethodUpdatingType _updating_type;
  jobject _method;
  DSUClass * _dsu_class;
  DSUMethod * _next;
public:
  DSUMethod();
  ~DSUMethod();

  virtual bool validate(TRAPS);

  void set_dsu_class(DSUClass * klass){
    _dsu_class = klass;
  }

  void set_next(DSUMethod* next) {
    _next = next;
  }

  DSUMethod* next() const {
    return _next;
  }

  DSUClass * dsu_class() const {
    return _dsu_class;
  }

  jdusMethodUpdatingType updating_type() const {return _updating_type;}
  methodOop method() const;

  void set_updating_type(jdusMethodUpdatingType updating_type) { _updating_type = updating_type;}
  void set_method (jobject method) {_method=method;}

  virtual void print();
};

// A DSUClass contains updating information about a class.
// The class may be 
class DSUClass : public DSUObject {
private:  
  DSUClass * _next;
  DSUClassLoader* _dsu_class_loader;
  jdusClassUpdatingType _updating_type;

  // symbolHandle not java.lang.String
  jobject _name;
  jobject _new_name;

  jobject _klass;
  jobject _scratch_klass;

  //indicate all data required by updating has been prepared.
  // 1). detailed updating type
  // 2). restricted method
  // 3). 
  bool _prepared;

  jobject _mix_klass;
  jobject _temp_klass;
  jobject _object_transformer_method;
  jobject _class_transformer_method;

  
  DSUMethod * _first_method;
  DSUMethod * _last_method;

  jint _class_bytes_count;
  unsigned char* _class_bytes;

  // Cached the value
  int     _min_object_size_in_bytes;
  int     _min_vtable_size;
  jobject _ycsc_old;
  jobject _ycsc_new;

  jint    _match_static_count;
  jint * _match_static_fields;

  

public:
  DSUClass();
  ~DSUClass();

  void initialize();

  virtual bool validate(TRAPS);

  void set_name(jobject name){
    _name = name;
  }

  void set_new_name (jobject new_name){
    _new_name = new_name;
  }

  DSUClassLoader* dsu_class_loader() const { 
    assert(_dsu_class_loader != NULL, "sanity check");
    return _dsu_class_loader;
  }


  // changed classes and deleted classes have old versions
  jdusError resolve_old_version(instanceKlassHandle & old_version, TRAPS);
  jdusError resolve_new_version(instanceKlassHandle &new_version, TRAPS);
  jdusError resolve_new_version_at_safe_point(instanceKlassHandle &new_version, TRAPS);
  jdusError resolve_new_version_by_jdus_thread(instanceKlassHandle &new_version, TRAPS);
  
  jdusError refresh_updating_type(instanceKlassHandle old_version, instanceKlassHandle new_version, TRAPS);

  bool require_old_version() const;
  bool old_version_resolved() const;
  bool require_new_version() const;
  bool new_version_resolved() const;

  jdusError prepare(TRAPS);
  bool prepared() const {return _prepared;}

  // compare and determine the updating type of these classes.
  jdusError compare_and_normalize_class(instanceKlassHandle old_version, instanceKlassHandle new_version, TRAPS);

  symbolOop new_name ()const { return (symbolOop)JNIHandles::resolve(_new_name);}
  symbolOop name()   const{return (symbolOop)JNIHandles::resolve(_name);}

  methodOop object_transformer_method() const {
    return (methodOop)JNIHandles::resolve(_object_transformer_method);
  }
  void set_object_transformer_method(jobject object_transformer_method){
    this->_object_transformer_method = object_transformer_method;
  }

  methodOop class_transformer_method() const {
    return (methodOop)JNIHandles::resolve(_class_transformer_method);
  }
  void set_class_transformer_method(jobject class_transformer_method){
    this->_class_transformer_method = class_transformer_method;
  }

  // cache klassOop here
  void set_klass_raw(klassOop* klass) { _klass = (jobject)klass;}
  klassOop* klass_raw() const {return (klassOop*)_klass;}

  // during parsing from jvmtiClassDefinition, 
  // we may set _klass = symbol to indicate that the class is included in the new version.
  void set_klass_symbol(jobject klass) {
    _klass = klass;
  }

  //bool has_new_version() const{
  //  return _klass != NULL;
  //}

  jdusClassUpdatingType updating_type() const {return _updating_type;}

  klassOop klass() const {return (klassOop)JNIHandles::resolve(_klass);}
  klassOop scratch_klass() const {return (klassOop)JNIHandles::resolve(_scratch_klass);}
  klassOop mix_klass() const {return (klassOop)JNIHandles::resolve(_mix_klass);}
  klassOop temp_klass() const {return (klassOop)JNIHandles::resolve(_temp_klass);}

  jint class_bytes_count() const {return _class_bytes_count;}
  unsigned char* class_bytes() const {return _class_bytes;}
  
  DSUClass* next() const {return _next;}
  void set_next(DSUClass * next) { _next = next;}

  DSUMethod* first_method() const {return _first_method;}
  DSUMethod* last_method() const {return _last_method;}

  DSUMethod* find_method(symbolHandle name, symbolHandle desc);

  void methods_do(void f(DSUMethod * dsu_class,TRAPS), TRAPS);

  void add_method(DSUMethod *methods);

  void set_class_bytes_count(jint class_bytes_count) {_class_bytes_count = class_bytes_count;}
  void set_class_bytes(unsigned char* class_bytes) {_class_bytes = class_bytes;}

  void set_updating_type(jdusClassUpdatingType updating_type) {_updating_type = updating_type;}
  void set_klass(jobject klass) {_klass = klass;}
  void set_scratch_klass(jobject klass) {_scratch_klass = klass;}
  void set_mix_klass(jobject klass) {_mix_klass = klass;}
  void set_temp_klass(jobject klass) {_temp_klass = klass;}

  jint *match_static_fields() const{ return _match_static_fields;}
  void set_match_static_fields(jint * match_static_fields){_match_static_fields = match_static_fields;}

  jint match_static_count() const{ return _match_static_count;}
  void set_match_static_count(jint match_static_count){_match_static_count = match_static_count;}

  DSUClassLoader * dsu_class_loader(){
    return _dsu_class_loader;
  }

  int from_rn() const;
  int to_rn() const;

  void set_dsu_class_loader(DSUClassLoader * dsu_class_loader){
    _dsu_class_loader = dsu_class_loader;
  }
  
  

  int min_object_size_in_bytes() const{
    return _min_object_size_in_bytes;
  }

  void set_min_object_size_in_bytes(int size) {
    _min_object_size_in_bytes = size;
  }

  int min_vtable_size() const {
    return _min_vtable_size;
  }

  void set_min_vtable_size(int size){
    _min_vtable_size = size;
  }

  // youngest common super class could be decided by compares class hierarchies of the new class and old class
  // if we use equal(A equal A' iff A == A') to map common relation, then the yscs() return the real klassOop
  // if we use equvialent (A equvialent A' iff A.real_name == A'.real_name), then the ycsc() return the real_name symbolOop

  klassOop ycsc_old() const{
    return (klassOop)JNIHandles::resolve(_ycsc_old);
  }

  klassOop ycsc_new() const{
    return (klassOop)JNIHandles::resolve(_ycsc_new);
  }

  void set_ycsc_old(jobject ycsc){
    _ycsc_old = ycsc;
  }

  void set_ycsc_new(jobject ycsc){
    _ycsc_new = ycsc;
  }

  DSUMethod* allocate_method(methodHandle method, TRAPS);

  virtual void print();

};


class DSUClassLoader : public DSUObject {
private:

  DSU*  _dsu;

  // sibling DSUClassLoader in the belonging DSU
  DSUClassLoader * _next;

  // id of this DSUClassLoader
  // symbolHandle
  jobject _id;
  // id of the loaded classloader of this DSUClassLoader
  // symbolHandle
  jobject _lid;
  DSUClassLoader * _loaded_loader;

  // works like class path
  DSUStreamProvider * _stream_provider;

  // the class laoder
  jobject _class_loader;
  // the instanceKlass
  jobject _helper_class;
  jobject _transformer_class;


  // class file  
  DSUClass* _first_class;
  DSUClass* _last_class;

  //DSUClass* allocate_class(TRAPS);
public:
  
  klassOop helper_class() const {return (klassOop)JNIHandles::resolve(_helper_class);}
  klassOop transformer_class() const {return (klassOop)JNIHandles::resolve(_transformer_class);}
  oop      classloader() const {return (oop)JNIHandles::resolve(_class_loader);}

  void resolve(TRAPS);
  bool resolved();

  DSUClassLoader * loaded_loader() const{
    return _loaded_loader;
  }

  virtual bool validate(TRAPS);

  void set_loaded_loader(DSUClassLoader * loaded_loader){
    _loaded_loader = loaded_loader;
  }

  DSUClass *first_class() const { return _first_class;}
  DSUClass *last_class() const { return _last_class;}
  void add_class(DSUClass* klass);
  
  void set_helper_class(jobject helper_class) {_helper_class = helper_class;}
  void set_transformer_class(jobject transformer_class) {_transformer_class = transformer_class;}
  void set_class_loader(jobject class_loader) {_class_loader = class_loader;}

  DSUClassLoader();
  ~DSUClassLoader();

  void set_next(DSUClassLoader * next){
    _next = next;
  }

  DSUClassLoader * next()const {
    return _next;
  }

  void set_dsu(DSU * dsu){
    _dsu = dsu;
  }

  DSU* dsu() const{
    return _dsu;
  }

  void set_id(jobject id){
    this->_id = id;
  }

  symbolOop id(){
    return (symbolOop)JNIHandles::resolve(this->_id);
  }

  void set_lid(jobject lid){
    this->_lid = lid;
  }

  symbolOop lid(){
    return (symbolOop)JNIHandles::resolve(this->_lid);
  }

  // prepare this DSU operation
  jdusError prepare(TRAPS);

  void set_stream_provider(DSUStreamProvider * stream_provider){ _stream_provider = stream_provider; }
  DSUStreamProvider * stream_provider() const { return _stream_provider;}

  // just create the klassOop and does not put it into any dictionary.
  jdusError  load_new_version(symbolHandle name, instanceKlassHandle &new_class, TRAPS);
  // allocate a new class if it does not exist.
  DSUClass* allocate_class(symbolHandle name, TRAPS);
  
  DSUClass* find_class_by_name(symbolHandle name);

  virtual void print();
};

class DSU : public DSUObject {
private:

  int  _from_rn;
  int  _to_rn;

  // A linked list of class loader
  DSUClassLoader* _first_class_loader;
  DSUClassLoader* _last_class_loader;

  jdusRequestState _request_state;
  jdusPolicy       _policy;
  const char * _dynamic_patch;

  DSU* _next;

  DSUStreamProvider* _shared_stream_provider;

public:
  DSU();
  ~DSU();

  // Currently, we use a shared stream provider model
  void set_shared_stream_provider(DSUStreamProvider* stream_provider) { _shared_stream_provider = stream_provider;}
  DSUStreamProvider* shared_stream_provider() const { return _shared_stream_provider; }

  virtual bool validate(TRAPS);

  static DSU* build_from_jvmti(jint class_count, jvmtiClassDefinition *_class_defs, TRAPS);  
  // build a DSU object from the dynamic patch (HotSpot Style)
  static DSU* build_from_dynamic_patch(const char * dynamic_patch, TRAPS);

  // prepare this DSU operation
  jdusError prepare(TRAPS);

  jdusPolicy policy() const {
    return _policy;
  }

  void set_policy(jdusPolicy policy){
    _policy = policy;
  }

  jdusRequestState request_state() const{
    return _request_state;
  }

  bool is_finished()    const {return _request_state == JDUS_REQUEST_FINISHED;}
  bool is_discarded()   const {return _request_state == JDUS_REQUEST_DISCARDED;}
  bool is_interrupted() const {return _request_state == JDUS_REQUEST_INTERRUPTED;}
  bool is_init()        const {return _request_state == JDUS_REQUEST_INIT;}

  // eager update: all objects are updated eagerly. 
  // But no pointers are updated eagerly, i.e., we sill have to use MixObjects.
  // Pointers are updated during copying or moving GC, which will merge MixObjects.
  bool is_eager_update()         const {return ForceEagerUpdate;}
  // eager update pointer: all objects are updated eagerly.
  // All MixObjects are merged 
  bool is_eager_update_pointer() const {return ForceEagerUpdate;}
  // lazy update: all objects are updated lazily. 
  // Pointers are updated during copying or moving GC, which will merge MixObjects.
  bool is_lazy_update()          const {return !(is_eager_update() && is_eager_update_pointer());}

  void set_request_state(jdusRequestState request_state){
    _request_state = request_state;
  }


  void classes_do(void f(DSUClass * dsu_class,TRAPS), TRAPS);
  

  void set_from_rn(int from_rn){
    _from_rn = from_rn;
  }
  void set_to_rn(int to_rn){
    _to_rn = to_rn;
  }
  int from_rn() const {return _from_rn;}
  int to_rn()   const {return _to_rn;}

  DSU* next() const {return _next;}
  void set_next(DSU* next) { _next = next;}

  // get added first class loader
  DSUClassLoader* first_class_loader() const { return _first_class_loader;}
  DSUClassLoader* last_class_loader() const { return _last_class_loader;}

  // allocate a class loader with given id and lid
  DSUClassLoader* allocate_class_loader(symbolHandle id, symbolHandle lid, TRAPS);
  // append class loader to the list, no checking
  void add_class_loader(DSUClassLoader* class_loader);
    
  // query DSUClass
  DSUClass* find_class_by_name(symbolHandle name);
  DSUClass* find_class_by_name_and_loader(symbolHandle name, Handle loader);

  // id is a global symbolHandle NOT is a java.lang.String object
  DSUClassLoader * find_class_loader_by_id(symbolHandle id);
  DSUClassLoader * find_class_loader_by_loader(Handle loader);
  //DSUClassLoader * find_class_loader_by_oop(Handle cl);
  //DSUClassLoader * find_class_loader_by_jni_handle(jobject class_loader);  
  virtual void print();
};

// provide stream for new classes
class DSUStreamProvider: public CHeapObj{
public:
  virtual ClassFileStream* open_stream(const char * name) = 0;
};

class DSUPathEntryStreamProvider: public DSUStreamProvider{
private:
  ClassPathEntry * _first_entry;
  ClassPathEntry * _last_entry;
public:
  DSUPathEntryStreamProvider();
  ~DSUPathEntryStreamProvider();
  void append_path(char* path, TRAPS);
  void add_entry(ClassPathEntry* entry);
  virtual ClassFileStream* open_stream(const char * name);
};



class DSUDynamicPatchBuilder : public ResourceObj{
public:
  enum DynamicPatchCommand{
    UnknownCommand = -1,
    DynamicPatchFirstCommand = 0,
    ClassLoaderCommand = DynamicPatchFirstCommand,
    ClassPathCommand,
    AddClassCommand,
    ModClassCommand,
    DelClassCommand,
    TransformerCommand,
    LoaderHelperCommand,
    DynamicPatchCommandCount
  };
  static const char * command_names[];
private:
  DSU * _dsu;
  DSUClassLoader* _current_class_loader;
  DSUClassLoader* _default_class_loader;
  DSUPathEntryStreamProvider* _shared_stream_provider;

  void use_class_loader(char *line, TRAPS);
  void build_default_class_loader(TRAPS);
  void use_default_class_loader();
  void append_class_path_entry(char * line, TRAPS);
  void append_added_class(char * line, TRAPS);
  void append_modified_class(char * line, TRAPS);
  void append_deleted_class(char * line, TRAPS);
  void append_transformer(char * line, TRAPS);
  void append_loader_helper(char * line, TRAPS);
  DSUClassLoader* current_class_loader();
  static DynamicPatchCommand parse_command_name(const char* line, int * bytes_read);
  void parse_file(const char * file, TRAPS);
  void parse_line(char * line, TRAPS);
  void populate_sub_classes(TRAPS);
  void populate_sub_classes(DSUClass* dsu_class, instanceKlassHandle ik, TRAPS);
public:
  DSUDynamicPatchBuilder();
  ~DSUDynamicPatchBuilder();
  void build(const char * dynamic_patch, TRAPS);
  DSU* dsu() const;
};

//typedef struct _jdusClassLoader jdusClassLoader;

class JDUS: AllStatic {
  friend VM_JDUSOperation;
private:
  // Hashtable holding jdus loaded classes.

  // the head of dsu list
  // only can be changed at VM safe point
  static DSU*                   _first_dsu;
  static DSU*                   _last_dsu;
  // The current working DSU
  static volatile DSU*          _active_dsu;
  // the largest revision number
  static int                    _latest_rn;

  static Dictionary*            _jdus_dictionary;
  //static PlaceholderTable*      _jdus_placeholder;
  static JDUSThread*            _jdus_thread;

  static methodOop              _implicit_update_method;

  static klassOop               _developer_interface_klass;

  static void create_DevelopInterface_klass(TRAPS);
  //static void create_DSU_klass(TRAPS);
  //static void create_DSUClass_klass(TRAPS);
  //static void create_DSUClassLoader_klass(TRAPS);
  //static void create_DSUMethod_klass(TRAPS);
  //static int  calc_loader_count (JNIEnv * env, Handle java_loader, TRAPS);

  static void set_active_dsu(DSU* dsu);
  static void increment_system_rn();
  static void install_dsu(DSU *dsu);
public:
  const static int MAX_REVISION_NUMBER;

public:

  static DSU* active_dsu();
  static void finish_active_dsu();
  static void discard_active_dsu();


  static jdusClassUpdatingType join(jdusClassUpdatingType this_type, jdusClassUpdatingType that_type);
  static const char * class_updating_type_name(jdusClassUpdatingType ct);

  static klassOop developer_interface_klass();

  
  static Dictionary* dictionary() {return _jdus_dictionary;}
  static klassOop resolve_jdus_klass(
    symbolHandle class_name,
    Handle class_loader,
    Handle protection_domain,
    /*bool is_superclass,*/
    TRAPS);
  static klassOop resolve_jdus_klass_or_null(
    symbolHandle class_name,
    Handle class_loader,
    Handle protection_domain,
    /*bool is_superclass,*/
    TRAPS);

  static jdusClassUpdatingType get_updating_type(instanceKlassHandle ikh, TRAPS);
  
  static void prepare_super_class(instanceKlassHandle ikh, TRAPS);
  static void prepare_interfaces(instanceKlassHandle ikh, TRAPS);

  static void realloc_decreased_obj(Handle obj, int old_size_in_bytes, int new_size_in_bytes);

  static void copy_fields(oop src, oop dst, instanceKlass * ik);
  static void copy_fields(oop src, oop dst, oop mix_dst, instanceKlass* ik);
  static void copy_field(oop src, oop dst, int src_offset, int dst_offset, BasicType type);

  static void copy_fields(Handle src, Handle dst, instanceKlassHandle ikh,TRAPS);
  static void copy_fields(Handle src, Handle dst, Handle mix_dst, instanceKlassHandle ikh,TRAPS);
  static void copy_field(Handle src, Handle dst, int src_offset, int dst_offset, BasicType type);
  static void read_value(Handle obj, int offset, BasicType type, JavaValue &result,TRAPS);
  static void read_arg(Handle obj, int offset, BasicType type, JavaCallArguments &args,TRAPS);

  static void add_jdus_klass(instanceKlassHandle k, TRAPS);
  static void add_jdus_klass_place_holder(Handle loader, symbolHandle class_name, TRAPS);

  static JDUSThread* get_jdus_thread() {return _jdus_thread;}

  static JDUSThread* make_jdus_thread(const char * name);

  static void repatch_method(methodOop method,bool print_replace, TRAPS);


  static void initialize(TRAPS);
  static void jdus_thread_init();

  static oop merge_mixobjects(oop old_obj);
  static oop merge_mixobjects(oop old_obj, oop new_obj);
  static void jdus_thread_loop();

  //parse support
  /*static jdusError parse_DSU (DSU * dsu,TRAPS);
  static jdusError parse_DSUClassLoader (DSUClassLoader * dsu_classloader, TRAPS);
  static jdusError parse_DSUClass (DSUClass * dsu_class, TRAPS);
  static jdusError parse_DSUMethod (DSUMethod * dsu_method, TRAPS);*/


  static void  parse_old_field_annotation(instanceKlassHandle the_class,
    instanceKlassHandle transformer,typeArrayHandle annotation,typeArrayHandle &result,TRAPS);

  static void oops_do(OopClosure* f);


  //These methods should be called without data race
  static void merge_jdus_dictionary(TRAPS);
  
  static int  system_revision_number();  
  static bool validate_DSU(DSU *dsu);

  static DSU* get_DSU(int from_rn);

  static void wakeup_JDUS_thread();
  static void waitRequest();
  static void notifyRequest();
  //XXX remember that not alive does not mean is dead. 
  // It may have not born yet.
  static bool is_class_not_born_at(instanceKlassHandle the_class, int rn);
  static bool is_class_born_at(instanceKlassHandle the_class, int rn) ;
  static bool is_class_alive(instanceKlassHandle the_class, int rn) ;
  static bool is_class_dead_at(instanceKlassHandle the_class, int rn) ;
  static bool is_class_dead(instanceKlassHandle the_class, int rn) ;

  static bool check_application_threads(jdusPolicy policy);
  static void repair_application_threads(jdusPolicy policy);

  static void install_return_barrier_all_threads();
  static void install_return_barrier_single_thread(JavaThread *thread, intptr_t * barrier);

  static bool check_single_thread(JavaThread * thread);
  static void repair_single_thread(JavaThread * thread);

  static void check_class_initialized(JavaThread *thread, instanceKlassHandle klass);

};

class JDUSEagerUpdate : public AllStatic {

private:
  // an array of weak global handle
  static jobject*  _candidates;
  static int       _candidates_length;
  static volatile bool _be_updating;
  static bool      _update_pointers;
  static volatile bool _has_been_updated;
public:	

  static void initialize(TRAPS);

  static void configure_eager_update(GrowableArray<jobject> * candidates, bool update_pointers);

  static void install_eager_update_return_barrier(TRAPS);

  static void clear_invalid_object_checks(TRAPS);

  static void clear_invalid_object_checks_for_single_class(klassOop k_oop, oop loader, TRAPS);

  static void clear_mixobjects_checks(TRAPS);

  static void clear_mixobjects_checks_for_single_class(klassOop k_oop, oop loader, TRAPS);

  static void start_eager_update(JavaThread *thread);
  static void collect_dead_instances_at_safepoint(int dead_time, GrowableArray<jobject>* result, TRAPS);
};

/////////////////////////////////////////////////////
// Code copied from jvmtiRedefineClassesTrace.hpp
////////////////////////////////////////////////////


// JDUS Trace Definition
//
// 0x00000000 |           0 - default, no tracing message
// 0x00000001 |           1 - print timer information of VMJDUSOperation
// 0x00000002 |           2 - general debug
// 0x00000004 |           4 - general trace
// 0x00000008 |           8 - parse DSU
// 0x00000010 |          16 - load DSU and DSU class loaders
// 0x00000020 |          32 - load DSU classes
// 0x00000040 |          64 - load DSU methods
// 0x00000080 |         128 - trace check and repair thread
// 0x00000100 |         256 - trace eager update
// 0x00000200 |         512 - redefine class
// 0x00000400 |        1024 - swap class
// 0x00000800 |        2048 - recompile class
// 0x00001000 |        4096 - trace transformer in sharedRuntime
// 0x00002000 |        8192 - trace resolve invalid member

#define JDUS_TRACE(level, args) \
  if ((TraceJDUS & level) != 0) { \
    ResourceMark rm; \
    tty->print("[JDUS]-0x%x: ", level); \
    tty->print_cr args; \
  } while (0)

#define JDUS_INFO(args) \
  if ((TraceJDUS & 0x00000001) != 0) { \
    ResourceMark rm; \
    tty->print("[JDUS]-[Info]: "); \
    tty->print_cr args; \
  } while (0)

#define JDUS_DEBUG(args) \
  if ((TraceJDUS & 0x00000002) != 0) { \
    ResourceMark rm; \
    tty->print("[JDUS]-[Debug]: "); \
    tty->print_cr args; \
  } while (0)

#define JDUS_WARN(args) \
  { \
    ResourceMark rm; \
    tty->print("[JDUS]-[Warn]: "); \
    tty->print_cr args; \
  } while (0)

#define JDUS_TRACE_WITH_THREAD(level, thread, args) \
  if ((TraceJDUS & level) != 0) { \
    ResourceMark rm(thread); \
    tty->print("[JDUS]-0x%x: ", level); \
    tty->print_cr args; \
  } while (0)

#define JDUS_TRACE_MESG(args) \
  if ((TraceJDUS & 0x00000004) != 0) { \
    ResourceMark rm; \
    tty->print("[JDUS]: "); \
    tty->print_cr args; \
  } while (0)

// Macro for checking if TraceRedefineClasses has a specific bit
// enabled. Returns true if the bit specified by level is set.
#define JDUS_TRACE_ENABLED(level) ((TraceJDUS & level) != 0)

// Macro for checking if TraceRedefineClasses has one or more bits
// set in a range of bit values. Returns true if one or more bits
// is set in the range from low..high inclusive. Assumes that low
// and high are single bit values.
//
// ((high << 1) - 1)
//     Yields a mask that removes bits greater than the high bit value.
//     This algorithm doesn't work with highest bit.
// ~(low - 1)
//     Yields a mask that removes bits lower than the low bit value.
#define JDUS_TRACE_IN_RANGE(low, high) \
(((TraceJDUS & ((high << 1) - 1)) & ~(low - 1)) != 0)

// Timer support macros. Only do timer operations if timer tracing
// is enabled. The "while (0)" is so we can use semi-colon at end of
// the macro.
#define JDUS_TIMER_START(t) \
  if (JDUS_TRACE_ENABLED(0x00000001)) { \
    t.start(); \
  } while (0)
#define JDUS_TIMER_STOP(t) \
  if (JDUS_TRACE_ENABLED(0x00000001)) { \
    t.stop(); \
  } while (0)

/////////////////////////////////////////////////////
// End of trace definition
////////////////////////////////////////////////////

#endif

