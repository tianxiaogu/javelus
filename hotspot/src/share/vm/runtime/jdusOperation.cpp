/*
* Copyright (C) 2012  Tianxiao Gu. All rights reserved.
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
* 
* Please contact Instituite of Computer Software, Nanjing University, 
* 163 Xianlin Avenue, Nanjing, Jiangsu Provience, 210046, China,
* or visit moon.nju.edu.cn if you need additional information or have any
* questions.
*/

#include "precompiled.hpp"
#include "runtime/jdusOperation.hpp"
#include "runtime/jdus.hpp"
#include "interpreter/oopMapCache.hpp"
#include "interpreter/rewriter.hpp"
#include "memory/oopFactory.hpp"
#include "memory/heapInspection.hpp"
#include "classfile/javaClasses.hpp"
#include "code/codeCache.hpp"
#include "runtime/deoptimization.hpp"
#include "runtime/relocator.hpp"
#include "runtime/javaCalls.hpp"

#include "oops/klass.inline.hpp"



JDUSTask::JDUSTask(VM_JDUSOperation *op):
_op(op),_next(NULL){}



objArrayOop VM_JDUSOperation::_old_methods = NULL;
objArrayOop VM_JDUSOperation::_new_methods = NULL;
methodOop*  VM_JDUSOperation::_matching_old_methods = NULL;
methodOop*  VM_JDUSOperation::_matching_new_methods = NULL;
methodOop*  VM_JDUSOperation::_deleted_methods      = NULL;
methodOop*  VM_JDUSOperation::_added_methods        = NULL;
int         VM_JDUSOperation::_matching_methods_length = 0;
int         VM_JDUSOperation::_deleted_methods_length  = 0;
int         VM_JDUSOperation::_added_methods_length    = 0;
klassOop    VM_JDUSOperation::_the_class_oop = NULL;
klassOop    VM_JDUSOperation::_scratch_class_oop = NULL;
klassOop    VM_JDUSOperation::_mix_class_oop = NULL;

GrowableArray<DSUClass>*  VM_JDUSOperation::_classes_to_relink = NULL;

// VM_JDUSOperation
//
//
/////////////////////

VM_JDUSOperation::VM_JDUSOperation(DSU *dsu){
  _dsu = dsu;
}

VM_JDUSOperation::~VM_JDUSOperation() {
  if (_dsu != NULL){
    delete _dsu;
  }
}

bool VM_JDUSOperation::is_modifiable_class(oop klass_mirror) {
  // classes for primitives cannot be redefined
  if (java_lang_Class::is_primitive(klass_mirror)) {
    return false;
  }
  klassOop the_class_oop = java_lang_Class::as_klassOop(klass_mirror);
  // classes for arrays cannot be redefined
  if (the_class_oop == NULL || !Klass::cast(the_class_oop)->oop_is_instance()) {
    return false;
  }
  return true;
}

// Helper function for loading new class out of VM Safe point.
// This means that we cannot make modification to data that may influence the execution.
jdusError VM_JDUSOperation::load_new_class(
  DSUClass *dsu_class,
  instanceKlassHandle &scratch_class, 
  symbolHandle the_class_sym, 
  Handle the_class_loader, 
  Handle protection_domain, 
  TRAPS){

    const jdusClassUpdatingType updating_type = dsu_class->updating_type();
    assert(updating_type == JDUS_CLASS_STUB ||
      (updating_type >= JDUS_CLASS_BC && updating_type <= JDUS_CLASS_BOTH ), "change type with new version");

    assert(dsu_class->class_bytes() != NULL  && dsu_class->class_bytes_count() > 0,"Class bytes must..");

    ClassFileStream st((u1*) dsu_class->class_bytes(),
      dsu_class->class_bytes_count(), (char *)"__VM_JDUSOperation__");

    klassOop k = SystemDictionary::parse_stream(the_class_sym,
      the_class_loader,
      protection_domain,
      &st,
      THREAD);

    if (HAS_PENDING_EXCEPTION) {
      symbolOop ex_name = PENDING_EXCEPTION->klass()->klass_part()->name();
      CLEAR_PENDING_EXCEPTION;

      if (ex_name == vmSymbols::java_lang_UnsupportedClassVersionError()) {
        return JDUS_ERROR_UNSUPPORTED_VERSION;
      } else if (ex_name == vmSymbols::java_lang_ClassFormatError()) {
        return JDUS_ERROR_INVALID_CLASS_FORMAT;
      } else if (ex_name == vmSymbols::java_lang_ClassCircularityError()) {
        return JDUS_ERROR_CIRCULAR_CLASS_DEFINITION;
      } else if (ex_name == vmSymbols::java_lang_NoClassDefFoundError()) {
        // The message will be "XXX (wrong name: YYY)"
        return JDUS_ERROR_NAMES_DONT_MATCH;
      } else if (ex_name == vmSymbols::java_lang_OutOfMemoryError()) {
        return JDUS_ERROR_OUT_OF_MEMORY;
      } else {  // Just in case more exceptions can be thrown..
        JDUS_TRACE_MESG(("Fails verification, exception name is %s, class name is %s." , 
          ex_name->as_C_string(), dsu_class->name()->as_C_string()));
        return JDUS_ERROR_FAILS_VERIFICATION;
      }
    }


    scratch_class = instanceKlassHandle(THREAD, k);
    if(the_class_sym.is_null()){
      the_class_sym = symbolHandle(THREAD,scratch_class->name());
    }


    dsu_class->set_scratch_klass(JNIHandles::make_global(scratch_class));

    // TODO, we donot add stub classs and swapped class into JDUS dictionary
    // So move this method to the caller.
    // TODO, add all classes into JDUS dictionary, and update super class accordingly
    //JDUS::add_jdus_klass(scratch_class,THREAD);

    ////////////
    set_restricted_methods(dsu_class);
    ////////////

    return JDUS_ERROR_NONE;
}


jdusError VM_JDUSOperation::load_new_stub_class(DSUClass *dsu_class, TRAPS){
  HandleMark    hm(THREAD);
  ResourceMark  rm(THREAD);


  const jdusClassUpdatingType updating_type = dsu_class->updating_type();
  assert(updating_type == JDUS_CLASS_STUB,"swapping enable class is only STUB");

  //tty->print_cr("[JDUS] Load new stub class %s.", dsu_class->name()->as_C_string());

  Handle the_class_loader = Handle(THREAD,dsu_class->dsu_class_loader()->classloader());
  Handle pd;
  symbolHandle null_name;
  instanceKlassHandle scratch_class;

  jdusError ret = load_new_class(dsu_class,scratch_class, null_name,the_class_loader,pd,THREAD);

  if(HAS_PENDING_EXCEPTION){
    symbolOop ex_name = PENDING_EXCEPTION->klass()->klass_part()->name();
    tty->print_cr("[JDUS] Load new stub class [%s] meeting exception %s.", dsu_class->name()->as_C_string(), ex_name->as_C_string());
    return ret;
  }

  if(ret != JDUS_ERROR_NONE){
    return ret;
  }

  assert(scratch_class.not_null(), "the new load class must be not null");

  Handle lockObject = SystemDictionary::compute_loader_lock_object(the_class_loader, THREAD);
  SystemDictionary::check_loader_lock_contention(lockObject, THREAD);
  {
    ObjectLocker ol(lockObject, THREAD);
    if(scratch_class->is_loaded()){
      tty->print_cr("%d", scratch_class->get_init_state());
    }
    assert(!scratch_class->is_loaded(), "sanity check0");
    // TODO here we just defined the stub classes.
    SystemDictionary::define_instance_class(scratch_class,THREAD);

    if(!scratch_class->is_loaded()){
      scratch_class->name()->print();
    }

    assert(scratch_class->is_loaded(), "sanity check1");
    if(!scratch_class->is_loaded()){
      scratch_class->set_init_state(instanceKlass::loaded);
    }
    assert(scratch_class->is_loaded(), "sanity check2");
  }
  scratch_class->link_class(THREAD);
  //scratch_class->eager_initialize(CHECK_(JDUS_ERROR_INTERNAL));

  if(HAS_PENDING_EXCEPTION){
    tty->print_cr("[JDUS] Link stub class [%s] has pending exception.", dsu_class->name()->as_C_string());
    THREAD->pending_exception()->print();
    return JDUS_ERROR_LINK_NEW_CLASS;
  }

  assert(scratch_class->is_linked(),"Class Must be Linked");

  // TODO we add this class to 
  JDUS::add_jdus_klass(scratch_class,THREAD);

  scratch_class->set_DSU_state(instanceKlass::jdus_has_been_added);

  return JDUS_ERROR_NONE;
}

jdusError VM_JDUSOperation::load_new_class_for_swapping(DSUClass *dsu_class, TRAPS){
  HandleMark    hm(THREAD);
  ResourceMark  rm(THREAD);

  const jdusClassUpdatingType updating_type = dsu_class->updating_type();
  assert(updating_type == JDUS_CLASS_BC,"swapping enable class is only BC");

  instanceKlassHandle the_class (THREAD, dsu_class->klass());
  Handle the_class_loader(THREAD, the_class->class_loader());
  Handle pd (THREAD, the_class->protection_domain());
  symbolHandle the_class_sym (THREAD,the_class->name());

  instanceKlassHandle scratch_class;

  jdusError ret = load_new_class(dsu_class,scratch_class, the_class_sym,the_class_loader,pd,THREAD);

  if(ret != JDUS_ERROR_NONE){
    return ret;
  }

  if (!the_class->is_linked()) {
    if(the_class->is_in_error_state()){
      tty->print_cr("[JDUS] Old class %s is in error state.", the_class->name()->as_C_string());
    }
    the_class->link_class(THREAD);
    if (HAS_PENDING_EXCEPTION) {
      symbolOop ex_name = PENDING_EXCEPTION->klass()->klass_part()->name();
      CLEAR_PENDING_EXCEPTION;
      if (ex_name == vmSymbols::java_lang_OutOfMemoryError()) {
        return JDUS_ERROR_OUT_OF_MEMORY;
      } else {
        the_class()->print();
        tty->print_cr("[JDUS] Link old class error for %s. exception is %s. [%d]", dsu_class->name()->as_C_string(),ex_name->as_C_string(),the_class->is_in_error_state());
        return JDUS_ERROR_LINK_OLD_CLASS;
      }
    }
  }



  if(the_class->previous_version() != NULL){
    {
      // copy flags set at runtime.  
      HandleMark hm(THREAD);
      // We should swap flags(invalid/MixNew) to scratch class
      objArrayHandle old_methods (THREAD, the_class->methods());
      objArrayHandle new_methods (THREAD, scratch_class->methods());
      int nof_methods = old_methods->length();
      assert(nof_methods == new_methods->length(),"invariant");
      for(int i=0; i<nof_methods; i++){
        methodHandle old_mh (THREAD, (methodOop)old_methods->obj_at(i));
        methodHandle new_mh (THREAD, (methodOop)new_methods->obj_at(i));

        assert(old_mh->name() == new_mh->name(), "invariant");
        assert(old_mh->signature() == new_mh->signature(), "invariant");

        if(old_mh->is_mix_new()){
          new_mh->set_is_mix_new();
        }
        if(old_mh->is_mix_invalid_method()){
          new_mh->set_is_mix_invalid_method();
        }
      }
    }

    {
      // we should swap old fields 
      HandleMark hm(hm);
      typeArrayHandle old_fields (THREAD, the_class->fields());
      typeArrayHandle new_fields (THREAD, scratch_class->fields());
      int nof_fields = old_fields->length();
      assert(nof_fields == new_fields->length(),"invariant");

      for(int i=0; i<nof_fields; i++){
        int old_flags = old_fields->int_at(i + instanceKlass::access_flags_offset);
        int new_flags = new_fields->int_at(i + instanceKlass::access_flags_offset);

        if((old_flags & JVM_ACC_IS_MIX_NEW_MEMBER) != 0){
          new_flags = new_flags | JVM_ACC_IS_MIX_NEW_MEMBER;
          new_fields->int_at_put(i + instanceKlass::access_flags_offset, new_flags);
        }
        if((old_flags & JVM_ACC_IS_INVALID_MEMBER) != 0){
          new_flags = new_flags | JVM_ACC_IS_INVALID_MEMBER;
          new_fields->int_at_put(i + instanceKlass::access_flags_offset, new_flags);
        }
      }
    }

  }

  // We just allocate constantPoolCache here and do not initialize vtable and itable.
  Rewriter::rewrite(scratch_class, THREAD);
  if (HAS_PENDING_EXCEPTION) {
    symbolOop ex_name = PENDING_EXCEPTION->klass()->klass_part()->name();
    CLEAR_PENDING_EXCEPTION;
    if (ex_name == vmSymbols::java_lang_OutOfMemoryError()) {
      return JDUS_ERROR_OUT_OF_MEMORY;
    } else {
      return JDUS_ERROR_REWRITER;
    }
  }

  //TODO, we add new class to temporal dictionary here.
  JDUS::add_jdus_klass(scratch_class,THREAD);

  the_class->set_DSU_state(instanceKlass::jdus_will_be_swapped);

  return JDUS_ERROR_NONE;

}


jdusError VM_JDUSOperation::load_new_class_for_redefining(DSUClass *dsu_class, TRAPS){
  HandleMark    hm(THREAD);
  ResourceMark  rm(THREAD);

  const jdusClassUpdatingType updating_type = dsu_class->updating_type();

  assert(updating_type >= JDUS_CLASS_SMETHOD && updating_type <= JDUS_CLASS_BOTH,"redefining enable class must greater than BC.");

  instanceKlassHandle the_class (THREAD, dsu_class->klass());
  Handle the_class_loader(THREAD, the_class->class_loader());
  Handle pd (THREAD, the_class->protection_domain());
  symbolHandle the_class_sym (THREAD,the_class->name());

  instanceKlassHandle scratch_class;

  jdusError ret = load_new_class(dsu_class,scratch_class, the_class_sym,the_class_loader,pd,THREAD);

  if(HAS_PENDING_EXCEPTION){
    JDUS_WARN(("Load new class [%s] for redefining meets error.", dsu_class->name()->as_C_string()));
    return JDUS_ERROR_INTERNAL;
  }

  if(ret != JDUS_ERROR_NONE){
    JDUS_WARN(("Load new class [%s] for redefining meets error %d.", dsu_class->name()->as_C_string(),ret));
    return ret;
  }
  if (!the_class->is_linked()) {
    the_class->link_class(THREAD);
    if (HAS_PENDING_EXCEPTION) {
      symbolOop ex_name = PENDING_EXCEPTION->klass()->klass_part()->name();

      CLEAR_PENDING_EXCEPTION;
      if (ex_name == vmSymbols::java_lang_OutOfMemoryError()) {
        return JDUS_ERROR_OUT_OF_MEMORY;
      } else {
        JDUS_TRACE_MESG(("Link old class error for %s.", dsu_class->name()->as_C_string()));
        return JDUS_ERROR_LINK_OLD_CLASS;
      }
    }
  }

  // Modification to the class, if we fail to update, we should clear this information.
  // XXX These modifications only have effect when the class is dead.
  //     If we do not set the class as dead, it will have no side effect.
  /////////////////////////////////////////////////////////////////
  set_restricted_methods(dsu_class);
  /////////////////////////////////////////////////////////////////

  // Modification to the scratch class, so 
  // We only need set this flag when performing lazy updating.
  if (!(dsu_class->dsu_class_loader()->dsu()->is_eager_update() || dsu_class->dsu_class_loader()->dsu()->is_eager_update_pointer())) {
    set_invalid_methods(the_class,scratch_class,dsu_class,CHECK_(JDUS_ERROR_INVALID_MEMBER));
  }



  // We just allocate constantPoolCache here and do not initialize vtable and itable.
  Rewriter::rewrite(scratch_class, THREAD);
  if (HAS_PENDING_EXCEPTION) {
    symbolOop ex_name = PENDING_EXCEPTION->klass()->klass_part()->name();
    CLEAR_PENDING_EXCEPTION;
    if (ex_name == vmSymbols::java_lang_OutOfMemoryError()) {
      return JDUS_ERROR_OUT_OF_MEMORY;
    } else {
      return JDUS_ERROR_REWRITER;
    }
  }

  {
    ResourceMark rm(THREAD);
    // no exception should happen here since we explicitly
    // do not check loader constraints.
    // compare_and_normalize_class_versions has already checked:
    //  - classloaders unchanged, signatures unchanged
    //  - all instanceKlasses for redefined classes reused & contents updated
    scratch_class->vtable()->initialize_vtable(false, THREAD);
    scratch_class->itable()->initialize_itable(false, THREAD);
    assert(!HAS_PENDING_EXCEPTION || (THREAD->pending_exception()->is_a(SystemDictionary::ThreadDeath_klass())), "redefine exception");
#ifdef ASSERT
    scratch_class->vtable()->verify(tty);
#endif

  }

  // compute matching fields
  // This has impact on whether do create temp version.
  compute_and_set_fields(the_class,scratch_class,dsu_class,CHECK_(JDUS_ERROR_COMPUTE_SET_FIELDS));
  set_transformer_method(the_class,scratch_class,dsu_class,CHECK_(JDUS_ERROR_SET_TRANSFORMER));

  // TODO, we have add this class into dictionary during 
  JDUS::add_jdus_klass(scratch_class,THREAD);

  // create jdus version list
  if (do_create_mix_version(the_class,scratch_class,updating_type)) {
    // clone method for the_class
    create_mix_version(the_class, scratch_class, dsu_class, CHECK_(JDUS_ERROR_MIX_VERSION));
  }else {
    dsu_class->set_mix_klass((jobject)NULL);
  }

  // create temp version for running transfromer
  if(do_create_temp_version(the_class,scratch_class,updating_type)){
    create_temp_version(the_class, scratch_class, dsu_class, CHECK_(JDUS_ERROR_TEMP_VERSION));
  }else {
    dsu_class->set_temp_klass((jobject)NULL);
  }

  the_class->set_DSU_state(instanceKlass::jdus_will_be_redefined);

  return JDUS_ERROR_NONE;
}

jdusError VM_JDUSOperation::prepare_dsu(DSU* dsu, TRAPS) {
  
  jdusError ret = dsu->prepare(THREAD);

  if (ret != JDUS_ERROR_NONE){
    return ret;
  }

  if (HAS_PENDING_EXCEPTION){
  
    tty->print_cr("Prepare DSU Error!!");

    return JDUS_ERROR_TO_BE_ADDED;
  }

  return JDUS_ERROR_NONE;
}

jdusError VM_JDUSOperation::load_new_class_versions(DSU* dsu, TRAPS) {

  DSUClassLoader *dsu_loader = dsu->first_class_loader();
  for (; dsu_loader != NULL; dsu_loader = dsu_loader->next() ) {
    DSUClass* dsu_class = dsu_loader->first_class();
    for (;dsu_class!=NULL; dsu_class = dsu_class->next()){      
      const jdusClassUpdatingType updating_type = dsu_class->updating_type();

      jdusError ret = JDUS_ERROR_NONE;

      switch(updating_type){
      case JDUS_CLASS_DEL: {
        klassOop klass = dsu_class->klass();
        if(klass!= NULL){
          instanceKlass::cast(klass)->set_DSU_state(instanceKlass::jdus_will_be_deleted);
        }
        break;}
      case JDUS_CLASS_MC: {
        klassOop klass = dsu_class->klass();
        if(klass!= NULL){
          instanceKlass::cast(klass)->set_DSU_state(instanceKlass::jdus_will_be_recompiled);
        }
        break;}
      case JDUS_CLASS_NONE:
        break;
      case JDUS_CLASS_STUB:{
        ret = load_new_stub_class(dsu_class,THREAD);
        break;}
      case JDUS_CLASS_BC:{
        ret = load_new_class_for_swapping(dsu_class, THREAD);
        break;}
      case JDUS_CLASS_SMETHOD:
      case JDUS_CLASS_SFIELD:
      case JDUS_CLASS_SBOTH:
      case JDUS_CLASS_METHOD:
      case JDUS_CLASS_FIELD:
      case JDUS_CLASS_BOTH:{
        ret = load_new_class_for_redefining(dsu_class,THREAD);
        break;
                           }
      default:
        JDUS_TRACE_MESG(("Unkown change type is %d." , updating_type));
        ShouldNotReachHere();
      }

      if(ret != JDUS_ERROR_NONE){
        return ret;
      }
    }
  }
  return JDUS_ERROR_NONE;
}

objArrayHandle VM_JDUSOperation::clone_methods(instanceKlassHandle scratch_class, TRAPS) {
  HandleMark    hm(THREAD);
  ResourceMark  rm(THREAD);

  objArrayHandle methods = objArrayHandle(THREAD, scratch_class->methods());
  objArrayHandle nullHandle;
  objArrayOop clone_methods = oopFactory::new_system_objArray(methods->length(), CHECK_(nullHandle));
  objArrayHandle c_methods = objArrayHandle(THREAD, clone_methods);
  for(int i=0; i<methods->length(); i++){
    methodHandle mh = methodHandle((methodOop)methods->obj_at(i));
    methodHandle cmh (oopFactory::clone_method(mh(), THREAD));
    cmh->make_mixobjects_update_method(cmh,THREAD);
    mh->set_fake_method(cmh());
    tty->print_cr("clone methods");
    mh->print_name(tty);
    tty->print_cr("\n");
  }

  return c_methods;
}

void VM_JDUSOperation::create_temp_version(instanceKlassHandle the_class,
  instanceKlassHandle scratch_class, DSUClass *dsu_class,TRAPS){
    jdusClassUpdatingType updating_type = dsu_class->updating_type();
    instanceKlassHandle temp_klass;
    //FIXME assume super class has no mix-new fields here.
    if( !do_create_temp_version(the_class,scratch_class,updating_type)){
      return ;
    }

    HandleMark    hm(THREAD);
    ResourceMark  rm(THREAD);

    // 1). create temp class

    instanceKlassHandle source;

    if (dsu_class->mix_klass()!=NULL){
      source = instanceKlassHandle(THREAD, dsu_class->mix_klass());
    }else {
      source = scratch_class;
    }

    temp_klass = instanceKlass::clone_instanceKlass(source,THREAD);

    //
    temp_klass->set_copy_to_size(scratch_class->size_helper());

    dsu_class->set_temp_klass(JNIHandles::make_global(temp_klass));

}


void VM_JDUSOperation::create_mix_version(instanceKlassHandle the_class,
  instanceKlassHandle scratch_class, DSUClass *dsu_class,TRAPS){
    jdusClassUpdatingType updating_type = dsu_class->updating_type();
    instanceKlassHandle mix_klass;
    //FIXME assume super class has no mix-new fields here.
    if( !do_create_mix_version(the_class,scratch_class,updating_type)){
      return ;
    }


    // 1). create mix class

    HandleMark    hm(THREAD);
    ResourceMark  rm(THREAD);

    mix_klass = instanceKlass::clone_instanceKlass(scratch_class,THREAD);



    const int old_object_size = the_class->size_helper() << LogBytesPerWord ;

    //MixOldClass has the old object size
    mix_klass->set_layout_helper(the_class->layout_helper());

    OopMapBlock* oop_map = scratch_class->start_of_nonstatic_oop_maps();
    OopMapBlock* mix_map = mix_klass->start_of_nonstatic_oop_maps();
    const int oop_map_count = scratch_class->nonstatic_oop_map_count();




    if (UseCompressedOops) {
      assert(false,"NotImplemented Yet");
      //    while (map < end_map) {
      //
      //      ++map;
      //    }
    } else {
      for(int i=0; i< oop_map_count; i++){
        int off_start = oop_map[i].offset();
        int off_count =  oop_map[i].count();
        int c1_count = old_object_size - off_start;
        if(c1_count < off_count){
          int c1_offset;
          if(c1_count > 0){
            c1_offset = off_start;
          }else {
            c1_count = 0;
            c1_offset = off_start; //0;
          }
          mix_map[i].set_offset(c1_offset);
          mix_map[i].set_count(c1_count);
        }
      }
    }

    assert(mix_klass->is_linked(), "mix class is linked ..");

    mix_klass->set_init_state(instanceKlass::fully_initialized);
    mix_klass->set_copy_to_size(scratch_class->size_helper());

    dsu_class->set_mix_klass(JNIHandles::make_global(mix_klass));

}


bool VM_JDUSOperation::doit_prologue() {
  if (_dsu == NULL) {
    _res = JDUS_ERROR_NULL_POINTER;
    return false;
  }

  if(_dsu->first_class_loader() == NULL){
    JDUS_TRACE_MESG(("No class loader, empty DSU, no class to be updated!"));
    return false;
  }

  DSUClassLoader *dsu_loader = _dsu->first_class_loader();
  int to_be_updated = 0;
  for (; dsu_loader != NULL; dsu_loader = dsu_loader->next() ) {
    DSUClass* dsu_class = dsu_loader->first_class();    
    for(;dsu_class != NULL; dsu_class=dsu_class->next()){
      to_be_updated++;      
      if(dsu_class == NULL){
        JDUS_TRACE_MESG(("Null DSUClass!"));
        return false;
      }
    }
  }

  if(to_be_updated == 0){
    JDUS_TRACE_MESG(("Empty DSU, no class to be updated!"));
    return false;
  }


  if(is_discarded()){
    _res = JDUS_ERROR_BAD_REQUEST_TYPE;
  }



  // This is the first time invoking the DSU,
  // so do some init work...
  if(is_init()){
    Thread* thread = Thread::current();

    // first of all, set the active DSU;
    JDUS::set_active_dsu(_dsu);
    _dsu->set_from_rn(JDUS::system_revision_number());
    _dsu->set_to_rn(JDUS::system_revision_number()+1);

    //_res = load_new_class_versions(dsu(), thread);

    _res = prepare_dsu(_dsu, thread);

    if (_res != JDUS_ERROR_NONE) {
      // Free os::malloc allocated memory in load_new_class_version.
      free_memory();
      {
        // notify the JDUS_Thread that we are fail to execute
        MutexLocker locker(JDUSRequest_lock);
        JDUSRequest_lock->notify_all();
      }
      JDUS_TRACE_MESG(("Discard DSU request, Error Code %d",_res));
      set_request_state(JDUS_REQUEST_DISCARDED);
      return false;
    } 

    collect_classes_to_relink(thread);
    // Here we install the DSU 
    // Without synchronization    
    return true;
  }

  // This is another try of the same DSU.
  // This is mainly because you choose to update program only at safe point.
  if(is_interrupted()){
    JDUS_TRACE_MESG(("[JDUS] Continue a interrupted DSU..."));
  }
  return true;
}


void VM_JDUSOperation::free_memory(){
  // free DSU or not

}

void VM_JDUSOperation::doit_epilogue() {
  if(is_discarded()){
    // TODO do clear work...

  }else if(is_finished()){

  }else{
    //Notify request here or not..

  }
}

void VM_JDUSOperation::flush_dependent_code(TRAPS) {
  CodeCache::mark_all_nmethods_for_deoptimization();

  ResourceMark rm(THREAD);
  DeoptimizationMarker dm;

  // Deoptimize all activations depending on marked nmethods
  Deoptimization::deoptimize_dependents();

  // Make the dependent methods not entrant (in VM_Deoptimize they are made zombies)
  CodeCache::make_marked_nmethods_not_entrant();
}

void VM_JDUSOperation::append_classes_to_relink(instanceKlassHandle ikh){
  DSUClass dsu_class;

  dsu_class.initialize();

  dsu_class.set_updating_type(JDUS_CLASS_BC);
  dsu_class.set_klass_raw((klassOop*)ikh.raw_value());
  //dsu_class.set_name(JNIHandles::make_global(ikh->name()));

  // does it required?
  //ikh->name()->print();tty->cr();
  ikh->set_DSU_state(instanceKlass::jdus_will_be_recompiled);

  _classes_to_relink->append(dsu_class);

  // For safe deallocate heap object
  dsu_class.set_klass_raw(NULL);
}

void VM_JDUSOperation::free_classes_to_relink(){
  if(_classes_to_relink!=NULL){
    /*const int length = _classes_to_relink->length();
    for(int i=0;i<length;i++){
    DSUClass* c = _classes_to_relink->at(i);
    delete c;
    }*/
    delete _classes_to_relink;
    _classes_to_relink = NULL;
  }
}

// scan the class constant pool if we find a swapped or redefined class.
// then, mark this class as recompile
void VM_JDUSOperation::check_and_append_relink_class(klassOop k_oop,
  oop initiating_loader, TRAPS) {
    Klass * k = k_oop->klass_part();
    if(k->oop_is_instance()){
      //HandleMark hm(THREAD);
      instanceKlassHandle ikh (k_oop);

      // only fix user defined class
      if(ikh->class_loader() == NULL){
        return;
      }

      //if (ik->is_mix_new()){
      //	return;
      //}

      if (ikh->will_be_updated()) {
        // we will handle it, skip
        return;
      }

      bool should_relink = false;

      {

        HandleMark hm(THREAD); // used to clear aikh below

        constantPoolHandle cp(ikh->constants());

        int index = 1;
        for (index = 1; index < cp->length(); index++){
          // Index 0 is unused

          jbyte tag = cp->tag_at(index).value();
          switch (tag) {
          case JVM_CONSTANT_Class : {
            klassOop entry = cp->klass_at(index,THREAD);//*(constants->obj_at_addr(index));
            if (entry->is_klass() && entry->klass_part()->oop_is_instance()) {
              // We only consider resolved class, 
              // For class unresolved here and resolved before updating, we will implement a incremental approach.
              // TODO Do we can only unresolved dead class here?						
              instanceKlassHandle aikh (entry);
              if(aikh->will_be_swapped() || aikh->will_be_redefined()){
                should_relink = true;							
              }
            }
            break;
                                    }
          case JVM_CONSTANT_Long :
          case JVM_CONSTANT_Double :
            index++;
            break;
          default:
            break;
          } // end of switch

          if(should_relink){						
            break;
          }
        }

      }

      if (should_relink) {
        VM_JDUSOperation::append_classes_to_relink(ikh);
        return;
      }
    }

}

void VM_JDUSOperation::collect_classes_to_relink(TRAPS){
  //HandleMark hm(THREAD);

  {
    MutexLocker sd_mutex(SystemDictionary_lock);
    _classes_to_relink = new (ResourceObj::C_HEAP) GrowableArray<DSUClass>(1000,true);
    SystemDictionary::classes_do(check_and_append_relink_class,CHECK);	
  }

  const int length = _classes_to_relink->length();

  for(int i=0; i<length; i++){
    DSUClass* c = _classes_to_relink->adr_at(i);
    KlassHandle kh (*(c->klass_raw()));
    assert(kh()->is_klass(),"kh must be a klass");
    c->set_klass(JNIHandles::make_global(kh));
    c->set_name(JNIHandles::make_global(kh->name()));
  }

  if(JDUS_TRACE_ENABLED(0x00000004)){
    //const int length = _classes_to_relink->length();
    JDUS_TRACE_MESG(("Find %d relink classes", length));
    for(int i=0; i<length; i++){
      DSUClass* c = _classes_to_relink->adr_at(i);
      JDUS_TRACE_MESG(("%d\t %s", i, c->name()->as_C_string()));
    }
  }
}

void VM_JDUSOperation::relink_collected_classes(TRAPS){
  const int length = _classes_to_relink->length();
  //tty->print_cr("length of relink collected classes is %d.",length);
  for(int i=0; i<length; i++){
    DSUClass* c = _classes_to_relink->adr_at(i);
    relink_single_class(c,CHECK);
  }
}

void VM_JDUSOperation::doit(){
  elapsedTimer dsu_timer;

  // start timer
  JDUS_TIMER_START(dsu_timer);

  Thread *thread = Thread::current();

  ///////////////////////////////////////////////////////
  // 1). Check whether we are reaching a DSU safe point.
  ///////////////////////////////////////////////////////

  {
    bool sys_safe = JDUS::check_application_threads(policy());
    if (sys_safe){
      JDUS_WARN(("At safe point, the update will be performed."));
    }else {

      //If not safe, we perform return barrier installation.
      JDUS::install_return_barrier_all_threads();

      if (policy_system_safe_point() || policy_passive_system_safe_point()){
        JDUS_WARN(("Not at DSU safepoint, the DSU is interrupted.."));
        set_request_state(JDUS_REQUEST_INTERRUPTED);
        return ;
      }else {
        JDUS_WARN(("Not system-wide safe but update still continues, policy is %d", policy()));
      }
    }
  }

  // /////////////////////////////////////////////////////////////////////
  // 2). perfrom the dynamic update once we are reaching a DSU safe point
  // /////////////////////////////////////////////////////////////////////

  // 2.1). unlink compiled code
  flush_dependent_code(thread);

  // Use dsu->classes_do() instead of this iterating
  //DSUClassLoader *dsu_loader = _dsu->first();
  //for (; dsu_loader != NULL; dsu_loader = dsu_loader->next() ) {
  //  int class_count = dsu_loader->class_count();
  //  for(int j=0; j<class_count; j++){
  //    DSUClass *dsu_class = dsu_loader->get_class(j);

  //    
  //  }
  //}

  // 2.2). update each class contained in this DSU
  _dsu->classes_do(update_single_class,thread);

  relink_collected_classes(thread);

  if (thread->has_pending_exception()){
    thread->clear_pending_exception();
    JDUS_WARN(("Update class meets exception."));
    return ;
  }

  // 2.3). update bytecode unchanged methods
  {
    //TraceTime t("Repair application threads");
    JDUS::repair_application_threads(policy());
  }



  // 2.4). update objects
  if ( dsu()->is_eager_update() || dsu()->is_eager_update_pointer() ){
    // !!!! this growableArray is allocated on C heap;
    ResourceMark rm;


    // Record DSU Request pausing time without collect objects.
    JDUS_TIMER_STOP(dsu_timer);
    JDUS_TRACE(0x00000001,("DSU request update code time: %3.7f (s).",dsu_timer.seconds()));
    JDUS_TIMER_START(dsu_timer);

    GrowableArray<jobject> * results = new GrowableArray<jobject>(1000);
    JDUSEagerUpdate::collect_dead_instances_at_safepoint(JDUS::system_revision_number(),results, thread);



    if(thread->has_pending_exception()){
      JDUS_WARN(("Collect dead objects eagerly fails!", results->length()));
      thread->clear_pending_exception();			
    } else {

      JDUS_DEBUG(("JDUS::system_revision_number() is %d", JDUS::system_revision_number()));

      JDUSEagerUpdate::configure_eager_update(results,dsu()->is_eager_update_pointer());
      JDUSEagerUpdate::install_eager_update_return_barrier(thread);
      if(thread->has_pending_exception()){
        JDUS_WARN(("install eager update barrier error!"));
      }
    }

    JDUS_TIMER_STOP(dsu_timer);
    JDUS_TRACE(0x00000001,("DSU collects %d objects time: %3.7f (s).",results->length(),dsu_timer.seconds()));
    JDUS_TIMER_START(dsu_timer);

  }else {
    // For lazy update, we 
  }

  // 2.5). Now we are finish all updating work, do some log
  // Increment the system revision now.
  //JDUS::increment_system_rn();
  JDUS::finish_active_dsu();

  // Disable any dependent concurrent compilations
  SystemDictionary::notice_modification();

  //The request is finished
  set_request_state(JDUS_REQUEST_FINISHED);


  // Record DSU Request pausing time without collect objects.
  JDUS_TIMER_STOP(dsu_timer);
  JDUS_TRACE(0x00000001,("DSU request pause time: %3.7f (s).",dsu_timer.seconds()));
  if (JDUS_TRACE_ENABLED(0x00000001)){
      time_t tloc;
      (void*)time(&tloc);
      tty->print("[JDUS] DSU request finished at %s",ctime(&tloc));
  }
}

// happens before VM safe point
void VM_JDUSOperation::set_transformer_method(instanceKlassHandle the_class,
        instanceKlassHandle scratch_class, DSUClass *dsu_class, TRAPS) {
    HandleMark hm(THREAD);
    ResourceMark rm(THREAD);


    methodOop object_transformer = dsu_class->object_transformer_method();
    methodOop class_transformer  = dsu_class->class_transformer_method();

    if (object_transformer == NULL
      && class_transformer == NULL){
        // no transoformer method
        return ;
    }
    //XXX set up args for transformers
    DSUClassLoader * dsu_class_loader = dsu_class->dsu_class_loader();
    assert(dsu_class_loader != NULL, "DSUClassLoader must not be NULL!");

    klassOop transformer_oop = dsu_class_loader->transformer_class();

    instanceKlassHandle transformer (THREAD, transformer_oop);

    if(object_transformer != NULL){
      //tty->print_cr("[JDUS] Set object transformer for class %s.", the_class->name()->as_C_string());
      methodHandle mh (THREAD, object_transformer);
      scratch_class->set_object_transformer(mh());
      typeArrayHandle annotations (THREAD, transformer->get_method_parameter_annotations_of(mh->method_idnum()));
      typeArrayHandle result;
      JDUS::parse_old_field_annotation(the_class,transformer,annotations,result,THREAD);
      scratch_class->set_object_transformer_args(result());
    }

    if(class_transformer != NULL){
      //tty->print_cr("[JDUS] Set class transformer for class %s.", the_class->name()->as_C_string());
      methodHandle mh (THREAD, class_transformer);
      scratch_class->set_class_transformer(mh());
      typeArrayHandle annotations (THREAD, transformer->get_method_parameter_annotations_of(mh->method_idnum()));
      typeArrayHandle result;
      JDUS::parse_old_field_annotation(the_class,transformer,annotations,result,THREAD);
      scratch_class->set_class_transformer_args(result());
    }


}


void VM_JDUSOperation::compute_ycsc(
  instanceKlassHandle the_class, 
  instanceKlassHandle scratch_class, 
  instanceKlassHandle &ycsc_old, 
  instanceKlassHandle &ycsc_new, 
  TRAPS){
    ycsc_old = instanceKlassHandle (THREAD,the_class->super());

    while(ycsc_old.not_null() ){
      ycsc_new = (THREAD,scratch_class->super());
      while(ycsc_new.not_null()){
        if(ycsc_new->name() == ycsc_old->name()){
          break;
        }
        ycsc_new = instanceKlassHandle(THREAD,ycsc_new->super());  
      }

      if(ycsc_new.not_null()){
        break;
      }else {
        //tty->print_cr("[JDUS] Super next is null, class hierarchy changed %s.", scratch_class->name()->as_C_string());
      }
      ycsc_old = instanceKlassHandle(THREAD,ycsc_old->super());
    }

    // every class has an common super class, which is java.lang.Object.
    // We donot update class of java.lang.Object...
    // We use *equivalent* not *equal* to decide common relation.
    // A euqal B means A == B
    // A equivalent B means A.name == B.name
    // Note: equal implies equivalent
    assert(ycsc_old.not_null() && ycsc_new.not_null(),"super next could be java.lang.Object");

    // Now old_super_h must be equialent with super_next;
    assert(ycsc_new->name() == ycsc_old->name(),"invariant");
}

void VM_JDUSOperation::compute_and_cache_common_info(
  instanceKlassHandle the_class,
  instanceKlassHandle scratch_class,
  DSUClass *dsu_class,
  TRAPS){
  HandleMark hm(THREAD);
  // compute and set min_vtable_size
  int min_vtable_length = the_class->vtable_length();
  int min_object_size   = the_class->size_helper();
  instanceKlassHandle previous_version (THREAD, the_class->previous_version());
  while(previous_version.not_null()){
    int vl = previous_version->vtable_length();
    int os = previous_version->size_helper();
    previous_version = instanceKlassHandle(THREAD,previous_version->previous_version());
    if( vl < min_vtable_length){
      min_vtable_length = vl;
    }
    if( os < min_object_size){
      min_object_size = os;
    }
  }
  dsu_class->set_min_vtable_size(min_vtable_length);
  dsu_class->set_min_object_size_in_bytes(min_object_size << LogBytesPerWord);
}

// 1). caculate MatchField
// 2). caculate MixNewField
// 3). caculate MixOldField for Merge
void VM_JDUSOperation::compute_and_set_fields(instanceKlassHandle the_class,
  instanceKlassHandle scratch_class, DSUClass *dsu_class, TRAPS) {
    HandleMark hm(THREAD);
    ResourceMark rm(THREAD);

    jdusClassUpdatingType changeType = dsu_class->updating_type();
    if (!do_redefine_scratch_klass(the_class,scratch_class,changeType)){
      assert(false,"sanity check fail");
      return ;
    }

    // compute
    compute_and_cache_common_info(the_class,scratch_class,dsu_class,THREAD);

    bool do_print = false;/*the_class->name()->starts_with("org/apache/catalina/core/ContainerBase")*/;

    typeArrayHandle old_fields = typeArrayHandle(THREAD,the_class->fields());
    constantPoolHandle old_constants = constantPoolHandle(THREAD, the_class->constants());

    typeArrayHandle new_fields = typeArrayHandle(THREAD,scratch_class->fields());
    constantPoolHandle new_constants = constantPoolHandle(THREAD, scratch_class->constants());

    // We walk through the chain of evolution to find the minimal object size.
    const int min_object_size = dsu_class->min_object_size_in_bytes();

    const int old_object_size = the_class->size_helper() << LogBytesPerWord ;
    const int new_object_size = scratch_class->size_helper() << LogBytesPerWord;

    const int old_fields_length = old_fields->length();
    const int new_fields_length = new_fields->length();

    int match_instance_index = 0;
    const int match_size = (new_fields_length > old_fields_length? new_fields_length : old_fields_length)
      /instanceKlass::next_offset * instanceKlass::next_match_field ;
    int match_static_index = match_size;


    int* match_fields = NEW_RESOURCE_ARRAY(int, match_size);
    char * field_space = NEW_RESOURCE_ARRAY(char, new_object_size);
    if(field_space == NULL){
      return ;
    }
    enum {
      padding     =  0,
      mixoldfield =  1,
      mixnewfield =  2,
    };

    // !!!!
    memset(field_space,padding,new_object_size);

    bool eager_update = dsu_class->dsu_class_loader()->dsu()->is_eager_update();
    bool eager_update_pointers = dsu_class->dsu_class_loader()->dsu()->is_eager_update_pointer();


    for (int i = 0; i < new_fields_length; i += instanceKlass::next_offset ) {
      jushort n_flag = new_fields->ushort_at(i + instanceKlass::access_flags_offset);

      int n_name_index = new_fields->ushort_at(i + instanceKlass::name_index_offset);
      int n_sig_index  = new_fields->ushort_at(i + instanceKlass::signature_index_offset);
      symbolOop n_name = new_constants->symbol_at(n_name_index);
      symbolOop n_sig  = new_constants->symbol_at(n_sig_index);

      int n_offset     = scratch_class->offset_from_fields(i);
      BasicType n_field_type = FieldType::basic_type(n_sig);
      int n_field_size = type2aelembytes(n_field_type); 
      int field_end    = n_offset + n_field_size;

      bool is_mix_new_field = false;

      if((n_flag & JVM_ACC_STATIC) == 0  ){
        // this is a mix new member
        //AccessFlags flags = fd.access_flags();
        //flags.set_is_mix_new_member();
        //store new access_flags

        // We compare end of field with minimal object size to decide possible MixNewField.
        if (field_end > min_object_size /*old_object_size*/){
          // MixNewField
          is_mix_new_field = true;
          if(do_print){
            tty->print_cr("[JDUS] Set invalid and mix new Member, %s.", n_name->as_C_string());
          }
          memset(field_space+n_offset, (char)mixnewfield, n_field_size);
          if(eager_update){
            // no invalid checks but with mixnewfield checks
            n_flag = (jushort)(n_flag | JVM_ACC_IS_MIX_NEW_MEMBER);
          }else if(eager_update_pointers){
            // n_flag is n flag
          }else {
            n_flag = (jushort)(n_flag | JVM_ACC_IS_MIX_NEW_MEMBER | JVM_ACC_IS_INVALID_MEMBER);
          }

          new_fields->ushort_at_put( i + instanceKlass::access_flags_offset,
            n_flag);

        }else {
          // MixOldField

          if(do_print){
            tty->print_cr("[JDUS] Set invalid and mix old Member, %s.", n_name->as_C_string());
          }

          memset(field_space+n_offset, (char)mixoldfield, n_field_size);

          if(!(eager_update||eager_update_pointers)) {
            n_flag = (jushort)(n_flag | JVM_ACC_IS_INVALID_MEMBER);
          }else {
            // eager update, no invalid check
          }

          new_fields->ushort_at_put(i + instanceKlass::access_flags_offset,
            n_flag);
        }

      }
      {
        for ( int j=0; j<old_fields_length; j+= instanceKlass::next_offset ){
          int o_name_index = old_fields->ushort_at(j+instanceKlass::name_index_offset);
          int o_sig_index  = old_fields->ushort_at(j+instanceKlass::signature_index_offset);
          short o_flag     = old_fields->ushort_at(j+instanceKlass::access_flags_offset);
          symbolOop o_name = old_constants->symbol_at(o_name_index);
          symbolOop o_sig  = old_constants->symbol_at(o_sig_index);
          if (o_name == n_name && o_sig == n_sig ){
            int o_offset = the_class->offset_from_fields(j);
            if (((n_flag & JVM_ACC_STATIC) == 0 ) && ((o_flag & JVM_ACC_STATIC) == 0)){
              // a pair of match instance fields
              // flags may change,for example private => public
              // we use negative offset to indicate the field is a MixNewField.
              if((o_flag & JVM_ACC_IS_MIX_NEW_MEMBER) == 0){
                match_fields[match_instance_index + instanceKlass::old_match_offset] = o_offset;
              } else {
                match_fields[match_instance_index + instanceKlass::old_match_offset] = -o_offset;
              }

              // we use negative offset to indicate the field is a MixNewField.
              if((n_flag & JVM_ACC_IS_MIX_NEW_MEMBER) == 0){
                match_fields[match_instance_index + instanceKlass::new_match_offset] = n_offset;
              }else {
                match_fields[match_instance_index + instanceKlass::new_match_offset] = -n_offset;
              }

              match_fields[match_instance_index + instanceKlass::match_field_type] = n_field_type;
              match_instance_index += instanceKlass::next_match_field;
              break;
            }else if(((n_flag & JVM_ACC_STATIC) != 0 ) && ((o_flag & JVM_ACC_STATIC) != 0)){
              // a pair of match static fields
              match_static_index -= instanceKlass::next_match_field;
              match_fields[match_static_index + instanceKlass::old_match_offset] = o_offset;
              match_fields[match_static_index + instanceKlass::new_match_offset] = n_offset;
              match_fields[match_static_index + instanceKlass::match_field_type] = n_field_type;
              break;
            }else {
              tty->print_cr("[JDUS] Matched fields flags changed. [%s %s %s].", scratch_class->name()->as_C_string(),o_name->as_C_string(),o_sig->as_C_string());
            }
          }else {

          }
        }// end for
      }// end local
    }

    // find the youngest common super class (ycsc)
    instanceKlassHandle ycsc_old;
    instanceKlassHandle ycsc_new;

    compute_ycsc(the_class,scratch_class,ycsc_old,ycsc_new,THREAD);

    if(ycsc_old() == ycsc_new()){
      jobject ycsc = JNIHandles::make_global(ycsc_old);
      dsu_class->set_ycsc_old(ycsc);
      dsu_class->set_ycsc_new(ycsc);
    }else {
      dsu_class->set_ycsc_old(JNIHandles::make_global(ycsc_old));
      dsu_class->set_ycsc_new(JNIHandles::make_global(ycsc_new));
    }

    // Start of setting matched instance fields
    // Direct super of new class

    instanceKlassHandle super_old (THREAD,the_class->super());
    typeArrayHandle super_match_field_h(THREAD,ycsc_new->match_fields());

    int match_instance_count = 0;
    int super_match_instance_count = 0;
    if(super_match_field_h.not_null()){
      super_match_instance_count = super_match_field_h->length();
      match_instance_count += super_match_instance_count;
    }
    match_instance_count += match_instance_index;
    if (match_instance_count > 0){
      typeArrayOop oop_match_field = oopFactory::new_permanent_intArray(match_instance_count+instanceKlass::next_match_field,THREAD);
      assert(oop_match_field != NULL, "sanity check");
      if(super_match_instance_count > 0){
        memcpy(oop_match_field->int_at_addr(0),super_match_field_h->int_at_addr(0),super_match_instance_count*sizeof(int));
      }
      if(match_instance_index > 0){
        memcpy(oop_match_field->int_at_addr(super_match_instance_count),match_fields,match_instance_index*sizeof(int));
      }
      //TODO we use new_match_offset == 0 to indicate this is an clean tuple.
      // We clean memory from old_match_offset + match_field_type
      int ycsc_old_object_size = ycsc_old->size_helper()<< LogHeapWordSize;
      oop_match_field->int_at_put(match_instance_count + instanceKlass::old_match_offset,ycsc_old_object_size);
      oop_match_field->int_at_put(match_instance_count + instanceKlass::new_match_offset,0);
      oop_match_field->int_at_put(match_instance_count + instanceKlass::match_field_type,old_object_size - ycsc_old_object_size);
      scratch_class->set_match_fields(oop_match_field);
    }
    // End of setting matched instance fields

    // Start of setting matched static fields
    int match_static_count = match_size-match_static_index;
    if (match_static_count > 0){
      int *match_static_fields = NEW_C_HEAP_ARRAY(int,match_static_count);
      memcpy(match_static_fields,match_fields+match_static_index,match_static_count*sizeof(int));
      dsu_class->set_match_static_count(match_static_count);
      dsu_class->set_match_static_fields(match_static_fields);
    }
    // End of setting matched static fields


    const int mix_old_size = new_fields_length /instanceKlass::next_offset * instanceKlass::next_mix_old_field;
    int* mix_old_fields = NEW_RESOURCE_ARRAY(int, mix_old_size);

    bool in_mix_old = false;
    int mix_index = 0;
    int mix_offset = 0;
    for (int i=0;i<new_object_size;i++){
      if(field_space[i] == mixoldfield){
        if (in_mix_old == false){
          in_mix_old = true;
          mix_offset = i;
        }
      }else {
        if(in_mix_old == true){
          in_mix_old = false;
          mix_old_fields[mix_index + instanceKlass::mix_old_field_offset] = mix_offset;
          mix_old_fields[mix_index + instanceKlass::mix_old_field_length] = i - mix_offset;
          mix_index += instanceKlass::next_mix_old_field;
        }
      }
    }

    instanceKlassHandle super (THREAD,scratch_class->super());
    const int new_super_object_size = super->size_helper() << LogBytesPerWord;
    int super_delta = new_super_object_size > instanceOopDesc::header_size();
    typeArrayOop super_mixoldfield = super->mix_old_fields();
    typeArrayHandle super_mixoldfield_h (THREAD, super_mixoldfield);
    int mix_old_count = 0;
    int super_mix_old_count = 0;
    if (super_mixoldfield_h.not_null()){
      super_mix_old_count = super_mixoldfield_h->length();
      mix_old_count += super_mix_old_count;
    }else if(super_delta>0){
      super_mix_old_count = instanceKlass::next_mix_old_field;
      mix_old_count += super_mix_old_count;
    }
    mix_old_count += mix_index;
    if(mix_old_count > 0){
      typeArrayOop oop_mixoldfield = oopFactory::new_permanent_intArray(mix_old_count,THREAD);
      if(super_mix_old_count > 0){
        if(super_mixoldfield_h.not_null()){
          memcpy(oop_mixoldfield->int_at_addr(0),super_mixoldfield_h->int_at_addr(0),super_mix_old_count*sizeof(int));
        }else {
          //
          oop_mixoldfield->int_at_put(0 + instanceKlass::mix_old_field_offset, instanceOopDesc::header_size());
          oop_mixoldfield->int_at_put(0 + instanceKlass::mix_old_field_length, super_delta);
        }
      }
      if(mix_index > 0){
        memcpy(oop_mixoldfield->int_at_addr(super_mix_old_count),mix_old_fields,mix_index*sizeof(int));
      }
      scratch_class->set_mix_old_fields(oop_mixoldfield);
    }
}

void VM_JDUSOperation::compute_added_deleted_matching_methods() {
  methodOop old_method;
  methodOop new_method;

  _matching_old_methods = NEW_RESOURCE_ARRAY(methodOop, _old_methods->length());
  _matching_new_methods = NEW_RESOURCE_ARRAY(methodOop, _old_methods->length());
  _added_methods        = NEW_RESOURCE_ARRAY(methodOop, _new_methods->length());
  _deleted_methods      = NEW_RESOURCE_ARRAY(methodOop, _old_methods->length());

  _matching_methods_length = 0;
  _deleted_methods_length  = 0;
  _added_methods_length    = 0;

  int nj = 0;
  int oj = 0;
  while (true) {
    if (oj >= _old_methods->length()) {
      if (nj >= _new_methods->length()) {
        break; // we've looked at everything, done
      }
      // New method at the end
      new_method = (methodOop) _new_methods->obj_at(nj);
      _added_methods[_added_methods_length++] = new_method;
      ++nj;
    } else if (nj >= _new_methods->length()) {
      // Old method, at the end, is deleted
      old_method = (methodOop) _old_methods->obj_at(oj);
      _deleted_methods[_deleted_methods_length++] = old_method;
      ++oj;
    } else {
      old_method = (methodOop) _old_methods->obj_at(oj);
      new_method = (methodOop) _new_methods->obj_at(nj);
      if (old_method->name() == new_method->name()) {
        if (old_method->signature() == new_method->signature()) {
          _matching_old_methods[_matching_methods_length  ] = old_method;
          _matching_new_methods[_matching_methods_length++] = new_method;
          ++nj;
          ++oj;
        } else {
          // added overloaded have already been moved to the end,
          // so this is a deleted overloaded method
          _deleted_methods[_deleted_methods_length++] = old_method;
          ++oj;
        }
      } else { // names don't match
        if (old_method->name()->fast_compare(new_method->name()) > 0) {
          // new method
          _added_methods[_added_methods_length++] = new_method;
          ++nj;
        } else {
          // deleted method
          _deleted_methods[_deleted_methods_length++] = old_method;
          ++oj;
        }
      }
    }
  }
  assert(_matching_methods_length + _deleted_methods_length == _old_methods->length(), "sanity");
  assert(_matching_methods_length + _added_methods_length == _new_methods->length(), "sanity");
}

void VM_JDUSOperation::update_single_class(DSUClass *dsu_class, TRAPS){
  //if(dsu_class->is_parsing_error()){
  //  JDUS_TRACE_MESG(("A DSU class [%s] is in error state.",dsu_class->name()->as_C_string()));
  //  return ;
  //}

  //if(!dsu_class->is_fully_parsed()){
  //  // A un resolved class;
  //  // Do a check
  //  //dsu_class->resolve(thread);
  //  if(dsu_class->is_fully_parsed()){
  //    JDUS_TRACE_MESG(("An unresolved DSU class [%s] is resovled at safe point.",dsu_class->name()->as_C_string()));
  //    //return ;
  //  }else{
  //    JDUS_TRACE_MESG(("Skip an unresolved DSU class [%s].",dsu_class->name()->as_C_string()));
  //    return;
  //  }
  //}


  jdusClassUpdatingType updating_type = dsu_class->updating_type();

  //tty->print("[JDUS] Start update class [");
  //dsu_class->name()->print();
  //tty->print("], change type is %d.", (int)updating_type);

  switch (updating_type) {
  case JDUS_CLASS_NONE:
    break;
  case JDUS_CLASS_STUB: {
    // a stub class
    klassOop klass = dsu_class->klass();
    if (klass != NULL) {
      instanceKlass::cast(klass)->set_DSU_state(instanceKlass::jdus_has_been_added);
    }
    break;
                        }
  case JDUS_CLASS_MC: {
    relink_single_class(dsu_class, CHECK);
    break;
                      }
  case JDUS_CLASS_BC:{
    swap_single_class(dsu_class, CHECK);
    break;}
  case JDUS_CLASS_SFIELD:
  case JDUS_CLASS_SMETHOD:
  case JDUS_CLASS_SBOTH:
  case JDUS_CLASS_FIELD:
  case JDUS_CLASS_METHOD:
  case JDUS_CLASS_BOTH: {
    redefine_single_class(dsu_class,CHECK);
    break;
                        }
  case JDUS_CLASS_DEL: {
    // set restricted method
    remove_single_class(dsu_class,CHECK);
    break;
                       }
  default:
    ShouldNotReachHere();
  }
}

void VM_JDUSOperation::remove_single_class(DSUClass *dsu_class,TRAPS){
  //TODO do nothing for CV(Concurrent Version)
  //Here, we only mark methods as invalid members.
  //since only dynamic dispatching can trigger deleted members.
  klassOop k = dsu_class->klass();
  if (k == NULL) {
    return ;
  }
  if(!instanceKlass::cast(k)->is_linked()){
    return;
  }

  

  instanceKlass *ik = instanceKlass::cast(k);
  ik->set_DSU_state(instanceKlass::jdus_has_been_deleted);
  
  // 1). we first make all methods defined in delted class un executable.
  mark_old_and_obsolete_methods(dsu_class,CHECK);

  // 2). make all entries in the virtual table inherited from super class to be 
  klassVtable * vtable = ik->vtable();
  const int super_vtable_length = instanceKlass::cast(ik->super())->vtable_length();
  for (int i=0; i<super_vtable_length; i++) {
    methodOop method = vtable->method_at(i);
    instanceKlass *method_ik = instanceKlass::cast(method->method_holder());
    if (method_ik->has_been_jdus_redefined()) {
      instanceKlass * new_ik = instanceKlass::cast(method_ik->next_version());
      // TODO, here should be optimized, allocate a new vtable is unnecessary.
      (*vtable->adr_method_at(i)) = new_ik->vtable()->method_at(i);
    }else if (method_ik->has_been_deleted()){
      // Here, the deleted super classes must already be set with a valid method.
      (*vtable->adr_method_at(i)) = method_ik->vtable()->method_at(i);
    }
  }

  // TODO, we only treat remove deleted classes as redefine  the deleted class to its alive super classes.

}

void VM_JDUSOperation::swap_single_class(DSUClass *dsu_class, TRAPS){
  HandleMark hm(THREAD);
  ResourceMark rm(THREAD);

  jdusClassUpdatingType updating_type  = dsu_class->updating_type();

  //int method_count = dsu_class->methods_count();
  //DSUMethod** methods = dsu_class->methods();
  //  oop the_class_mirror = JNIHandles::resolve_non_null(dsu_class->klass());
  //  klassOop the_class_oop = java_lang_Class::as_klassOop(the_class_mirror);
  instanceKlassHandle the_class = instanceKlassHandle(THREAD, dsu_class->klass());

  JDUS_TRACE(0x000000400,
    ("Swap single class %s, change type is %d",the_class->name()->as_C_string(),updating_type)
    );

  instanceKlassHandle scratch_class (THREAD, dsu_class->scratch_klass());

  assert(scratch_class.not_null(), "sanity check");

  _old_methods = the_class->methods();
  _new_methods = scratch_class->methods();
  _the_class_oop = the_class();
  _scratch_class_oop = scratch_class();

  compute_added_deleted_matching_methods();

  update_jmethod_ids();
  mark_old_and_obsolete_methods(dsu_class,THREAD);
  // Swap methods
  the_class->set_methods(_new_methods);
  scratch_class->set_methods(_old_methods);

  //Swap fields
  typeArrayOop old_fields = the_class->fields();
  the_class->set_fields(scratch_class->fields());
  scratch_class->set_fields(old_fields);

  constantPoolOop old_constants = the_class->constants();
  the_class->set_constants(scratch_class->constants());
  scratch_class->constants()->set_pool_holder(the_class());
  scratch_class->set_constants(old_constants);
  old_constants->set_pool_holder(scratch_class());

  typeArrayOop old_inner_classes = the_class->inner_classes();
  the_class->set_inner_classes(scratch_class->inner_classes());
  scratch_class->set_inner_classes(old_inner_classes);


  {
    ResourceMark rm(THREAD);
    // no exception should happen here since we explicitly
    // do not check loader constraints.
    // compare_and_normalize_class_versions has already checked:
    //  - classloaders unchanged, signatures unchanged
    //  - all instanceKlasses for redefined classes reused & contents updated

    the_class->vtable()->initialize_vtable(false, THREAD);
    the_class->itable()->initialize_itable(false, THREAD);

    assert(!HAS_PENDING_EXCEPTION || (THREAD->pending_exception()->is_a(SystemDictionary::ThreadDeath_klass())), "redefine exception");
  }

  //We will rename the old class;
  //So link it here.
  //{
  //  ResourceMark rm(THREAD);
  //  // no exception should happen here since we explicitly
  //  // do not check loader constraints.
  //  // compare_and_normalize_class_versions has already checked:
  //  //  - classloaders unchanged, signatures unchanged
  //  //  - all instanceKlasses for redefined classes reused & contents updated
  //  scratch_class->vtable()->initialize_vtable(false, THREAD);
  //  scratch_class->itable()->initialize_itable(false, THREAD);
  //  assert(!HAS_PENDING_EXCEPTION || (THREAD->pending_exception()->is_a(SystemDictionary::ThreadDeath_klass())), "redefine exception");
  //}

  the_class->set_source_debug_extension(
    scratch_class->source_debug_extension());

  if (scratch_class->access_flags().has_localvariable_table() !=
    the_class->access_flags().has_localvariable_table()) {

      AccessFlags flags = the_class->access_flags();
      if (scratch_class->access_flags().has_localvariable_table()) {
        flags.set_has_localvariable_table();
      } else {
        flags.clear_has_localvariable_table();
      }
      the_class->set_access_flags(flags);
  }

  // Replace class annotation fields values
  typeArrayOop old_class_annotations = the_class->class_annotations();
  the_class->set_class_annotations(scratch_class->class_annotations());
  scratch_class->set_class_annotations(old_class_annotations);

  // Replace fields annotation fields values
  objArrayOop old_fields_annotations = the_class->fields_annotations();
  the_class->set_fields_annotations(scratch_class->fields_annotations());
  scratch_class->set_fields_annotations(old_fields_annotations);

  // Replace methods annotation fields values
  objArrayOop old_methods_annotations = the_class->methods_annotations();
  the_class->set_methods_annotations(scratch_class->methods_annotations());
  scratch_class->set_methods_annotations(old_methods_annotations);

  // Replace methods parameter annotation fields values
  objArrayOop old_methods_parameter_annotations =
    the_class->methods_parameter_annotations();
  the_class->set_methods_parameter_annotations(
    scratch_class->methods_parameter_annotations());
  scratch_class->set_methods_parameter_annotations(old_methods_parameter_annotations);

  // Replace methods default annotation fields values
  objArrayOop old_methods_default_annotations =
    the_class->methods_default_annotations();
  the_class->set_methods_default_annotations(
    scratch_class->methods_default_annotations());
  scratch_class->set_methods_default_annotations(old_methods_default_annotations);

  // Replace minor version number of class file
  u2 old_minor_version = the_class->minor_version();
  the_class->set_minor_version(scratch_class->minor_version());
  scratch_class->set_minor_version(old_minor_version);

  // Replace major version number of class file
  u2 old_major_version = the_class->major_version();
  the_class->set_major_version(scratch_class->major_version());
  scratch_class->set_major_version(old_major_version);

  // Replace CP indexes for class and name+type of enclosing method
  u2 old_class_idx  = the_class->enclosing_method_class_index();
  u2 old_method_idx = the_class->enclosing_method_method_index();
  the_class->set_enclosing_method_indices(
    scratch_class->enclosing_method_class_index(),
    scratch_class->enclosing_method_method_index());
  scratch_class->set_enclosing_method_indices(old_class_idx, old_method_idx);

  // SystemDictionary::classes_do(adjust_cpool_cache_and_vtable, THREAD);

  // Replace lifetime information.
  int old_born_rn = the_class->born_rn();
  the_class->set_born_rn(scratch_class->born_rn());
  scratch_class->set_born_rn(old_born_rn);

  //TODO  Should we add scratch class in the revision chain??
  //the_class->set_previous_version(scratch_class());
  //XXX Rember this is used for repair application threads
  scratch_class->set_next_version(the_class());

  // XXX Make old class dead here.
  scratch_class->set_dead_rn(dsu_class->to_rn());
  scratch_class->set_is_mix_invalid();
  scratch_class->set_copy_to_size(scratch_class->size_helper());
  scratch_class->set_DSU_state(instanceKlass::jdus_has_been_swapped);
  // the class now is new and clear the DSU state of it.
  the_class->set_DSU_state(instanceKlass::jdus_none_DSU_state);

  if (the_class->oop_map_cache() != NULL) {
    // Flush references to any obsolete methods from the oop map cache
    // so that obsolete methods are not pinned.
    the_class->oop_map_cache()->flush_obsolete_entries();
  }

  update_subklass_vtable_and_itable(the_class,THREAD);

#ifdef ASSERT
  the_class->vtable()->verify(tty);
#endif
}

void VM_JDUSOperation::update_subklass_vtable_and_itable(instanceKlassHandle the_class,TRAPS){
  ResourceMark rm(THREAD);
  instanceKlassHandle subklass (THREAD, the_class->subklass_oop());
  while(subklass.not_null()){
    if(!( subklass->will_be_redefined() || subklass->will_be_swapped() )){
      //tty->print_cr("[JDUS] Update subclass %s vtable and itable..",subklass->name()->as_C_string());
      {
        ResourceMark rm(THREAD);
        // no exception should happen here since we explicitly
        // do not check loader constraints.
        // compare_and_normalize_class_versions has already checked:
        //  - classloaders unchanged, signatures unchanged
        //  - all instanceKlasses for redefined classes reused & contents updated
        //tty->print_cr("JDUS reinit sub class vtable for class %s", subklass->name()->as_C_string());

        subklass->vtable()->initialize_vtable(false, THREAD);
        subklass->itable()->initialize_itable(false, THREAD);
        assert(!HAS_PENDING_EXCEPTION || (THREAD->pending_exception()->is_a(SystemDictionary::ThreadDeath_klass())), "redefine exception");

#ifdef ASSERT
        //subklass->vtable()->verify(tty);
#endif

      }
      update_subklass_vtable_and_itable(subklass,THREAD);
    }
    subklass = instanceKlassHandle(THREAD,subklass->next_sibling_oop());
  }
}

// Redefine the class
// 1). remove old class and preserve some data to the new class
// 2). link new class
//
//
void VM_JDUSOperation::redefine_single_class(DSUClass *dsu_class,TRAPS) {
  HandleMark hm(THREAD);
  ResourceMark rm(THREAD);

  jdusClassUpdatingType updating_type  = dsu_class->updating_type();

  instanceKlassHandle the_class = instanceKlassHandle(THREAD, dsu_class->klass());
  instanceKlassHandle scratch_class (THREAD, dsu_class->scratch_klass());


  // 0x00000200 see jdus.hpp
  JDUS_TRACE(0x00000200,
    ("[JDUS] Redefine single class %s, change type is %s. [0x%08x]",
    the_class->name()->as_C_string(),
    JDUS::class_updating_type_name(updating_type),
    scratch_class())
    );



  instanceKlassHandle mix_class;
  if (dsu_class->mix_klass() != NULL) {
    mix_class = instanceKlassHandle (THREAD, dsu_class->mix_klass());
  }

  _old_methods = the_class->methods();
  _new_methods = scratch_class->methods();
  _the_class_oop = the_class();
  _scratch_class_oop = scratch_class();
  _mix_class_oop = mix_class();

  compute_added_deleted_matching_methods();

  update_jmethod_ids();

  mark_old_and_obsolete_methods(dsu_class,THREAD);


  install_scratch_class(the_class, scratch_class, updating_type , THREAD);

  //==========================================================================
  // Because we have create vtable and itable at load_new_class_for_redefing.
  // And if there exists an class that is swapped with another one, the old 
  // content has been updated at that time.
  //==========================================================================
  instanceKlassHandle new_super(scratch_class->super());
  assert(!new_super->is_mix_invalid(), "should not be invalid.");
  {
    ResourceMark rm(THREAD);
    // no exception should happen here since we explicitly
    // do not check loader constraints.
    // compare_and_normalize_class_versions has already checked:
    //  - classloaders unchanged, signatures unchanged
    //  - all instanceKlasses for redefined classes reused & contents updated

    klassVtable * new_vtable = scratch_class->vtable();
    const int new_vtable_length = new_vtable->length();
    bool invalid = false;
    for(int i=0; i<new_vtable_length; i++){
      methodOop method = new_vtable->method_at(i);
      if(method->method_holder()->klass_part()->is_mix_invalid()){
        invalid = true;
        break;
      }
    }

    if(invalid){
      scratch_class->vtable()->initialize_vtable(false, THREAD);
      scratch_class->itable()->initialize_itable(false, THREAD);
      assert(!HAS_PENDING_EXCEPTION || (THREAD->pending_exception()->is_a(SystemDictionary::ThreadDeath_klass())), "redefine exception");
    }

#ifdef ASSERT
    for(int i=0; i<new_vtable_length; i++){
      methodOop method = new_vtable->method_at(i);
      assert(!method->method_holder()->klass_part()->is_mix_invalid(),"sanity check");
    }
#endif 
  }

  {
    //TraceTime t("Run default class transformer");
    // 1). run class transformer first
    run_class_transformer(dsu_class, the_class, scratch_class, THREAD);
  }

  {
    //TraceTime t("Post fix class");
    //Set Invalid Flags in the class
    // Set Invalid flags
    post_fix_the_class(the_class, scratch_class, dsu_class, updating_type, THREAD);
    // Set Invalid and MixNew flags
    post_fix_scratch_class(the_class, scratch_class, dsu_class, updating_type, THREAD);
    post_fix_ycsc(the_class, scratch_class, dsu_class, updating_type, THREAD);
    post_fix_mix_version(the_class, scratch_class, dsu_class, updating_type, THREAD);
    post_fix_temp_version(the_class, scratch_class, dsu_class, updating_type, THREAD);
  }
}

// Revert this class to its initial state
// 1). re-pactch bytecodes, e.g., revert VM bytecode to its Spec Bytecodes.
// 2). release constantPoolCache
void VM_JDUSOperation::relink_single_class(DSUClass *dsu_class, TRAPS){
  HandleMark hm(THREAD);
  ResourceMark rm(THREAD);

  instanceKlassHandle the_class(THREAD,dsu_class->klass());

  if(!the_class->is_linked()){
    JDUS_DEBUG(("Relink an unlinked class [%s].",the_class->name()->as_C_string()));
    return ;
  }

  JDUS_TRACE(0x00000800,("Relink a linked class [%s].",the_class->name()->as_C_string()));

  constantPoolOop constants = the_class->constants();
  int index = 1;
  for (index = 1; index < constants->length(); index++){
    // Index 0 is unused
    jbyte tag = constants->tag_at(index).value();
    switch (tag) {
    case JVM_CONSTANT_Class : {
      klassOop entry = constants->klass_at(index,THREAD);//*(constants->obj_at_addr(index));
      if (entry->is_klass()) {
        if (entry->klass_part()->oop_is_instance()){
          // clear resolved old class
          // TODO Do we can only unresolved dead class here?
          //instanceKlass* ik = instanceKlass::cast(entry);
          //if(ik->will_be_updated()){
          constants->unresolved_klass_at_put(index, Klass::cast(entry)->name());
          //}
        }
      }
      break;
                              }
    case JVM_CONSTANT_Long :
    case JVM_CONSTANT_Double :
      index++;
      break;
    default:
      break;
    } // end of switch
  }

  // We should repatch all VM bytecodes to Java bytecode

  objArrayOop methods = the_class->methods();
  int length = methods->length();

  bool print_replace = false;//the_class->name()->starts_with("org/apache/catalina/core/ContainerBase");

  //DSUMethod ** methods = dsu_class->methods();
  //int length = dsu_class->methods_count();
  for(int i=0; i<length; i++){
    methodOop method = (methodOop) methods->obj_at(i);

    JDUS::repatch_method(method,print_replace,THREAD);

    //method->set_method_data(NULL);
    methodDataOop md = method->method_data();
    if(md != NULL){
      for (ProfileData* data = md->first_data();
        md->is_valid(data);
        data = md->next_data(data)) {
          if(data->is_ReceiverTypeData()){
            ReceiverTypeData * rtd = (ReceiverTypeData*)data;
            for (uint row = 0; row < rtd->row_limit(); row++) {
              klassOop recv = rtd->receiver_unchecked(row);
              if (recv != NULL ) {
                if(recv->klass_part()->is_mix_invalid()){
                  klassOop next = instanceKlass::cast(recv)->next_version();
                  // next may be null and next version
                  // TODO if we allow concurrent update, here next should the latest one?
                  assert(next != NULL,"next version cannot be null");
                  rtd->set_receiver(row,next);
                }
              }
            }
          }
      }
    }
  }

  constantPoolCacheOop cache =  constants->cache();
  assert(cache != NULL, "cache must not be null.");
  cache->reset();

  the_class->set_DSU_state(instanceKlass::jdus_has_been_recompiled);
}

bool VM_JDUSOperation::do_redefine_scratch_klass(instanceKlassHandle the_class,
  instanceKlassHandle scratch_class,
  jdusClassUpdatingType updating_type) {
  if (updating_type >= JDUS_CLASS_SMETHOD && updating_type <= JDUS_CLASS_BOTH){
    return true;
  }
  return false;
    //  int old_object_size = the_class->object_size();
    //  int new_object_size = scratch_class->object_size();
    //  return old_object_size != new_object_size;
}

bool VM_JDUSOperation::do_create_mix_version(instanceKlassHandle the_class,
  instanceKlassHandle scratch_class,jdusClassUpdatingType updating_type) {
  if (updating_type >= JDUS_CLASS_FIELD && updating_type <= JDUS_CLASS_BOTH){
    int min_old_instance_size = the_class->size_helper();
    int new_instance_size = scratch_class->size_helper();

    instanceKlassHandle previous_version (the_class->previous_version());
    while(previous_version.not_null()){
      int os = previous_version->size_helper();
      previous_version = instanceKlassHandle(previous_version->previous_version());
      if(os < min_old_instance_size){
        min_old_instance_size = os;
      }
    }

    //return old_instance_size != new_instance_size;
    return min_old_instance_size < new_instance_size;
  }
  return false;
}

bool VM_JDUSOperation::do_create_temp_version(instanceKlassHandle the_class,
  instanceKlassHandle scratch_class,jdusClassUpdatingType updating_type) {
    return scratch_class->match_fields()!=NULL || scratch_class->object_transformer()!= NULL;
}


bool VM_JDUSOperation::do_compute_match_fields(instanceKlassHandle the_class,
  instanceKlassHandle scratch_class,jdusClassUpdatingType updating_type) {
    if (updating_type == JDUS_CLASS_FIELD 
      || updating_type == JDUS_CLASS_BOTH 
      || updating_type == JDUS_CLASS_SFIELD 
      || updating_type == JDUS_CLASS_SBOTH ){
        return true;
    }
    return false;
}

bool VM_JDUSOperation::do_run_class_transformer(instanceKlassHandle the_class,
  instanceKlassHandle scratch_class, jdusClassUpdatingType updating_type) {
    if (updating_type == JDUS_CLASS_FIELD 
      || updating_type == JDUS_CLASS_BOTH 
      || updating_type == JDUS_CLASS_SFIELD 
      || updating_type == JDUS_CLASS_SBOTH ){
        return true;
    }
    return false;
}

void VM_JDUSOperation::install_scratch_class(instanceKlassHandle the_class,
  instanceKlassHandle scratch_class,jdusClassUpdatingType updating_type, TRAPS) {
    //if( do_redefine_scratch_klass(the_class, scratch_class,updating_type)){
    SystemDictionary::redefine_instance_class(the_class, scratch_class, THREAD);
    the_class->set_next_version(scratch_class());
    scratch_class->set_previous_version(the_class());
    //}
}


//
//
//
//
//
//
void VM_JDUSOperation::invalid_old_instance_class(instanceKlassHandle the_class,
  instanceKlassHandle scratch_class,
  instanceKlassHandle mix_version,
  jdusClassUpdatingType updating_type,
  TRAPS){
    HandleMark   hm(THREAD);
    ResourceMark rm(THREAD);

    assert(false, "deprecated");

    bool do_redefine = do_redefine_scratch_klass(the_class, scratch_class,updating_type);
    bool do_create_mix_class = do_create_mix_version(the_class, scratch_class,updating_type);

    assert(do_redefine,"here must be do redefine");

    //tty->print_cr("[JDUS] Invalid old instance klass, do_redefine:%d,create_mix_class:%d",do_redefine, do_create_mix_class);

    int old_klass_size = the_class->object_size();
    int new_klass_size = scratch_class->object_size();

    int vtable_offset = instanceKlass::vtable_start_offset();

    int old_itable_offset = the_class->itable_offset_in_words();
    int old_vtable_length = the_class->vtable_length();
    int old_itable_length = the_class->itable_length();

    int new_itable_offset = scratch_class->itable_offset_in_words();
    int new_vtable_length = scratch_class->vtable_length();
    int new_itable_length = scratch_class->itable_length();

    // invariant
    int invalid_itable_length = itableOffsetEntry::size();
    // ths space is aligned
    int invalid_vtable_length = align_object_offset(old_vtable_length)
      + align_object_offset(old_itable_length)
      - align_object_offset(invalid_itable_length);
    // so the invalid_vtable_length is aligned


    if(do_create_mix_class) {
      tty->print_cr("[JDUS] Create MixClass [%s]", mix_version->name()->as_C_string());
      // fields definition has changed
      // need create c1 and c2 and check mix new field
      objArrayOop methods = scratch_class->methods();
      int length = methods->length();
      for(int i=0; i<length; i++){
        methodOop method = (methodOop)methods->obj_at(i);
        //invokestatic
        if(method->is_static()){
          continue;
        }
        //invokespecial
        if(method->is_private()){
          continue;
        }
        int vtable_offset_end = vtable_offset + (method->vtable_index() + 1) * vtableEntry::size();
        if(vtable_offset_end > invalid_vtable_length){
          method->set_is_mix_new();
        }
      }


      if(mix_version.not_null()){
        //copy vtable from scratch to c1 and c2
        if(new_vtable_length > 0){
          // memcpy copy contents in bytes but the length is in sizeof Heapword
          memcpy(mix_version->start_of_vtable(),scratch_class->start_of_vtable(), new_vtable_length * HeapWordSize);
        }


        //copy itable from scratch to c1 and c2
        //need or not?
        if(new_itable_length > 0){
          // always > 0...
          memcpy(mix_version->start_of_itable(),scratch_class->start_of_itable(), new_itable_length * HeapWordSize);
        }

      }
    }


    if(do_redefine){

      //invalid the_class
      //redefine scratch_class
      the_class->set_is_mix_invalid();
      scratch_class->set_is_mix_new();

      the_class->set_next_version(scratch_class());
      scratch_class->set_previous_version(the_class());
      //FIXME ! Hack for Dynamic Type Checking
      the_class->initialize_supers(scratch_class(),CHECK);

      if(do_create_mix_class){
        the_class->set_mix_version(mix_version());
        scratch_class->set_mix_version(mix_version());
        mix_version->set_is_mix_old();
        mix_version->set_previous_version(the_class());
        mix_version->set_next_version(scratch_class());
        // FIXME!!!
        // hack for Dynamic Type Checking
        mix_version->initialize_supers(scratch_class(),CHECK);
      }else {
        the_class->set_mix_version(NULL);
        scratch_class->set_mix_version(NULL);
      }


      the_class->set_vtable_length(invalid_vtable_length);
      the_class->set_itable_length(invalid_itable_length);
      // initialize the invalid itable
      // This makes all interface method lookup result in IncompatibleClassFormatError
      itableOffsetEntry* offset_entry = (itableOffsetEntry*)the_class->start_of_itable();
      offset_entry->initialize(NULL, 0);

      // get vtable with new vtable_length
      vtableEntry* vtable = the_class->vtable()->table();
      klassVtable* new_vtable = scratch_class->vtable();

      //tty->print_cr("invvl %d newvl %d", invalid_vtable_length, new_vtable_length);
      int available_vtable_length = invalid_vtable_length > new_vtable_length
        ?new_vtable_length:invalid_vtable_length;
      int i=0;
      for(i=0; i<available_vtable_length; i++){
        methodOop new_m = new_vtable->method_at(i);
        methodOop new_copy_m = new_m->fake_method();
        if(new_copy_m != NULL){
          new_copy_m->set_access_flags(new_m->access_flags());
          methodHandle mh (new_copy_m);
          mh->set_vtable_index(new_m->vtable_index());
          vtable[i].raw_set(mh());
        }else {
          vtable[i].raw_set(new_m);
        }
      }
      for (;i<invalid_vtable_length; i++){
        // XXX FIXME!!!
        // set More Method NULL?
        vtable[i].raw_set(NULL);
      }
    }

    constantPoolOop old_constants = the_class->constants();




    the_class->set_fields(scratch_class->fields());
    the_class->set_constants(scratch_class->constants());




    objArrayOop old_methods = the_class->methods();
    the_class->set_methods(_new_methods);

    // Copy class annotation fields values
    typeArrayOop old_class_annotations = the_class->class_annotations();
    the_class->set_class_annotations(scratch_class->class_annotations());


    // Copy fields annotation fields values
    objArrayOop old_fields_annotations = the_class->fields_annotations();
    the_class->set_fields_annotations(scratch_class->fields_annotations());

    // Copy methods annotation fields values
    objArrayOop old_methods_annotations = the_class->methods_annotations();
    the_class->set_methods_annotations(scratch_class->methods_annotations());

    // Copy methods parameter annotation fields values
    objArrayOop old_methods_parameter_annotations =
      the_class->methods_parameter_annotations();
    the_class->set_methods_parameter_annotations(
      scratch_class->methods_parameter_annotations());

    // Copy methods default annotation fields values
    objArrayOop old_methods_default_annotations =
      the_class->methods_default_annotations();
    the_class->set_methods_default_annotations(
      scratch_class->methods_default_annotations());

    // Copy minor version number of class file
    the_class->set_minor_version(scratch_class->minor_version());

    // Copy major version number of class file
    the_class->set_major_version(scratch_class->major_version());

    // Copy CP indexes for class and name+type of enclosing method
    the_class->set_enclosing_method_indices(
      scratch_class->enclosing_method_class_index(),   
      scratch_class->enclosing_method_method_index());

}

void VM_JDUSOperation::post_fix_the_class(instanceKlassHandle the_class,
  instanceKlassHandle scratch_class,
  DSUClass *dsu_class,
  jdusClassUpdatingType updating_type,
  TRAPS){

    if (the_class->oop_map_cache() != NULL) {
      // Flush references to any obsolete methods from the oop map cache
      // so that obsolete methods are not pinned.
      the_class->oop_map_cache()->flush_obsolete_entries();
    }

    klassOop array_klasses = the_class->array_klasses();
    if(array_klasses != NULL){
      ResourceMark rm(THREAD);

      //tty->print_cr("[JDUS] objArray klass %s", array_klasses->klass_part()->oop_is_objArray()?"true":"false");
      objArrayKlass * oak = objArrayKlass::cast(array_klasses);
      oak->set_element_klass(scratch_class());
      oak->set_bottom_klass(scratch_class());

      klassOop high_dimension = oak->higher_dimension();
      while(high_dimension != NULL){
        oak = objArrayKlass::cast(high_dimension);
        oak->set_bottom_klass(scratch_class());
        high_dimension = oak->higher_dimension();
      }

      if(scratch_class->array_klasses()== NULL){
        //tty->print_cr("[JDUS] Array class of scratch class [%s] is null.", scratch_class->name()->as_C_string());
        scratch_class->set_array_klasses(array_klasses);
      }else {
        //tty->print_cr("[JDUS] Array class of scratch class [%s] is not null.", scratch_class->name()->as_C_string());
      }
    }

    if(updating_type != JDUS_CLASS_FIELD && updating_type != JDUS_CLASS_BOTH){
      // TODO see comments on _transform_level in instanceKlass.hpp
      the_class->set_transform_level(1); 
    }
    // the class died during an update to to_rn
    // XXX Mark the class as dead here.
    the_class->set_is_mix_invalid();
    the_class->set_force_update(true);

    the_class->set_dead_rn(dsu_class->to_rn());
    the_class->set_DSU_state(instanceKlass::jdus_has_been_redefined);

    // Continuous Update Support
    // previous class may have MixOldClass
    if(the_class->mix_version() != NULL){
      instanceKlassHandle old_mix_version (THREAD,the_class->mix_version());
      old_mix_version->set_is_mix_invalid();
      old_mix_version->set_dead_rn(dsu_class->to_rn());
    }
}


// Post fix action for Youngest Common Super Classes
void VM_JDUSOperation::post_fix_ycsc(instanceKlassHandle the_class,
  instanceKlassHandle scratch_class,
  DSUClass *dsu_class,
  jdusClassUpdatingType updating_type,
  TRAPS){


    HandleMark hm(THREAD);
    instanceKlassHandle ycsc_old(THREAD,dsu_class->ycsc_old());
    instanceKlassHandle ycsc_new(THREAD,dsu_class->ycsc_new());

    instanceKlassHandle super_old(THREAD,the_class->super());
    instanceKlassHandle super_new(THREAD,scratch_class->super());
    ResourceMark rm(THREAD);

    const int min_object_size_in_bytes = dsu_class->min_object_size_in_bytes();
    const int min_vtable_size = dsu_class->min_vtable_size();

    while(true){
      if(super_old == ycsc_old){
        break; 
      }
      tty->print_cr("old ycsc, %s",super_old->name()->as_C_string());
      make_invalid_class(super_old, min_object_size_in_bytes,min_vtable_size,THREAD);
      super_old = instanceKlassHandle(THREAD, super_old->super());
    }

    while(true){
      if(super_new == ycsc_new){
        break; 
      }
      tty->print_cr("new ycsc, %s",super_new->name()->as_C_string());
      make_invalid_class(super_new,min_object_size_in_bytes,min_vtable_size,THREAD);
      super_new = instanceKlassHandle(THREAD, super_new->super());
    }

    {
      ResourceMark rm(THREAD);

      klassVtable * old_vtable = the_class->vtable();
      klassVtable * new_vtable = scratch_class->vtable();
      int old_length = old_vtable->length();
      int new_length = new_vtable->length();
      int min_length = old_length < new_length? old_length : new_length;

      // in fact min_length should be the vtable length of the youngest common super class
      for(int i=0; i<min_length; i++){
        methodOop old_method = old_vtable->method_at(i);
        methodOop new_method = new_vtable->method_at(i);
        if(old_method!= NULL && old_method->is_method()){
          if(old_method->is_mix_invalid_method()){
            continue;
          }
          if(old_method != new_method){
            // old method is not mix new method and not equal with that in new vtable entry
            // then it is a valid method in ycsc that only overriden by the new class hierarchy.
            old_method->set_is_mix_invalid_method();

            methodHandle mh(THREAD,old_method);
            mh->link_mixobjects_method(mh,THREAD);
          }
        }
      }

    }
}

void VM_JDUSOperation::post_fix_scratch_class(instanceKlassHandle the_class,
  instanceKlassHandle scratch_class,
  DSUClass *dsu_class,
  jdusClassUpdatingType updating_type,
  TRAPS){

    //if(do_redefine_scratch_klass(the_class,scratch_class,updating_type)){
    //int old_vtable_length = the_class->vtable_length();
    int min_vtable_length = dsu_class->min_vtable_size();
    assert(min_vtable_length > 0, "must be preseted");

    // XXX This could be moved at loading time.
    // fields definition has changed
    // need create c1 and c2 and check mix new field
    objArrayOop methods = scratch_class->methods();
    int length = methods->length();
    for(int i=0; i<length; i++){
      methodOop method = (methodOop)methods->obj_at(i);
      //invokestatic
      if(method->is_static()){
        continue;
      }
      //invokespecial
      if(method->is_private()){
        continue;
      }
      if(method->vtable_index()>min_vtable_length/*old_vtable_length*/){
        method->set_is_mix_new();
      }
    }

    instanceKlass* superik = scratch_class->superklass();
    assert(superik != NULL, "should not be null");
    int dead_rn = superik->dead_rn();
    if(dead_rn == dsu_class->to_rn()){
      // a swapped super class
      scratch_class->set_super(superik->next_version());
      scratch_class->initialize_supers(superik->next_version(), THREAD);
    }

    //
    scratch_class->set_is_mix_new();
    // We should adjust the born rn of scratch class to increment by one.
    scratch_class->set_born_rn(dsu_class->to_rn());
}

void VM_JDUSOperation::post_fix_mix_version(instanceKlassHandle the_class,
  instanceKlassHandle scratch_class,
  DSUClass *dsu_class,
  jdusClassUpdatingType updating_type,
  TRAPS){

    if(dsu_class->mix_klass()!=NULL){
      HandleMark   hm(THREAD);
      ResourceMark rm(THREAD);


      assert(dsu_class->mix_klass() != NULL, "MixClass should not be NULL");

      instanceKlassHandle mix_version(THREAD,dsu_class->mix_klass());
      //link the MixOldClass with Old Class

      //tty->print_cr("[JDUS] Create Mix-Class [%s].", mix_version->name()->as_C_string());

      int new_vtable_length = scratch_class->vtable_length();
      int new_itable_length = scratch_class->itable_length();

      //copy vtable from scratch to mix
      if(new_vtable_length > 0){
        // memcpy copy contents in bytes but the length is in sizeof Heapword
        memcpy(mix_version->start_of_vtable(),scratch_class->start_of_vtable(), new_vtable_length * HeapWordSize);
      }


      //copy itable from scratch to mix
      //need or not?
      if(new_itable_length > 0){
        // always > 0...
        memcpy(mix_version->start_of_itable(),scratch_class->start_of_itable(), new_itable_length * HeapWordSize);
      }


      // XXX 
      // TODO Here we just make it easy to ..
      // Mix Version is belonged to old
      the_class->set_mix_version(dsu_class->mix_klass());

      mix_version->set_previous_version(the_class());
      mix_version->set_next_version(scratch_class());

      // XXX hack on type system of JVM
      //
      mix_version->set_super(NULL);
      mix_version->set_secondary_supers(NULL);
      mix_version->initialize_supers(scratch_class(),THREAD);

      if(HAS_PENDING_EXCEPTION){
        tty->print_cr("[JDUS] Initialize super error for mix class.");
      }

      mix_version->set_is_mix_old();
      mix_version->set_array_klasses(scratch_class->array_klasses());

      // Increment born rn here after update...
      mix_version->set_born_rn(dsu_class->to_rn());
      mix_version->set_prototype_header(markOopDesc::biased_locking_prototype());
    }

}


void VM_JDUSOperation::post_fix_temp_version(instanceKlassHandle the_class,
  instanceKlassHandle scratch_class,
  DSUClass *dsu_class,
  jdusClassUpdatingType updating_type,
  TRAPS){
    if(do_create_temp_version(the_class, scratch_class,updating_type)){
      HandleMark   hm(THREAD);
      ResourceMark rm(THREAD);

      assert(dsu_class->temp_klass() != NULL, "Temp Class should not be NULL");

      instanceKlassHandle temp_version(THREAD,dsu_class->temp_klass());
      //link the MixOldClass with Old Class
      //tty->print_cr("[JDUS] Create Temp Class [%s].", temp_version->name()->as_C_string());

      int new_vtable_length = scratch_class->vtable_length();
      int new_itable_length = scratch_class->itable_length();

      //copy vtable from scratch to mix
      if(new_vtable_length > 0){
        // memcpy copy contents in bytes but the length is in sizeof Heapword
        memcpy(temp_version->start_of_vtable(),scratch_class->start_of_vtable(), new_vtable_length * HeapWordSize);
      }


      //copy itable from scratch to mix
      //need or not?
      if(new_itable_length > 0){
        // always > 0...
        memcpy(temp_version->start_of_itable(),scratch_class->start_of_itable(), new_itable_length * HeapWordSize);
      }


      // XXX 
      // TODO Here we just make it easy to ..
      // Mix Version is belonged to old
      the_class->set_temp_version(dsu_class->temp_klass());


      temp_version->set_previous_version(the_class());
      // TODO temp version does not has next version
      //temp_version->set_next_version(scratch_class());
      temp_version->set_super(scratch_class());

      temp_version->initialize_supers(scratch_class(),THREAD);

      if(HAS_PENDING_EXCEPTION){
        tty->print_cr("[JDUS] Initialie super error for tempx class.");
      }

      // This will cause invalid object check
      temp_version->set_is_mix_invalid();
      temp_version->set_dead_rn(dsu_class->to_rn());

      //temp_version->set_init_state(instanceKlass::linked);
    }


}


void VM_JDUSOperation::rename_and_define_old_class(instanceKlassHandle the_class,
  instanceKlassHandle scratch_class,
  DSUClass *dsu_class,
  jdusClassUpdatingType updating_type,
  TRAPS){

}

// Run default transformer here
// 
void VM_JDUSOperation::run_class_transformer(DSUClass * dsu_class, instanceKlassHandle the_class,
  instanceKlassHandle scratch_class, TRAPS) {
    if (do_redefine_scratch_klass(the_class,scratch_class,dsu_class->updating_type())) {
      int count = dsu_class->match_static_count();
      int * match_fields = dsu_class->match_static_fields();

      // copy unchanged static fields from old instanceKlass to new instanceKlass
      for (int i=0; i<count; i+=instanceKlass::next_match_field){
        int old_offset = match_fields[i + instanceKlass::old_match_offset];
        int new_offset = match_fields[i + instanceKlass::new_match_offset];
        BasicType type = (BasicType)match_fields[i + instanceKlass::match_field_type];
        JDUS::copy_field(the_class,scratch_class,old_offset,new_offset,type);
      }

      methodOop class_transformer = dsu_class->class_transformer_method();
      if(class_transformer == NULL){
        // scratch_class->set_initialization_state_and_notify(instanceKlass::fully_initialized,THREAD);
        // XXX Can we set the scratch class can be eager initialized here??
      }
    }
}

void VM_JDUSOperation::set_restricted_methods(DSUClass *dsu_class) {  
  DSUMethod * dsu_method = dsu_class->first_method();
  for (;dsu_method != NULL; dsu_method = dsu_method->next()){
    methodOop method = dsu_method->method();
    if (dsu_method->updating_type() == JDUS_METHOD_BC){
      method->set_is_restricted_method();
    }else if (dsu_method->updating_type() == JDUS_METHOD_DEL){
      method->set_is_restricted_method();
    }
  }
}

void VM_JDUSOperation::update_jmethod_ids(){
  for (int j = 0; j < _matching_methods_length; ++j) {
    methodOop old_method = _matching_old_methods[j];
    jmethodID jmid = old_method->find_jmethod_id_or_null();
    if (jmid != NULL) {
      // There is a jmethodID, change it to point to the new method
      methodHandle new_method_h(_matching_new_methods[j]);
      JNIHandles::change_method_associated_with_jmethod_id(jmid, new_method_h);
      assert(JNIHandles::resolve_jmethod_id(jmid) == _matching_new_methods[j],
        "should be replaced");
    }
  }
}

void VM_JDUSOperation::mark_old_and_obsolete_methods(DSUClass *dsu_class, TRAPS) {
  ResourceMark rm(THREAD);

  
  jdusClassUpdatingType changeType = dsu_class->updating_type();

  DSUMethod* dsu_method = dsu_class->first_method();
  for (;dsu_method!=NULL;dsu_method=dsu_method->next()){
    methodHandle mh(THREAD,(methodOop)dsu_method->method());
    if(mh->is_static() || 
      mh->name() == vmSymbols::object_initializer_name() 
      || mh->name()== vmSymbols::class_initializer_name()){
        // should we set old here??
        mh->set_is_old();
        continue;
    }

    mh->set_is_old();
    mh->set_is_mix_invalid_method();
    // TODO, here we would implement a link_mixobjects_obsolote_method.
    mh->link_mixobjects_method(mh,THREAD);
  }
}

// This method is used for make class under YCSC which itself is not redefined in update.
// This method should be invoked at DSU safe point.
void VM_JDUSOperation::make_invalid_class(
  instanceKlassHandle klass,
  int min_object_size_in_bytes, 
  int min_vtable_size,
  TRAPS
  ){

    typeArrayOop fields = klass->fields();

    int fields_length = fields->length();
    for(int i=0; i<fields_length; i+=instanceKlass::next_offset){
      jushort flags = fields->ushort_at(i + instanceKlass::access_flags_offset);
      int offset    = klass->offset_from_fields(i);
      int sig_index = fields->ushort_at(i + instanceKlass::signature_index_offset);
      symbolOop signature = klass->constants()->symbol_at(sig_index);

      BasicType field_type = FieldType::basic_type(signature);
      int field_size = type2aelembytes(field_type); 
      int field_end    = offset + field_size;
      if(field_end > min_object_size_in_bytes){
        fields->ushort_at_put(i + instanceKlass::access_flags_offset,(jushort)(flags | JVM_ACC_IS_INVALID_MEMBER | JVM_ACC_IS_MIX_NEW_MEMBER));
      }else {
        fields->ushort_at_put(i + instanceKlass::access_flags_offset,(jushort)(flags | JVM_ACC_IS_INVALID_MEMBER));
      }
    }

    objArrayOop methods = klass->methods();
    int methods_length  = methods->length();
    for(int i=0; i<methods_length; i++){
      methodOop method = (methodOop)methods->obj_at(i);
      if(/*method->name() == vmSymbols::object_initializer_name()
         || */method->name() == vmSymbols::class_initializer_name()
         ){
           continue;
      }

      if(method->is_static()){
        continue;
      }


      if(method->vtable_index() > min_vtable_size){
        method->set_is_mix_new();
      }
      method->set_is_mix_invalid_method();

      methodHandle mh(THREAD, method);
      mh->link_mixobjects_method(mh,THREAD);
    }

    klass->set_is_mix_invalid();
}

void VM_JDUSOperation::set_invalid_methods(
  instanceKlassHandle the_class,
  instanceKlassHandle scratch_class,
  DSUClass* dsu_class,
  TRAPS){
  if(do_redefine_scratch_klass(the_class,scratch_class, dsu_class->updating_type())){
    HandleMark hm(THREAD);
    
    assert(scratch_class.not_null(), "scratch_class must not be null.");
    objArrayHandle methods (THREAD, scratch_class->methods());

    assert(methods.not_null(),"methods must not be null.");
    int length = methods->length();
    
    for(int i=0;i<length;i++){
        methodOop m = (methodOop)methods->obj_at(i);
        methodHandle mh (THREAD,m);
        assert(mh.not_null(),"Methdod must not be null.");
        if(/*mh->name() == vmSymbols::object_initializer_name()
           || */mh->name() == vmSymbols::class_initializer_name()
           ){
             //XXX object initializer is not check in default
             //force explicit
             continue;
        }

        if(mh->is_static()){
          continue;
        }

        mh->set_is_mix_invalid_method();
        assert(mh->interpreter_entry() == NULL,"Method must not be linked here.");
        //mh->link_mixobjects_method(mh,THREAD);
      }
    }

}

VM_RelinkMixObjects::VM_RelinkMixObjects(Handle * obj,Handle * new_mix_new_obj):
_obj(obj),_new_mix_new_obj(new_mix_new_obj)
{
}

bool VM_RelinkMixObjects::doit_prologue(){
  if(_obj == NULL){
    return false;
  }
  if(_new_mix_new_obj == NULL){
    return false;
  }

  if(!(*_obj)->mark()->is_mixobject()){
    return false;
  }

  if((*_new_mix_new_obj)->mark()->is_mixobject()){
    return false;
  }
  return true;
}

void VM_RelinkMixObjects::doit(){
  oop mix_old_obj     = (*_obj)();
  oop old_mix_new_obj = (oop)mix_old_obj->mark()->decode_mixobject_pointer();
  oop new_mix_new_obj = (*_new_mix_new_obj)();

  // set real mark to new mix new object
  markOop real_mark = old_mix_new_obj->mark();
  new_mix_new_obj->set_mark(real_mark);

  // link new mix new object to mix old object
  markOop new_mark  = markOopDesc::encode_mixobject_pointer_as_mark(new_mix_new_obj);
  mix_old_obj->set_mark(new_mark);
}


VM_UnlinkMixObjects::VM_UnlinkMixObjects(Handle * obj):
_obj(obj)
{
}

bool VM_UnlinkMixObjects::doit_prologue(){
  return _obj != NULL && (*_obj)->mark()->is_mixobject();
}

void VM_UnlinkMixObjects::doit(){
  oop mix_old_obj     = (*_obj)();
  oop old_mix_new_obj = (oop)mix_old_obj->mark()->decode_mixobject_pointer();

  markOop real_mark = old_mix_new_obj->mark();
  mix_old_obj->set_mark(real_mark);
}
