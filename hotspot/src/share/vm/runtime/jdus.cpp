/*
* Copyright (C) 2012  Tianxiao Gu. All rights reserved.
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
* 
* Please contact Instituite of Computer Software, Nanjing University, 
* 163 Xianlin Avenue, Nanjing, Jiangsu Provience, 210046, China,
* or visit moon.nju.edu.cn if you need additional information or have any
* questions.
*/


#include "precompiled.hpp"
#include "runtime/jdus.hpp"
#include "runtime/jdusOperation.hpp"
#include "runtime/interfaceSupport.hpp"
#include "runtime/javaCalls.hpp"
#include "memory/oopFactory.hpp"
#include "prims/jni.h"
#include "prims/jvm_misc.hpp"
#include "prims/jvmtiRedefineClasses.hpp"
#include "prims/methodComparator.hpp"
#include "runtime/vm_operations.hpp"
#include "interpreter/bytecodes.hpp"
#include "classfile/javaClasses.hpp"
#include "classfile/dictionary.hpp"
#include "runtime/handles.hpp"
#include "runtime/jniHandles.hpp"
#include "runtime/frame.hpp"
#include "runtime/vframe.hpp"
#include "runtime/vframe_hp.hpp"
#include "runtime/timer.hpp"
#include "runtime/sharedRuntime.hpp"
#include "ci/ciEnv.hpp"
#include "compiler/compileBroker.hpp"
#include "gc_interface/collectedHeap.hpp"
#include "utilities/hashtable.hpp"

#include "interpreter/rewriter.hpp"



DSUObject::DSUObject():
_validated(false)
{

}

DSUObject::~DSUObject(){}

void DSUObject::print(){
  //tty->print_cr("DSUObject state is %d",_state);
}

void DSU::print(){
  //tty->print_cr("DSU state is %d",_state);
}

void DSUClassLoader::print(){
  //tty->print_cr("DSUClassLoader state is %d",_state);
}

void DSUClass::print(){
  //tty->print_cr("DSUClass state is %d",_state);
}

void DSUMethod::print(){
  //tty->print_cr("DSUMethod state is %d",_state);
}

DSU::DSU():
  _first_class_loader(NULL),
  _last_class_loader(NULL),
  _policy(JDUS_SYSTEM_SAFE_POINT),
  _request_state(JDUS_REQUEST_INIT),
  _shared_stream_provider(NULL)
{

}

jdusError DSU::prepare(TRAPS){
  DSUClassLoader* dsu_class_loader = first_class_loader();
  for(;dsu_class_loader != NULL;dsu_class_loader = dsu_class_loader->next()){
    jdusError ret = dsu_class_loader->prepare(THREAD);
    if (ret != JDUS_ERROR_NONE){
      return ret;
    }
    if (HAS_PENDING_EXCEPTION){
      return JDUS_ERROR_TO_BE_ADDED;
    }
  }

  return JDUS_ERROR_NONE;
}

bool DSU::validate(TRAPS){
  return false;
}

// only add class does not require a new version.
bool DSUClass::require_old_version() const {
  return updating_type() != JDUS_CLASS_ADD;
}

bool DSUClass::old_version_resolved() const{
  return _klass!=NULL && JNIHandles::resolve(_klass)->is_klass();
}

// only del class does not require a new version.
bool DSUClass::require_new_version() const {
  return updating_type() != JDUS_CLASS_DEL;
}

bool DSUClass::new_version_resolved() const{
  return _scratch_klass!=NULL && JNIHandles::resolve(_scratch_klass)->is_klass();
}

// Resolve klass and set transformers
jdusError DSUClass::resolve_old_version(instanceKlassHandle &old_version, TRAPS){  
  ResourceMark rm(THREAD);

  if(!require_old_version()){
    return JDUS_ERROR_NONE;
  }

  if(old_version_resolved()){
    old_version = instanceKlassHandle(THREAD, klass());
    return JDUS_ERROR_NONE;
  }

  // make sure the class loader has been resolved.
  if (!dsu_class_loader()->resolved()) {
    dsu_class_loader()->resolve(CHECK_(JDUS_ERROR_RESOLVE_CLASS_LOADER_FAIL));
  }

  assert(dsu_class_loader()->resolved(), "sanity check");

  symbolHandle name_sym (THREAD, name());
  assert(name_sym.not_null(), "Sanity check");

  Handle loader (THREAD, dsu_class_loader()->classloader());
  Handle null_pd;
  
  //instanceKlassHandle klass_h (THREAD, klass());
  

  //
  klassOop klass = SystemDictionary::find(name_sym,loader,null_pd,THREAD);
  
  if(HAS_PENDING_EXCEPTION){      
    JDUS_WARN(("Find class [%s] error.", name_sym->as_C_string()));
    return JDUS_ERROR_TO_BE_ADDED;
  }

  if (klass == NULL){
    ResourceMark rm(THREAD);
    tty->print_cr("[JDUS] Could not find loaded class [%s]. Abort parsing of the class. ", name_sym->as_C_string());
    return JDUS_ERROR_TO_BE_ADDED;
  }


  old_version = instanceKlassHandle(THREAD,klass);

  if(old_version->is_in_error_state()){
    JDUS_WARN(("[JDUS] Resolve class %s error state.", name_sym->as_C_string()));
    return JDUS_ERROR_TO_BE_ADDED;
  }

  if (!old_version->is_linked()) {
    old_version->link_class(THREAD);
    if (HAS_PENDING_EXCEPTION) {
      symbolOop ex_name = PENDING_EXCEPTION->klass()->klass_part()->name();

      CLEAR_PENDING_EXCEPTION;
      if (ex_name == vmSymbols::java_lang_OutOfMemoryError()) {
        return JDUS_ERROR_OUT_OF_MEMORY;
      } else {
        //JDUS_TRACE_MESG(("Link old class error for %s.", dsu_class->name()->as_C_string()));
        return JDUS_ERROR_LINK_OLD_CLASS;
      }
    }
  }

  set_klass(JNIHandles::make_global(old_version));
    
  instanceKlassHandle transformer (THREAD,dsu_class_loader()->transformer_class());

  if(transformer.not_null()){
    
    objArrayHandle methods (THREAD, transformer->methods());
    int length = methods->length();
    if(length > 0){
      const char * prefix = "(";
      //const char * subfix = ")V";
      int len_pre = strlen(prefix);
      //int len_sub = strlen(subfix);
      int str_length = name_sym->utf8_length()  + len_pre /* + len_sub*/;
      char * buf = NEW_RESOURCE_ARRAY(char,str_length+1);
      sprintf(buf, "%s%s",prefix,name_sym->as_klass_external_name()/*,subfix*/);
      for (int i=0; i<length; i++){
        methodHandle method = methodHandle(THREAD,(methodOop)methods->obj_at(i));
        if(method->signature()->starts_with(buf,str_length)){
          if(method->name() == vmSymbols::jdsuClass_name()){
            if(class_transformer_method() == NULL){
              tty->print_cr("[JDUS] Set class transformer during parsing.");
              set_class_transformer_method(JNIHandles::make_global(method));
            }else {}
          }else if (method->name() == vmSymbols::jdsuObject_name()){
            if(object_transformer_method() == NULL){
              set_object_transformer_method(JNIHandles::make_global(method));
            }else {}
          }
        }
      }
    }
  }

  return JDUS_ERROR_NONE;
}

jdusError DSUClass::resolve_new_version(instanceKlassHandle &new_version, TRAPS){
  if (!require_new_version()){
    return JDUS_ERROR_NONE;
  }

  if (new_version_resolved()){
    return JDUS_ERROR_NONE;
  }

  if(Thread::current()->is_JDUS_thread()){
    return resolve_new_version_by_jdus_thread(new_version, THREAD);
  }

  return resolve_new_version_at_safe_point(new_version, THREAD);
}


// new class is resolved out of VM safe point.
// so such classes are added into the temporal dictionary.
jdusError DSUClass::resolve_new_version_by_jdus_thread(instanceKlassHandle &new_version, TRAPS){
  // must be invoked in JDUS thread
  assert(THREAD->is_JDUS_thread(), "sanity check");

  klassOop k = scratch_klass();
  if (k!=NULL && k->is_klass()){
    new_version = instanceKlassHandle(k);
    return JDUS_ERROR_NONE;
  }

  symbolHandle name_h (name());
  jdusError ret = dsu_class_loader()->load_new_version(name_h, new_version, THREAD);

  if (new_version.not_null()){
    set_scratch_klass(JNIHandles::make_global(new_version));
    JDUS::add_jdus_klass(new_version,THREAD);
  }

  return ret;
}

jdusError DSUClass::resolve_new_version_at_safe_point(instanceKlassHandle &new_version, TRAPS){
  return JDUS_ERROR_NONE;
}

jdusError DSUClass::prepare(TRAPS){
  if (prepared()){
    // already prepared
    return JDUS_ERROR_NONE;
  }


  jdusError ret;
  
  instanceKlassHandle old_version;
  ret = resolve_old_version(old_version, THREAD);
  if(ret != JDUS_ERROR_NONE){
    //return ret;
    return ret;
  }

  if (updating_type() == JDUS_CLASS_DEL){
    if (old_version.not_null()){
      _prepared = true;
    }
    return JDUS_ERROR_NONE;
  }

  instanceKlassHandle new_version;
  ret = resolve_new_version(new_version, THREAD);
  if(ret != JDUS_ERROR_NONE){
    //return ret;
    return ret;
  }

  if (updating_type() == JDUS_CLASS_ADD){
    if (new_version.not_null()){
      _prepared = true;
    }
    return JDUS_ERROR_NONE;
  }

  // prepare super classes of new version here
  if (old_version.not_null()){
    JDUS::prepare_super_class(old_version, CHECK_(JDUS_ERROR_PREPARE_SUPER_FAIL));
    JDUS::prepare_interfaces(old_version, CHECK_(JDUS_ERROR_PREPARE_INTERFACES_FAIL));
  }

  ret = refresh_updating_type(old_version, new_version, THREAD);

  if (ret != JDUS_ERROR_NONE){
    return ret;
  }

  // create MixVersion,TempVersion...
  // create other data
  
  if (updating_type() == JDUS_CLASS_BC){
    old_version->set_DSU_state(instanceKlass::jdus_will_be_swapped);
  }else if(updating_type() == JDUS_CLASS_DEL){
    old_version->set_DSU_state(instanceKlass::jdus_will_be_deleted);
  }else if(updating_type() == JDUS_CLASS_ADD){
    old_version->set_DSU_state(instanceKlass::jdus_will_be_added);
  }else if(updating_type() > JDUS_CLASS_SMETHOD && updating_type() < JDUS_CLASS_BOTH){
    old_version->set_DSU_state(instanceKlass::jdus_will_be_redefined);
  }else {
    ShouldNotReachHere();
  }

  _prepared = true;

  return JDUS_ERROR_NONE;
}



jdusError DSUClass::refresh_updating_type(instanceKlassHandle old_version, instanceKlassHandle new_version, TRAPS){
  if (updating_type()!=JDUS_CLASS_NONE) {
    return JDUS_ERROR_NONE;
  }

  if (old_version.is_null()) {
    // old class has not been resolved
    return JDUS_ERROR_TO_BE_ADDED;
  }

  if (new_version.is_null()){
    // error here, new classes must be loaded here.
    return JDUS_ERROR_TO_BE_ADDED;
  }

  jdusError ret;

  ret = compare_and_normalize_class(old_version, new_version, THREAD);

  if(ret != JDUS_ERROR_NONE){
    // handle error here
    return ret;
  }

  return JDUS_ERROR_NONE;
}

jdusClassUpdatingType JDUS::join(jdusClassUpdatingType this_type, jdusClassUpdatingType that_type) {  
  switch(this_type){
  case JDUS_CLASS_UNKNOWN:
  case JDUS_CLASS_NONE:
    return that_type;
  case JDUS_CLASS_MC:
    // no upgrade from MC
    ShouldNotReachHere();
    break;
  case JDUS_CLASS_BC:
  case JDUS_CLASS_INDIRECT_BC:
  case JDUS_CLASS_SMETHOD:
  case JDUS_CLASS_SFIELD:
  case JDUS_CLASS_SBOTH:
  case JDUS_CLASS_METHOD:
  case JDUS_CLASS_FIELD:
  case JDUS_CLASS_BOTH:
    return this_type>that_type ? this_type:that_type;
  case JDUS_CLASS_ADD:
  case JDUS_CLASS_DEL:
    // no upgrade
    assert(this_type == that_type, "sanity");
    return this_type;
  case JDUS_CLASS_STUB:
    // STUB should be deprecated
    // no preparation for STUB
  default:
    ShouldNotReachHere();
  }
  return JDUS_CLASS_UNKNOWN;
}

jdusError DSUClass::compare_and_normalize_class(instanceKlassHandle old_version, instanceKlassHandle new_version, TRAPS){
  const jdusClassUpdatingType old_updating_type = updating_type();
  jdusClassUpdatingType new_updating_type = JDUS_CLASS_NONE;

  HandleMark hm(THREAD);

  assert(old_version->name() == new_version->name(), "name should be the same");
  assert(old_version->class_loader() == new_version->class_loader(), "class loader should also be the same");
  assert(old_version->super() != NULL , "javelus does not updating library classes");
  assert(new_version->super() != NULL , "javelus does not updating library classes");
  

  // XXX Compare the name, since body changed class could be ...
  // first check whether super is the same  
  
  {
    instanceKlassHandle old_super(THREAD, old_version->super());
    instanceKlassHandle new_super(THREAD, new_version->super());
    if (old_super() != new_super()){
      // super class has changed
      if (old_super->name() != new_super->name()
        || old_super->class_loader() != new_super->class_loader() ){
          // really different
      }

      jdusClassUpdatingType super_updating_type = JDUS::get_updating_type(old_super, CHECK_(JDUS_ERROR_GET_UPDATING_TYPE));
      new_updating_type = JDUS::join(new_updating_type, super_updating_type);
    }
  }

  {
    objArrayHandle k_old_interfaces ( old_version->local_interfaces());
    objArrayHandle k_new_interfaces ( new_version->local_interfaces());
    int n_intfs = k_old_interfaces->length();
    if (n_intfs != k_new_interfaces->length()) {
      //new_updating_type = JDUS_CLASS_BOTH;
      new_updating_type = JDUS::join(new_updating_type, JDUS_CLASS_BOTH);
    }else {
      // same size, compare elements seperatedly
      for (int i = 0; i < n_intfs; i++) {
        instanceKlassHandle old_interface(THREAD, (klassOop)k_old_interfaces->obj_at(i));
        instanceKlassHandle new_interface(THREAD, (klassOop)k_new_interfaces->obj_at(i));
        if (old_interface() != new_interface()){
          if (old_interface->name() != new_interface->name()
            || old_interface->class_loader() != new_interface->class_loader() ){
              // really different
          }
          jdusClassUpdatingType interface_updating_type = JDUS::get_updating_type(old_interface, CHECK_(JDUS_ERROR_GET_UPDATING_TYPE));
          new_updating_type = JDUS::join(new_updating_type, interface_updating_type);
        }
      }
    }
  }

  if (old_version->is_in_error_state()) {
    // TBD #5057930: special error code is needed in 1.6
    assert(false, "to be decided");
  }

  jushort old_flags = (jushort) old_version->access_flags().get_flags();
  jushort new_flags = (jushort) new_version->access_flags().get_flags();
  if (old_flags != new_flags) {
    //new_updating_type = JDUS::join(new_updating_type, JDUS_CLASS_BC);
    //new_updating_type = JDUS::join(new_updating_type, JDUS_CLASS_NONE);
    assert(false, "to be considered");
  }


  {
    /****************************************************/
    /* Here we do a very coarse grained comparison.     */
    /****************************************************/
    // 
    // Check if the number, names, types and order of fields declared in these classes
    // are the same.
    typeArrayOop k_old_fields = old_version->fields();
    typeArrayOop k_new_fields = new_version->fields();
    int n_fields = k_old_fields->length();
    if (n_fields != k_new_fields->length()) {
      // TBD: Should we distinguish JDUS_CLASS_SFIELD and JDUS_CLASS_FIELD here??
      //new_updating_type = JDUS::join(new_updating_type, JDUS_CLASS_FIELD);
      new_updating_type = JDUS::join(new_updating_type, JDUS_CLASS_BOTH);
    }else {
      for (int i = 0; i < n_fields; i += instanceKlass::next_offset) {
        // access
        old_flags = k_old_fields->ushort_at(i + instanceKlass::access_flags_offset);
        new_flags = k_new_fields->ushort_at(i + instanceKlass::access_flags_offset);
        if ((old_flags ^ new_flags) & JVM_RECOGNIZED_FIELD_MODIFIERS) {
          // TBD: Should we distinguish JDUS_CLASS_SFIELD and JDUS_CLASS_FIELD here??
          new_updating_type = JDUS::join(new_updating_type, JDUS_CLASS_BOTH);
        }
        // offset
        if (k_old_fields->short_at(i + instanceKlass::low_offset) !=
          k_new_fields->short_at(i + instanceKlass::low_offset) ||
          k_old_fields->short_at(i + instanceKlass::high_offset) !=
          k_new_fields->short_at(i + instanceKlass::high_offset)) {
            new_updating_type = JDUS::join(new_updating_type, JDUS_CLASS_BOTH);
        }
        // name and signature
        jshort name_index = k_old_fields->short_at(i + instanceKlass::name_index_offset);
        jshort sig_index = k_old_fields->short_at(i +instanceKlass::signature_index_offset);
        symbolOop name_sym1 = old_version->constants()->symbol_at(name_index);
        symbolOop sig_sym1 = old_version->constants()->symbol_at(sig_index);
        name_index = k_new_fields->short_at(i + instanceKlass::name_index_offset);
        sig_index = k_new_fields->short_at(i + instanceKlass::signature_index_offset);
        symbolOop name_sym2 = new_version->constants()->symbol_at(name_index);
        symbolOop sig_sym2 = new_version->constants()->symbol_at(sig_index);
        if (name_sym1 != name_sym2 || sig_sym1 != sig_sym2) {
          new_updating_type = JDUS::join(new_updating_type, JDUS_CLASS_BOTH);
        }
      }
    }
  }

  {
    // Do a parallel walk through the old and new methods. Detect
    // cases where they match (exist in both), have been added in
    // the new methods, or have been deleted (exist only in the
    // old methods).  The class file parser places methods in order
    // by method name, but does not order overloaded methods by
    // signature.  In order to determine what fate befell the methods,
    // this code places the overloaded new methods that have matching
    // old methods in the same order as the old methods and places
    // new overloaded methods at the end of overloaded methods of
    // that name. The code for this order normalization is adapted
    // from the algorithm used in instanceKlass::find_method().
    // Since we are swapping out of order entries as we find them,
    // we only have to search forward through the overloaded methods.
    // Methods which are added and have the same name as an existing
    // method (but different signature) will be put at the end of
    // the methods with that name, and the name mismatch code will
    // handle them.
    objArrayHandle k_old_methods(old_version->methods());
    objArrayHandle k_new_methods(new_version->methods());
    int n_old_methods = k_old_methods->length();
    int n_new_methods = k_new_methods->length();

    int ni = 0;
    int oi = 0;
    while (true) {
      methodHandle k_old_method;
      methodHandle k_new_method;
      DSUMethod* dsu_method = NULL;
      enum { matched, added, deleted, undetermined } method_was = undetermined;

      if (oi >= n_old_methods) {
        if (ni >= n_new_methods) {
          break; // we've looked at everything, done
        }
        // New method at the end
        k_new_method = methodHandle((methodOop) k_new_methods->obj_at(ni));
        method_was = added;
      } else if (ni >= n_new_methods) {
        // Old method, at the end, is deleted
        k_old_method = methodHandle((methodOop) k_old_methods->obj_at(oi));
        method_was = deleted;
      } else {
        // There are more methods in both the old and new lists
        k_old_method = methodHandle((methodOop) k_old_methods->obj_at(oi));
        k_new_method = methodHandle((methodOop) k_new_methods->obj_at(ni));
        if (k_old_method->name() != k_new_method->name()) {
          // Methods are sorted by method name, so a mismatch means added
          // or deleted
          if (k_old_method->name()->fast_compare(k_new_method->name()) > 0) {
            method_was = added;
          } else {
            method_was = deleted;
          }
        } else if (k_old_method->signature() == k_new_method->signature()) {
          // Both the name and signature match
          method_was = matched;
        } else {
          // The name matches, but the signature doesn't, which means we have to
          // search forward through the new overloaded methods.
          int nj;  // outside the loop for post-loop check
          for (nj = ni + 1; nj < n_new_methods; nj++) {
            methodHandle mh = methodHandle((methodOop)k_new_methods->obj_at(nj));
            if (k_old_method->name() != mh->name()) {
              // reached another method name so no more overloaded methods
              method_was = deleted;
              break;
            }
            if (k_old_method->signature() == mh->signature()) {
              // found a match so swap the methods
              k_new_methods->obj_at_put(ni, mh());
              k_new_methods->obj_at_put(nj, k_new_method());
              k_new_method = mh;
              method_was = matched;
              break;
            }
          }

          if (nj >= n_new_methods) {
            // reached the end without a match; so method was deleted
            method_was = deleted;
          }
        }
      }

      switch (method_was) {
      case matched:
        // methods match, be sure modifiers do too      
        {
          u2 new_num = k_new_method->method_idnum();
          u2 old_num = k_old_method->method_idnum();
          if (new_num != old_num) {
            methodOop idnum_owner = new_version->method_with_idnum(old_num);
            if (idnum_owner != NULL) {
              // There is already a method assigned this idnum -- switch them
              idnum_owner->set_method_idnum(new_num);
            }
            k_new_method->set_method_idnum(old_num);
            // TBD
            //swap_all_method_annotations(old_num, new_num, scratch_class);
          }

          // allocate a DSUMethod
          dsu_method = this->allocate_method(k_old_method, CHECK_(JDUS_ERROR_GET_UPDATING_TYPE));
          dsu_method->set_updating_type(JDUS_METHOD_NONE);
          // compare the method implementation here

          if (!MethodComparator::methods_EMCP(k_old_method(), k_new_method())) {
            dsu_method->set_updating_type(JDUS_METHOD_BC);
            new_updating_type = JDUS::join(new_updating_type, JDUS_CLASS_BC);
          }else {
            // should we build method data here??
            methodOopDesc::build_interpreter_method_data(k_new_method, THREAD);
            if (HAS_PENDING_EXCEPTION) {
              assert((PENDING_EXCEPTION->is_a(SystemDictionary::OutOfMemoryError_klass())), "we expect only an OOM error here");
              CLEAR_PENDING_EXCEPTION;
            }
          }
        }
        RC_TRACE(0x00008000, ("Method matched: new: %s [%d] == old: %s [%d]",
          k_new_method->name_and_sig_as_C_string(), ni,
          k_old_method->name_and_sig_as_C_string(), oi));
        // advance to next pair of methods
        ++oi;
        ++ni;
        break;
      case added:
        // method added, see if it is OK      
        new_updating_type = JDUS::join(new_updating_type, JDUS_CLASS_METHOD);      
        {
          u2 num = old_version->next_method_idnum();
          if (num == constMethodOopDesc::UNSET_IDNUM) {
            // cannot add any more methods
            new_updating_type = JDUS::join(new_updating_type, JDUS_CLASS_METHOD);
          }
          u2 new_num = k_new_method->method_idnum();
          methodOop idnum_owner = new_version->method_with_idnum(num);
          if (idnum_owner != NULL) {
            // There is already a method assigned this idnum -- switch them
            idnum_owner->set_method_idnum(new_num);
          }
          k_new_method->set_method_idnum(num);
          // TBD
          //swap_all_method_annotations(new_num, num, scratch_class);
        }
        RC_TRACE(0x00008000, ("Method added: new: %s [%d]",
          k_new_method->name_and_sig_as_C_string(), ni));
        ++ni; // advance to next new method
        break;
      case deleted:
        new_updating_type = JDUS::join(new_updating_type, JDUS_CLASS_METHOD);

        // allocate a DSUMethod here;
        dsu_method = this->allocate_method(k_old_method, CHECK_(JDUS_ERROR_GET_UPDATING_TYPE));
        dsu_method->set_updating_type(JDUS_METHOD_DEL);

        RC_TRACE(0x00008000, ("Method deleted: old: %s [%d]",
          k_old_method->name_and_sig_as_C_string(), oi));
        ++oi; // advance to next old method
        break;
      default:
        ShouldNotReachHere();
      }
    }
  }

  if (old_updating_type == new_updating_type){
    // warn here
  }

  set_updating_type(new_updating_type);

  return JDUS_ERROR_NONE;
}


void DSU::add_class_loader(DSUClassLoader* class_loader){
  if (_first_class_loader == NULL){
    assert(_last_class_loader == NULL, "sanity check");
    _first_class_loader = _last_class_loader = class_loader;
    return;
  }

  assert(_last_class_loader != NULL, "sanity check");
  class_loader->set_next(NULL);
  _last_class_loader->set_next(class_loader);
  _last_class_loader = _last_class_loader->next();
}

DSUClassLoader* DSU::allocate_class_loader(symbolHandle id, symbolHandle lid, TRAPS){
  if(id.is_null() || id->utf8_length() == 0 ){
    id = vmSymbolHandles::default_class_loader_id();
  }

  if(lid.is_null() || lid->utf8_length() == 0 ){
    lid = vmSymbolHandles::default_class_loader_lid();
  }

  DSUClassLoader * test = find_class_loader_by_id(id);
  if(test != NULL){
    return test;
  }

  DSUClassLoader* result = new DSUClassLoader();
  result->set_dsu(this);
  result->set_id(JNIHandles::make_global(id));
  result->set_lid(JNIHandles::make_global(lid));
  // add the new allocated DSUClassLoader to this DSU
  add_class_loader(result);

  return result;
}



DSU::~DSU() {
  DSUClassLoader* p = first_class_loader();
  while(p != NULL){
    DSUClassLoader * q = p;
    p = p->next();
    delete q;
  }
  _first_class_loader = _last_class_loader = NULL;
}

// build the DSU from jvmtiClassDefinition
DSU* DSU::build_from_jvmti(jint class_count, jvmtiClassDefinition *_class_defs, TRAPS){
  return NULL;
}

// Iterate all classes in this class.
void 	DSU::classes_do(void f(DSUClass * dsu_class, TRAPS), TRAPS) {
  for (DSUClassLoader* dsu_loader = first_class_loader(); dsu_loader!=NULL; dsu_loader=dsu_loader->next()) {
    for(DSUClass *dsu_class = dsu_loader->first_class();dsu_class!=NULL;dsu_class=dsu_class->next()) {
      f(dsu_class, CHECK);
    }
  }
}


DSUClassLoader * DSU::find_class_loader_by_id(symbolHandle id){
  for (DSUClassLoader* dsu_loader = first_class_loader(); dsu_loader!=NULL; dsu_loader=dsu_loader->next()) {
    if(dsu_loader->id() == id()){
      return dsu_loader;
    }
  } 
  return NULL;
}

DSUClassLoader * DSU::find_class_loader_by_loader(Handle loader){
  for (DSUClassLoader* dsu_loader = first_class_loader(); dsu_loader!=NULL; dsu_loader=dsu_loader->next()) {
    if(dsu_loader->classloader() == loader()){
      return dsu_loader;
    }
  } 
  return NULL;
}

DSUClass* DSU::find_class_by_name(symbolHandle name){
  for (DSUClassLoader* dsu_loader = first_class_loader(); dsu_loader!=NULL; dsu_loader=dsu_loader->next()) {
    DSUClass* dsu_class = dsu_loader->find_class_by_name(name);
    if (dsu_class != NULL){
      return dsu_class;
    }
  } 
  return NULL;
}

DSUClass* DSU::find_class_by_name_and_loader(symbolHandle name, Handle loader){
  DSUClassLoader* dsu_loader = find_class_loader_by_loader(loader);
  if (dsu_loader != NULL){
    return dsu_loader->find_class_by_name(name);
  }
  return NULL;
}

DSUPathEntryStreamProvider::DSUPathEntryStreamProvider():
_first_entry(NULL),
_last_entry(NULL)
{

}

DSUPathEntryStreamProvider::~DSUPathEntryStreamProvider()
{
  // to be added
}

void DSUPathEntryStreamProvider::append_path(char* path, TRAPS) {
  struct stat st;
  if (os::stat((char *)path, &st) == 0) {
    // File or directory found
    ClassPathEntry* new_entry = NULL;
    ClassLoader::create_class_path_entry((char *)path, st, &new_entry, LazyBootClassLoader);
    add_entry(new_entry);
  }
}

void DSUPathEntryStreamProvider::add_entry(ClassPathEntry* entry) {
  if (_first_entry == NULL){
    assert(_last_entry == NULL, "sanity check");
    entry->set_next(NULL);
    _first_entry = _last_entry = entry;
    return;
  }

  entry->set_next(NULL);
  _last_entry->set_next(entry);  
  _last_entry = _last_entry->next();
}

ClassFileStream* DSUPathEntryStreamProvider::open_stream(const char* name){
  ClassFileStream* stream = NULL;  
  {  
    ClassPathEntry* e = _first_entry;
    while (e != NULL) {
      stream = e->open_stream(name);
      if (stream != NULL) {
        break;
      }
      e = e->next();
    }
  }
  return stream;
}



const char * DSUDynamicPatchBuilder::command_names [] = {
    "classloader",
    "classpath",
    "addclass",
    "modclass",
    "delclass",
    "transformer",
    "loaderhelper",
  };

DSUDynamicPatchBuilder::DSUDynamicPatchBuilder():
  _dsu(NULL),
  _current_class_loader(NULL),
  _default_class_loader(NULL)
{
  // we use a shared class path entry here.
  // i.e., all class loaders share the same path
  _shared_stream_provider = new DSUPathEntryStreamProvider();  
}

DSUDynamicPatchBuilder::~DSUDynamicPatchBuilder(){
  // to be added
}

void DSUDynamicPatchBuilder::build(const char * path, TRAPS){
  if(path == NULL || path[0] == '\0'){
    JDUS_WARN(("build a DSU with empty dynamic patch."));
    return;
  }

  _dsu = new DSU();

  // set the shared stream provider
  _dsu->set_shared_stream_provider(_shared_stream_provider);

  // build the default class loader
  build_default_class_loader(CHECK);

  // parsing starts with default class loader
  use_default_class_loader();

  parse_file(path, CHECK);
  
  // post popular subclass caused by indirect changed super classes
  populate_sub_classes(CHECK);
}

// resolve all old version
// register place holder in the JDUS dictionary
void DSUDynamicPatchBuilder::populate_sub_classes(TRAPS) {
  DSUClassLoader* dsu_class_loader = dsu()->first_class_loader();
  for(;dsu_class_loader != NULL; dsu_class_loader = dsu_class_loader->next() ){
    DSUClass* dsu_class = dsu_class_loader->first_class();
    for(; dsu_class != NULL; dsu_class = dsu_class->next()){
      if(dsu_class->require_old_version()){
        HandleMark hm(THREAD);
        instanceKlassHandle old_version;
        dsu_class->resolve_old_version(old_version,CHECK);
        if (old_version.not_null()) {
          populate_sub_classes(dsu_class, old_version, CHECK);
        }
      }
    }
  }
}

// invariant: ik has a DSUClass
void DSUDynamicPatchBuilder::populate_sub_classes(DSUClass* dsu_class, instanceKlassHandle ik, TRAPS){
  HandleMark hm(THREAD);  
  
  assert(dsu_class!= NULL && ik.not_null() && ik()->is_klass(), "sanity");

  if (ik->subklass() == NULL){
    return;
  }

  instanceKlassHandle subklass(THREAD, ik->subklass()->as_klassOop());
  while(subklass.not_null()){
    Handle loader(THREAD, subklass->class_loader());
    symbolHandle class_name(THREAD, subklass->name());
    DSUClassLoader* sub_dsu_class_loader = dsu()->find_class_loader_by_loader(loader);

    if (sub_dsu_class_loader == NULL) {
      // Should we have to create a DSUClassLoader first?
      stringStream st;
      // st.print() uses too much stack space while handling a StackOverflowError
      // st.print("%s.class", h_name->as_utf8());
      st.print("%d", os::random());
      symbolHandle id = oopFactory::new_symbol_handle(st.as_string(), CHECK);      
      // null lid
      // TODO
      symbolHandle null_lid;
      sub_dsu_class_loader = dsu()->allocate_class_loader(id, null_lid, CHECK);
    }

    assert(sub_dsu_class_loader != NULL, "sanity check");

    DSUClass* sub_dsu_class = sub_dsu_class_loader->find_class_by_name(class_name);
    if (sub_dsu_class != NULL){
      // we already has created a DSUClass for this class during parsing.
      // do nothing here.
      //continue;
    }else {
      sub_dsu_class = sub_dsu_class_loader->allocate_class(class_name, CHECK);
      // TODO Here, a indirect updated class just copy the updating type of its super class.
      //sub_dsu_class->set_updating_type(JDUS_CLASS_INDIRECT);
      sub_dsu_class->set_updating_type(dsu_class->updating_type());
      populate_sub_classes(sub_dsu_class, subklass, CHECK);
    }
    if (subklass->next_sibling() == NULL){
      break;
    }
    subklass = instanceKlassHandle(THREAD, subklass->next_sibling()->as_klassOop());
  }
}

void DSUDynamicPatchBuilder::parse_file(const char * file, TRAPS){
  struct stat st;
  if (os::stat(file, &st) == 0) {
    // found file, open it
    int file_handle = os::open(file, 0, 0);
    if (file_handle != -1){
      // read contents into resource array
      ResourceMark rm(THREAD);
      u1* buffer = NEW_RESOURCE_ARRAY(u1, st.st_size);
      size_t num_read = os::read(file_handle, (char*) buffer, st.st_size);
      // close file
      os::close(file_handle);
      if (num_read == (size_t)st.st_size) {
        char * token = (char*) buffer;
        char * line = token;        
        int pos = 0;        
        while (pos != num_read){
          if(token[pos] == '\n'){
            token[pos] = '\0';
            parse_line(line, CHECK);
            line = token + pos + 1;
          }
          pos++;
        }
      }else {
        JDUS_WARN(("File size of dynamic patch error!"));      
      }
    }else {
      JDUS_WARN(("Dynamic patch does not exist!"));
    }
  }else {
    JDUS_WARN(("Status Fail! Dynamic patch does not exist!"));
  }
}


void DSUDynamicPatchBuilder::build_default_class_loader(TRAPS){
  DSUClassLoader* dsu_class_loader = dsu()->allocate_class_loader(
    vmSymbolHandles::default_class_loader_id(),
    vmSymbolHandles::default_class_loader_lid(),
    CHECK);
  
  dsu_class_loader->set_stream_provider(_shared_stream_provider);
  dsu_class_loader->resolve(CHECK); 

  _default_class_loader = dsu_class_loader;
}

DSUDynamicPatchBuilder::DynamicPatchCommand DSUDynamicPatchBuilder::parse_command_name(const char * line, int* bytes_read) {
  *bytes_read = 0;
  char command[33];
  int result = sscanf(line, "%32[a-z]%n", command, bytes_read);
  for (uint i = 0; i < ARRAY_SIZE(command_names); i++) {
    if (strcmp(command, command_names[i]) == 0) {
      return (DynamicPatchCommand)i;
    }
  }
  return UnknownCommand;
}

DSU* DSUDynamicPatchBuilder::dsu() const{
  assert(_dsu != NULL, "instantiate before using.");
  return _dsu;
}

DSUClassLoader* DSUDynamicPatchBuilder::current_class_loader() {
  assert(_current_class_loader != NULL, "must be set before using");
  return _current_class_loader;
}

void DSUDynamicPatchBuilder::append_class_path_entry(char * line, TRAPS){
  _shared_stream_provider->append_path(line, CHECK);
}

// classloader [id] [lid]
void DSUDynamicPatchBuilder::use_class_loader(char *line, TRAPS){
  //assert(false, "to be added");

  char c_id[33];
  char c_lid[33];

  int result = sscanf(line, "%32[a-z] %32[a-z]", c_id, c_lid);

  symbolHandle id;
  symbolHandle lid;

  if (result > 0) {
    id = oopFactory::new_symbol_handle(c_id, CHECK);
  }

  if (result > 1) {
    lid = oopFactory::new_symbol_handle(c_lid, CHECK);
  }

  // getOrCreate class loader
  DSUClassLoader* dsu_class_loader = dsu()->allocate_class_loader(id, lid, CHECK);

  assert(dsu_class_loader != NULL, "sanity check");
  
  // set stream provider
  dsu_class_loader->set_stream_provider(_shared_stream_provider);

  // set current class loader
  _current_class_loader = dsu_class_loader;
}

void DSUDynamicPatchBuilder::append_added_class(char * line, TRAPS) {
  symbolHandle class_name = oopFactory::new_symbol_handle(line, CHECK);
  DSUClass * dsu_class = current_class_loader()->allocate_class(class_name, CHECK);

  dsu_class->set_updating_type(JDUS_CLASS_ADD);
  
  dsu_class->dsu_class_loader()->resolve(CHECK);
  assert(dsu_class->dsu_class_loader()->classloader() != NULL, "sanity check");

  // Now, we first register a place holder into the JDUS Dictionary (temporal dictionary)


  // TODO class must be ordered and than loaded
  // this operation should be moved to VM_JDUSOperation::do_prologue
  //dsu_class->prepare(CHECK);
}

void DSUDynamicPatchBuilder::append_modified_class(char * line, TRAPS) {
  symbolHandle class_name = oopFactory::new_symbol_handle(line, CHECK);
  DSUClass * dsu_class = current_class_loader()->allocate_class(class_name, CHECK);

  dsu_class->set_updating_type(JDUS_CLASS_NONE);
  //dsu_class->prepare(CHECK);
}

void DSUDynamicPatchBuilder::append_deleted_class(char * line, TRAPS) {
  symbolHandle class_name = oopFactory::new_symbol_handle(line, CHECK);
  DSUClass * dsu_class = current_class_loader()->allocate_class(class_name, CHECK);

  dsu_class->set_updating_type(JDUS_CLASS_DEL);
  //dsu_class->prepare(CHECK);
}

void DSUDynamicPatchBuilder::append_transformer(char * line, TRAPS) {
  symbolHandle class_name = oopFactory::new_symbol_handle(line, CHECK);
  assert(false, "to be implemented");
}

void DSUDynamicPatchBuilder::append_loader_helper(char * line, TRAPS) {
  symbolHandle class_name = oopFactory::new_symbol_handle(line, CHECK);
  assert(false, "to be implemented");
}

void DSUDynamicPatchBuilder::use_default_class_loader() {
  _current_class_loader = _default_class_loader;
}

void DSUDynamicPatchBuilder::parse_line(char *line, TRAPS){
  if (line[0] == '\0') return;
  if (line[0] == '#')  return;
  int bytes_read;
  
  DynamicPatchCommand command = parse_command_name(line, &bytes_read);
  line+=bytes_read;

  while(*line == ' ' || *line == '\t'){
    line++;
  }

  switch(command){
  case UnknownCommand:
    JDUS_WARN(("Unknown command during parsing dynamic patch. %s", line));
    return;
  case ClassLoaderCommand:
    use_class_loader(line,CHECK);
    return;
  case ClassPathCommand:
    append_class_path_entry(line, CHECK);
    return;
  case AddClassCommand:
    append_added_class(line, CHECK);
    return;
  case ModClassCommand:
    append_modified_class(line, CHECK);
    return;
  case DelClassCommand:
    append_deleted_class(line, CHECK);
    return;
  case TransformerCommand:
    append_transformer(line, CHECK);
    return;
  case LoaderHelperCommand:
    append_loader_helper(line, CHECK);
    return;
  }
}



DSUClassLoader::DSUClassLoader():
  _next(NULL),
  _class_loader(NULL),
  _helper_class(NULL),
  _transformer_class(NULL),
  _first_class(NULL),
  _last_class(NULL),
  _id(NULL),
  _lid(NULL)
{}


bool DSUClassLoader::validate(TRAPS){
  return false;
}

DSUClassLoader::~DSUClassLoader() {

  JNIHandles::destroy_global(_class_loader);
  JNIHandles::destroy_global(_helper_class);
  JNIHandles::destroy_global(_transformer_class);

  DSUClass* p = _first_class;
  while(p != NULL){
    DSUClass* q = p;
    p = p->next();
    delete q;
  }
  _first_class = NULL;
  _last_class = NULL;
}

jdusError DSUClassLoader::prepare(TRAPS) {
  // rsolve the class loader first
  resolve(CHECK_(JDUS_ERROR_RESOLVE_CLASS_LOADER_FAIL));
  
    
  // than prepare all classes
  for (DSUClass* dsu_class = first_class(); dsu_class!=NULL; dsu_class=dsu_class->next()) {
    jdusError ret = dsu_class->prepare(THREAD);
    
    if(ret != JDUS_ERROR_NONE){
      return ret;
    }

    if (HAS_PENDING_EXCEPTION){
      return JDUS_ERROR_TO_BE_ADDED;
    }
  }

  return JDUS_ERROR_NONE;
}

DSUClass * DSUClassLoader::find_class_by_name(symbolHandle name){
  for (DSUClass* dsu_class = first_class(); dsu_class!=NULL; dsu_class=dsu_class->next()) {
    if(dsu_class->name() == name()){
      return dsu_class;
    }
  } 
  return NULL;
}

bool DSUClassLoader::resolved(){
  return _class_loader != NULL;
}

void DSUClassLoader::resolve(TRAPS){
  if (id() ==  vmSymbols::default_class_loader_id()){
    // bind to the system class loader;
    Handle the_loader = Handle(THREAD, SystemDictionary::java_system_loader());    
    set_class_loader(JNIHandles::make_global(the_loader));
    return ;
  }

  if (this->helper_class() == NULL){
    return;
  }



  // run helper class to resolve these classloader
  assert(false, "to be added");

}

DSUClass* DSUClassLoader::allocate_class(symbolHandle name, TRAPS){
  if(name.is_null()|| name->utf8_length()==0){
    return NULL;
  }

  DSUClass* test = find_class_by_name(name);
  if(test != NULL){
    return test;
  }

  DSUClass* dsu_class = new DSUClass();

  dsu_class->set_name(JNIHandles::make_global(name));
  dsu_class->set_dsu_class_loader(this);

  // add class to this class loader
  this->add_class(dsu_class);

  return dsu_class;
}

jdusError  DSUClassLoader::load_new_version(symbolHandle name, instanceKlassHandle &new_class, TRAPS){
  ResourceMark rm(THREAD);
  
  if (!resolved()) {
    resolve(CHECK_(JDUS_ERROR_TO_BE_ADDED));
  }

  assert(resolved(), "sanity check");

  Handle the_class_loader (THREAD, classloader());

  // Check the state of the class loader.
  if(the_class_loader.is_null()){
    // TODO, error code should be adjust to the right one.
    return JDUS_ERROR_NULL_POINTER;
  }

  // Null protection domain
  // TODO, this should be consider later.
  Handle protection_domain;

  stringStream st;
  // st.print() uses too much stack space while handling a StackOverflowError
  // st.print("%s.class", h_name->as_utf8());
  st.print_raw(name->as_utf8());
  st.print_raw(".class");
  char* file_name = st.as_string();

  ClassFileStream* cfs = stream_provider()->open_stream(file_name);

  klassOop k = SystemDictionary::parse_stream(name,
    the_class_loader,
    protection_domain,
    cfs,
    THREAD);

  if (HAS_PENDING_EXCEPTION) {
    symbolOop ex_name = PENDING_EXCEPTION->klass()->klass_part()->name();
    CLEAR_PENDING_EXCEPTION;

    if (ex_name == vmSymbols::java_lang_UnsupportedClassVersionError()) {
      return JDUS_ERROR_UNSUPPORTED_VERSION;
    } else if (ex_name == vmSymbols::java_lang_ClassFormatError()) {
      return JDUS_ERROR_INVALID_CLASS_FORMAT;
    } else if (ex_name == vmSymbols::java_lang_ClassCircularityError()) {
      return JDUS_ERROR_CIRCULAR_CLASS_DEFINITION;
    } else if (ex_name == vmSymbols::java_lang_NoClassDefFoundError()) {
      // The message will be "XXX (wrong name: YYY)"
      return JDUS_ERROR_NAMES_DONT_MATCH;
    } else if (ex_name == vmSymbols::java_lang_OutOfMemoryError()) {
      return JDUS_ERROR_OUT_OF_MEMORY;
    } else {  // Just in case more exceptions can be thrown..
      JDUS_TRACE_MESG(("Fails verification, exception name is %s, class name is %s." , 
        ex_name->as_C_string(), name->as_C_string()));
      return JDUS_ERROR_FAILS_VERIFICATION;
    }
  }

  new_class = instanceKlassHandle(THREAD, k);

    // We just allocate constantPoolCache here and do not initialize vtable and itable.
  Rewriter::rewrite(new_class, THREAD);
  if (HAS_PENDING_EXCEPTION) {
    symbolOop ex_name = PENDING_EXCEPTION->klass()->klass_part()->name();
    CLEAR_PENDING_EXCEPTION;
    if (ex_name == vmSymbols::java_lang_OutOfMemoryError()) {
      return JDUS_ERROR_OUT_OF_MEMORY;
    } else {
      return JDUS_ERROR_REWRITER;
    }
  }

  {
    ResourceMark rm(THREAD);
    // no exception should happen here since we explicitly
    // do not check loader constraints.
    // compare_and_normalize_class_versions has already checked:
    //  - classloaders unchanged, signatures unchanged
    //  - all instanceKlasses for redefined classes reused & contents updated
    new_class->vtable()->initialize_vtable(false, THREAD);
    new_class->itable()->initialize_itable(false, THREAD);
    assert(!HAS_PENDING_EXCEPTION || (THREAD->pending_exception()->is_a(SystemDictionary::ThreadDeath_klass())), "redefine exception");
#ifdef ASSERT
    new_class->vtable()->verify(tty);
#endif

  }

  return JDUS_ERROR_NONE;
}

void DSUClassLoader::add_class(DSUClass * klass){
  assert( klass->next() == NULL, "should not in any other list.");
  if(_first_class == NULL){
    assert(_last_class == NULL, "sanity check");
    _first_class = _last_class = klass;
  }else {
    assert(_last_class != NULL, "sanity check");
    _last_class->set_next(klass);
    _last_class = klass;
  }
}

DSUClass::DSUClass():
  _updating_type(JDUS_CLASS_NONE),
  _next(NULL),
  _first_method(NULL),
  _last_method(NULL),
  _klass(NULL),
  _class_bytes_count(0),
  _class_bytes(NULL),
  _scratch_klass(NULL),
  _mix_klass(NULL),
  _temp_klass(NULL),
  _match_static_count(0),
  _match_static_fields(NULL),
  _class_transformer_method(NULL),
  _object_transformer_method(NULL),
  _ycsc_old(NULL),
  _ycsc_new(NULL),
  _min_object_size_in_bytes(0),
  _min_vtable_size(0),
  _prepared(false)
{}


bool DSUClass::validate(TRAPS){
  return false;
}

int DSUClass::from_rn() const {return dsu_class_loader()->dsu()->from_rn();}

int DSUClass::to_rn() const {return dsu_class_loader()->dsu()->to_rn();}

void DSUClass::initialize(){
  set_dsu_class_loader(NULL);

  set_updating_type(JDUS_CLASS_NONE);
  set_class_bytes(NULL);
  set_class_bytes_count(0);
  set_klass((jobject)NULL);

  set_match_static_count(0);
  set_match_static_fields(NULL);

  set_name((jstring)NULL);
  set_new_name((jstring)NULL);
  set_mix_klass((jobject)NULL);
  set_temp_klass((jobject)NULL);
  set_scratch_klass((jobject)NULL);
  set_class_transformer_method(NULL);
  set_object_transformer_method(NULL);

  set_ycsc_old(NULL);
  set_ycsc_new(NULL);
  set_min_object_size_in_bytes(0);
  set_min_vtable_size(0);
}

DSUClass::~DSUClass() {
  DSUMethod* p = first_method();

  while(p != NULL){
    DSUMethod * q = p;
    p = p->next();
    delete q;
  }

  _first_method = _last_method = NULL;

  if (_match_static_fields != NULL){
    FREE_C_HEAP_ARRAY(jint,_match_static_fields);
  }



  JNIHandles::destroy_global(_name);

  JNIHandles::destroy_global(_new_name);

  if(JNIHandles::is_global_handle(_klass)){
    JNIHandles::destroy_global(_klass);
  }

  JNIHandles::destroy_global(_scratch_klass);
  JNIHandles::destroy_global(_mix_klass);
  JNIHandles::destroy_global(_temp_klass);
  JNIHandles::destroy_global(_object_transformer_method);
  JNIHandles::destroy_global(_class_transformer_method);
  JNIHandles::destroy_global(_ycsc_old);
  JNIHandles::destroy_global(_ycsc_new);
}

DSUMethod* DSUClass::allocate_method(methodHandle method, TRAPS){
  DSUMethod* dsu_method = new DSUMethod();

  dsu_method->set_dsu_class(this);
  dsu_method->set_method(JNIHandles::make_global(method));

  return dsu_method;
}

void DSUClass::add_method(DSUMethod * method){
  assert( method->next() == NULL, "should not in any other list.");
  if(_first_method == NULL){
    assert(_last_method == NULL, "sanity check");
    _first_method = _last_method = method;
  }else {
    assert(_last_method != NULL, "sanity check");
    _last_method->set_next(method);
    _last_method = method;
  }
}

void DSUClass::methods_do(void f(DSUMethod * dsu_method,TRAPS), TRAPS){
  for(DSUMethod* p = first_method();p!=NULL;p=p->next()){
    f(p,CHECK);
  }
}

DSUMethod* DSUClass::find_method(symbolHandle name, symbolHandle desc){
  for(DSUMethod* p = first_method();p!=NULL;p=p->next()){
    methodHandle mh(p->method());
    if(mh->name() == name() && mh->signature() == desc()){
      return p;
    }
  }
  return NULL;
}

DSUMethod::DSUMethod():
  _updating_type(JDUS_METHOD_NONE),
  _method(NULL)
{}

DSUMethod::~DSUMethod() {
  JNIHandles::destroy_global(_method);
}


bool DSUMethod::validate(TRAPS){
  return false;
}

methodOop DSUMethod::method() const {
  return (methodOop)JNIHandles::resolve(_method);
}


//TODO here we just replace the handle of methodOop.
//Because the compiler interface may use the old method as the new method.
//Currently we donot solve the missmatching...
void StackRepairClosure::do_oop(oop *p) {
  //if((*p)!= NULL ){
  //  if((*p)->is_method()){
  //    //methodOop method = (methodOop) (*p);
  //    //if(method->is_old()){
  //    //  klassOop klass = method->constants()->pool_holder();
  //    //  if(klass->klass_part()->is_mix_invalid()){
  //    //    instanceKlass* ik = instanceKlass::cast(instanceKlass::cast(klass)->next_version());
  //    //    (*p) = ik->find_method(method->name(),method->signature());
  //    //  }
  //    //}
  //  }else if((*p)->is_instance()){
  //    //tty->print_cr("p is 0x%8x, (*p) is 0x%8x",p,*p);
  //    //SharedRuntime::transform_object_at_safepoint((*p), _thread);
  //  }
  //}
}


//return revision number of current thread.
JVM_ENTRY(jint, CurrentRevisionNumber(JNIEnv *env, jclass cls))
  return thread->current_revision_number();
JVM_END

  // GetMixThat:
  // return the MixNewObject if it exists
JVM_ENTRY(jobject, GetMixThat(JNIEnv *env,jclass cls,jobject o))
  oop obj = JNIHandles::resolve_non_null(o);
if(obj->mark()->is_mixobject()){
  return JNIHandles::make_local(env, (oop)obj->mark()->decode_mixobject_pointer());
}
return o;
JVM_END

JVM_ENTRY(jobject, ReplaceObject(JNIEnv *env, jclass clas, jobject old_o, jobject new_o))
  //jobject ReplaceObject(JNIENv *env, jclass clas, jobject old_o, jobject new_o){
  oop old_obj = JNIHandles::resolve_non_null(old_o);
  oop new_obj = JNIHandles::resolve_non_null(new_o);

  Handle o (THREAD, old_obj);
  Handle n (THREAD, old_obj);

instanceKlassHandle ikh(THREAD,new_obj->klass());

assert(!(ikh->is_mix_invalid() || ikh->is_mix_old()),  "only mix new or new here");

Handle o_mix (THREAD, o());
if (o->mark()->is_mixobject()){
  o_mix = Handle(THREAD, (oop) o->mark()->decode_mixobject_pointer());
}

JDUS::copy_fields(n,o,o_mix,ikh,THREAD);
return JNIHandles::make_local(env,old_obj);

JVM_END






JVM_ENTRY(void, InvokeDSU(JNIEnv *env,jclass cls, jstring dynamic_patch, jboolean sync))

  Handle h_patch (THREAD, JNIHandles::resolve_non_null(dynamic_patch));
  const char* str   = java_lang_String::as_utf8_string(h_patch());

  DSUDynamicPatchBuilder patch_builder;

  patch_builder.build(str, CHECK);

  if (HAS_PENDING_EXCEPTION) {
    Handle pending_exception (THREAD, PENDING_EXCEPTION);
    pending_exception->print();
    // cleanup outside the handle mark.
    //report parse DSU error?  
    JDUS_WARN(("Parse dynamic patch met exceptions, clear DSU"));
    CLEAR_PENDING_EXCEPTION;
    return ;
  }

  DSU* dsu = patch_builder.dsu();

  dsu->validate(CHECK);

  VM_JDUSOperation * op = new VM_JDUSOperation(dsu);
  JDUSTask* task = new JDUSTask(op);

  JDUS::get_jdus_thread()->add_task(task);
  if (sync)
  {
    MutexLocker locker(JDUSRequest_lock);
    //tty->print_cr("wait until DSU finish try");
    JDUSRequest_lock->wait();
  }

JVM_END


const char * JDUS::class_updating_type_name(jdusClassUpdatingType ct){
  switch(ct){
  case JDUS_CLASS_NONE:
    return "JDUS_CLASS_NONE";
  case JDUS_CLASS_MC:
    return "JDUS_CLASS_MC";
  case JDUS_CLASS_BC:
    return "JDUS_CLASS_BC";
  case JDUS_CLASS_SMETHOD:
    return "JDUS_CLASS_SMETHOD";
  case JDUS_CLASS_SFIELD:
    return "JDUS_CLASS_SFIELD";
  case JDUS_CLASS_SBOTH:
    return "JDUS_CLASS_SBOTH";
  case JDUS_CLASS_METHOD:
    return "JDUS_CLASS_METHOD";
  case JDUS_CLASS_FIELD:
    return "JDUS_CLASS_FIELD";
  case JDUS_CLASS_BOTH:
    return "JDUS_CLASS_BOTH";
  case JDUS_CLASS_DEL:
    return "JDUS_CLASS_DEL";
  case JDUS_CLASS_STUB:
    return "JDUS_CLASS_STUB";
  default:
    JDUS_WARN(("int to jdusClassUpdatingType error!, unsupported type %d",ct));
    ShouldNotReachHere();
  }
  return "JDUS_CLASS_NONE";
}

jdusClassUpdatingType intToClassChangeType(jint intType){
  switch(intType){
  case 0:
    return JDUS_CLASS_NONE;
  case 1:
    return JDUS_CLASS_MC;
  case 2:
    return JDUS_CLASS_BC;
  case 3:
    return JDUS_CLASS_SMETHOD;
  case 4:
    return JDUS_CLASS_SFIELD;
  case 5:
    return JDUS_CLASS_SBOTH;
  case 6:
    return JDUS_CLASS_METHOD;
  case 7:
    return JDUS_CLASS_FIELD;
  case 8:
    return JDUS_CLASS_BOTH;
  case 9:
    return JDUS_CLASS_DEL;
  case 10:
    return JDUS_CLASS_STUB;
  default:
    JDUS_WARN(("int to jdusClassUpdatingType error!, unsupported type %d",intType));
    ShouldNotReachHere();		
  }
  return JDUS_CLASS_NONE;
}


JVM_ENTRY(void, RedefineSingleClass(JNIEnv *env, jclass cls, jstring name, jbyteArray file))
  //%note jni_3
  assert(false, "Deprecated!!");
  Handle loader;
  Handle protection_domain;
  // Find calling class
  instanceKlassHandle k (THREAD, thread->security_get_caller_class(1));
  if (k.not_null()) {
    loader = Handle(THREAD, k->class_loader());
  } else {
    // We call ClassLoader.getSystemClassLoader to obtain the system class loader.
    loader = Handle(THREAD, SystemDictionary::java_system_loader());
  }


  oop name_oop = JNIHandles::resolve_non_null(name);
  Handle name_handle(THREAD, name_oop);
  //char * char_name = java_lang_String::as_utf8_string(sym_oop);

  symbolHandle sym = java_lang_String::as_symbol(name_handle, THREAD);
  jclass result = NULL;

  result = find_class_from_class_loader(env, sym, true, loader,
    protection_domain, true, thread);
  
  oop tmp = JNIHandles::resolve_non_null(file);
  typeArrayOop a = typeArrayOop(JNIHandles::resolve_non_null(file));

  int len = a->length();
  jbyte *bytes = NEW_C_HEAP_ARRAY(jbyte, len);

  memcpy(bytes, a->byte_at_addr(0), sizeof(jbyte)*len);

  jvmtiClassDefinition class_definitions;
  class_definitions.klass = result;
  class_definitions.class_byte_count = len;
  class_definitions.class_bytes = (unsigned char *)bytes;

  VM_RedefineClasses op(1, &class_definitions, jvmti_class_load_kind_redefine);
  VMThread::execute(&op);
  FREE_C_HEAP_ARRAY(jbyte, bytes);

JVM_END



Dictionary*   JDUS::_jdus_dictionary = NULL;
JDUSThread*   JDUS::_jdus_thread = NULL;
methodOop     JDUS::_implicit_update_method = NULL;
klassOop      JDUS::_developer_interface_klass = NULL;

DSU*          JDUS::_first_dsu = NULL;
DSU*          JDUS::_last_dsu = NULL;
volatile DSU* JDUS::_active_dsu = NULL;
int           JDUS::_latest_rn =  0;
const int     JDUS::MAX_REVISION_NUMBER = 100;

klassOop JDUS::developer_interface_klass(){
  return _developer_interface_klass;
}

// TODO this should be synchronized
void JDUS::set_active_dsu(DSU* dsu){
  assert(Thread::current()->is_JDUS_thread(), "sanity");
  assert(_active_dsu == NULL, "sanity");
  _active_dsu = dsu;
}

DSU* JDUS::active_dsu() {
  return (DSU*)_active_dsu;
}

void JDUS::finish_active_dsu(){
  assert(SafepointSynchronize::is_at_safepoint(), "sanity" );
  assert(_active_dsu != NULL, "sanity");

  install_dsu(active_dsu());

  _active_dsu = NULL;

  increment_system_rn();
}

void JDUS::discard_active_dsu(){
  assert(SafepointSynchronize::is_at_safepoint(), "sanity" );
  assert(_active_dsu != NULL, "sanity");

  delete _active_dsu;
  _active_dsu = NULL;
}

DSU* JDUS::get_DSU(int from_rn){
  if(_first_dsu == NULL){
    return NULL;
  }

  for(DSU* dsu = _first_dsu; dsu!=NULL; dsu=dsu->next()){
    if(dsu->from_rn() == from_rn){
      return dsu;
    }
  }
  return NULL;
}

void JDUS::create_DevelopInterface_klass(TRAPS) {

  // create constantPool

  klassOop super_klass = SystemDictionary::Object_klass();
  instanceKlassHandle super(THREAD,super_klass);
  objArrayHandle local_interfaces(THREAD, Universe::the_empty_system_obj_array());
  typeArrayHandle fields (THREAD, Universe::the_empty_short_array());

  // create methods
  // 1. <init>
  // 2. invokeDSU
  enum {
    init_index = 0,
    invokeDSU_index =1 ,
    redefine_index = 2,
    getmixthat_index = 3,
    replaceObject_index = 4,
    crn_index = 5,
    total_methods
  };
  objArrayHandle methods;

  {
    objArrayOop m = oopFactory::new_system_objArray(total_methods, CHECK);
    methods = objArrayHandle(THREAD, m);
  }

  methodHandle m_init;
  {
    methodOop moop_init = oopFactory::new_method(
      5, // 0 aload_0; 1 invokespecial Object.<init>; 4 return;
      accessFlags_from(JVM_ACC_PRIVATE),
      0,
      0,
      0,
      methodOopDesc::IsSafeConc,
      THREAD);
    m_init = methodHandle(THREAD, moop_init);

  }

  methodHandle m_invokeDSU;
  {
    methodOop moop_invokeDSU = oopFactory::new_method(
      0,
      accessFlags_from( JVM_ACC_PUBLIC | JVM_ACC_STATIC | JVM_ACC_NATIVE),
      0,
      0,
      0,
      methodOopDesc::IsSafeConc,
      CHECK);
    m_invokeDSU = methodHandle(THREAD, moop_invokeDSU);
  }

  methodHandle m_redefine;
  {
    methodOop moop_redefine = oopFactory::new_method(
      0,
      accessFlags_from( JVM_ACC_PUBLIC | JVM_ACC_STATIC | JVM_ACC_NATIVE),
      0,
      0,
      0,
      methodOopDesc::IsSafeConc,
      CHECK);
    m_redefine = methodHandle(THREAD, moop_redefine);
  }

  methodHandle m_getmixthat;
  {
    methodOop moop_getmixthat = oopFactory::new_method(
      0,
      accessFlags_from( JVM_ACC_PUBLIC | JVM_ACC_STATIC | JVM_ACC_NATIVE),
      0,
      0,
      0,
      methodOopDesc::IsSafeConc,
      CHECK);
    m_getmixthat = methodHandle(THREAD, moop_getmixthat);
  }

  methodHandle m_replaceObject;
  {
    methodOop moop_repalceObject = oopFactory::new_method(
      0,
      accessFlags_from( JVM_ACC_PUBLIC | JVM_ACC_STATIC | JVM_ACC_NATIVE),
      0,
      0,
      0,
      methodOopDesc::IsSafeConc,
      CHECK);
    m_replaceObject = methodHandle(THREAD, moop_repalceObject);
  }

  methodHandle m_crn;

  {
    methodOop moop_crn = oopFactory::new_method(
      0,
      accessFlags_from( JVM_ACC_PUBLIC | JVM_ACC_STATIC | JVM_ACC_NATIVE),
      0,
      0,
      0,
      methodOopDesc::IsSafeConc,
      CHECK);
    m_crn = methodHandle(THREAD, moop_crn);
  }


  enum {
    MethodRef_Object_init_index = 1,
    Class_DeveloperInterface_index,
    Class_Object_index,
    Symbol_init_name_index,
    Symbol_init_sig_index,
    NameAndType_init_index,
    Symbol_invokeDSU_name_index,
    Symbol_invokeDSU_sig_index,
    Symbol_redefine_name_index,
    Symbol_redefine_sig_index,
    Symbol_getMixThat_name_index,
    Symbol_getMixThat_sig_index,
    Symbol_replaceObject_name_index,
    Symbol_replaceObject_sig_index,
    Symbol_crn_name_index,
    Symbol_crn_sig_index,
    Limit
    //    Symbol_invokeDSU_signature_index = Symbol_init_sig_index,
  };


  constantPoolHandle cp;
  {
    constantPoolOop cp_oop = oopFactory::new_constantPool(Limit,
      methodOopDesc::IsSafeConc,
      CHECK);
    cp = constantPoolHandle(THREAD, cp_oop);
  }

  cp->method_at_put(MethodRef_Object_init_index,Class_Object_index,NameAndType_init_index);
  //cp->klass_at_put(Class_DeveloperInterface_index,NULL);
  cp->klass_at_put(Class_Object_index, SystemDictionary::Object_klass());
  cp->symbol_at_put(Symbol_init_name_index, vmSymbols::object_initializer_name());
  cp->symbol_at_put(Symbol_init_sig_index, vmSymbols::void_method_signature());
  cp->name_and_type_at_put(NameAndType_init_index,Symbol_init_name_index,Symbol_init_sig_index);
  cp->symbol_at_put(Symbol_invokeDSU_name_index, vmSymbols::invokeDSU_name());
  cp->symbol_at_put(Symbol_invokeDSU_sig_index, vmSymbols::string_boolean_void_signature());
  cp->symbol_at_put(Symbol_redefine_name_index, vmSymbols::redefineSingleClass_name());
  cp->symbol_at_put(Symbol_redefine_sig_index, vmSymbols::string_byte_array_signature());
  cp->symbol_at_put(Symbol_getMixThat_name_index, vmSymbols::getMixThat_name());
  cp->symbol_at_put(Symbol_getMixThat_sig_index, vmSymbols::object_object_signature());
  cp->symbol_at_put(Symbol_replaceObject_name_index, vmSymbols::replaceObject_name());
  cp->symbol_at_put(Symbol_replaceObject_sig_index, vmSymbols::object_object_object_signature());
  cp->symbol_at_put(Symbol_crn_name_index, vmSymbols::crn_name());
  cp->symbol_at_put(Symbol_crn_sig_index, vmSymbols::void_int_signature());

  m_init->set_constants(cp());
  m_init->set_name_index(Symbol_init_name_index);
  m_init->set_signature_index(Symbol_init_sig_index);
  m_init->set_exception_table(Universe::the_empty_int_array());

  unsigned char code[5] = {
    Bytecodes::_aload_0,
    Bytecodes::_invokespecial,
    0x00,
    (unsigned char) MethodRef_Object_init_index,
    Bytecodes::_return
  };
  m_init->set_code(code);
  m_init->compute_size_of_parameters(THREAD);

  m_invokeDSU->set_constants(cp());
  m_invokeDSU->set_name_index(Symbol_invokeDSU_name_index);
  m_invokeDSU->set_signature_index(Symbol_invokeDSU_sig_index);
  m_invokeDSU->set_exception_table(Universe::the_empty_int_array());
  m_invokeDSU->compute_size_of_parameters(THREAD);

  m_redefine->set_constants(cp());
  m_redefine->set_name_index(Symbol_redefine_name_index);
  m_redefine->set_signature_index(Symbol_redefine_sig_index);
  m_redefine->set_exception_table(Universe::the_empty_int_array());
  m_redefine->compute_size_of_parameters(THREAD);

  m_getmixthat->set_constants(cp());
  m_getmixthat->set_name_index(Symbol_getMixThat_name_index);
  m_getmixthat->set_signature_index(Symbol_getMixThat_sig_index);
  m_getmixthat->set_exception_table(Universe::the_empty_int_array());
  m_getmixthat->compute_size_of_parameters(THREAD);

  m_replaceObject->set_constants(cp());
  m_replaceObject->set_name_index(Symbol_replaceObject_name_index);
  m_replaceObject->set_signature_index(Symbol_replaceObject_sig_index);
  m_replaceObject->set_exception_table(Universe::the_empty_int_array());
  m_replaceObject->compute_size_of_parameters(THREAD);

  m_crn->set_constants(cp());
  m_crn->set_name_index(Symbol_crn_name_index);
  m_crn->set_signature_index(Symbol_crn_sig_index);
  m_crn->set_exception_table(Universe::the_empty_int_array());
  m_crn->compute_size_of_parameters(THREAD);


  methods->obj_at_put(init_index,m_init());
  methods->obj_at_put(invokeDSU_index,m_invokeDSU());
  methods->obj_at_put(redefine_index,m_redefine());
  methods->obj_at_put(getmixthat_index,m_getmixthat());
  methods->obj_at_put(replaceObject_index,m_replaceObject());
  methods->obj_at_put(crn_index,m_crn());

  //set up entry
  //m_invokeDSU->link_method(m_invokeDSU,CHECK);

  instanceKlassHandle ikh;
  {
    klassOop ik = oopFactory::new_instanceKlass(
      super->vtable_length(),
      super->itable_length(),
      0,
      super->nonstatic_oop_map_count(),
      super->reference_type(),
      CHECK
      );
    ikh = instanceKlassHandle(THREAD, ik);

    ///
    _developer_interface_klass = ikh();
  }
  ikh->set_class_loader(NULL);
  ikh->set_access_flags(accessFlags_from( JVM_ACC_PUBLIC ));
  ikh->set_should_verify_class(false);
  ikh->set_layout_helper(super->layout_helper());
  //ikh->set_class_loader(NULL);


  ikh->set_nonstatic_field_size(super->nonstatic_field_size());
  ikh->set_has_nonstatic_fields(super->has_nonstatic_fields());
  ikh->set_static_oop_field_size(0);
  cp->set_pool_holder(ikh());
  ikh->set_constants(cp());
  ikh->set_local_interfaces(local_interfaces());
  ikh->set_fields(fields());
  ikh->set_methods(methods());
  ikh->set_has_final_method();

  ikh->set_method_ordering(Universe::the_empty_int_array());
  // The instanceKlass::_methods_jmethod_ids cache and the
  // instanceKlass::_methods_cached_itable_indices cache are
  // both managed on the assumption that the initial cache
  // size is equal to the number of methods in the class. If
  // that changes, then instanceKlass::idnum_can_increment()
  // has to be changed accordingly.
  ikh->set_initial_method_idnum(methods->length());
  ikh->set_name(vmSymbols::org_javelus_DeveloperInterface());
  cp->klass_at_put(Class_DeveloperInterface_index, ikh());
  ikh->set_protection_domain(super->protection_domain());

  ikh->set_transitive_interfaces(super->transitive_interfaces());

  ikh->set_minor_version(0);
  ikh->set_major_version(50);

  ikh->set_force_update(false);
  ikh->set_born_rn(0);
  ikh->set_dead_rn(JDUS::MAX_REVISION_NUMBER);
  ikh->set_copy_to_size(ikh->size_helper());
  ikh->set_inner_classes(Universe::the_empty_short_array());

  ikh->initialize_supers(super(), CHECK);


  //m_invokeDSU->init_intrinsic_id();
  //m_redefine->init_intrinsic_id();
  //m_getmixthat->init_intrinsic_id();

  SystemDictionary::define_instance_class(ikh,THREAD);
  //--- finish load

  // link
  ikh->link_class(THREAD);
  ikh->initialize(THREAD);


  //register native method
  m_invokeDSU->set_native_function(
    CAST_FROM_FN_PTR(address, &InvokeDSU),
    methodOopDesc::native_bind_event_is_interesting);

  m_redefine->set_native_function(
    CAST_FROM_FN_PTR(address, &RedefineSingleClass),
    methodOopDesc::native_bind_event_is_interesting);

  m_getmixthat->set_native_function(
    CAST_FROM_FN_PTR(address, &GetMixThat),
    methodOopDesc::native_bind_event_is_interesting);

  m_getmixthat->set_native_function(
    CAST_FROM_FN_PTR(address, &ReplaceObject),
    methodOopDesc::native_bind_event_is_interesting);

  m_crn->set_native_function(
    CAST_FROM_FN_PTR(address, &CurrentRevisionNumber),
    methodOopDesc::native_bind_event_is_interesting);


}




void JDUS::initialize(TRAPS) {
  create_DevelopInterface_klass(CHECK);
  JDUSEagerUpdate::initialize(CHECK);
}



void JDUS::jdus_thread_init() {
  _jdus_thread            = make_jdus_thread("JDUS Thread");
  _jdus_dictionary        = new Dictionary(100);
}

//Currently, we use two pass iteration to update a single thread..
//Why? I don't know.
// In first pass, we use a vframeStream.
// In second pass, we use a stackFrame
// check_single_thread is only used for policy THREAD_SAFE_POINT.
// So any target rn must be less or equal than 
bool JDUS::check_single_thread(JavaThread * thread){
  assert(thread == JavaThread::current(),"Only the thread itself can update itself");
  int from_rn = thread->current_revision_number();
  int to_rn   = from_rn + 1;

  if(to_rn > JDUS::system_revision_number()){
    // it is a invalid update...
    return false;
  }

  bool do_set_barrier= false;

  bool thread_safe = true;

  // The return_barrier_id has been cleared before calling this method.
  //assert(thread->return_barrier_id() != NULL, "If it is old and it must have return barrier");

  HandleMark hm(thread);
  for(vframeStream vfst(thread); !vfst.at_end(); vfst.next()) {
    methodHandle method (thread,vfst.method());

    if(!method->method_holder()->klass_part()->oop_is_instance()){
      continue;
    }

    instanceKlassHandle ik (thread, method->method_holder());


    if(ik->dead_rn() == to_rn){
      // If the method is changed, it is restricted for any updating
      // XXX changed methods must have already been marked dead here.
      // Changed method must die at to_rn
      if (method->is_restricted_method() ) {
        thread_safe = false;
        // Here we install return barrier
        // TODO Here we just set the id and will install it at repair_thread
        // set barrier at caller if possible
        do_set_barrier = true;
      }else {
        if(do_set_barrier){
          thread->set_return_barrier_id(vfst.frame_id());
        }

        // Here we must check whether the frame is a optimized dead method.
        // If it is we must deoptimize current frame
        if(method->is_native()){
          continue;
        }else if(vfst.is_interpreted_frame()){
          continue;
        }else {
          // Here the method is a compiled dead method
          // XXX the method will do a re-walk from top to find the frame and ....
          Deoptimization::deoptimize_frame(thread,vfst.frame_id());
        }
      }
    }else if(do_set_barrier){
      thread->set_return_barrier_id(vfst.frame_id());
    }

  }// end of frame iteration

  //if(!thread_safe){
  //	tty->print_cr("Unsafe to update");
  //	thread->print_stack_on(tty);
  //}

  return thread_safe;
}

void JDUS::repair_single_thread(JavaThread * thread){
  assert(thread == JavaThread::current(),"only the thread itself can repair itself.");

  //  tty->print_cr("repair single thread");

  int from_rn = thread->current_revision_number();
  // note we 
  int to_rn   = from_rn;// + 1;

  frame * current = NULL;
  frame * callee = NULL;
  StackFrameStream callee_fst(thread);
  int count = 0;
  for(StackFrameStream fst(thread); !fst.is_done(); fst.next()) {

    current = fst.current();

    if(count>0){
      callee = callee_fst.current();
      callee_fst.next();
    }

    count ++;
    if(current->is_interpreted_frame()){

      ResourceMark rm;

      // update method and constantpoolcache
      methodHandle method (thread,current->interpreter_frame_method());



      // XXX We do not know
      instanceKlassHandle ik (thread, method->method_holder());

      //TODO
      //We must set up the old flag during VM_JDUSOperation
      if(ik->dead_rn() == to_rn){
        // TODO replace loosely restricted method
        // Here we can not allow any changed method
        assert( !method->is_restricted_method(), "No restricted method here !!");
        // tty->print_cr("repair single thread %s", method->name_and_sig_as_C_string());
        //TODO Currently We on-stack-replace the loosely-stricted method with next matched version.
        instanceKlassHandle new_ik (thread, ik->next_version());
        methodHandle new_method (thread, new_ik->find_method(method->name(), method->signature()));
        assert(new_method.not_null(),"loosely restricted method must have a new version");
        int bci = current->interpreter_frame_bci();


        assert(new_method.not_null(), "old non-rm should have a matched version");
        assert(new_method->is_perm(),"must be perm");

        methodOopDesc::build_interpreter_method_data(new_method, thread);
        if(thread->has_pending_exception()){
          JDUS_WARN(("build interpreter method data meets exception in repair_single_thread"));
        }
        current->interpreter_frame_set_method(new_method());
        current->interpreter_frame_set_bcp(new_method->bcp_from(bci));
        *(current->interpreter_frame_cache_addr()) = new_method->constants()->cache();

        // TODO, see deoptimzation.
        // We must clear the resolved flags in cp cache.
        // But we must preserve the size of parameters.

        if(callee != NULL && callee->is_interpreted_frame()){
          Bytecodes::Code code  = Bytecodes::code_at(method(),method->bcp_from(bci));

          //if(!(code >= Bytecodes::_invokevirtual  && code <= Bytecodes::_invokedynamic)){
          //  tty->print_cr("code is %d, bci is  %d", code, bci);
          //  current->print_value();
          //  callee->print_value();
          //  method->print_codes();
          //}
          if(code >= Bytecodes::_invokevirtual  && code <= Bytecodes::_invokedynamic){
            Bytecodes::Code new_code  = Bytecodes::code_at(new_method(),new_method->bcp_from(bci));

            assert(code==new_code, "new code must be old code");

            int old_entry_index = Bytes::get_native_u2((address)method->bcp_from(bci) +1);
            int new_entry_index = Bytes::get_native_u2((address)new_method->bcp_from(bci) +1);
            ConstantPoolCacheEntry * new_entry = new_method->constants()->cache()->entry_at(new_entry_index);

            if (!new_entry->is_resolved(code)){
              ConstantPoolCacheEntry * old_entry = method->constants()->cache()->entry_at(old_entry_index);
              constantPoolCacheOopDesc::copy_method_entry(
                method->constants()->cache(),
                old_entry_index,
                new_method->constants()->cache(),
                new_entry_index,
                thread
                );
            }else {
              JDUS_WARN(("[JDUS] Thread repair finds a resolved new entry"));
            }
          }else {
            assert(bci == 0 || code == Bytecodes::_monitorenter,"other cases");
          }

        }
      }
    }else if(current->is_compiled_frame()){

    }else if(current->is_native_frame()){

    }
  }// end for frame walk loop

}

void JDUS::install_return_barrier_all_threads(){
  for (JavaThread* thr = Threads::first(); thr != NULL; thr = thr->next()) {
    if(thr->is_Compiler_thread()){

      continue;
    }
    //thread is walkable
    intptr_t * barrier = thr->return_barrier_id();
    if(barrier == NULL){
      continue;
    }
    if(thr->has_last_Java_frame()){
      install_return_barrier_single_thread(thr,barrier);
    }
  }
}

// the barrier is the id of the rm.
// We should replace its caller's pc.
void JDUS::install_return_barrier_single_thread(JavaThread * thread, intptr_t * barrier){
  assert(thread->return_barrier_id()==barrier,"just check return barrier id.");

  for(StackFrameStream fst(thread); !fst.is_done(); fst.next()) {
    frame * current = fst.current();
    if(current->id()== barrier){

      // XXX we only replace return address of interpreter frame???.
      // The compiled frame was replaced by Deoptimization.
      //if(! current->is_interpreted_frame()){
      //  return ;
      //}

      // TODO Note here we only support wake_up DSU and eager update RBType
      thread->set_return_barrier_type(JavaThread::_wakeup_dsu);

      if(current->is_interpreted_frame()){
        // 1). fetch the Bytecode
        // 
        Bytecodes::Code code = Bytecodes::code_at(current->interpreter_frame_method(),current->interpreter_frame_bcp());

        TosState tos = as_TosState(current->interpreter_frame_method()->result_type());
        address barrier_addr;
        if(code == Bytecodes::_invokedynamic 
          || code == Bytecodes::_invokeinterface){
            barrier_addr = Interpreter::return_entry_with_barrier(tos,5);
        }else {
          barrier_addr = Interpreter::return_entry_with_barrier(tos,3);
        }

        //tty->print_cr("[JDUS] patch pc of return barrier");

        current->patch_pc(thread,barrier_addr);


      }else if(current->is_compiled_frame()){
        // Do nothing.
        // XXX set the address of
        if(current->is_deoptimized_frame()){
          // We do not depotimize a deoptimized frame.
          //tty->print_cr("[JDUS] return barrier is deoptimized frame");
        }else {
          // We just deoptimize it
          //tty->print_cr("[JDUS] return barrier is compiled frame");
          Deoptimization::deoptimize(thread,*current,fst.register_map());
        }
      }


      return;
    }
  }
}


// This method will check all application threads.
// If there exists restricted method on stack
// XXX remember!!
// Return true if it is system-wide safe.
// System-wide safe <==> all threads are safe to update to system revision number.
bool JDUS::check_application_threads(jdusPolicy policy) {
  assert(SafepointSynchronize::is_at_safepoint(),
    "DSU safepoint must be in VM safepoint");

  bool do_print = JDUS_TRACE_ENABLED(0x00000080);

  // In default, to revision number is current system revision number plus one.
  int sys_from_rn = JDUS::system_revision_number();
  int sys_to_rn = sys_from_rn + 1;
  bool sys_safe = true; 

  ResourceMark rm;

  for (JavaThread* thr = Threads::first(); thr != NULL; thr = thr->next()) {

    if(!thr->has_last_Java_frame()){
      continue;
    }/*else {
     tty->print_cr("JavaThread::");
     }*/

    bool thread_safe = true;
    int t_from_rn = thr->current_revision_number();

    // In default, the target rn will be increment by one.
    int t_to_rn   = t_from_rn + 1;

    if(do_print){
      tty->print_cr("[JDUS] Check thread %s.",thr->get_thread_name());
    }


    bool do_set_barrier = false;

    for(vframeStream vfst(thr); !vfst.at_end(); vfst.next()) {
      methodOop method = vfst.method();
      klassOop  holder = method->method_holder();

      instanceKlass *ik = instanceKlass::cast(holder);

      if( holder->klass_part()->oop_is_instance() ){
        //We only check instanceKlass
        //continue;
      }else {
        continue;
      }


      if( t_from_rn == sys_from_rn){
        // The thread is in the youngest version and will updated to a new version


        assert(ik->dead_rn() >= sys_to_rn, "the method must be alive here");

        // If the method is changed, it is restricted for any updating
        if (method->is_restricted_method()) {
          thread_safe = false;
          // Here we install return barrier
          // TODO Here we just set the id and will install it at repair_thread
          if(do_print){
            tty->print_cr(" * [%s] - [%d,%d)",method->name_and_sig_as_C_string(),ik->born_rn(),ik->dead_rn());
          }

          do_set_barrier = true;

        }else if(do_set_barrier){
          // Return barrier can only be put after the caller of the oldest restricted method.
          if(do_print){
            tty->print_cr(" + [%s] - [%d,%d)",method->name_and_sig_as_C_string(),ik->born_rn(),ik->dead_rn());
          }

          thr->set_return_barrier_id(vfst.frame_id());
        }else {
          if(do_print){
            tty->print_cr(" - [%s] - [%d,%d) I[%d]",method->name_and_sig_as_C_string(),ik->born_rn(),ik->dead_rn(),vfst.is_interpreted_frame());
          }


        }
      }else if(t_from_rn < sys_from_rn){
        //assert(thr->return_barrier_id() != NULL, "If it is old and it must have return barrier");
        if(do_print){
          tty->print_cr(" # [%s] - [%d,%d) I[%d]",method->name_and_sig_as_C_string(),ik->born_rn(),ik->dead_rn(),vfst.is_interpreted_frame());
        }

        thread_safe = false;
        break;
      }else {
        // t_from_rn must <= sys_from_rn
        ShouldNotReachHere();
      }
    }// end of thread walking

    if( !thread_safe
      /*t_to_rn != sys_to_rn*/){
        // We can not update all thread to the system to rn
        sys_safe = false;
    }

  }// end of all thread walking


  return sys_safe;
}

klassOop JDUS::resolve_jdus_klass_or_null(
  symbolHandle class_name,
  Handle class_loader,
  Handle protection_domain,
  /*bool is_superclass,*/
  TRAPS){
    //assert(THREAD->is_JDUS_thread(),"must be jdus thread");
    unsigned int d_hash = dictionary()->compute_hash(class_name, class_loader);
    int d_index = dictionary()->hash_to_index(d_hash);
    klassOop probe = dictionary()->find(d_index, d_hash, class_name, class_loader,
      protection_domain, THREAD);

    if (probe != NULL ){
      return probe;
    }

    // Check DSUClass again

    DSUClassLoader* dsu_class_loader = active_dsu()->find_class_loader_by_loader(class_loader);
    if (dsu_class_loader == NULL){
      return NULL;
    }

    DSUClass* dsu_class = dsu_class_loader->find_class_by_name(class_name);

    if (dsu_class == NULL){
      return NULL;
    }
    

    // resolve new version
    instanceKlassHandle new_version;
    dsu_class->resolve_new_version(new_version, CHECK_NULL);

    assert(new_version.not_null(), "sanity check");

    return new_version();
}

jdusClassUpdatingType JDUS::get_updating_type(instanceKlassHandle ikh, TRAPS){
  HandleMark hm(THREAD);

  symbolHandle class_name(THREAD, ikh->name());
  Handle class_loader(THREAD, ikh->class_loader());

  DSUClass* dsu_class = active_dsu()->find_class_by_name_and_loader(class_name, class_loader);

  if (dsu_class != NULL && dsu_class->prepared()){
    return dsu_class->updating_type();
  }

  return JDUS_CLASS_UNKNOWN;
}
  

void JDUS::prepare_super_class(instanceKlassHandle ikh, TRAPS){
  HandleMark hm(THREAD);

  instanceKlassHandle super_class(THREAD, ikh->super());
  assert(super_class.not_null(), "no java.lang.Object class");

  symbolHandle class_name (THREAD, super_class->name());
  Handle class_loader (THREAD, super_class->class_loader());
  DSUClass* dsu_class = active_dsu()->find_class_by_name_and_loader(class_name, class_loader);

  if (dsu_class != NULL){
    dsu_class->prepare(CHECK);

    assert(dsu_class->prepared(),"sanity now, ");
  }
  
}

void JDUS::prepare_interfaces(instanceKlassHandle ikh, TRAPS){
  HandleMark hm(THREAD);

  objArrayHandle interfaces (THREAD, ikh->local_interfaces());

  const int n_itfc = interfaces->length();
  for(int i=0; i<n_itfc; i++) {
    instanceKlassHandle itfc(THREAD, (klassOop)interfaces->obj_at(i));
    assert(itfc.not_null(), "no java.lang.Object class");

    symbolHandle class_name (THREAD, itfc->name());
    Handle class_loader (THREAD, itfc->class_loader());
    DSUClass* dsu_class = active_dsu()->find_class_by_name_and_loader(class_name, class_loader);

    if (dsu_class != NULL){
      dsu_class->prepare(CHECK);

      assert(dsu_class->prepared(),"sanity now, ");
    }
  } 
}

klassOop JDUS::resolve_jdus_klass(
  symbolHandle class_name,
  Handle class_loader,
  Handle protection_domain,
  /*bool is_superclass,*/
  TRAPS){
    klassOop klass = resolve_jdus_klass_or_null(class_name, class_loader, protection_domain, THREAD);
    if (HAS_PENDING_EXCEPTION || klass == NULL) {
      KlassHandle k_h(THREAD, klass);
      // can return a null klass
      klass = SystemDictionary::handle_resolution_exception(class_name, class_loader, protection_domain, true, k_h, THREAD);
    }
    return klass;
}

void JDUS::add_jdus_klass(instanceKlassHandle k, TRAPS) {

  assert(active_dsu() != NULL, "sanity check");

  MutexLocker ml(SystemDictionary_lock, THREAD);
  symbolHandle name_h(THREAD, k->name());
  Handle class_loader_h(THREAD, k->class_loader());
  //  unsigned int d_hash = dictionary()->compute_hash(name_h, class_loader_h);
  //  int d_index = dictionary()->hash_to_index(d_hash);
  dictionary()->add_klass(name_h, class_loader_h, k);
  if(!k->is_linked()){
    k->set_init_state(instanceKlass::linked);
  }  
}



class KlassRepairClosure: public OopClosure {
public:
  virtual void do_oop(oop* p) {
    klassOop k = (*p)->klass();
    if(k->is_method()){
      methodOop method = (methodOop) (*p);
      //TODO compiler interface may use old method as new method
      // It is a bug, but currently we donot have a nice method to solve it.
      if (method->is_old()){
        tty->print_cr("null method");
        //(*p) = JNIHandles::deleted_handle();
      }
    }
  }
  virtual void do_oop(narrowOop* p) {
    //  ShouldNotReachHere();
  }
};




// After install new classes, we can repair the stack to conform to the new world.
// XXX This method must be called at the end of redefining all classes.
// * We will increment the rn of safe thread
// * All replaced classes have been marked as dead.
void JDUS::repair_application_threads(jdusPolicy policy) {
  StackRepairClosure src;
  DoNothinCodeBlobClosure dcbc;

  int sys_from_rn = JDUS::system_revision_number();
  int sys_to_rn = sys_from_rn + 1;

  //bool do_print = true;
  Thread* thread = Thread::current();

  for (JavaThread* thr = Threads::first(); thr != NULL; thr = thr->next()) {
    //Set current thread..
    //src.set_thread(thr);

    //if(thr->is_Compiler_thread()){
    //  //      tty->print_cr("Compiler Thread");
    //  CompilerThread * compiler_thread = thr->as_CompilerThread();
    //  CompileTask * task = compiler_thread->task();
    //  if(task != NULL){
    //    methodOop method = (methodOop)JNIHandles::resolve(task->method_handle());
    //    if(method->is_old()){
    //      // TODO currently we does not know how to solve this bug..
    //      tty->print_cr("A compiling old method");
    //      ciEnv * env = compiler_thread->env();
    //      assert(env!=NULL, "env must not be NULL");
    //      env->record_failure("Compileing an old method");
    //    }
    //  }
    //  CompileQueue * queue = compiler_thread->queue();
    //  //tty->print_cr("Compiler Thread, queue size %d",queue->size());
    //  while(!queue->is_empty()){
    //    //        tty->print_cr("Pop out existing compiletask");
    //    CompileTask * task = queue->get();
    //    //delete task;
    //    //        task->print();
    //  }

    //  continue;
    //}

    //fetch the barrier
    intptr_t * barrier = thr->return_barrier_id();
    bool do_update_thread = barrier == NULL;
    int t_from_rn = thr->current_revision_number();
    // Each time, we can only do one step update.
    int t_to_rn   = t_from_rn + 1;

    if(t_to_rn != sys_to_rn){
      // The thread does not belong to current update
      // 
      continue;
    }



    if (thr->has_last_Java_frame()) {

      JDUS_TRACE(0x00000080,("Repair thread %s.",thr->get_thread_name()));

      frame * callee = NULL;
      StackFrameStream callee_fst(thr);
      int count = 0;
      for(StackFrameStream fst(thr); !fst.is_done(); fst.next()) {
        frame *current = fst.current();
        if(count > 0){
          callee = callee_fst.current();
          callee_fst.next();
        }
        count++;
        if(current->is_interpreted_frame()){
          // update method and constantpoolcache
          methodOop method = current->interpreter_frame_method();
          klassOop ik_oop = method->method_holder();
          instanceKlass *ik = (instanceKlass *)ik_oop->klass_part();

          JDUS_TRACE(0x00000080,(" - [%s] - [%d,%d) %d %d", method->name_and_sig_as_C_string(), ik->born_rn(), ik->dead_rn(), method->is_native(), method->is_old()));

          //TODO
          //We must set up the old flag during VM_JDUSOperation
          if(ik->dead_rn() == sys_to_rn){
            if(policy == JDUS_THREAD_SAFE_POINT
              && method->is_restricted_method()){
                // TODO we allow strictly restricted method for thread safe point policy
                // We will find the oldest changed frame and replace its caller's PC
                // The caller's PC(return address) is in the caller's frame and is just belower(above in fact) 
                // See patch_pc in runtime/frame.cpp

                // The only suituation that a RM can be active on stack..

            }else {
              // TODO replace loosely restricted method
              // Here we can not allow any changed method
              //tty->print("Replace stack method:");
              //method->print();
              assert( !method->is_restricted_method(), "No restricted method here !!");

              // XXX We do not know
              methodOop new_method = NULL;

              //TODO Currently We on-stack-replace the loosely-stricted method with next matched version.
              klassOop new_ik_oop = ik->next_version();
              instanceKlass *new_ik = (instanceKlass *)new_ik_oop->klass_part();
              new_method = new_ik->find_method(method->name(), method->signature());





              assert(new_method !=NULL,"loosely restricted method must have a new version");
              assert(new_method->is_method(),"must be a method");        
              assert(new_method->is_perm(),"must be perm");

              assert(!method->is_native(), "should not be native");
              assert(!new_method->is_native(), "should not be native");

              //methodOopDesc::build_interpreter_method_data(new_method, thread);
              //if (method->method_data() == NULL) {
              //  methodDataOop method_data = oopFactory::new_methodData(method, thread);
              //  if(thread->has_pending_exception()){
              //    JDUS_WARN(("Build method data error during repair application threads"));
              //    return;
              //  }
              //  method->set_method_data(method_data);
              //  assert(method_data->is_oop(),"must be oop");
              //}

              if(thread->has_pending_exception()){
                JDUS_WARN(("build interpreter method data meets exception in repair_single_thread"));
              }
              int bci = current->interpreter_frame_bci();
              current->interpreter_frame_set_method(new_method);
              current->interpreter_frame_set_bcp(new_method->bcp_from(bci));
              *(current->interpreter_frame_cache_addr()) = new_method->constants()->cache();



              //assert(caller_frame.is_interpreted_frame(), "other we cannot handle.");

              // TODO, see deoptimzation.
              // We must clear the resolved flags in cp cache.
              // But we must preserve the size of parameters.
              if(callee != NULL && callee->is_interpreted_frame()){

                // TODO callee may be a deoptimized interpreteed frame.

                Bytecodes::Code code  = Bytecodes::code_at(method,method->bcp_from(bci));

                // in fact bci may be monitor entry

                assert(bci>0, "can invokestatic be the first stmt.");

                if(!(code >= Bytecodes::_invokevirtual  && code <= Bytecodes::_invokedynamic)){
                  tty->print_cr("code is %d, bci is  %d", code, bci);
                  current->print_value();
                  method->print_codes();
                }

                assert(code >= Bytecodes::_invokevirtual  && code <= Bytecodes::_invokedynamic, "must be an invoke"  );
                int old_entry_index = Bytes::get_native_u2((address)method->bcp_from(bci) +1);
                int new_entry_index = Bytes::get_native_u2((address)new_method->bcp_from(bci) +1);
                assert(new_entry_index > 0, "new entry index must be greater than 0");

                Bytecodes::Code new_code  = Bytecodes::code_at(new_method,new_method->bcp_from(bci));

                assert(code==new_code, "new code must be old code");

                if(new_entry_index > new_method->constants()->cache()->length()){
                  tty->print_cr("%d %d", old_entry_index, new_entry_index);
                  new_method->print();
                  tty->print_cr("end print method");
                }

                assert(new_entry_index < new_method->constants()->cache()->length(), "new entry index must be lesser than cache length");


                ConstantPoolCacheEntry * new_entry = new_method->constants()->cache()->entry_at(new_entry_index);

                if (!new_entry->is_resolved(code)){
                  ConstantPoolCacheEntry * old_entry = method->constants()->cache()->entry_at(old_entry_index);
                  constantPoolCacheOopDesc::copy_method_entry(
                    method->constants()->cache(),
                    old_entry_index,
                    new_method->constants()->cache(),
                    new_entry_index,
                    thr
                    );

                }else {
                  ShouldNotReachHere();
                }
              }



            }
          }
        }
        else if(current->is_compiled_frame()){
          CodeBlob * cb = current->cb();
          if(cb->is_nmethod()){
            nmethod* nm = (nmethod*)cb;
            methodOop m = nm->method();
            JDUS_TRACE(0x00000080,(" - [%s] )", m->name_and_sig_as_C_string()));
          }
        }else if(current->is_native_frame()){

        }
        //current->oops_do(&src, &dcbc, fst.register_map());
        //callee = current;
      }// end for frame walk loop

      //At last we can set the rn of the thread to sys_to_rn
      // TODO
      if(do_update_thread){
        thr->increment_revision_number();
        //tty->print_cr("[JDUS] Thread is udpated to %d.",thr->current_revision_number());
        assert(thr->current_revision_number()==sys_to_rn,"Thread must be updated after update stack");
      }else {
        //tty->print_cr("[JDUS] Thread is not udpated, crn is %d.",thr->current_revision_number());
      }

    }else {
      //tty->print_cr("[JDUS] Un-walkable Java thread...");
      //thr->print();
      //tty->cr();
    }

    // update handles
    //thr->active_handles()->oops_do(&src);
    //thr->handle_area()->oops_do(&src);
  }
}

void JDUS::repatch_method(methodOop method,bool print_replace, TRAPS){
  // We cannot tolerate a GC in this block, because we've
  // cached the bytecodes in 'code_base'. If the methodOop
  // moves, the bytecodes will also move.
  No_Safepoint_Verifier nsv;
  Bytecodes::Code c;
  Bytecodes::Code java_c;
  // Bytecodes and their length
  const address code_base = method->code_base();
  const int code_length = method->code_size();

  ResourceMark rm(THREAD);

  if(print_replace){
    tty->print_cr("[JDUS] Re-patch VM bytecode to Java byteocode of method %s.", method->name_and_sig_as_C_string());
  }

  int bc_length;
  for (int bci = 0; bci < code_length; bci += bc_length) {
    address bcp = code_base + bci;

    c = (Bytecodes::Code)(*bcp);
    java_c = Bytecodes::java_code(c);


    switch(c){
    case     Bytecodes::_fast_agetfield:
    case     Bytecodes::_fast_bgetfield:
    case     Bytecodes::_fast_cgetfield:
    case     Bytecodes::_fast_dgetfield:
    case     Bytecodes::_fast_fgetfield:
    case     Bytecodes::_fast_igetfield:
    case     Bytecodes::_fast_lgetfield:
    case     Bytecodes::_fast_sgetfield:

    case     Bytecodes::_fast_aputfield:
    case     Bytecodes::_fast_bputfield:
    case     Bytecodes::_fast_cputfield:
    case     Bytecodes::_fast_dputfield:
    case     Bytecodes::_fast_fputfield:
    case     Bytecodes::_fast_iputfield:
    case     Bytecodes::_fast_lputfield:
    case     Bytecodes::_fast_sputfield:

    case     Bytecodes::_fast_aload_0  :
    case     Bytecodes::_fast_iaccess_0:
    case     Bytecodes::_fast_aaccess_0:
    case     Bytecodes::_fast_faccess_0:

    case     Bytecodes::_fast_iload    :
    case     Bytecodes::_fast_iload2   :
    case     Bytecodes::_fast_icaload  :
      {
        if(print_replace){
          tty->print_cr(" - replace %s with %s at %d, bc length = %d",Bytecodes::name(c),Bytecodes::name(java_c),bci, bc_length);
        }
        (*bcp) = java_c;
        break;
      }


    }



    // Since we have the code, see if we can get the length
    // directly. Some more complicated bytecodes will report
    // a length of zero, meaning we need to make another method
    // call to calculate the length.
    bc_length = Bytecodes::length_for(java_c);
    if (bc_length == 0) {
      bc_length = Bytecodes::length_at(method, bcp);

      // length_at will put us at the bytecode after the one modified
      // by 'wide'. We don't currently examine any of the bytecodes
      // modified by wide, but in case we do in the future...
      if (c == Bytecodes::_wide) {

        c = (Bytecodes::Code)bcp[1];
      }
    }

    assert(bc_length != 0, "impossible bytecode length");
  }
}


oop JDUS::merge_mixobjects(oop old_obj, oop new_obj) {
  return SharedRuntime::merge_mixobjects(old_obj,new_obj);
}

oop JDUS::merge_mixobjects(oop old_obj) {
  return SharedRuntime::merge_mixobjects(old_obj,old_obj->that());
}


void JDUS::jdus_thread_loop() {
  JDUSThread * thread = JDUSThread::current();

  ResourceMark rm;

  while(true){
    JDUSTask * task = thread->next_task();

    if(task == NULL){
      tty->print_cr("[JDUS] Fetch task error!!");
      continue;
    }

    while(true) {

      // any time ,there is only one VM_JDUSOperation in processing.
      // So any access to the static fields of class JDUS from VM_JDUSOperation 
      // has no need of any synchronization.

      VM_JDUSOperation * op = task->operation();
      VMThread::execute(op);
      JDUS::notifyRequest();

      if (op->is_finished()){
        // Request is finished..
        tty->print_cr("[JDUS] DSU Request is finished");
        break;
      }else if(op->is_discarded()){
        // Request is discarded..
        tty->print_cr("[JDUS] DSU Request is discarded");
        break;
      }else if(op->is_interrupted()){
        tty->print_cr("[JDUS] DSU Request is interrupted");
      }else {
        tty->print_cr("[JDUS] Unexpected result for DSU Request.");
        break;
      }

      // wait until current task finished or discarded
      // The op being executed will notify me.
      // TODO timeout doest not implement yet!!!
      thread->sleep(1000);
    }

    delete task;
  }
}


// --------------------------- Synchronization Support -----------------------------


void JDUS::wakeup_JDUS_thread(){
  JDUS::get_jdus_thread()->wakeup();
}


void JDUS::waitRequest(){
  MutexLocker locker(JDUSRequest_lock);
  JDUSRequest_lock->wait();
}

void JDUS::notifyRequest(){
  MutexLocker locker(JDUSRequest_lock);
  JDUSRequest_lock->notify_all();
}




// -------------------------- End of Synchronization ------------------------------

JDUSThread* JDUS::make_jdus_thread(const char * name) {
  EXCEPTION_MARK;
  JDUSThread* jdus_thread = NULL;

  klassOop k =
    SystemDictionary::resolve_or_fail(vmSymbolHandles::java_lang_Thread(),
    true, CHECK_0);
  instanceKlassHandle klass (THREAD, k);
  instanceHandle thread_oop = klass->allocate_instance_handle(CHECK_0);
  Handle string = java_lang_String::create_from_str(name, CHECK_0);

  // Initialize thread_oop to put it into the system threadGroup
  Handle thread_group (THREAD,  Universe::system_thread_group());
  JavaValue result(T_VOID);
  JavaCalls::call_special(&result, thread_oop,
    klass,
    vmSymbolHandles::object_initializer_name(),
    vmSymbolHandles::threadgroup_string_void_signature(),
    thread_group,
    string,
    CHECK_0);

  {
    MutexLocker mu(Threads_lock, THREAD);
    jdus_thread = new JDUSThread();
    // At this point the new CompilerThread data-races with this startup
    // thread (which I believe is the primoridal thread and NOT the VM
    // thread).  This means Java bytecodes being executed at startup can
    // queue compile jobs which will run at whatever default priority the
    // newly created CompilerThread runs at.


    // At this point it may be possible that no osthread was created for the
    // JavaThread due to lack of memory. We would have to throw an exception
    // in that case. However, since this must work and we do not allow
    // exceptions anyway, check and abort if this fails.

    if (jdus_thread == NULL || jdus_thread->osthread() == NULL){
      vm_exit_during_initialization("java.lang.OutOfMemoryError",
        "unable to create new native thread");
    }

    java_lang_Thread::set_thread(thread_oop(), jdus_thread);

    // Note that this only sets the JavaThread _priority field, which by
    // definition is limited to Java priorities and not OS priorities.
    // The os-priority is set in the CompilerThread startup code itself
    java_lang_Thread::set_priority(thread_oop(), NearMaxPriority);
    // CLEANUP PRIORITIES: This -if- statement hids a bug whereby the compiler
    // threads never have their OS priority set.  The assumption here is to
    // enable the Performance group to do flag tuning, figure out a suitable
    // CompilerThreadPriority, and then remove this 'if' statement (and
    // comment) and unconditionally set the priority.

    // Compiler Threads should be at the highest Priority
    //    if ( CompilerThreadPriority != -1 )
    //      os::set_native_priority( jdus_thread, CompilerThreadPriority );
    //    else
    os::set_native_priority( jdus_thread, os::java_to_os_priority[NearMaxPriority]);

    // Note that I cannot call os::set_priority because it expects Java
    // priorities and I am *explicitly* using OS priorities so that it's
    // possible to set the compiler thread priority higher than any Java
    // thread.

    java_lang_Thread::set_daemon(thread_oop());

    jdus_thread->set_threadObj(thread_oop());
    Threads::add(jdus_thread);
    Thread::start(jdus_thread);
  }
  // Let go of Threads_lock before yielding
  os::yield(); // make sure that the compiler thread is started early (especially helpful on SOLARIS)

  return jdus_thread;
}


void JDUS::copy_fields(oop src, oop dst, instanceKlass* ik){
  typeArrayOop fields = ik->fields();
  int new_length = fields->length();
  for (int i=0; i<new_length; i+=instanceKlass::next_offset){
    int i_flags  = fields->ushort_at(i + instanceKlass::access_flags_offset);

    if((i_flags & JVM_ACC_STATIC) != 0){
      // skip static field
      continue;
    }

    int offset   = ik->offset_from_fields(i);
    int i_sig    = fields->ushort_at(i + instanceKlass::signature_index_offset);
    symbolOop s_sig = ik->constants()->symbol_at(i_sig);

    BasicType type = FieldType::basic_type(s_sig);
    assert((i_flags & JVM_ACC_IS_MIX_NEW_MEMBER) == 0,"Here we can not meet mix new field.");

    switch(type) {
    case T_BOOLEAN:
      dst->bool_field_put(offset,src->bool_field(offset));
      break;
    case T_BYTE:
      dst->byte_field_put(offset,src->byte_field(offset));
      break;
    case T_SHORT:
      dst->short_field_put(offset,src->short_field(offset));
      break;
    case T_CHAR:
      dst->char_field_put(offset,src->char_field(offset));
      break;
    case T_OBJECT:
    case T_ARRAY:
      dst->obj_field_raw_put(offset,src->obj_field(offset));
      break;
    case T_INT:
      dst->int_field_put(offset,src->int_field(offset));
      break;
    case T_LONG:
      dst->long_field_put(offset,src->long_field(offset));
      break;
    case T_FLOAT:
      dst->float_field_put(offset,src->float_field(offset));
      break;
    case T_DOUBLE:
      dst->double_field_put(offset,src->double_field(offset));
      break;
    case T_VOID:
      break;
    default:
      int size = type;
      if(size < 0){
        size = -size;
        memcpy(((char*)dst)+offset,((char*)src)+offset,size);
      }else {
        ShouldNotReachHere();
      }
    }
  }
}


void JDUS::copy_fields(Handle src, Handle dst, instanceKlassHandle ikh,TRAPS){
  typeArrayHandle fields (THREAD, ikh->fields());
  int new_length = fields->length();
  for (int i=0; i<new_length; i+=instanceKlass::next_offset){
    int i_flags  = fields->ushort_at(i + instanceKlass::access_flags_offset);

    if((i_flags & JVM_ACC_STATIC) != 0){
      // skip static field
      continue;
    }

    int offset   = ikh->offset_from_fields(i);
    int i_sig    = fields->ushort_at(i + instanceKlass::signature_index_offset);
    symbolOop s_sig = ikh->constants()->symbol_at(i_sig);

    BasicType type = FieldType::basic_type(s_sig);
    assert((i_flags & JVM_ACC_IS_MIX_NEW_MEMBER) == 0,"Here we can not meet mix new field.");

    JDUS::copy_field(src,dst,offset,offset,type);
  }
}

void JDUS::copy_fields(oop src, oop dst, oop mix_dst, instanceKlass* ikh){
  typeArrayOop fields = ikh->fields();
  int new_length = fields->length();

  for (int i=0; i<new_length; i+=instanceKlass::next_offset){

    int i_flags  = fields->ushort_at(i + instanceKlass::access_flags_offset);

    if((i_flags & JVM_ACC_STATIC) != 0){
      // skip static field
      continue;
    }
    int offset   = ikh->offset_from_fields(i);
    int i_sig    = fields->ushort_at(i + instanceKlass::signature_index_offset);
    symbolOop s_sig = ikh->constants()->symbol_at(i_sig);
    BasicType type = FieldType::basic_type(s_sig);
    if ((i_flags & JVM_ACC_IS_MIX_NEW_MEMBER)==0){
      JDUS::copy_field(src,dst,offset,offset,type);
    }else {
      JDUS::copy_field(src,mix_dst,offset,offset,type);
    }
  }
}

// copy field from src to dst
// src MUST not be MixObjects
// dst MAY be MixObjects
void JDUS::copy_fields(Handle src, Handle dst, Handle mix_dst, instanceKlassHandle ikh,TRAPS){
  typeArrayHandle fields (THREAD, ikh->fields());
  int new_length = fields->length();

  for (int i=0; i<new_length; i+=instanceKlass::next_offset){

    int i_flags  = fields->ushort_at(i + instanceKlass::access_flags_offset);

    if((i_flags & JVM_ACC_STATIC) != 0){
      // skip static field
      continue;
    }
    int offset   = ikh->offset_from_fields(i);
    int i_sig    = fields->ushort_at(i + instanceKlass::signature_index_offset);
    symbolOop s_sig = ikh->constants()->symbol_at(i_sig);
    BasicType type = FieldType::basic_type(s_sig);
    if ((i_flags & JVM_ACC_IS_MIX_NEW_MEMBER)==0){
      JDUS::copy_field(src,dst,offset,offset,type);
    }else {
      JDUS::copy_field(src,mix_dst,offset,offset,type);
    }
  }
}

void JDUS::realloc_decreased_obj(Handle obj, int old_size_in_bytes, int new_size_in_bytes){
  assert(obj->is_instance(),"we can only update instance objects.");
  int old_size_in_words = old_size_in_bytes >>LogBytesPerWord;
  int new_size_in_words = new_size_in_bytes >>LogBytesPerWord;
  int delta = old_size_in_words - new_size_in_words;
  //if(!(delta>0 && (delta % 2) == 0)){
  //	tty->print_cr("%d %d %d %d %d",delta, old_size_in_bytes,new_size_in_bytes,old_size_in_words,new_size_in_words);
  //}

  assert(delta>0 && (delta % 2) == 0, "old size must be larger than new size and with 8 alignment.");

  //tty->print_cr("[JDUS] Realloc obj with size delta %d.", delta);

  // old objects address
  HeapWord* hw = (HeapWord*) obj();
  // residual space head
  hw = hw + new_size_in_words;
  // Now hw is pointed to the end of obj.
  CollectedHeap::fill_with_objects(hw,delta);


#ifdef ASSERT
  oop o= oop(hw);
  assert(o->is_oop(),"fill ok");	
#endif
}

void JDUS::copy_field(oop src, oop dst, int src_offset, int dst_offset, BasicType type){
  switch(type) {
  case T_BOOLEAN:
    dst->bool_field_put(dst_offset,src->bool_field(src_offset));
    break;
  case T_BYTE:
    dst->byte_field_put(dst_offset,src->byte_field(src_offset));
    break;
  case T_SHORT:
    dst->short_field_put(dst_offset,src->short_field(src_offset));
    break;
  case T_CHAR:
    dst->char_field_put(dst_offset,src->char_field(src_offset));
    break;
  case T_OBJECT:
  case T_ARRAY:
    dst->obj_field_raw_put(dst_offset,src->obj_field(src_offset));
    break;
  case T_INT:
    dst->int_field_put(dst_offset,src->int_field(src_offset));
    break;
  case T_LONG:
    dst->long_field_put(dst_offset,src->long_field(src_offset));
    break;
  case T_FLOAT:
    dst->float_field_put(dst_offset,src->float_field(src_offset));
    break;
  case T_DOUBLE:
    dst->double_field_put(dst_offset,src->double_field(src_offset));
    break;
  case T_VOID:
    break;
  default:
    ShouldNotReachHere();
  }
}

void JDUS::copy_field(Handle src, Handle dst, int src_offset, int dst_offset, BasicType type){
  switch(type) {
  case T_BOOLEAN:
    dst->bool_field_put(dst_offset,src->bool_field(src_offset));
    break;
  case T_BYTE:
    dst->byte_field_put(dst_offset,src->byte_field(src_offset));
    break;
  case T_SHORT:
    dst->short_field_put(dst_offset,src->short_field(src_offset));
    break;
  case T_CHAR:
    dst->char_field_put(dst_offset,src->char_field(src_offset));
    break;
  case T_OBJECT:
  case T_ARRAY:
    dst->obj_field_raw_put(dst_offset,src->obj_field(src_offset));
    break;
  case T_INT:
    dst->int_field_put(dst_offset,src->int_field(src_offset));
    break;
  case T_LONG:
    dst->long_field_put(dst_offset,src->long_field(src_offset));
    break;
  case T_FLOAT:
    dst->float_field_put(dst_offset,src->float_field(src_offset));
    break;
  case T_DOUBLE:
    dst->double_field_put(dst_offset,src->double_field(src_offset));
    break;
  case T_VOID:
    break;
  default:
    ShouldNotReachHere();
  }
}

void JDUS::read_value(Handle obj, int offset, BasicType type, JavaValue &result,TRAPS){
  result.set_type(type);
  switch(type) {
  case T_BOOLEAN:
    result.set_jint(obj->bool_field(offset));
    break;
  case T_BYTE:
    result.set_jint(obj->byte_field(offset));
    break;
  case T_SHORT:
    result.set_jint(obj->short_field(offset));
    break;
  case T_CHAR:
    result.set_jint(obj->char_field(offset));
    break;
  case T_OBJECT:
  case T_ARRAY:
    result.set_jobject(JNIHandles::make_local(THREAD,obj->obj_field(offset)));
    break;
  case T_INT:
    result.set_jint(obj->int_field(offset));
    break;
  case T_LONG:
    result.set_jlong(obj->long_field(offset));
    break;
  case T_FLOAT:
    result.set_jfloat(obj->float_field(offset));
    break;
  case T_DOUBLE:
    result.set_jdouble(obj->double_field(offset));
    break;
  case T_VOID:
    break;
  default:
    ShouldNotReachHere();
  }
}

void JDUS::read_arg(Handle obj, int offset, BasicType type, JavaCallArguments &args, TRAPS){
  switch(type) {
  case T_BOOLEAN:
    args.push_int(obj->bool_field(offset));
    break;
  case T_BYTE:
    args.push_int(obj->byte_field(offset));
    break;
  case T_SHORT:
    args.push_int(obj->short_field(offset));
    break;
  case T_CHAR:
    args.push_int(obj->char_field(offset));
    break;
  case T_OBJECT:
  case T_ARRAY:
    args.push_oop(Handle(THREAD,obj->obj_field(offset)));
    break;
  case T_INT:
    args.push_int(obj->int_field(offset));
    break;
  case T_LONG:
    args.push_long(obj->long_field(offset));
    break;
  case T_FLOAT:
    args.push_float(obj->float_field(offset));
    break;
  case T_DOUBLE:
    args.push_double(obj->double_field(offset));
    break;
  case T_VOID:
    break;
  default:
    ShouldNotReachHere();
  }
}





void  JDUS::parse_old_field_annotation(
  instanceKlassHandle the_class,
  instanceKlassHandle transformer,
  typeArrayHandle annotations,
  typeArrayHandle &result,
  TRAPS){

    if (annotations.is_null()){
      return;
    }

    int length = annotations->length();

    /*
    The RuntimeVisibleParameterAnnotations attribute has the following
    format:
    RuntimeVisibleParameterAnnotations_attribute {
    u2 attribute_name_index;
    u4 attribute_length;
    u1 num_parameters;
    { u2 num_annotations;
    annotation annotations[num_annotations];
    } parameter_annotations[num_parameters];
    }

    */
    enum{
      attribute_name_length = 2,
      attribute_length_length = 4,
      num_parameters_length = 1,
      num_annotations_length = 2,
      type_index_length = 2,
      num_element_value_pairs_length = 2,
      element_name_index_length = 2,
      tag_length = 1,
      const_value_index_length = 2,
    };

    enum{
      num_parameters_index = 0,
      first_parameter_annotation_index = 1,
    };

    /*
    annotation {
    u2 type_index;
    u2 num_element_value_pairs;
    { u2 element_name_index;
    element_value value;
    } element_value_pairs[num_element_value_pairs]
    }
    element_value {
    u1 tag;
    union {
    u2 const_value_index;
    { u2 type_name_index;
    u2 const_name_index;
    } enum_const_value;
    u2 class_info_index;
    annotation annotation_value;
    { u2 num_values;
    element_value values[num_values];
    } array_value;
    } value;
    }
    */



    int num_parameters = annotations->bool_at(num_parameters_index);
    assert(num_parameters > 0 ,"must contains the old object at least" );

    u4 attribute_length = annotations->length();

    //tty->print_cr("[JDUS] Original annotation: ");
    //for(int i=0;i<annotations->length();i++){
    //  tty->print("%x ", annotations->bool_at(i));
    //}
    //tty->cr();
    //u4 attribute_length = Bytes::get_Java_u4((address)annotations->byte_at_addr(attribute_name_index));
    //tty->print_cr("[JDUS] Parse annotations, number of parameters %d, attribute length is %d",num_parameters, attribute_length);
    if (num_parameters > 1){
      // set up annotation
      result  =  typeArrayHandle (THREAD,
        oopFactory::new_permanent_intArray((num_parameters - 1)*instanceKlass::next_transformer_field,THREAD));

      u4 next_parameter_annotation_index = first_parameter_annotation_index;
      int transformer_field_index = 0;
      // the first parameter annotation

      int num_annotations = Bytes::get_Java_u2(annotations->bool_at_addr(next_parameter_annotation_index));
      //tty->print_cr("[JDUS] Parse annotations, next pararmeter annotation index %d, transformer field index is %d",next_parameter_annotation_index,transformer_field_index);
      if (num_annotations == 0){
        // the changed annotation
        // skip 'this' parameter
        next_parameter_annotation_index += num_annotations_length;
        //tty->print_cr("[JDUS] Parse annotations, next pararmeter annotation index %d, transformer field index is %d",next_parameter_annotation_index,transformer_field_index);
      }else {
        ShouldNotReachHere();
      }

      while(next_parameter_annotation_index < attribute_length){
        //tty->print_cr("[JDUS] Parse annotations, next pararmeter annotation index %d, transformer field index is %d",next_parameter_annotation_index,transformer_field_index);
        num_annotations = Bytes::get_Java_u2(annotations->bool_at_addr(next_parameter_annotation_index));
        //tty->print_cr("[JDUS] Parse annotations, number of annotations %d, transformer field index is %d",num_annotations,transformer_field_index);
        //int parameter_annotation_length = num_annotations_length;

        int next_annotation_index = next_parameter_annotation_index + num_annotations_length;

        assert(num_annotations == 1, "Only support parse OldField annotation");

        enum {
          type_index_index = 0,
          num_element_value_paris_index = 2,
        };

        int type_index = Bytes::get_Java_u2(annotations->bool_at_addr(next_annotation_index + type_index_index));
        //parameter_annotation_length += 2;
        int num_element_value_paris = Bytes::get_Java_u2(annotations->bool_at_addr(
          next_annotation_index + num_element_value_paris_index));
        //parameter_annotation_length += 2;

        assert(num_element_value_paris == 3, "see OldField ");

        enum{
          element_name_index_index = 0,  // 0
          // element value
          tag_index = element_name_index_index + element_name_index_length, // 2
          string_const_value_index_index = tag_index + tag_length, // 3
          next_element_pair_length = string_const_value_index_index + const_value_index_length, // 5
        };

        symbolOop holder = NULL;
        symbolOop name = NULL;
        symbolOop signature = NULL;

        int next_element_pair_index = next_annotation_index + type_index_length + num_element_value_pairs_length;
        for(int i=0;i<3;i++,next_element_pair_index += next_element_pair_length){
          int element_name_index = Bytes::get_Java_u2(annotations->bool_at_addr(next_element_pair_index + element_name_index_index));
          int tag = annotations->bool_at(next_element_pair_index + tag_index);
          int string_const_value_index = Bytes::get_Java_u2(annotations->bool_at_addr(next_element_pair_index + string_const_value_index_index));

          assert(tag == 's', "tag must be s");

          symbolOop element_name = transformer->constants()->symbol_at(element_name_index);
          if(element_name == vmSymbols::clazz_name()){
            holder = transformer->constants()->symbol_at(string_const_value_index);
          }else if (element_name == vmSymbols::name_name()){
            name = transformer->constants()->symbol_at(string_const_value_index);
          }else if (element_name == vmSymbols::signature_name()){
            signature = transformer->constants()->symbol_at(string_const_value_index);
          }else {
            ShouldNotReachHere();
          }
        }

        assert( holder!= NULL && name != NULL && signature != NULL, "the field ID must be full specified");

        instanceKlassHandle super (THREAD, the_class());
        fieldDescriptor fd;
        bool finded = false;
        while(super.not_null() && super->name() == holder){
          finded = super->find_local_field(name,signature,&fd);
          if(finded){
            break;
          }
          super = instanceKlassHandle(super->super());
        }
        if (finded){
          BasicType type = fd.field_type();
          int offset = fd.offset();
          result->int_at_put(transformer_field_index + instanceKlass::transformer_field_type,(int)type);
          if(fd.access_flags().is_mix_new_member()){
            // We use negative offset to indicate the field is a MixNewField.
            result->int_at_put(transformer_field_index + instanceKlass::transformer_field_offset,-offset);
          }else {
            result->int_at_put(transformer_field_index + instanceKlass::transformer_field_offset,offset);
          }
          transformer_field_index += instanceKlass::next_transformer_field;
        }else {
          ShouldNotReachHere();
        }
        next_parameter_annotation_index += 2 + 2 + 2 + 3 * (2 + 1 + 2);
      }
    }
}


void JDUS::oops_do(OopClosure* f){
  if(dictionary() != NULL){
    dictionary()->oops_do(f);
  }

  oop * developer_interface_klass_addr = (oop*)&_developer_interface_klass;
  f->do_oop(developer_interface_klass_addr);

}

void JDUS::merge_jdus_dictionary(TRAPS){
  // Here we just move klass from jdus dictionary to system dictionary.

  // XXX currently, this work has been done by the VM_JDUSOperation.

}

//No synchronization here.
//We only process one DSU request at a time.
void JDUS::increment_system_rn(){
  if(_latest_rn >= JDUS::MAX_REVISION_NUMBER){
    tty->print_cr("Revision Number Overflow!!!");
  }
  _latest_rn ++;
}

int JDUS::system_revision_number(){
  return _latest_rn;
}

void JDUS::install_dsu(DSU * dsu){
  // current we only can make active DSU unactive
  assert(dsu == active_dsu(), "sanity");

  dsu->set_next(NULL);

  if (_first_dsu == NULL){
    assert(_last_dsu == NULL, "sanity");
    _first_dsu = _last_dsu = dsu;    
    return ;
  }

  _last_dsu->set_next(dsu);
  _last_dsu = _last_dsu->next();  
}

bool JDUS::validate_DSU(DSU *dsu ){
  if(dsu == NULL){
    return false;
  }

  DSUClassLoader *dsu_loader = dsu->first_class_loader();
  for (; dsu_loader != NULL; dsu_loader = dsu_loader->next() ) {
    DSUClass* dsu_class = dsu_loader->first_class();
    for(;dsu_class!=NULL;dsu_class = dsu_class->next()){
      if (dsu_class== NULL){
        return false;
      }

      if (dsu_class->klass() == NULL && dsu_class->updating_type() != JDUS_CLASS_STUB) {
        tty->print_cr("Validate DSU: has no old class to update");
        return false;
      }

      /*if (dsu_class->methods_count() > 0 && dsu_class->methods() == NULL){
        tty->print_cr("Validate DSU: methods table is null");
        return false;
      }*/

      DSUMethod* dsu_method = dsu_class->first_method();
      for (;dsu_method!=NULL; dsu_method = dsu_method->next() ){
        if ( dsu_method->method() == NULL ){
          tty->print_cr("Validate DSU: method resolved failed");
          return false;
        }
      }


      if (dsu_class->updating_type() == JDUS_CLASS_MC 
        || dsu_class->updating_type() == JDUS_CLASS_NONE
        || dsu_class->updating_type() == JDUS_CLASS_DEL){
          continue;
      }

      if (dsu_class->class_bytes_count() == 0) {
        tty->print_cr("Validate DSU: class bytes is null");
        return false;
      }
      if (dsu_class->class_bytes() == NULL) {
        tty->print_cr("Validate DSU: class bytes is null");
        return false;
      }
    }
  }

  return true;
}

void JDUS::check_class_initialized(JavaThread *thread, instanceKlassHandle new_ikh){
  // inside intialize() there is a check
  new_ikh->initialize(thread);
  if(thread->has_pending_exception()){
    JDUS_WARN(("Initialize class failed during update mixobjects."));
  }
}

bool JDUS::is_class_not_born_at(instanceKlassHandle the_class, int rn){
  //  assert(rn>=0 && rn<=JDUS::system_revision_number(),"revison must be valid");
  return the_class->born_rn() < rn;
}

bool JDUS::is_class_born_at(instanceKlassHandle the_class, int rn){
  return the_class->born_rn() == rn;
}

bool JDUS::is_class_alive(instanceKlassHandle the_class, int rn){
  return the_class->born_rn()<= rn && rn < the_class->dead_rn();
}

bool JDUS::is_class_dead_at(instanceKlassHandle the_class, int rn){
  return the_class->dead_rn() ==rn;
}

bool JDUS::is_class_dead(instanceKlassHandle the_class, int rn){
  return the_class->dead_rn() <=rn;
}

/////////////////////////////////////////////////////////////////////////
// JDUS Eager Update 
/////////////////////////////////////////////////////////////////////////




jobject*  JDUSEagerUpdate::_candidates        = NULL;
int       JDUSEagerUpdate::_candidates_length = 0;
volatile bool      JDUSEagerUpdate::_be_updating      = false;
bool      JDUSEagerUpdate::_update_pointers  = false;
volatile bool      JDUSEagerUpdate::_has_been_updated = false;
void JDUSEagerUpdate::initialize(TRAPS){

}


void JDUSEagerUpdate::configure_eager_update(GrowableArray<jobject> * candidates, bool update_pointers){
  //_candidates = candidates;
  const int length = candidates->length();
  _candidates_length = length;
  if(length > 0){
    _candidates = (jobject*) os::malloc(sizeof(jobject)*length);
    for (int i=0;i<length;i++){
      _candidates[i] = candidates->at(i);
    }

#ifdef ASSERT
    for (int i=0;i<length;i++){
      oop obj = JNIHandles::resolve(_candidates[i]);
      assert(obj->is_instance(),"we only update instance object.");			
    }
#endif
  }else {
    _candidates = NULL;
  }

  _update_pointers = update_pointers;
  _has_been_updated = false;
}

void JDUSEagerUpdate::start_eager_update(JavaThread *thread){
  {
    MutexLocker ml(JDUSEagerUpdate_lock);

    while(_be_updating){
      JDUSEagerUpdate_lock->wait();
    }

    if (_has_been_updated){
      {
        ResourceMark rm(thread);
        JDUS_TRACE(0x00000100,("Thread [%s] leaving without take update.", thread->get_thread_name()));
      }

      JDUSEagerUpdate_lock->notify_all();
      return;
    }


    _be_updating = true;
    {
      ResourceMark rm(thread);
      JDUS_TRACE(0x00000100,("Start Eager Update by the thread [%s].", thread->get_thread_name()));
    }

    elapsedTimer dsu_timer;

    // start timer
    JDUS_TIMER_START(dsu_timer);


    int mixobjects_size = 0;
    const int candidates_size = _candidates_length;

    if (_candidates!= NULL && candidates_size != 0) {

      HandleMark hm(thread);


      for(int i=0; i< candidates_size; i++){
        jobject o = _candidates[i];

        oop obj = JNIHandles::resolve(o);

        if (obj != NULL ){
          assert(obj->is_instance(),"we only transform instance objects.");
          Handle h (obj);
          SharedRuntime::transform_object_common(h, thread);
          if (thread->has_pending_exception()){
            tty->print_cr("Transforming Objects Meets Exceptions!");
            thread->clear_pending_exception();
            return;
          }

          // This path testing is really application dependent.
          if(h->mark()->is_mixobject()){						
            mixobjects_size++;
          }
          JNIHandles::destroy_weak_global(o);
        }

        _candidates[i] = NULL;
      }

      os::free(_candidates);
      _candidates = NULL;
      _candidates_length = 0;		
    }

    // Report time, as our data collect script hardly depends on this.
    JDUS_TIMER_STOP(dsu_timer);
    JDUS_TRACE(0x00000001,("DSU eager updating pause time: %3.7f (s). Candidate size is %d, mixobjects size is %d.",dsu_timer.seconds(),candidates_size,mixobjects_size));
    JDUS_TIMER_START(dsu_timer);

    if (mixobjects_size>0) {
      if (_update_pointers){
        Universe::heap()->collect(GCCause::_jvmti_force_gc);			
      }else {
        JDUS_WARN(("You configure Eager Update without Updating Pointers, but we have met MixObjects."));
      }
    }
    tty->print_cr("stop");
    JDUS_TIMER_STOP(dsu_timer);
    JDUS_TRACE(0x00000001,("DSU eager updating total pause time: %3.7f (s).",dsu_timer.seconds()));
    if (JDUS_TRACE_ENABLED(0x00000001)){
      time_t tloc;
      (void*)time(&tloc);
      tty->print("[JDUS] DSU Eager Object Updating finished at %s",ctime(&tloc));
    }
    _be_updating = false;
    _has_been_updated = true;

    JDUSEagerUpdate_lock->notify_all();
  }
  {
    ResourceMark rm(thread);
    JDUS_TRACE(0x00000100,("Finish Eager Update by the thread [%s].", thread->get_thread_name()));
  }
}

class DeadInstanceClosure : public ObjectClosure {
private:
  GrowableArray<jobject>* _result;
  int                 _dead_time;
  int                 _count;
public:
  DeadInstanceClosure(int dead_time, GrowableArray<jobject>* result) : _dead_time(dead_time), _result(result), _count(0) {};
  int dead_time() const {return _dead_time;}
  int count() const {return _count;}
  void do_object(oop obj) {
    _count++;
    if (Klass::cast(obj->klass())->instances_require_update(dead_time())) {
      assert(obj->is_instance(), "currently we only update instance objects.");
      Handle o(obj);			
      _result->append(JNIHandles::make_weak_global(o));
    }
  }
};

void JDUSEagerUpdate::collect_dead_instances_at_safepoint(int dead_time, GrowableArray<jobject>* result, TRAPS) {
  assert(SafepointSynchronize::is_at_safepoint(), "all threads are stopped");
  HandleMark hm(THREAD);
  //Heap_lock->lock();

  //assert(Heap_lock->is_locked(), "should have the Heap_lock");

  // Ensure that the heap is parsable
  Universe::heap()->ensure_parsability(false);  // no need to retire TALBs

  // Iterate over objects in the heap
  DeadInstanceClosure dic(dead_time, result);
  // If this operation encounters a bad object when using CMS,
  // consider using safe_object_iterate() which avoids perm gen
  // objects that may contain bad references.
  Universe::heap()->object_iterate(&dic);
  JDUS_DEBUG(("Total Count is %d.", dic.count()));
  //Heap_lock->unlock();
}

// Install a return barrier that waiting for the JDUSEagerUpdate is finished.
void JDUSEagerUpdate::install_eager_update_return_barrier(TRAPS){

  for (JavaThread* thr = Threads::first(); thr != NULL; thr = thr->next()) {
    if(thr->is_Compiler_thread()){
      continue;
    }

    if(thr->has_last_Java_frame()){


      ////////////////////////////////
      // Currently We assume that there is no Native Method will calling a Java Method before eager updating has finished.
      // Note that JNI will always do object validity checks.
      // As we have not implement clear all flags after eager update and eager update pointers.
      // We just choose not to set them if we invoke VM with flag -XX:+ForceEagerUpdate and -XX:+ForceEagerUpdatePointers;
      ////////////////////////////////
      frame * callee  = NULL ;
      frame * current = NULL;
      int count = 0;
      StackFrameStream fst_callee(thr);
      for(StackFrameStream fst(thr); !fst.is_done(); fst.next()) {
        //tty->print_cr("count %d",count);
        current= fst.current();
        //current->print_on(tty);
        if(count >0 ){
          callee = fst_callee.current();
          //callee->print_on(tty);
          fst_callee.next();
        }
        count++;

        // install return barrier in the last java sp, i.e., the first executed method after
        if(current->is_interpreted_frame()){
          // 1). fetch the Bytecode

          methodOop method = current->interpreter_frame_method();
          assert(method->is_method(),"sanity check");
          //if (method->is_native()){
          //  continue;
          //}

          if(callee != NULL){
            if(callee->is_interpreted_frame()){
              Bytecodes::Code code = Bytecodes::code_at(current->interpreter_frame_method(),current->interpreter_frame_bcp());
              int bci = current->interpreter_frame_bci();
              assert(bci>=0,"in the body");

              if(bci>0){
                // in the body
                assert(code >= Bytecodes::_invokevirtual  && code <= Bytecodes::_invokedynamic, "must be an invoke"  );

                TosState tos = as_TosState(callee->interpreter_frame_method()->result_type());
                address barrier_addr;
                address normal_addr;
                if(code == Bytecodes::_invokedynamic 
                  || code == Bytecodes::_invokeinterface){
                    normal_addr = Interpreter::return_entry(tos,5);
                    barrier_addr = Interpreter::return_entry_with_barrier(tos,5);
                }else {
                  normal_addr = Interpreter::return_entry(tos,3);
                  barrier_addr = Interpreter::return_entry_with_barrier(tos,3);
                }

                //callee->interpreter_frame_method()->print();

                if(!(normal_addr == current->pc())){
                  tty->print_cr("normal_addr %d 0x%08x 0x%08x pc 0x%08x",tos, Interpreter::return_entry(tos,5),Interpreter::return_entry(tos,3),current->pc());
                  method->print_codes();
                }

                assert(normal_addr == current->pc(), "sanity check");

                // TODO
                current->patch_pc(thr,barrier_addr);
                thr->set_return_barrier_type(JavaThread::_eager_update);
                // XXX Note, unlike return barrier id for wake_up_dsu, which is install at the caller of restricted method
                // here, we install the return barrier at the top most Java frame.
                thr->set_return_barrier_id(current->id());
              }else {
                // trigger on other runtime call

                thr->set_return_barrier_type(JavaThread::_eager_update);
                // XXX Note, unlike return barrier id for wake_up_dsu, which is install at the caller of restricted method
                // here, we install the return barrier at the top most Java frame.
                thr->set_return_barrier_id(current->id());
              }
            }// end of interpreted frame
            else if (callee->is_safepoint_blob_frame()){
              tty->print_cr("is safe point blob");
              assert(false,"safe point blob");
              //address barrier_addr = Interpreter::re

              //current->patch_pc(thr,barrier_addr);
              //thr->set_return_barrier_type(JavaThread::_eager_update);
              // XXX Note, unlike return barrier id for wake_up_dsu, which is install at the caller of restricted method
              // here, we install the return barrier at the top most Java frame.
              //thr->set_return_barrier_id(current->id());
            }else {
              current->print_on(tty);
              assert(false,"unhandle callee safe point blob");
            }
          }// end of if callee
          else{
            // we are return from a safe point
            thr->set_return_barrier_type(JavaThread::_eager_update);
            thr->set_return_barrier_id(current->id());
          }
        }else if(current->is_compiled_frame()){
          thr->set_return_barrier_type(JavaThread::_eager_update);
          thr->set_return_barrier_id(current->id());
          // Do nothing.
          // XXX set the address of
          if(current->is_deoptimized_frame()){
            // We do not depotimize a deoptimized frame.
            //tty->print_cr("[JDUS] return barrier is deoptimized frame");
          }else {
            // We just deoptimize it
            //tty->print_cr("[JDUS] return barrier is compiled frame");
            Deoptimization::deoptimize(thr,*current,fst.register_map());
          }
        }else {
          // skip continue frame
          continue;
        }

        // stop iterate the stack!
        break;

      }



      assert(thr->return_barrier_id() != NULL, "return barrier id must  not be null!!");  
      {
        ResourceMark rm;

        JDUS_TRACE(0x00000100, ("install eager update [%d] return barrier at 0x%08x for thread %s.", 
          thr->return_barrier_type(),
          thr->return_barrier_id(),
          thr->get_thread_name()));
      }
    }
    //#ifdef ASSERT
    //    thr->verify();
    //#endif
  }
}

// remove all invalid fields/method flags in the method and constantPoolCache
// invalid all compiled method
// relink method
void JDUSEagerUpdate::clear_invalid_object_checks(TRAPS){
  assert(SafepointSynchronize::is_at_safepoint(),"should be in safe point!");

  // TODO: Do nothing here, we have not implemented.

  //SystemDictionary::classes_do(clear_invalid_object_checks_for_single_class, CHECK);
}


void JDUSEagerUpdate::clear_invalid_object_checks_for_single_class(klassOop k_oop, oop loader, TRAPS){
  Klass *k = k_oop->klass_part();
  if (k->oop_is_instance()) {
    HandleMark hm(THREAD);

    instanceKlass *ik = (instanceKlass *) k;

    // skip bootstrap classes;
    if (ik->class_loader() == NULL){
      return;
    }

    if (ik->is_mix_new()){
      typeArrayOop fields = ik->fields();
      int fields_length = fields->length();
      for (int i = 0; i < fields_length; i += instanceKlass::next_offset ) {
        jushort flags = fields->ushort_at(i + instanceKlass::access_flags_offset);
        fields->ushort_at_put(i + instanceKlass::access_flags_offset,(jushort)(flags & ~JVM_ACC_IS_INVALID_MEMBER));
      }
    }
    constantPoolCacheOop cache = ik->constants()->cache();
    if(cache->is_containing_invalid()){
      cache->clear_invalid_object_flags();
    }
  }
}

// remove all mixobject fields/method flags
// invalid all compiled method
// relink method
void JDUSEagerUpdate::clear_mixobjects_checks(TRAPS){
  assert(SafepointSynchronize::is_at_safepoint(),"should be in safe point!");

}

void JDUSEagerUpdate::clear_mixobjects_checks_for_single_class(klassOop k_oop, oop loader, TRAPS){

}
