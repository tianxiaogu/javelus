
PWD=`dirname $0`

RUN_CONF=${PWD}/run.conf

. $RUN_CONF $@

JAVA_OPTS="-javaagent:${JAVELUS_PRJ_HOME}/DeveloperInterface/louvadev.jar $JAVA_OPTS"

CATALINA_HOME=${PWD}/scratch

echo ===========================================
echo   Run tomcat standalone
echo ===========================================
echo   JAVA: $JAVA
echo   JAVA_OPTS: $JAVA_OPTS
echo   ITERATION: $ITERATION
#echo   DSU_SPEC_FILE: $DSU_SPEC_FILE
echo   COMMON_LIB: $COMMON_LIB
echo   BMS_LIB: $BMS_LIB
echo   CLASSPATH: $LOCALCLASSPATH
echo ===========================================

$JAVA $JAVA_OPTS -cp $LOCALCLASSPATH -Dcatalina.home=$CATALINA_HOME org.dacapo.tomcat.Startup start 
