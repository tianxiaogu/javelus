@echo off


set "RUN_CONF=%~dp0\run.conf.bat"

if exist "%RUN_CONF%" (
   echo Calling %RUN_CONF%
   call "%RUN_CONF%" %*
   IF ERRORLEVEL 1 EXIT /B 1
) else (
   echo Config file not found %RUN_CONF%
   EXIT /B 1
)

set "JAVA_OPTS=-javaagent:%JAVELUS_PRJ_HOME%\DeveloperInterface\louvadev.jar %JAVA_OPTS%"


set "CATALINA_HOME=%~dp0\scratch"



echo ===========================================
echo   Run tomcat standalone
echo ===========================================
echo   JAVA: %JAVA%
echo   JAVA_OPTS: %JAVA_OPTS%
echo   ITERATION: %ITERATION%
echo   DSU_SPEC_FILE: %DSU_SPEC_FILE%
echo   COMMON_LIB: %COMMON_LIB%
echo   BMS_LIB: %BMS_LIB%
echo   CLASSPATH: %LOCALCLASSPATH%
echo ===========================================

"%JAVA%" %JAVA_OPTS% -cp %LOCALCLASSPATH% -Dcatalina.home=%CATALINA_HOME% org.dacapo.tomcat.Startup start 
