@ECHO off

IF "%1" == "clean" (
  CALL :clean
  EXIT /B 0
)

SET PWD=%~dp0

REM ==========================================
REM Set BMS
REM ==========================================
SET "BMS=%1"
SHIFT

IF "%BMS%" == "" (
  echo No Benchmark to run
  exit /B 2
)
REM ==========================================

REM ==========================================
REM Set Java Executable Path
REM ==========================================
SET JAVELUS_PRJ_HOME=%PWD%..\..\
REM SET "JAVA=java"
REM SET "JAVA=%JAVELUS_PRJ_HOME%hotspot\build\vs\compiler2\fastdebug\hotspot.exe"
SET "JAVA=%JAVELUS_PRJ_HOME%hotspot\build\vs\compiler2\product\hotspot.exe"
REM SET "JAVA=E:\projects\hotspotall\hotspot\build\vs\compiler2\product\hotspot.exe"
REM SET "JAVA=E:\Projects\hotspotall\base\hotspot\build\vs\compiler2\product\hotspot.exe"
REM ==========================================

REM ==========================================
REM Set Java opts
REM ==========================================
REM SET "JAVA_OPTS=-Xms512M -Xmx512M -XX:-UseParallelOldGCCompacting -XX:+UseSerialGC -XX:+PrintMiscellaneous -XX:+WizardMode"
SET "JAVA_OPTS=-Xms1024M -Xmx1024M -XX:+PrintGC -XX:TraceJDUS=129 -XX:-UseParallelOldGCCompacting -XX:+UseSerialGC -XX:+ForceEagerUpdate -XX:+ForceEagerUpdatePointers"
REM SET "JAVA_OPTS=-server"
REM SET "JAVA_OPTS=-javaagent:E:\projects\hotspotall\DevelopInterface\louvadev.jar"
REM ==========================================



REM ==========================================
REM Set path of old version Tomcat
REM ==========================================
SET "FROM=7.0.3"
echo %1
IF "%1" NEQ "" (
  SET "FROM=%1"
)
SET "TO=7.0.4"
IF "%2" NEQ "" (
  SET "TO=%2"
)
SET "BMS_LIB=%PWD%%BMS%-%FROM%\"
REM ==========================================



REM ==========================================
REM Set the path of the dynamic patch file here.
REM ==========================================
SET "DSU_SPEC_FILE=-d %PWD%dynamic_patch\%BMS%-%FROM%-%TO%\javelus.dsu"
REM SET "DSU_SPEC_FILE="
REM ==========================================


REM ==========================================
REM Set iteration number, default is 20
REM ==========================================
IF "%ITERATION%" == "" (
  SET "ITERATION=20"
)
REM ==========================================


REM ==========================================
REM Set local class path
REM ==========================================
SET LOCALCLASSPATH=
CALL "%PWD%lcp.bat" "%PWD%dacapo-%BMS%.jar"
SET "COMMON_LIB=%PWD%lib"
FOR %%i IN ("%COMMON_LIB%\*.jar") DO CALL "%PWD%lcp.bat" "%%i"
FOR %%i IN ("%BMS_LIB%\*.jar") DO CALL "%PWD%lcp.bat" "%%i"
REM ==========================================


exit /B 0


REM ==========================================
REM Clean variables
REM ==========================================
:clean


ECHO Clean all variables

SET BMS=
SET PWD=
SET JAVELUS_PRJ_HOME=
SET JAVA=
SET JAVA_OPTS=
SET FROM=
SET TO=
SET BMS_LIB=
SET DSU_SPEC_FILE=
SET ITERATION=
SET COMMON_LIB=
SET LOCALCLASSPATH=

EXIT /B 0
REM ==========================================
