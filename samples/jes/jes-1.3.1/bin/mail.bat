@set LOCALCLASSPATH=""
@for %%i in ("..\lib\*.jar") do call "lcp.bat" "%%i"

if "%JAVA%" == "" (
    set JAVA=java
    )

%JAVA% %JAVA_OPTS% -cp %LOCALCLASSPATH% com.ericdaugherty.mail.server.Mail ..
