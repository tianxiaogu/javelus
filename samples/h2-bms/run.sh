PWD=`dirname $0`


RUN_CONF=$PWD/run.conf

source $RUN_CONF $@


if [ "x$BMS" == "x" ]; then
    echo "No Benchmark to run"
    exit -1
fi


echo ===========================================
echo        Run benchmark ${BMS}
echo ===========================================
echo   JAVA: $JAVA
echo   JAVA_OPTS: $JAVA_OPTS
echo   ITERATION: $ITERATION
echo   DSU_SPEC_FILE: $DSU_SPEC_FILE
echo   COMMON_LIB: $COMMON_LIB
echo   BMS_LIB: $BMS_LIB
echo   CLASSPATH: $LOCALCLASSPATH
echo ===========================================

$JAVA $JAVA_OPTS -cp $LOCALCLASSPATH org.dacapo.harness.TestHarness -n $ITERATION $DSU_SPEC_FILE $BMS
