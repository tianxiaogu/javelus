@ECHO off



SET "RUN_CONF=%PWD%run.conf.bat"

IF EXIST "%RUN_CONF%" (
   ECHO Call %RUN_CONF%
   CALL "%RUN_CONF%" %*
   IF ERRORLEVEL 1 EXIT /B 1
) ELSE (
   ECHO Config file not found %RUN_CONF%
   EXIT /B 1
)

ECHO ===========================================
ECHO        Run benchmark %BMS%
ECHO ===========================================
ECHO   JAVA: %JAVA%
ECHO   JAVA_OPTS: %JAVA_OPTS%
ECHO   ITERATION: %ITERATION%
ECHO   DSU_SPEC_FILE: %DSU_SPEC_FILE%
ECHO   COMMON_LIB: %COMMON_LIB%
ECHO   BMS_LIB: %BMS_LIB%
ECHO   CLASSPATH: %LOCALCLASSPATH%
ECHO ===========================================

"%JAVA%" %JAVA_OPTS% -cp %LOCALCLASSPATH% org.dacapo.harness.TestHarness -n %ITERATION% %DSU_SPEC_FILE%  %BMS%


CALL "%RUN_CONF%" clean
SET RUN_CONF=
