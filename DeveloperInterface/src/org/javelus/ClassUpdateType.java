/*
 * Copyright (C) 2012  Tianxiao Gu. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Please contact Institute of Computer Software, Nanjing University, 
 * 163 Xianlin Avenue, Nanjing, Jiangsu Provience, 210046, China,
 * or visit moon.nju.edu.cn if you need additional information or have any
 * questions.
*/
package org.javelus;

public enum ClassUpdateType implements UpdateType{
	/* MATCH */
	NONE,
	/* Only Machine Code */
	MC,
	/*Only Bytecode */
	BC,
	/* Class signature changed */
	S_METHOD,
	S_FIELD,
	S_ALL,
	
	METHOD,
	FIELD,
	ALL,
	
	/* Only OLD */
	DEL,
	/**
	 * Only NEW
	 */
	ADD;
	
/*	static ClassChangeType [][] JOIN = new ClassChangeType[][]{
		          NO,MC,BC,SM,SF,SA,MT,FL,AL,DE,AD                                                                
		 NO {NONE     ,MC,BC,S_METHOD,S_FIELD,S_ALL,METHOD,FIELD,ALL,DEL,ADD},
		 MC {MC       ,MC,BC,S_METHOD,S_FIELD,S_ALL,METHOD,FIELD,ALL,DEL,ADD},
		 BC {BC       ,BC,BC,S_METHOD,S_FIELD,S_ALL,METHOD,FIELD,ALL,DEL,ADD},
		 SM {S_METHOD ,BC,BC,S_METHOD,S_FIELD,S_ALL,METHOD,FIELD,ALL,DEL,ADD},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
	};
	
	
*/	
	/**
	 * this join larger
	 * @param larger
	 * @return
	 */
	public ClassUpdateType join(ClassUpdateType larger){
		if(compareTo(larger) > 0){
			return larger.join(this);
		}

		switch (larger) {
		case NONE:
		case MC:
		case BC:
		case S_METHOD:
			return larger;
		case S_FIELD:
			if(this == S_METHOD){
				return S_ALL;
			}
			return larger;
		case S_ALL:
			return larger;
		case METHOD:
			if(this == S_FIELD || this == S_ALL){
				return ALL;
			}
			return larger;
		case FIELD:
			if(this == S_METHOD || this == S_ALL || this == METHOD){
				return ALL;
			}
		case ALL:
		case DEL: 
		case ADD:
			return larger;
		default:
		}
		return larger;
	}
	
	/**
	 * XXX
	 * @return
	 */
	public int intValue(){
		return this.ordinal();
	}
	
	public boolean isDeleted(){
		return this == DEL;
	}
	
	public boolean isAdded(){
		return this == ADD;
	}
	
	public boolean isChanged(){
		return this.compareTo(MC) >= 0  && this.compareTo(ALL) <=0;
	}

	@Override
	public boolean isUnchanged() {
		return this == NONE;
	}

}