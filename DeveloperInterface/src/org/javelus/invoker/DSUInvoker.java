/*
 * Copyright (C) 2012  Tianxiao Gu. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Please contact Institute of Computer Software, Nanjing University, 
 * 163 Xianlin Avenue, Nanjing, Jiangsu Provience, 210046, China,
 * or visit moon.nju.edu.cn if you need additional information or have any
 * questions.
*/
package org.javelus.invoker;
import java.io.*;

import javax.xml.parsers.ParserConfigurationException;

import org.javelus.DSU;
import org.javelus.DSUSpecReader;
import org.xml.sax.SAXException;
public class DSUInvoker{

	public static void redefineSingleClass(String name, String filePath){

		try{
			BufferedInputStream in = new BufferedInputStream(
					new FileInputStream(filePath));

			ByteArrayOutputStream out = new ByteArrayOutputStream(1024);

			byte[] tmp = new byte[1024];
			int size = 0;
			while((size= in.read(tmp))!=-1){
				out.write(tmp,0,size);
			}
			in.close();

			byte[] content = out.toByteArray();

			org.javelus.DeveloperInterface.redefineSingleClass(name,content);	
		}catch(Exception e){
			e.printStackTrace();
		}

	}

	public static Object getMixThat(Object o){
		return  org.javelus.DeveloperInterface.getMixThat(o);
	}

	public static void invokeDSU(String dsu_spec, boolean sync){
/*		DSU dsu = null;
		try {
			dsu = DSUSpecReader.read(dsu_spec);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}*/
		if (dsu_spec != null){
			org.javelus.DeveloperInterface.invokeDSU(dsu_spec,sync);
		}else {
			System.err.println();
		}
	}
	
	public static void invokeDSU(String dsu_spec){
		invokeDSU(dsu_spec,true);
	}
	
}
