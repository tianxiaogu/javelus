/*
 * Copyright (C) 2012  Tianxiao Gu. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Please contact Institute of Computer Software, Nanjing University, 
 * 163 Xianlin Avenue, Nanjing, Jiangsu Provience, 210046, China,
 * or visit moon.nju.edu.cn if you need additional information or have any
 * questions.
*/
package org.javelus;

public interface DSUConstants {
	
	// class change type
	int JDUS_CLASS_STUB = -1;
	int JDUS_CLASS_NONE = 0;
	int JDUS_CLASS_MC = 1;
	int JDUS_CLASS_BC = 2;
	int JDUS_CLASS_FIELD = 3;
	int JDUS_CLASS_METHOD = 4;
	int JDUS_CLASS_BOTH = 5;
	int JDUS_CLASS_DEL = 6;

	// method change type
	int JDUS_METHOD_NONE = 0;
	int JDUS_METHOD_MC = 1;
	int JDUS_METHOD_BC = 2;
	int JDUS_METHOD_DEL = 3;
}
