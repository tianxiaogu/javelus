/*
 * Copyright (C) 2012  Tianxiao Gu. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Please contact Institute of Computer Software, Nanjing University, 
 * 163 Xianlin Avenue, Nanjing, Jiangsu Provience, 210046, China,
 * or visit moon.nju.edu.cn if you need additional information or have any
 * questions.
*/
package org.javelus;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
/**
 * read a XML-based DSU-specification file
 * 
 */
public class DSUSpecReader implements DSUSpecConstants{

	/**
	 * helper method
	 * @param parentElement
	 * @param nodeName
	 * @return a child element
	 */
	static Element getDSUChildElementByLocalName(Element parentElement, String nodeName) {
		NodeList children = parentElement.getChildNodes();
		for (int i = 0; i < children.getLength(); i++) {
			Node node = children.item(i);
			if (nodeName.equals(node.getNodeName()) ) {
				return (Element) node;
			}
		}
		return null;
	}

	/**
	 * Helper method 
	 * @param parentElement
	 * @param nodeName
	 * @return list of child element
	 */
	static List<Element> getDSUChildElementsByLocalName(Element parentElement, String nodeName){
		List<Element> list = new LinkedList<Element>();
		NodeList children = parentElement.getChildNodes();
		for (int i = 0; i < children.getLength(); i++) {
			Node node = children.item(i);
			if (nodeName.equals(node.getNodeName()) ) {
				list.add((Element) node);
			}
		}
		return list;
	}

	/**
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	public static DSU read(String filePath) throws ParserConfigurationException, SAXException, IOException{
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();		
		Document document;
		if(filePath != null){
			FileInputStream fis = new FileInputStream(filePath);
			document = builder.parse(fis);//VM.DsuSpecificationFile);
		}else {
			document = builder.parse(ClassLoader.getSystemClassLoader().getResourceAsStream(DEFAULT_FILE_NAME));
		}
		return xml2DSU(document);
	}
	
		
	/**
	 * @param document
	 * @return DSU
	 * @throws IOException
	 */
	static DSU xml2DSU(Document document) throws IOException{
		Element dsuElement = document.getDocumentElement();

		DSU dsu = new DSU();
		List<Element> cls =getDSUChildElementsByLocalName(dsuElement,DSUCLASSLOADER_TAG);
		int size = cls.size();
		
		if (size > 0){
			DSUClassLoader[] dsuCLs = new DSUClassLoader[size];
			for(int i=0; i<size; i++){
				dsuCLs[i] = xml2DSUClassLoader(cls.get(i));
			}
			dsu.classLoader = dsuCLs;
		}
		return dsu;
	}

	/**
	 * parse a class element
	 * @param classElement
	 * @return a dsuclass
	 */
	static DSUClass xml2DSUClass(Element classElement){
		if(classElement == null){
			return null;
		}
		//1).parse name
		DSUClass dsuClass = new DSUClass();
		dsuClass.name = classElement.getAttribute(CLASS_NAME_ATT);

		//2).parse change type
		String classChangeType = classElement.getAttribute(CLASS_CHANGED_TYPE_ATT);
		dsuClass.changeType = classChangeTypeToInt(classChangeType);

		//3). parse methods
		List<Element> methodElement = getDSUChildElementsByLocalName(classElement,DSUMETHOD_TAG);
		int size = methodElement.size();
		DSUMethod[] methods = new DSUMethod[size];
		for(int i=0;i<size;i++){
			methods[i] = xml2DSUMethod(methodElement.get(i));
		}
		dsuClass.dsuMethod = methods;
		
		Element fileElement = getDSUChildElementByLocalName(classElement, FILE_TAG);
		dsuClass.classBytes = xml2File(fileElement);
		
		return dsuClass;
	}

	static DSUClass xml2Helper(Element classElement){
		if(classElement == null){
			return null;
		}
		//1).parse name
		DSUClass dsuClass = new DSUClass();
		dsuClass.name = classElement.getAttribute(CLASS_NAME_ATT);
		
		Element fileElement = getDSUChildElementByLocalName(classElement, FILE_TAG);
		dsuClass.classBytes = xml2File(fileElement);
		
		return dsuClass;
	}
	
	static DSUClass xml2Transformer(Element classElement){
		if(classElement == null){
			return null;
		}
		//1).parse name
		DSUClass dsuClass = new DSUClass();
		dsuClass.name = classElement.getAttribute(CLASS_NAME_ATT);
		
		Element fileElement = getDSUChildElementByLocalName(classElement, FILE_TAG);
		dsuClass.classBytes = xml2File(fileElement);
		
		return dsuClass;
	}
	
	static byte[] xml2File(Element fileElement){
		if (fileElement == null){
			return null;
		}
		try{

			String filePath = fileElement.getTextContent().trim();
			URL url = new URL(filePath);
			BufferedInputStream in = new BufferedInputStream(url.openStream());

			ByteArrayOutputStream out = new ByteArrayOutputStream(1024);

			byte[] tmp = new byte[1024];
			int size = 0;
			while((size= in.read(tmp))!=-1){
				out.write(tmp,0,size);
			}
			in.close();
			return out.toByteArray();	
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * parse a classloader
	 * @param clElement
	 * @return a classloader
	 */
	static DSUClassLoader xml2DSUClassLoader(Element clElement){
		if(clElement == null){
			return null;
		}
		
		DSUClassLoader dsuCL= new DSUClassLoader();
		
		//1). parse helper class
		Element helperClassElement = getDSUChildElementByLocalName(clElement, HELPER_TAG);
		dsuCL.helperClass = xml2Helper(helperClassElement);

		//2). parse helper class
		Element transformerElement = getDSUChildElementByLocalName(clElement, TRANSFORMER_TAG);
		dsuCL.transformer = xml2Transformer(transformerElement);
		
		//3). parse id
		
		dsuCL.id = clElement.getAttribute("id");
		dsuCL.lid = clElement.getAttribute("lid");

		//3). parse dsu class
		List<Element> classesElement = getDSUChildElementsByLocalName(clElement, DSUCLASS_TAG);
		int size = classesElement.size();
		DSUClass[] classes = new DSUClass[size];
		for(int i=0; i<size; i++){
			classes[i] =  xml2DSUClass(classesElement.get(i));
		}
		dsuCL.dsuClass = classes;
		
		return dsuCL;
	}
	

	static DSUMethod xml2DSUMethod(Element methodElement){
		if (methodElement == null){
			return null;
		}
		DSUMethod dsuMethod = new DSUMethod();
		dsuMethod.name = methodElement.getAttribute(METHOD_NAME_ATT);
		dsuMethod.signature = methodElement.getAttribute(METHOD_DESC_ATT);
		
		String methodChangedType = methodElement.getAttribute(CODE_CHANGED_TYPE_ATT);
		dsuMethod.changeType = methodChangeTypeToInt(methodChangedType);
		return dsuMethod;
	}
	
	
	static int classChangeTypeToInt (String changeType) {
		return ClassUpdateType.valueOf(changeType).intValue();
	}
	
	static int methodChangeTypeToInt (String changeType) {
		return CodeUpdateType.valueOf(changeType).intValue();
	}
	
}
