package org.javelus;

public interface DSUSpecConstants {
	String DEFAULT_FILE_NAME = "javelus.dsu";
	
	String DSUCLASS_TAG = "class";
	String DSUMETHOD_TAG = "method";
	String DSUCLASSLOADER_TAG = "classloader";
	String HELPER_TAG = "helper";
	String TRANSFORMER_TAG = "transformer";
	String FILE_TAG = "file";
	
	String CLASS_NAME_ATT = "name";
	String CLASSLOADER_ATT = "classloader";
	String TRANSFORMER_ATT = "transformer";
	String CLASS_CHANGED_TYPE_ATT = "classChanged";
	
	
	
	String METHOD_NAME_ATT = "name";
	String METHOD_DESC_ATT = "desc";
	String CODE_CHANGED_TYPE_ATT = "codeChanged";
	
	
	
	String CLASSLOADER_ID_ATT = "id";
	String CLASSLOADER_BINDING_ATT = "binding";
}
