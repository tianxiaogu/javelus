/*
 * Copyright (C) 2012  Tianxiao Gu. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Please contact Institute of Computer Software, Nanjing University, 
 * 163 Xianlin Avenue, Nanjing, Jiangsu Provience, 210046, China,
 * or visit moon.nju.edu.cn if you need additional information or have any
 * questions.
*/
package org.javelus.upt.hotspot;

import java.util.Collections;
import java.util.Iterator;

public class AllClassIterator implements Iterator<DSUClass> {

	Iterator<DSUClassPath> currentCPIterator;

	Iterator<DSUClass> currentClassIterator;

	public AllClassIterator(Iterator<DSUClassPath> currentCPIterator) {
		this.currentCPIterator = currentCPIterator;
		if (this.currentCPIterator.hasNext()) {
			currentClassIterator = currentCPIterator.next()
					.getDSUClassIterator();
		} else {
			currentClassIterator = Collections.<DSUClass>emptyList().iterator();
		}
	}

	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
		boolean hasNext = currentClassIterator.hasNext();
		if (!hasNext) {
			if (currentCPIterator.hasNext()) {
				currentClassIterator = currentCPIterator.next()
						.getDSUClassIterator();
				hasNext = currentClassIterator.hasNext();
			}
		}
		return hasNext;
	}

	@Override
	public DSUClass next() {
		// TODO Auto-generated method stub
		return currentClassIterator.next();
	}

	@Override
	public void remove() {
		// TODO Auto-generated method stub
		currentClassIterator.remove();
	}

}
