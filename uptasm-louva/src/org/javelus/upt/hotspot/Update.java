/*
 * Copyright (C) 2012  Tianxiao Gu. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Please contact Institute of Computer Software, Nanjing University, 
 * 163 Xianlin Avenue, Nanjing, Jiangsu Provience, 210046, China,
 * or visit moon.nju.edu.cn if you need additional information or have any
 * questions.
*/
package org.javelus.upt.hotspot;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.javelus.ClassUpdateType;

import org.javelus.upt.comparator.DSUClassComparator;
import org.javelus.upt.comparator.IndirectUpdate;

/**
 * @author tiger
 * 
 */
public class Update {

	static Logger logger = Logger.getLogger(Update.class);
	
	private List<DSUClassPath> oldCPs;

	private List<DSUClassPath> newCPs;

	/**
	 * TODO add multi class path support
	 * class need reload(re-resolve in runtime)
	 */
	private Map<String, DSUClass> loadEntireClass = new HashMap<String, DSUClass>();

	/**
	 * class in ClassUpdateType.BC
	 */
	private List<DSUClass> loadSomeMethods = new ArrayList<DSUClass>();

	/**
	 * class in ClassUpdateType.BC
	 */
	private List<DSUClass> indirectUpdates = new ArrayList<DSUClass>();

	private List<DSUClass> deletedClasses = new ArrayList<DSUClass>();
	private List<DSUClass> addedClasses = new ArrayList<DSUClass>();
	

	/**
	 * @return all classes need reload at DSU time
	 */
	public List<DSUClass> getReloadClass() {
		return new ArrayList<DSUClass>(loadEntireClass.values());
	}

	/**
	 * @return a iterator to iterate all classes in OLDCPs
	 */
	public Iterator<DSUClass> getOldClassIterator() {
		return new AllClassIterator(oldCPs.iterator());
	}
	
	public Iterator<DSUClass> getNewClassIterator() {
		return new AllClassIterator(newCPs.iterator());
	}
	
	public List<DSUClass> getDeletedClasses(){
		return deletedClasses;
	}

	/**
	 * @return old classes in topological order
	 */
	public Iterator<DSUClass> getSortedOldClasses() {
		return classesInTopologicalOrder(getOldClassIterator());
	}

	public Iterator<DSUClass> getSortedNewClasses() {
		return classesInTopologicalOrder(getNewClassIterator());
	}
	
	public Update() {
		oldCPs = new ArrayList<DSUClassPath>();
		newCPs = new ArrayList<DSUClassPath>();
	}

	public Update(List<DSUClassPath> oldCPs, List<DSUClassPath> newCPs) {
		this.oldCPs = oldCPs;
		this.newCPs = newCPs;
	}

	public Update(DSUClassPath oldCP, DSUClassPath newCP) {
		this();
		this.oldCPs.add(oldCP);
		this.newCPs.add(newCP);
	}

	public void addOldCP(DSUClassPath oldCP) {
		this.oldCPs.add(oldCP);
	}

	public void addNewCP(DSUClassPath newCP) {
		this.newCPs.add(newCP);
	}

	/**
	 * do some initial work before compute update information
	 * 
	 * @param oldClass
	 * @param newClass
	 */
	protected void initClass(DSUClass oldClass, DSUClass newClass) {
		oldClass.setNewVersion(newClass);
		newClass.setOldVersion(oldClass);
	}

	/**
	 * 
	 * @param name
	 * @return class with the name in newCPs
	 */
	protected DSUClass findNewRVMClass(String name) {
		DSUClass newClass = null;

		for (DSUClassPath cp : newCPs) {
			newClass = cp.findDSUClass(name);
			if (newClass != null) {
				return newClass;
			}
		}

		return null;
	}


	/**
	 */
	public void firstPass() {
		Iterator<DSUClass> allClassIterator = getSortedOldClasses();
		
		while (allClassIterator.hasNext()) {
			DSUClass cls = allClassIterator.next();
			// if(cls.isLoaded()){
			firstPass(cls);
			// }
		}
		// find all added new class
		findNewAddedClasses();
	}

	/**
	 * Must be called after first pass
	 * 
	 */
	protected void findNewAddedClasses(){
		Iterator<DSUClass> allClassIterator = getNewClassIterator();
		
		while (allClassIterator.hasNext()) {
			DSUClass cls = allClassIterator.next();
			if(cls.hasOldVersion()){
				continue;
			}
			cls.updateChangedType(ClassUpdateType.ADD);
			//System.out.println("New Added Class:" + cls.getChangeType());
			addedClasses.add(cls);
		}
	}
	
	/**
	 * @param klass
	 */
	protected void firstPass(DSUClass klass) {
		if (klass.isLoaded()) {
			String name = klass.getName();
			DSUClass newClass = findNewRVMClass(name);
			if (newClass != null) {
				if (newClass.isLoaded()) {
					initClass(klass, newClass);
					
					
					
					DSUClassComparator.compareClassStructure(klass, newClass);
					if (DSUClassComparator.shouldLoadEntireClass(klass)) {
						logger.debug("load entire class [" + name+"].");
						addLoadEntireClass(name, klass);
//						if(klass.getDeclaringClass()!=null){
//							name=klass.getDeclaringClass().getName();
//							addLoadEntireClass(name, klass.getDeclaringClass());
//						}
					}

					// compare method code
					DSUClassComparator.compareMethodBody(klass, newClass);
					if (DSUClassComparator.shouldLoadSomeMethodBodies(klass,
							newClass)) {
						logger.debug("load class methods [" + name+"].");
						loadSomeMethods.add(klass);
					}

//					HashSet<DSUClass> subClasses = klass.getSubClasses();
//					for (DSUClass subClass : subClasses) {
//						firstPass(subClass);
//					}

				} else {
					// old class's new RVMClass misses class node
					//System.err.format("Missing new class node [%s].\n",klass.getName());
					klass.updateChangedType(ClassUpdateType.DEL);
					logger.debug("deleted class [" + name+"].");
					deletedClasses.add(klass);
				}
			} else {
				// old class misses new RVMClass
				//System.err.format("Missing new class [%s].\n",klass.getName());
				klass.updateChangedType(ClassUpdateType.DEL);
				logger.debug("deleted class [" + name+"].");
				deletedClasses.add(klass);
			}
		} else {
			// old class misses class node
			// System.err.println("RVMClass " + klass +
			// " Missing Class Node... Treate as un changed..");
			System.err.format("Missing old class node [%s].\n",klass.getName());
		}

	}

	public void secondPass() {
		Iterator<DSUClass> allClassIterator = getOldClassIterator();

		while (allClassIterator.hasNext()) {
			DSUClass cls = allClassIterator.next();
			if (cls.isLoaded()) {
				secondPass(cls);
			}
		}
	}

	
	/**
	 * for method and filed entry
	 * @param referee
	 * @param owner
	 * @param name
	 * @param desc
	 */
	public boolean constantPoolChanged(DSUClass referer, String owner, String name, String desc){
		DSUClass referee = referer.getClassPath().findDSUClass(owner);
		if(referee == null){
			return false;
		}
		if(referee.needRedefineClass()){
			return true;
		}else if(referee.needReloadClass()){
			return true;
//			if(desc.indexOf(')') != -1){
//				DSUMethod method = referee.getMethod(name, desc);
//				return method.bcChanged();
//			}
			
		}
		return false;
	}
	
	/**
	 * for type insn
	 * @param referee
	 * @param owner
	 */
	public boolean constantPoolChanged(DSUClass referer, String owner){
		DSUClass referee = referer.getClassPath().findDSUClass(owner);
		if(referee == null){
			return false;
		}
		return referee.needRedefineClass();
	}
	
	/**
	 * @param klass
	 */
	void secondPass(DSUClass klass) {
		IndirectUpdate.detectMCChanged(klass, this);
		if (klass.getChangeType() == ClassUpdateType.MC) {
			logger.debug("indirect changed class [" +klass.getName()+"].");
			indirectUpdates.add(klass);
		}
	}

	String getPathString(List<DSUClassPath> pathList){
		StringBuilder sb = new StringBuilder();
		for(DSUClassPath path:pathList){
			sb.append("[");
			sb.append(path);
			sb.append("]");
		}
		
		
		return sb.toString();
	}
	
	/**
	 */
	public void computeUpdateInformation() {
		System.out.println("OldPath:" + getPathString(oldCPs));
		System.out.println("NewPath:" + getPathString(newCPs));
		firstPass();
		secondPass();
		logger.debug("there will be " + loadEntireClass.size() + " type changed classes.");
		logger.debug("there will be " + loadSomeMethods.size() + " method body changed classes.");
		logger.debug("there will be " + indirectUpdates.size() + " type changed classes.");
		logger.debug("there will be " + deletedClasses.size() + " deleted classes.");
		logger.debug("there will be " + addedClasses.size() + " added classes.");
	}

	/**
	 * 
	 * @param name
	 * @param klass
	 */
	protected void addLoadEntireClass(String name, DSUClass klass) {

		// klass.resolve();
		loadEntireClass.put(name, klass);

		HashSet<DSUClass> subClasses = klass.getSubClasses();
		for (DSUClass subClass : subClasses) {
			if (subClass.isLoaded() && subClass.hasNewVersion()) {
				// subClass.resolve();
				addLoadEntireClass(subClass.getName(), subClass);
				subClass.updateChangedType(klass.getChangeType());
			}
		}
	}

	/**
	 * Generate list of classes in topological order interface first
	 * 
	 * @param node
	 * @param list
	 */
	private static void classesInTopologicalOrder(DSUClass node,
			LinkedList<DSUClass> clazz) {
		if (!node.isInterface()) {
			clazz.add(node);
			for (DSUClass subClass : node.getSubClasses()) {
				classesInTopologicalOrder(subClass, clazz);
			}
		}
	}
	
	private static void interfacesInTopologicalOrder(DSUClass node,
			LinkedList<DSUClass> iface, HashSet<DSUClass> imark){
		if(node.isInterface() && !imark.contains(node)){
			iface.add(node);
			imark.add(node);
			for (DSUClass subClass : node.getSubClasses()) {
				interfacesInTopologicalOrder(subClass, iface,imark);
			}
		}
	}

	/**
	 * Returns class names in topologically sorted order.
	 */
	public static Iterator<DSUClass> classesInTopologicalOrder(
			Iterator<DSUClass> iterator) {
		LinkedList<DSUClass> clist = new LinkedList<DSUClass>();
		LinkedList<DSUClass> ilist = new LinkedList<DSUClass>();
		HashSet<DSUClass> imark = new HashSet<DSUClass>(100);
		while (iterator.hasNext()) {
			DSUClass clazz = iterator.next();

			if (!clazz.isLoaded()) {
				continue;
			}
			// all class directly inherit from java.lang.Object
			if (clazz.isInterface() && clazz.getDeclaredInterfaces().length == 0) {
				interfacesInTopologicalOrder(clazz,ilist, imark);
			}else if(clazz.isEnum()){
				//System.err.println("An Enum");
				classesInTopologicalOrder(clazz, clist);
			}else if(clazz.getSuperClass() == DSUClassPath.java_lang_Object_class){
				classesInTopologicalOrder(clazz, clist);
			}else {
				
			}
		}

		ilist.addAll(clist);

		return ilist.iterator();
	}

	public List<DSUClassPath> getOldCPs(){
		return oldCPs;
	}
	
	public void setOldCPs(List<DSUClassPath> oldCPs){
		this.oldCPs=oldCPs;
	}
	
	public List<DSUClassPath> getNewCPs(){
		return newCPs;
	}
	
	public void setNewCPs(List<DSUClassPath> newCPs){
		this.newCPs=newCPs;
	}
	
	
	public void writeUpdate(File root){
		for(DSUClass klass : loadEntireClass.values()){
			klass.writeClassFile(root);
		}
		
		for(DSUClass klass : loadSomeMethods){
			klass.writeClassFile(root);
		}
	}
}
