/*
 * Copyright (C) 2012  Tianxiao Gu. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Please contact Institute of Computer Software, Nanjing University, 
 * 163 Xianlin Avenue, Nanjing, Jiangsu Provience, 210046, China,
 * or visit moon.nju.edu.cn if you need additional information or have any
 * questions.
*/
package org.javelus.upt.hotspot;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.tree.ClassNode;

import org.apache.log4j.Logger;
import org.javelus.upt.io.ClassPath;
import org.javelus.upt.io.PathElement;

/**
 * @author tiger
 * 
 */
public class DSUClassPath implements ClassPath {
	
	private static Logger logger = Logger.getLogger(DSUClassPath.class);

	public final static DSUClassPath BOOTSTRAP_CLASSPATH;// = new DSUBootstrapClassPath();

	public final static DSUClass java_lang_Object_class; 
	
	static {
		
		BOOTSTRAP_CLASSPATH = new DSUBootstrapClassPath();
		java_lang_Object_class = BOOTSTRAP_CLASSPATH.getOrCreateDSUClass("java/lang/Object");
	}
	
	public static boolean isInBootstrapCP(DSUClass clazz) {
		return BOOTSTRAP_CLASSPATH == clazz.getClassPath();
	}

	protected DSUClassPath superCP;

	protected PathElement[] pathElements;

	protected final Map<String, DSUClass> classes;

	public DSUClassPath getSuperCP() {
		return superCP;
	}

	public void setSuperCP(DSUClassPath superCP) {
		this.superCP = superCP;
	}

	public DSUClassPath() {
		classes = new HashMap<String, DSUClass>();
		setSuperCP(BOOTSTRAP_CLASSPATH);
	}

	public DSUClassPath(PathElement[] pathElements){
		this();
		setPathElements(pathElements);
	}
	
	public DSUClassPath(PathElement pathElement){
		this();
		setPathElements(new PathElement[]{pathElement});
	}
	
	public DSUClassPath(String pathString) {
		this(new PathElement[]{
				PathElement.createPathElement(pathString)
				});
	}

	
	/**
	 * 
	 * @param rvmClass
	 * @param content
	 * @throws Exception
	 */
	public void rewrite(DSUClass rvmClass, byte[] content) throws Exception {
		for(PathElement path : pathElements){
			path.writeClass(rvmClass, content);
		}
		
	}

	public Object addClass(ClassNode cn) {
		return getOrCreate(cn.name, cn);
	}

	public Object addClass(InputStream in) throws Exception {
		ClassNode cn = new ClassNode();
		ClassReader reader = new ClassReader(in);
		reader.accept(cn, 0);
		return addClass(cn);
	}

	protected DSUClass getOrCreate(String className, ClassNode cn) {
		DSUClass clazz = classes.get(className);
		if (clazz == null) {
			clazz = new DSUClass(this, className);
			//logger.debug("add "+className +" to class path");
			classes.put(className, clazz);
			clazz.setClassNode(cn);
		} else {
			clazz.setClassNode(cn);
		}
		return clazz;
	}

	protected DSUClass getOrCreate(String className) {
		return getOrCreate(className, null);
	}

	public DSUClass getOrCreateDSUClass(String name) {
		DSUClass clazz = null;
		clazz = classes.get(name);
		if (clazz != null) {
			return clazz;
		}
		if (superCP != null) {
			clazz = superCP.getOrCreateDSUClass(name);
		}
		if (clazz == null) {
			clazz = getOrCreate(name);
		}
		return clazz;
	}

	/**
	 * display all the class in this cp
	 * 
	 * @return a string hold infomation about classes in cp
	 */
	public String dump() {
		StringBuffer sb = new StringBuffer();
		Iterator<DSUClass> iterator = classes.values().iterator();
		while (iterator.hasNext()) {
			sb.append(iterator.next().getName());
			sb.append("\n");
		}

		return sb.toString();
	}

	/**
	 * @return a iterator to iterate all RVMClass in this
	 */
	public Iterator<DSUClass> getDSUClassIterator() {
		return classes.values().iterator();
	}

	/**
	 * @param name
	 * @return null if this cp doesnot contain the RVMClass
	 */
	public DSUClass findDSUClass(String name) {
		DSUClass clazz = superCP.findDSUClass(name);
		if (clazz == null) {
			return classes.get(name);
		}
		return clazz;
	}
	
	public DSUClass findLocalDSUClass(String name){
		return classes.get(name);
	}
	
	public PathElement[] getPathElements(){
		return pathElements;
	}
	
	public URL getClassURL(Object classID) {
		URL url = null;
		
		for(PathElement path : pathElements){
			url = path.getClassURL(classID);
			if(url != null){
				return url;
			}
		}
		return null;
	}
	
	public void setPathElements(PathElement[] pathElements){
		this.pathElements = pathElements;
		for(PathElement path : pathElements){
			path.setClassPath(this);
			path.readClasses();
		}
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		for(PathElement pathElement : pathElements){
			sb.append(pathElement);
			sb.append(File.pathSeparator);
		}
		return sb.toString();
	}

}
