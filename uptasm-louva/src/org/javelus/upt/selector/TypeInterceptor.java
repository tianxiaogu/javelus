package org.javelus.upt.selector;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodNode;

import com.sun.org.apache.bcel.internal.generic.Type;

public class TypeInterceptor {

	ClassNode interceptor;
	
	/**
	 * used to rename type;
	 */
	String suffix;
	
	public TypeInterceptor(String name,String suffix){
		interceptor = new ClassNode();
		interceptor.name = name;
		interceptor.access = Opcodes.ACC_PUBLIC;
		this.suffix = suffix;
	}
	
	@SuppressWarnings("unchecked")
	protected MethodNode addMethod(String name, String desc){
		MethodNode mn = new MethodNode();
		mn.name = name;
		mn.desc = desc;
		mn.access = Opcodes.ACC_PUBLIC | Opcodes.ACC_PRIVATE;
		
		interceptor.methods.add(mn);
		
		return mn;
	}
	
	String getBinaryName(String name){
		String bn = "L" + name.replace(".", "/") + ";";
		return bn;
	}
	
	public void addInstanceOf(String targetType){
		MethodNode mn = addMethod("instanceOf$"+targetType, "("+getBinaryName(targetType)+")V");
		
	}
	
	public void addCheckCast(String targetType){
		MethodNode mn = addMethod("checkcast$"+targetType, "("+getBinaryName(targetType)+")V");
	}
	
	public void addFieldAccess(String owner, String name, String desc, boolean is_put, boolean isStatic){
		String head = "get$";
		if(is_put){
			head = "set$";
		}
		MethodNode mn = addMethod(head+owner.replace(".", "_")+"$"+name, "(" + getBinaryName(owner)+")" + desc);
		
	}
	
	public void addVirtualMethodCall(){
		
	}
	
	public void addSpecialMethodCall(){
		
	}
	
	public void addStaticMethodCall(){
		
	}
	
	public void addInterfaceMethodCall(){
		
	}
	
}
