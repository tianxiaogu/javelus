/*
 * Copyright (C) 2012  Tianxiao Gu. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Please contact Institute of Computer Software, Nanjing University, 
 * 163 Xianlin Avenue, Nanjing, Jiangsu Provience, 210046, China,
 * or visit moon.nju.edu.cn if you need additional information or have any
 * questions.
*/
package org.javelus.upt.selector;

import org.javelus.ClassUpdateType;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.IntInsnNode;
import org.objectweb.asm.tree.JumpInsnNode;
import org.objectweb.asm.tree.LabelNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.VarInsnNode;

import org.javelus.upt.hotspot.DSUClass;
import org.javelus.upt.hotspot.DSUMethod;

public class InplaceSelector implements Opcodes{

	private int from;
	private int to;
	
	private String oldName;
	private DSUClass dsuClass;
	
	
	
	public InplaceSelector(int from, int to, String oldName, DSUClass dsuClass) {
		this.from = from;
		this.to = to;
		this.oldName = oldName;
		this.dsuClass = dsuClass;
	}
	
	static AbstractInsnNode createGetRNNode(){
		MethodInsnNode node = new MethodInsnNode(Opcodes.INVOKESTATIC,
				"org.javelus.DeveloperInterface", 
				"currentRevisionNumber",
				"()I");
		return node;
	}
	
	static AbstractInsnNode createIConstNode(int i){
		switch(i){
		case 0:
			return new InsnNode(Opcodes.ICONST_0);
		case 1:
			return new InsnNode(Opcodes.ICONST_1);
		case 2:
			return new InsnNode(Opcodes.ICONST_2);
		case 3:
			return new InsnNode(Opcodes.ICONST_3);
		case 4:
			return new InsnNode(Opcodes.ICONST_4);
		case 5:
			return new InsnNode(Opcodes.ICONST_5);
		default:
			return new IntInsnNode(Opcodes.BIPUSH, i);
		}
	}
	
	/**
	 * IRETURN, LRETURN, FRETURN, DRETURN, ARETURN, RETURN
	 * @param desc
	 * @return
	 */
	static AbstractInsnNode createReturnNode(String desc){
		int index = desc.indexOf(')');
		
		switch(desc.charAt(index+1)){
		case '[':
		case 'L':
			return new InsnNode(Opcodes.ARETURN);
		case 'V':
			return new InsnNode(Opcodes.RETURN);
		case 'B':
		case 'C':
		case 'I':
		case 'S':
		case 'Z':
			return new InsnNode(Opcodes.IRETURN);
		case 'J':
			return new InsnNode(Opcodes.LRETURN);
		case 'F':
			return new InsnNode(Opcodes.FRETURN);
		case 'D':
			return new InsnNode(Opcodes.DRETURN);
		}
		return null;
	}
	
	static AbstractInsnNode createLoadNode(int i, Type type){
		switch(type.getSort()){
		case Type.ARRAY:
		case Type.OBJECT:
			return new VarInsnNode(Opcodes.ALOAD,i);
		case Type.BOOLEAN:
		case Type.BYTE:
		case Type.CHAR:
		case Type.SHORT:
		case Type.INT:
			return new VarInsnNode(Opcodes.ILOAD,i);
		case Type.LONG:
			return new VarInsnNode(Opcodes.LLOAD,i);
		case Type.FLOAT:
			return new VarInsnNode(Opcodes.FLOAD,i);
		case Type.DOUBLE:
			return new VarInsnNode(Opcodes.DLOAD,i);
		}
		
		return null;
	}
	
	public void createSelector(){
		ClassUpdateType changeType = dsuClass.getChangeType();
		
		if(changeType == ClassUpdateType.BC){
			DSUMethod [] methods = dsuClass.getDeclaredMethods();
			for(DSUMethod method : methods){
				DSUMethod newVersion = method.getNewVersion();
				MethodNode mn = newVersion.getMethodNode();
				
				InsnList insnList = mn.instructions;
				LabelNode origin = new LabelNode();
				AbstractInsnNode cur = createGetRNNode();
				insnList.insert(cur); // get rn
				
				insnList.insert(cur,createIConstNode(from)); // compare rn
				cur = cur.getNext();
				
				insnList.insert(new JumpInsnNode(Opcodes.IF_ICMPNE,origin)); // jumnp
				cur = cur.getNext();
				
				// load parameter
				Type[] args = Type.getArgumentTypes(method.getDescriptor());
				
				int i=0;
				if(!method.isStatic()){
					insnList.insert(new VarInsnNode(Opcodes.ALOAD,0));
					i++;
				}
				
				for(Type t : args){
					insnList.insert(createLoadNode(i,t));
				}
				
				
				
				insnList.insert(createReturnNode(method.getDescriptor()));// return
				insnList.insert(origin);
				
				
			}
			
		}
	}
	
	public int getFrom() {
		return from;
	}
	public void setFrom(int from) {
		this.from = from;
	}
	public int getTo() {
		return to;
	}
	public void setTo(int to) {
		this.to = to;
	}
	public String getOldName() {
		return oldName;
	}
	public void setOldName(String oldName) {
		this.oldName = oldName;
	}
	public DSUClass getDsuClass() {
		return dsuClass;
	}
	public void setDsuClass(DSUClass dsuClass) {
		this.dsuClass = dsuClass;
	}
	
	
	
}
