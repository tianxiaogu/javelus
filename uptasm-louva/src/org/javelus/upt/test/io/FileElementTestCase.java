/*
 * Copyright (C) 2012  Tianxiao Gu. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Please contact Institute of Computer Software, Nanjing University, 
 * 163 Xianlin Avenue, Nanjing, Jiangsu Provience, 210046, China,
 * or visit moon.nju.edu.cn if you need additional information or have any
 * questions.
*/
package org.javelus.upt.test.io;

import static org.javelus.upt.test.update.UPTTestCase.getResourceStringFromClass;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Iterator;

import junit.framework.TestCase;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldNode;

import org.javelus.upt.hotspot.DSUClass;
import org.javelus.upt.hotspot.DSUClassPath;
import org.javelus.upt.io.FileElement;
import org.javelus.upt.io.PathElement;

public class FileElementTestCase extends TestCase{

	void backUpTestResources() throws Exception{
		String parent = getResourceStringFromClass(FileElementTestCase.class,".");
		String testFile = getResourceStringFromClass(FileElementTestCase.class,"Changed.class");
		File tmp = new File(parent + "tmp");
		tmp.mkdirs();
		File file = new File(tmp,"/Changed.class");
		FileInputStream fis = new FileInputStream(testFile);
		FileOutputStream fos = new FileOutputStream(file);
		byte[] buff = new byte[1024];
		int l;
		while((l=fis.read(buff))>0){
			fos.write(buff, 0, l);
		}
		fis.close();
		fos.close();
	}
	
	public void setUp() throws Exception{
		backUpTestResources();
	}

	public void testFilePathElement() throws Exception{
		String pathString = getResourceStringFromClass(FileElementTestCase.class,"Changed.class");
		
		FileElement fileElement = (FileElement) PathElement.createPathElement(pathString);
		
		DSUClassPath classPath = new DSUClassPath(fileElement);
		
		Iterator<DSUClass> it  = classPath.getDSUClassIterator();
		assertTrue(it.hasNext());
		
		DSUClass changed = it.next();
		assertNotNull(changed);
		
		assertFalse(it.hasNext());
		
		ClassNode cn = changed.getClassNode();
		assertNotNull(cn);
		
		assertTrue(cn.fields.size() == 2);
		
		FieldNode f0 = (FieldNode) cn.fields.get(0);
		FieldNode f1 = (FieldNode) cn.fields.get(1);
		
		cn.fields.set(0, f1);
		cn.fields.set(1, f0);
		
		ClassWriter cw = new ClassWriter(0);
		cn.accept(cw);
		
		fileElement.writeClass(changed, cw.toByteArray());
		
		FileElement fileElement1 = (FileElement) PathElement.createPathElement(pathString);
		Iterator<DSUClass> it1  = classPath.getDSUClassIterator();
		assertTrue(it1.hasNext());
		
		DSUClass changed1 = it1.next();
		assertNotNull(changed1);
		
		ClassNode cn1 = changed1.getClassNode();
		assertNotNull(cn1);
		
		assertTrue(cn1.fields.size() == 2);
		
		FieldNode f10 = (FieldNode) cn1.fields.get(0);
		FieldNode f11 = (FieldNode) cn1.fields.get(1);
		
		assertEquals(f0.name, f11.name);
		assertEquals(f1.name, f10.name);
	}
	
	public void tearDown(){
		String parent = getResourceStringFromClass(FileElementTestCase.class,".");
		File tmp = new File(parent + "/tmp");
		File file = new File(tmp,"/Changed.class");
		file.delete();
		tmp.delete();
	}
	
	
}
