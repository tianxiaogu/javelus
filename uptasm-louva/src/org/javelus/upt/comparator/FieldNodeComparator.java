/*
 * Copyright (C) 2012  Tianxiao Gu. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Please contact Institute of Computer Software, Nanjing University, 
 * 163 Xianlin Avenue, Nanjing, Jiangsu Provience, 210046, China,
 * or visit moon.nju.edu.cn if you need additional information or have any
 * questions.
*/
package org.javelus.upt.comparator;

import org.objectweb.asm.tree.FieldNode;

/**
 * @DILEPIS 对每一个域生成的FieldNode对象进行比较。
 * @author tiger
 * 
 */
public class FieldNodeComparator {

	/**
	 * @DILEPIS 比较域的签名是否发生变化
	 * @param f1
	 * @param f2
	 * @return true if the same
	 */
	public static boolean compareSignature(FieldNode f1, FieldNode f2) {
		return compareName(f1, f2) && compareType(f1, f2)
				&& f1.access == f2.access;
	}

	/**
	 * @DILEPIS 比较域的名称是否发生变化
	 * @param f1
	 * @param f2
	 * @return true if the same
	 */
	public static boolean compareName(FieldNode f1, FieldNode f2) {
		return f1.name.equals(f2.name);
	}

	/**
	 * @DILEPIS 比较域的类型是否发生变化
	 * @param f1
	 * @param f2
	 * @return true if the same
	 */
	public static boolean compareType(FieldNode f1, FieldNode f2) {
		return f1.desc.equals(f2.desc);
	}
}
