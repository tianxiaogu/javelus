/*
 * Copyright (C) 2012  Tianxiao Gu. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Please contact Institute of Computer Software, Nanjing University, 
 * 163 Xianlin Avenue, Nanjing, Jiangsu Provience, 210046, China,
 * or visit moon.nju.edu.cn if you need additional information or have any
 * questions.
*/
package org.javelus.upt.comparator;

import java.util.Comparator;

import org.javelus.upt.hotspot.DSUField;

public class DSUFieldComparator implements Comparator<DSUField> {
	
	static DSUFieldComparator singleton = new DSUFieldComparator();
	
	public static Comparator<DSUField> singleton(){
		return singleton;
	}
	
	public static int compareFields(DSUField o1, DSUField o2){
		// TODO Auto-generated method stub
		// FIXME at now we just compare name and descriptor
		int ans = o1.getName().compareTo(o2.getName());
		if (ans == 0) {
			return o1.getDescriptor().compareTo(o2.getDescriptor());
		}
		return ans;
	}
	
	@Override
	public int compare(DSUField o1, DSUField o2) {
		return compareFields(o1, o2);
	}

}
