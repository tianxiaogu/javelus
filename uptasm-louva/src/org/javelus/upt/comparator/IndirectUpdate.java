/*
 * Copyright (C) 2012  Tianxiao Gu. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Please contact Institute of Computer Software, Nanjing University, 
 * 163 Xianlin Avenue, Nanjing, Jiangsu Provience, 210046, China,
 * or visit moon.nju.edu.cn if you need additional information or have any
 * questions.
*/
package org.javelus.upt.comparator;


import org.javelus.ClassUpdateType;
import org.javelus.CodeUpdateType;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MultiANewArrayInsnNode;
import org.objectweb.asm.tree.TypeInsnNode;

import org.javelus.upt.hotspot.DSUClass;
import org.javelus.upt.hotspot.DSUMethod;
import org.javelus.upt.hotspot.Update;
import org.javelus.upt.io.Utils;

/**
 * @DILEPIS 找出那些字节码未改变，机器码需要更新的方法。
 * @author tiger
 * 
 */
public class IndirectUpdate implements Opcodes {

	/**
	 * @DILEPIS 判断类是否包含字节码未变化，但引用类结构变化的类的。
	 * @param klass
	 * @param reloadedClasses
	 * @return true if mc changed
	 */
	public static boolean detectMCChanged(DSUClass klass,Update update) {
		boolean mcChanged = false;
		DSUMethod[] methods = klass.getDeclaredMethods();
		for (DSUMethod m : methods) {
			if (m.bcChanged()) {
				continue;
			}
			if (doesMethodRefto(m, update)) {
				m.updateCodeUpdateType(CodeUpdateType.MC);
				mcChanged = true;
			}
		}
		if (mcChanged) {

			klass.updateChangedType(ClassUpdateType.MC);
		}

		return mcChanged;
	}

	/**
	 * @DILEPIS 判断方法是是否引用类结构变化的类。
	 * @param m
	 * @param reloadedClass
	 * @return true if method's MC changed
	 */
	static boolean doesMethodRefto(DSUMethod m,
			Update update) {
		InsnList insnNodes = m.getMethodNode().instructions;
		boolean isRefto = false;
		AbstractInsnNode insnNode = insnNodes.getFirst();
		while (insnNode != null) {
			int type = insnNode.getType();
			switch (type) {
			case AbstractInsnNode.FIELD_INSN:
				if (doesFieldInsnNodeRefto((FieldInsnNode) insnNode, m,
						update)) {
					isRefto = true;
				}
				break;
			case AbstractInsnNode.METHOD_INSN:
				if (doesMethodInsnNodeRefto((MethodInsnNode) insnNode, m,
						update)) {
					isRefto = true;
				}
				break;
			case AbstractInsnNode.MULTIANEWARRAY_INSN:
				if (doesMultiANewArrayInsnNodeRefto(
						(MultiANewArrayInsnNode) insnNode, m, update)) {
					isRefto = true;
				}
				break;
			case AbstractInsnNode.TYPE_INSN:
				if (doesTypeInsnNodeRefto((TypeInsnNode) insnNode,m, 
						update)) {
					isRefto = true;
				}
				break;
			default:
			}
			insnNode = insnNode.getNext();
		}

		return isRefto;
	}

	/**
	 * @DILEPIS 判断一条访问域的指令是否访问类结构改变的类
	 * @param insn
	 * @param method
	 * @param classes, Redefined classes
	 * @return true if field instruction refer to a offset changed field
	 */
	static boolean doesFieldInsnNodeRefto(FieldInsnNode insn, DSUMethod method,
			Update update) {
		return update.constantPoolChanged(method.getDeclaringClass(), insn.owner,insn.name,insn.desc);
	}

	/**
	 * @DILEPIS 判断调用方法的指令是否访问类结构改变的类
	 * @param insn
	 * @param method
	 * @param classes
	 * @return true if method instruction refer to a offset changed method
	 */
	static boolean doesMethodInsnNodeRefto(MethodInsnNode insn,
			DSUMethod method, Update update) {
		return update.constantPoolChanged(method.getDeclaringClass(), insn.owner,insn.name,insn.desc);
	}

	/**
	 * @DILEPIS 判断MULTIANEWARRAY指令是否访问类结构改变的类。
	 * @param insn
	 * @param classes
	 * @return true if MultiANewArray instruction refer to a offset changed type
	 */
	static boolean doesMultiANewArrayInsnNodeRefto(MultiANewArrayInsnNode insn,DSUMethod method, 
			Update update) {
		return false;
	}

	/**
	 * FIXME! Array is special!
	 * 
	 * @DILEPIS 判断访问类型的指令是否访问类结构改变的类。
	 * @param insn
	 * @param classes
	 * @return true if MultiANewArray instruction refer to changed type
	 */
	static boolean doesTypeInsnNodeRefto(TypeInsnNode insn,DSUMethod method, 
			Update update) {
		// getInnerMostInternalName here, for Dimension has been checked
		String internalName = Utils.getInnerMostInternalName(insn.desc);
		return update.constantPoolChanged(method.getDeclaringClass(), internalName);
	}

}
