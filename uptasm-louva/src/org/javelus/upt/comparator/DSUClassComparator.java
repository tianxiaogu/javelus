/*
 * Copyright (C) 2012  Tianxiao Gu. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Please contact Institute of Computer Software, Nanjing University, 
 * 163 Xianlin Avenue, Nanjing, Jiangsu Provience, 210046, China,
 * or visit moon.nju.edu.cn if you need additional information or have any
 * questions.
*/
package org.javelus.upt.comparator;

import java.util.Comparator;
import java.util.Iterator;

import org.javelus.ClassUpdateType;
import org.javelus.CodeUpdateType;
import org.objectweb.asm.tree.ClassNode;

import org.javelus.upt.hotspot.DSUClass;
import org.javelus.upt.hotspot.DSUClassPath;
import org.javelus.upt.hotspot.DSUField;
import org.javelus.upt.hotspot.DSUMethod;

/**
 * @author tiger
 * 
 */
public class DSUClassComparator implements Comparator<DSUClass> {

	@Override
	public int compare(DSUClass o1, DSUClass o2) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @param klass
	 * @return true if this class should be loaded during DSU
	 */
	public static boolean shouldLoadEntireClass(DSUClass klass) {
		if (!klass.isLoaded()) {
			return false;
		}
		if (DSUClassPath.isInBootstrapCP(klass)) {
			return false;
		}

		if (shouldLoadEntireClass(klass.getSuperClass())) {
			klass.updateChangedType(klass.getSuperClass().getChangeType());
		}
		
		for(DSUClass intf : klass.getDeclaredInterfaces()){
			if (shouldLoadEntireClass(intf)) {
				klass.updateChangedType(intf.getChangeType());
			}			
		}
		
//		if(klass.needRedefineClass() && klass.isInnerClass()){
//			DSUClass outer=klass.getDeclaringClass();
//			outer.updateChangedType(klass.getChangeType());
//		}
		
		return klass.needRedefineClass();
	}

	/**
	 * @DILEPIS 比较类结构
	 * @param oldClass
	 * @param newClass
	 */
	public static void compareClassStructure(DSUClass oldClass,
			DSUClass newClass) {
		ClassNode classNode = oldClass.getClassNode();
		ClassNode newClassNode = newClass.getClassNode();

		if (ClassNodeComparator.fieldsTableChanged(classNode, newClassNode)) {
			//XXX check class file changes first
			compareStaticFields(oldClass,newClass);
			compareInstanceFields(oldClass,newClass);
		}
		if (ClassNodeComparator.methodsTableChanged(classNode, newClassNode)) {
			compareStaticMethods(oldClass,newClass);
			compareInstanceMethods(oldClass,newClass);
		}
		if (ClassNodeComparator.superClassChanged(classNode, newClassNode)){
			DSUClass superClass = newClass.getSuperClass();
			if(superClass.isLoaded()){
				if(superClass.getStaticMethods().hasNext()){
					oldClass.updateChangedType(ClassUpdateType.S_METHOD);
				}
				if(superClass.getStaticFields().hasNext()){
					oldClass.updateChangedType(ClassUpdateType.S_FIELD);
				}
				if(superClass.getInstanceMethods().hasNext()){
					oldClass.updateChangedType(ClassUpdateType.METHOD);
				}
				if(superClass.getInstanceFields().hasNext()){
					oldClass.updateChangedType(ClassUpdateType.FIELD);
				}
				
			}else {
				
			}
			

		}
		if (ClassNodeComparator.generalInfomationChanged(classNode,
				newClassNode)) {
			System.err.format("ClassNodeComparator: general information changed of [%s,%s]\n",classNode.name,newClassNode.name);		
		}

	}

	public static void compareStaticFields(DSUClass oldClass, DSUClass newClass){
		Iterator<DSUField> oldStaticFields = oldClass.getStaticFields();
		Iterator<DSUField> newStaticFields = newClass.getStaticFields();
		if(!compareFields(oldStaticFields, newStaticFields)){
			oldClass.updateChangedType(ClassUpdateType.S_FIELD);
		}
	}
	
	public static void compareInstanceFields(DSUClass oldClass, DSUClass newClass){
		Iterator<DSUField> oldInstanceFields = oldClass.getInstanceFields();
		Iterator<DSUField> newInstanceFields = newClass.getInstanceFields();
		if(!compareFields(oldInstanceFields, newInstanceFields)){
			oldClass.updateChangedType(ClassUpdateType.FIELD);
		}
	}
	
	public static boolean compareMethods(Iterator<DSUMethod> oldMethods, Iterator<DSUMethod> newMethods){ 
		while(oldMethods.hasNext() && newMethods.hasNext()){
			if(DSUMethodComparator.compareMethods(oldMethods.next(), newMethods.next()) != 0){
				return false;
			}
		}
		if(oldMethods.hasNext()){
			return false;
		}
		
		if(newMethods.hasNext()){
			return false;
		}
		return true;
	}
	
	
	public static boolean compareFields(Iterator<DSUField> oldFields, Iterator<DSUField> newFields){
		while(oldFields.hasNext() && newFields.hasNext()){
			if(DSUFieldComparator.compareFields(oldFields.next(), newFields.next()) != 0){
				return false;
			}
		}
		if(oldFields.hasNext()){
			return false;
		}
		
		if(newFields.hasNext()){
			return false;
		}
		return true;
	}
	

	
	public static void compareStaticMethods(DSUClass oldClass, DSUClass newClass){
		Iterator<DSUMethod> oldStaticMethods = oldClass.getStaticMethods();
		Iterator<DSUMethod> newStaticMethods = newClass.getStaticMethods();
		if(!compareMethods(oldStaticMethods, newStaticMethods)){
			oldClass.updateChangedType(ClassUpdateType.S_METHOD);
		}
	}
	
	public static void compareInstanceMethods(DSUClass oldClass, DSUClass newClass){
		Iterator<DSUMethod> oldInstanceMethods = oldClass.getInstanceMethods();
		Iterator<DSUMethod> newInstanceMethods = newClass.getInstanceMethods();
		if(!compareMethods(oldInstanceMethods, newInstanceMethods)){
			oldClass.updateChangedType(ClassUpdateType.METHOD);
		}
	}
	
	/**
	 * @DILEPIS 判断该类是否需要重新加载某些方法。
	 * @param oldClass
	 * @param newClass
	 * @return true if this class need to reloaded method table
	 */
	public static boolean shouldLoadSomeMethodBodies(DSUClass oldClass,
			DSUClass newClass) {
		return (oldClass.getChangeType() == ClassUpdateType.BC);
	}

	/**
	 * @DILEPIS 比较方法体
	 * @param oldClass
	 * @param newClass
	 */
	public static void compareMethodBody(DSUClass oldClass, DSUClass newClass) {

		DSUMethod[] from = oldClass.getDeclaredMethods();

		boolean hasMethodChanged = false;
		for (DSUMethod oldMethod : from) {
			DSUMethod newMethod = (DSUMethod) oldMethod.getNewVersion();
			if (newMethod != null) {
				if (oldMethod.hasCode()) {
					if (newMethod.hasCode()) {
						if (!MethodNodeComparator.areMethodCodeTheSame(
								oldMethod.getMethodNode(),
								newMethod.getMethodNode())) {
							oldMethod.updateCodeUpdateType(CodeUpdateType.BC);
							hasMethodChanged = true;
						}
					} else {
						// old has code but new doesn't
					}

				} else if (oldMethod.isAnnotationMethod()) {
				} else {
					// a interface method
					if (newMethod.hasCode()) {

					} else if (newMethod.isAnnotationMethod()) {

					} else {

					}
				}

			} else {
				// a delete
				oldMethod.updateCodeUpdateType(CodeUpdateType.DEL);
				// 已经被判定了类更新了
				// hasMethodChanged = true;
			}
		}

		if (hasMethodChanged) {
			oldClass.updateChangedType(ClassUpdateType.BC);
		}
	}




}
