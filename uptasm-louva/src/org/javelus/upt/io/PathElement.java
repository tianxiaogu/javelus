/*
 * Copyright (C) 2012  Tianxiao Gu. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Please contact Institute of Computer Software, Nanjing University, 
 * 163 Xianlin Avenue, Nanjing, Jiangsu Provience, 210046, China,
 * or visit moon.nju.edu.cn if you need additional information or have any
 * questions.
*/
package org.javelus.upt.io;

import java.net.URL;

/**
 * @DILEPIS 路径元素用于读取该路径对应的位置的所包含的Java类文件，同时支持写入Java类文件。
 * @author tiger
 * 
 */
public abstract class PathElement {

	/**
	 * @DILEPIS 对应的路径地址
	 */
	protected String pathString;

	/**
	 * @DILEPIS 与之相关的ClassPath
	 */
	protected ClassPath classPath;

	public PathElement(String path) {
		this.pathString = path;
	}

	public PathElement(String path, ClassPath classPath) {
		this.pathString = path;
		this.classPath = classPath;
	}

	/**
	 * @DILEPIS 读取该路径对应的所有Java类文件，并将其加入到指定的ClassPath对象中。
	 */
	public abstract void readClasses();

	/**
	 * @DILEPIS
	 * 通过在读取类的时候生成的ID信息重写一个类文件
	 * @param classID
	 * @param content
	 * @throws Exception
	 */
	public abstract void writeClass(Object classID, byte[] content)
			throws Exception;

	public abstract URL getClassURL(Object classID);
	
	public void setClassPath(ClassPath classPath) {
		if (this.classPath == null) {
			this.classPath = classPath;
		}
	}

	public ClassPath getClassPath() {
		return this.classPath;
	}
	
	public String getPathString() {
		return pathString;
	}

	public String toString() {
		return pathString;
	}

	/**
	 * @DILEPIS 负责创建具体的路径元素的工厂方法
	 * @param pathString
	 * @return a path element
	 */
	public static PathElement createPathElement(String pathString) {
		if (pathString.endsWith(".class")) {
			return new FileElement(pathString);
		} else if (pathString.endsWith(".jar")||pathString.endsWith(".war")) {
			return new JarElement(pathString);
		} else {
			return new DirectoryElement(pathString);
		}
	}
}
