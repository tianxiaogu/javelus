/*
 * Copyright (C) 2012  Tianxiao Gu. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Please contact Institute of Computer Software, Nanjing University, 
 * 163 Xianlin Avenue, Nanjing, Jiangsu Provience, 210046, China,
 * or visit moon.nju.edu.cn if you need additional information or have any
 * questions.
*/
package org.javelus.upt.io;

import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.xml.serializer.OutputPropertiesFactory;
import org.apache.xml.serializer.Serializer;
import org.apache.xml.serializer.SerializerFactory;
import org.javelus.ClassUpdateType;
import org.javelus.CodeUpdateType;
import org.javelus.MemberUpdateType;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import org.javelus.upt.hotspot.DSUClass;
import org.javelus.upt.hotspot.DSUField;
import org.javelus.upt.hotspot.DSUMethod;
import org.javelus.upt.hotspot.Update;

/**
 * @author tiger
 * 
 */
@Deprecated
public class DSUWriter {
	Document document;

	int DEL_CLASS;
	int MC_CLASS;
	int BC_CLASS;
	int STATIC_FIELD_CLASS;
	int STATIC_METHOD_CLASS;
	int ALL_STATIC_CLASS;
	int FIELD_CLASS;
	int METHOD_CLASS;
	int ALL_CLASS;
	
	int MC_METHOD;
	int BC_METHOD;
	
	/**
	 * @param update
	 * @return an XML element
	 */
	Element update2xml(Update update) {
		Element updateElement = createDSUElement("update");
		
		Element classLoaderElement=createDSUElement("classloader");
		classLoaderElement.setAttribute("id", "");
		updateElement.appendChild(classLoaderElement);
		
//		List<RVMClassPath> newCPs=update.getNewCPs();
//		Iterator<RVMClassPath> it1=newCPs.iterator();
//		while(it1.hasNext()){
//			RVMClassPath cp=it1.next();
//			PathElement pathElement=cp.getPathElement();
//			String context=pathElement.getPathString();
//			
//			Element pathDSUElement=createDSUElement("path");
//			pathDSUElement.setTextContent(context);
//			classLoaderElement.appendChild(pathDSUElement);
//			if(pathElement instanceof JarElement ){
//				assert (context.endsWith(".jar")||context.endsWith(".war"));
//				int lastIndex=context.lastIndexOf('/');
//				if(lastIndex!=-1){
//					String transformerContext=context.substring(0, lastIndex+1);
//					pathDSUElement=createDSUElement("path");
//					pathDSUElement.setTextContent(transformerContext+"transformer.jar");
//					classLoaderElement.appendChild(pathDSUElement);
//				}
//			}
//			
//		}
//		
//		Element transformerElement=createDSUElement("transformer");
//		transformerElement.setAttribute("name", "LJDUSTransformers;");
//		classLoaderElement.appendChild(transformerElement);

		List<DSUClass> deletedClass = update.getDeletedClasses();
		for(DSUClass klass : deletedClass){
			if (klass.isLoaded() && klass.isUpdated()) {
				classLoaderElement.appendChild(class2xml(klass));
			}
		}
		
		Iterator<DSUClass> it = update.getSortedNewClasses();
		while (it.hasNext()) {
			DSUClass klass = it.next();
			DSUClass old   = klass.getOldVersion();
			if(old == null){
				// this is a new added class
				classLoaderElement.appendChild(class2xml(klass));
			}else if (old.isLoaded() && old.isUpdated()) {
				classLoaderElement.appendChild(class2xml(old));
			}
		}

		return updateElement;
	}

	
	private void recordMethod(DSUMethod method){
		CodeUpdateType type = method.getCodeUpdateType();
		switch(type){
		case MC:
			MC_METHOD++;
			break;
		case BC:
			BC_METHOD++;
			break;
		default:
		}
	}
	
	private void recordClass(DSUClass klass){
		ClassUpdateType type = klass.getChangeType();
		switch(type){
		case MC:
			MC_CLASS++;
			break;
		case BC:
			BC_CLASS++;
			break;
		case S_FIELD:
			STATIC_FIELD_CLASS++;
			break;
		case S_METHOD:
			STATIC_METHOD_CLASS++;
			break;
		case S_ALL:
			ALL_STATIC_CLASS++;
			break;
		case FIELD:
			FIELD_CLASS++;
			break;
		case METHOD:
			METHOD_CLASS++;
			break;
		case ALL:
			ALL_CLASS++;
			break;
			default:
		}
	}
	
	/**
	 * @param klass
	 * @return an XML element
	 */
	Element class2xml(DSUClass klass) {
		Element classElement = createDSUElement("class");
//		classElement.setAttribute("name",
//				"L" + klass.getName().replace('.', '/') + ";");
		
		classElement.setAttribute("name",klass.getName().replace('.', '/'));
		classElement.setAttribute("classChanged", klass.getChangeType()
				.toString());

		// 
		if (klass.needReloadClass()){
			Element fileElement=createDSUElement("file");
			fileElement.setTextContent(klass.getNewVersion().getClassFile().toExternalForm());
			classElement.appendChild(fileElement);
		}else if(klass.getChangeType() == ClassUpdateType.ADD){
			Element fileElement=createDSUElement("file");
			fileElement.setTextContent(klass.getClassFile().toExternalForm());
			classElement.appendChild(fileElement);
		}

		recordClass(klass);
		
		
		DSUMethod[] methods = klass.getDeclaredMethods();

	/*	if (methods != null && klass.getChangedType() == ClassChangedType.MC) {
			for (RVMMethod m : methods) {
				if (m.getCodeUpdateType() == CodeUpdateType.MC) {
					classElement.appendChild(method2xml(m));
				}
			}
			return classElement;
		}
*/
		if (methods != null) {
			for (DSUMethod m : methods) {
				classElement.appendChild(method2xml(m));
			}
		}

		DSUField[] fields = klass.getDeclaredFields();
		if(fields != null){
			for(DSUField f : fields){
				classElement.appendChild(field2xml(f));
			}
		}
		
		return classElement;
	}

	/**
	 * @param method
	 * @return an XML element
	 */
	Element method2xml(DSUMethod method) {
		Element methodElement = createDSUElement("method");

		methodElement.setAttribute("name", method.getName());
		methodElement.setAttribute("desc", method.getDescriptor());
		methodElement.setAttribute("codeChanged", method.getCodeUpdateType()
				.toString());

		recordMethod(method);
		return methodElement;
	}

	Element field2xml(DSUField field) {
		Element fieldElement = createDSUElement("field");
		fieldElement.setAttribute("name", field.getName());
		fieldElement.setAttribute("desc", field.getDescriptor());

		if(field.hasNewVersion()){
			fieldElement.setAttribute("memberChanged", MemberUpdateType.CHANGED.name());
		}else{
			fieldElement.setAttribute("memberChanged", MemberUpdateType.DEL.name());
		}
		
		return fieldElement;
	}

	Element transformer2xml() {
		return null;
	}

	Element classloader2xml() {
		return null;
	}

	Element createDSUElement(String tagName) {
		return document.createElement(tagName);
	}

	/**
	 * @param update
	 * @param output
	 */
	public void write(Update update, OutputStream output) {
		try {
			final DocumentBuilderFactory builderFactory = org.apache.xerces.jaxp.DocumentBuilderFactoryImpl
					.newInstance();
			builderFactory.setNamespaceAware(true);
			builderFactory.setValidating(false);
			DocumentBuilder builder = builderFactory.newDocumentBuilder();

			document = builder.newDocument();
			document.appendChild(update2xml(update));

			java.util.Properties xmlProps = OutputPropertiesFactory
					.getDefaultMethodProperties("xml");
			xmlProps.setProperty("indent", "yes");
			xmlProps.setProperty("standalone", "no");

			Serializer serializer = SerializerFactory.getSerializer(xmlProps);
			serializer.setOutputStream(output);
			serializer.asDOMSerializer().serialize(document);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String printHistogram(){
		StringBuilder sb = new StringBuilder();
		
		sb.append("Class Changed:\n");
		
		sb.append("MC:\t\t");
		sb.append(MC_CLASS);
		
		sb.append("\nBC:\t\t");
		sb.append(BC_CLASS);
		
		sb.append("\n\nSTATIC METHOD:\t\t");
		sb.append(STATIC_METHOD_CLASS);
		
		sb.append("\nSTATIC FIELD:\t\t");
		sb.append(STATIC_FIELD_CLASS);
		
		sb.append("\nSTATIC BOTH:\t\t");
		sb.append(ALL_STATIC_CLASS);
		
		sb.append("\n\nMETHOD:\t\t");
		sb.append(METHOD_CLASS);
		
		sb.append("\nFIELD:\t");
		sb.append(FIELD_CLASS);
		
		sb.append("\nBOTH:\t\t");
		sb.append(ALL_CLASS);
		
		sb.append("\n\nMethod Changed:\n");
		sb.append("MC:\t\t");
		sb.append(MC_METHOD);
		
		sb.append("\nBC:\t\t");
		sb.append(BC_METHOD);
		sb.append("\n");
		
		return sb.toString();
	}
	
	public static void main(String[] args) {
		// new DSUWriter().writer();
	}
}
