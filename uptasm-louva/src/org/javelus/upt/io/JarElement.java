/*
 * Copyright (C) 2012  Tianxiao Gu. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Please contact Institute of Computer Software, Nanjing University, 
 * 163 Xianlin Avenue, Nanjing, Jiangsu Provience, 210046, China,
 * or visit moon.nju.edu.cn if you need additional information or have any
 * questions.
*/
package org.javelus.upt.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarInputStream;
import java.util.jar.JarOutputStream;

import org.apache.log4j.Logger;

/**
 * @DILEPIS 实现从JAR文件中读写Java类文件
 * @author tiger
 * 
 */
public class JarElement extends PathElement {

	static Logger logger = Logger.getLogger(JarElement.class);
	
	Map<Object, String> classToEntry;

	public JarElement(String name) {
		super(name);
		classToEntry = new HashMap<Object, String>();
	}

	@Override
	public void readClasses() {
		// TODO Auto-generated method stub
		JarFile jarFile;
		try {
			jarFile = new JarFile(getPathString());
			for (Enumeration<JarEntry> e = jarFile.entries(); e
					.hasMoreElements();) {
				JarEntry je = e.nextElement();
				logger.debug("get an entry " + je.getName() + " in jar file " + getPathString()+".");
				if (je.getName().endsWith(".class")) {
					InputStream is = jarFile.getInputStream(je);
					Object classId = classPath.addClass(is);
					classToEntry.put(classId, je.getName());
				}
			}
			jarFile.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

		}
	}

	@Override
	public void writeClass(Object classID, byte[] content) throws Exception {
		// TODO Auto-generated method stub
		String entryName = classToEntry.get(classID);
		if (entryName != null) {
			File file = new File(getPathString());
			File tmpFile = File.createTempFile(file.getName(), "bak");
			tmpFile.delete();
			boolean renameOK = file.renameTo(tmpFile);
			if (!renameOK) {
				System.err.println("Rename to tmpFile error:"
						+ tmpFile.getName());
			}

			JarInputStream jis = new JarInputStream(new FileInputStream(tmpFile));
			JarOutputStream jos = new JarOutputStream(new FileOutputStream(file));

			JarEntry entry = jis.getNextJarEntry();
			byte[] buf = new byte[1024];
			while(entry!=null){
				if(entry.getName().equals(entryName)){
					jos.putNextEntry(new JarEntry(entryName));
					jos.write(content);
					jos.closeEntry();
				}else {
					jos.putNextEntry(new JarEntry(entry.getName()));
					int len;
					while((len = jis.read(buf))>0){
						jos.write(buf, 0, len);
					}
				}
				entry = jis.getNextJarEntry();
			}
			jos.close();
			jis.close();
		}
	}

	@Override
	public URL getClassURL(Object classID) {
		// TODO Auto-generated method stub
		String entryName = classToEntry.get(classID);
		if (entryName != null){
			File file = new File(getPathString());
			try {
				URL fileURL = file.toURI().toURL();
				URL	url = new URL(String.format("jar:%s!/%s",fileURL.toExternalForm(), entryName));
				return url;
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

}
