package org.javelus.upt.io;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import org.javelus.upt.hotspot.DSUClass;
import org.javelus.upt.hotspot.DSUClassPath;
import org.javelus.upt.hotspot.Update;

public class HotSpotDSUWriter {
	
	void appendDeletedClass(PrintWriter pw, DSUClass klass){
		pw.append("delclass ");
		pw.append(klass.getName());
		pw.append('\n');		
	}
	
	void appendAddedClass(PrintWriter pw, DSUClass klass){
		pw.append("addclass ");
		pw.append(klass.getName());
		pw.append('\n');		
	}
	
	void appendModifiedClass(PrintWriter pw, DSUClass klass){
		pw.append("modclass ");
		pw.append(klass.getName());
		pw.append('\n');		
	}
	
	void appendClassEntry(PrintWriter pw, String path) {
		pw.append("classpath ");
		pw.append(path);
		pw.append('\n');
	}
	
	public void write(Update update, OutputStream output) {
		
		PrintWriter pw = new PrintWriter(output);
		
		List<DSUClassPath> cps = update.getNewCPs();
		
		for(DSUClassPath cp : cps){
			PathElement[] paths = cp.getPathElements();
			for (PathElement p:paths){
				appendClassEntry(pw, p.getPathString());				
			}
		}
		
		List<DSUClass> deletedClass = update.getDeletedClasses();
		for(DSUClass klass : deletedClass){
			if (klass.isLoaded() && klass.isUpdated()) {
				appendDeletedClass(pw, klass);
			}
		}
		
		Iterator<DSUClass> it = update.getSortedNewClasses();
		while (it.hasNext()) {
			DSUClass klass = it.next();
			DSUClass old   = klass.getOldVersion();
			if(old == null){
				// this is a new added class
				appendAddedClass(pw, klass);
			}else if (old.isLoaded() && old.isUpdated()) {
				appendModifiedClass(pw, old);
			}
		}
		
		pw.flush();
	}
}
