/*
 * Copyright (C) 2012  Tianxiao Gu. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Please contact Institute of Computer Software, Nanjing University, 
 * 163 Xianlin Avenue, Nanjing, Jiangsu Provience, 210046, China,
 * or visit moon.nju.edu.cn if you need additional information or have any
 * questions.
*/
package org.javelus.upt.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * @DILEPIS 实现从文件夹读写Java类文件
 * @author tiger
 * 
 */
public class DirectoryElement extends PathElement {

	Map<Object, File> classToFile;

	public DirectoryElement(String path) {
		super(path);
		classToFile = new HashMap<Object, File>();
	}

	private void addClass(File dir) {
		if (dir.isDirectory()) {
			File[] files = dir.listFiles();
			for (File file : files) {
				if (file.getName().endsWith(".class") && file.isFile()) {
					FileInputStream in;
					try {
						in = new FileInputStream(file);
						Object classID = classPath.addClass(in);
						classToFile.put(classID, file);
						in.close();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} else if (file.isDirectory()) {
					addClass(file);
				}
			}
		}
	}

	@Override
	public void readClasses() {
		// TODO Auto-generated method stub
		File dir = new File(pathString);
		addClass(dir);
	}

	@Override
	public void writeClass(Object classID, byte[] content) throws Exception {
		// TODO Auto-generated method stub
		File file = classToFile.get(classID);
		if (file != null) {
			FileOutputStream fos = new FileOutputStream(file);
			fos.write(content);
		}
	}

	@Override
	public URL getClassURL(Object classID) {
		// TODO Auto-generated method stub
		File file = classToFile.get(classID);
		if(file != null){
			try {
				return file.toURI().toURL();
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}
}
