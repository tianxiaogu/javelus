/*
 * Copyright (C) 2012  Tianxiao Gu. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Please contact Institute of Computer Software, Nanjing University, 
 * 163 Xianlin Avenue, Nanjing, Jiangsu Provience, 210046, China,
 * or visit moon.nju.edu.cn if you need additional information or have any
 * questions.
*/
package org.javelus.upt.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @DILEPIS 按照指定的路径读写一个Java类文件
 * @author tiger
 * 
 */
public class FileElement extends PathElement {

	Object classID;

	public FileElement(String path) {
		super(path);
		if (!path.endsWith(".class")) {
			System.err.println("FileName Error");
		}
		// TODO Auto-generated constructor stub
	}

	@Override
	public void readClasses() {
		// TODO Auto-generated method stub
		File file = new File(getPathString());
		if (file.isFile()) {
			FileInputStream fis;
			try {
				fis = new FileInputStream(file);
				classID = classPath.addClass(fis);
				fis.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void writeClass(Object classID, byte[] content) throws Exception {
		// TODO Auto-generated method stub
		if (this.classID == classID) {
			File file = new File(getPathString());
			FileOutputStream fos = new FileOutputStream(file);
			fos.write(content);
			fos.close();
		}
	}

	@Override
	public URL getClassURL(Object classID) {
		// TODO Auto-generated method stub
		if (this.classID == classID){
			File file = new File(getPathString());
			try {
				return file.toURI().toURL();
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

}
