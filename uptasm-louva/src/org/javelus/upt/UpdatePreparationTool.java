/*
 * Copyright (C) 2012  Tianxiao Gu. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Please contact Institute of Computer Software, Nanjing University, 
 * 163 Xianlin Avenue, Nanjing, Jiangsu Provience, 210046, China,
 * or visit moon.nju.edu.cn if you need additional information or have any
 * questions.
*/
package org.javelus.upt;

import gnu.getopt.Getopt;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.objectweb.asm.tree.ClassNode;

import org.javelus.upt.gui.MainFrame;
import org.javelus.upt.hotspot.DSUClass;
import org.javelus.upt.hotspot.DSUClassPath;
import org.javelus.upt.hotspot.Update;
import org.javelus.upt.hotspot.objecttransformer.StubClassGenerator;
import org.javelus.upt.hotspot.objecttransformer.TransformerGenerator;
import org.javelus.upt.io.DSUWriter;
import org.javelus.upt.io.HotSpotDSUWriter;
import org.javelus.upt.io.PathElement;

/**
 * @author tiger
 * 
 */
public class UpdatePreparationTool {

	/**
	 * old upt mode
	 */
	public static final String MODE_OLD = "__old_mode__";
	/**
	 * new javelus mode
	 */
	public static final String MODE_NEW = "__new_mode__";
	public static final String MODE_DEFAULT = MODE_NEW;

	public static String MODE = MODE_DEFAULT;

	/**
	 * output for meta file
	 */
	public static String DEFAULT_DIRECTORY = "./";
	public static String OUTPUT_DIRECTORY = DEFAULT_DIRECTORY;

	//public static String VERSION_PREFIX = "uptasm";

	private static String OLD_CP;

	private static String NEW_CP;

	private static String transformerCP=null;
	private static String transformerName="JDUSTransformer";
	
	private static boolean generateDSUSpecification=true;
	private static boolean outputStubClass = true;
	private static boolean openGUI = false;
	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		processCommandLine(args);

		if(openGUI){
			MainFrame.main(null);
			return;
		}
		
		File outputDir = new File(OUTPUT_DIRECTORY);

		outputDir.mkdirs();
		
		// output
		if(generateDSUSpecification){
			Update update = createUpdate(OLD_CP, NEW_CP);
			update.computeUpdateInformation();
			//DSUWriter writer = new DSUWriter();
			HotSpotDSUWriter writer = new HotSpotDSUWriter();
			File dsuFile = new File(outputDir,"javelus.dsu");
			writer.write(update, new FileOutputStream(dsuFile));
			
			//FileOutputStream histogram = new FileOutputStream(OUTPUT_DIRECTORY+"histogram.txt");
			//histogram.write(writer.printHistogram().getBytes());
			//histogram.close();
			
			if(outputStubClass){
				StubClassGenerator.generate(update, OUTPUT_DIRECTORY);
			}
		}
		
		
		
		if(transformerCP != null){
			DSUClassPath classPathList = UpdatePreparationTool
					.createClassPath(transformerCP);

			Map<String, ClassNode> classes = new HashMap<String, ClassNode>();

			Iterator<DSUClass> it = classPathList.getDSUClassIterator();
			while (it.hasNext()) {
				DSUClass cls = it.next();
				if(cls.isLoaded()){
					classes.put(cls.getClassNode().name, cls.getClassNode());
				}
			}

			TransformerGenerator generator = new TransformerGenerator(classes,transformerName);
		
			System.out.println("Write generated transformers class into " + outputDir.getAbsolutePath() + ".");
			generator.write(outputDir);
			
		}
	}

	/**
	 * @param oldPath
	 * @param newPath
	 * @return a new update
	 * @throws Exception
	 */
	public static Update createUpdate(String oldPath, String newPath)
	throws Exception {
		DSUClassPath oldCPs = createClassPath(oldPath);
		DSUClassPath newCPs = createClassPath(newPath);

		// do update compute
		Update update = new Update(oldCPs, newCPs);
		return update;
	}

	/**
	 * @param path
	 * @return a new cp
	 * @throws Exception
	 */
	public static DSUClassPath createClassPath(String path)
	throws Exception {
		String[] paths = path.split(File.pathSeparator);
		List<PathElement> pathElements = new LinkedList<PathElement>();
		for (String p : paths) {
			if (!p.equals("")) {
				pathElements.add(PathElement.createPathElement(p));
			}
		}
		PathElement[] elements = new PathElement[pathElements.size()];
		return new DSUClassPath(pathElements.toArray(elements));
	}

	/**
	 * @param cp
	 * @return a new TransformerGenerator
	 */
	public static TransformerGenerator createGenerator(
			DSUClassPath classPath,
			String transformerName) {
		Iterator<DSUClass> iterator = classPath.getDSUClassIterator();
		Map<String, ClassNode> classes = new HashMap<String, ClassNode>();
		while (iterator.hasNext()) {
			DSUClass cur = iterator.next();
			ClassNode cn = cur.getClassNode();
			if (cn != null) {
				classes.put(cn.name, cn);
			}
		}
		if(transformerName != null){
			return new TransformerGenerator(classes,transformerName);
		}
		return new TransformerGenerator(classes);
	}

	/**
	 * @param pathList
	 * @return a new TransformerGenerator
	 * @throws Exception
	 */
	public static TransformerGenerator createGenerator(String pathList)
	throws Exception {
		return createGenerator(createClassPath(pathList), null);
	}

	public static TransformerGenerator createGenerator(String pathList, String transformerName)
	throws Exception {
		return createGenerator(createClassPath(pathList), transformerName);
	}
	
	/**
	 * @param args
	 */
	private static void processCommandLine(String[] args) {
		Getopt opt = new Getopt("UpdatePreparationTool", args, ":t:m:d:o:n:uhbg");
		opt.setOpterr(false);
		int c;
		while ((c = opt.getopt()) != -1) {
			switch (c) {
			case 'h': {
				printUsage();
				System.exit(0);
			}
			case 'o': {
				OLD_CP = opt.getOptarg();
				break;
			}
			case 'n': {
				NEW_CP = opt.getOptarg();
				break;
			}
			case 'd': {
				OUTPUT_DIRECTORY = opt.getOptarg();
				if(!OUTPUT_DIRECTORY.endsWith("/")){
					OUTPUT_DIRECTORY += "/";
				}
				break;
			}
			case 'b': {
				generateDSUSpecification= false;
				break;
			}
			case 'g': {
				outputStubClass = false;
				break;
			}
			case 't': {
				transformerCP = opt.getOptarg();
				break;
			}
			case 'u': {
				openGUI  = true;
				break;
			}
			case 'm': {
				transformerName = opt.getOptarg();
				break;
			}
			case ':': {
				System.out
				.println("UpdatePreparationTool: Missing Argument, Option "
						+ (char) opt.getOptopt());
				printUsage();
				System.exit(1);
			}
			case '?': {
				System.out.println("UpdatePreparationTool: Invalid Option "
						+ (char) opt.getOptopt());
				printUsage();
				System.exit(1);
			}
			default:
			}
		}

		if(openGUI){
			return;
		}
		
		if ((OLD_CP == null || NEW_CP == null)) {
			if(transformerCP == null){
				printUsage();
				System.exit(1);
			}else {
				generateDSUSpecification = false;
				outputStubClass =false;
			}
		}
		
	}

	/**
	 * 
	 */
	private static void printUsage() {
		System.out
		.println("Usage: UpdatePreparationTool -o old-file -n new-file [-d output-directory]\n"
				+ "   or: UpdatePreparetionTool -t stub-class-path -m transformer-name\n"
				+ "\t-o old-file\n"
				+ "\t-n new-file\n"
				+ "\t-d output directory\n"
				+ "\t-b close generate javelus.dsu\n"
				+ "\t-g generate stub class\n"
				+ "\t-t transformer class path\n"
				+ "\t-u gui version\n");
	}
}

